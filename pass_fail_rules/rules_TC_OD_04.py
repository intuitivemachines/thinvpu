import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_od_04
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_04_Test_0001.csv", time='thinvpu_time', log_var="img_1_num_features", op="==", value=2500, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_04_Test_0001.csv", time='thinvpu_time', log_var="img_2_num_features", op="==", value=2500, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_04_Test_0001.csv", time='thinvpu_time', log_var="lowes_features",     op=">=", value=800,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_04_Test_0001.csv", time='thinvpu_time', log_var="h_matix_0_1",        op=">=", value=0.99, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

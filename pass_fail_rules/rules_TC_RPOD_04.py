import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_rpod_04
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_04_Test_0001.csv", time='thinvpu_time', log_var="min_0",       op="==",  value=0,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_04_Test_0001.csv", time='thinvpu_time', log_var="max_0",       op="==",  value=0,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_04_Test_0001.csv", time='thinvpu_time', log_var="min_loc_x_0", op="==",  value=63, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_04_Test_0001.csv", time='thinvpu_time', log_var="min_loc_y_0", op="==",  value=61, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

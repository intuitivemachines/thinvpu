import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_rpod_03
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_03_Test_0001.csv", time='thinvpu_time', log_var="circle_diameter",  op="==",  value=1530, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_03_Test_0001.csv", time='thinvpu_time', log_var="num_circles",      op="==",  value=1, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_03_Test_0001.csv", time='thinvpu_time', log_var="centroid_point_x", op="==",  value=3694,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_03_Test_0001.csv", time='thinvpu_time', log_var="centroid_point_y", op="==",  value=2272,  tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

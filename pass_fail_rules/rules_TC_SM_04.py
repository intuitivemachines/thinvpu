import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_sm_04
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_0_0",     op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_0_1",     op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_8_22",    op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_10_23",   op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_11_5",    op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_45_19",   op="==", value=0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_04_Test_0001.csv", time='thinvpu_time', log_var="canny_arr_2128_19", op="==", value=0, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_od_01_b
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_0", op=">",  value=500.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_1", op=">",  value=500.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_0", op=">",  value=300.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_1", op="==", value=358,   tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="==", value=41.0,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="<=", value=50.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_B_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op=">",  value=40.0, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

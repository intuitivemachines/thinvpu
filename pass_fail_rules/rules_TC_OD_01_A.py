import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_od_01_a
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_0", op="==", value=678,   tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_1", op="==", value=34,    tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_0", op="==", value=602,   tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_1", op="==", value=63,    tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="==", value=162.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="<=", value=203.0, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_OD_01_A_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op=">",  value=150.0, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

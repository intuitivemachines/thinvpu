import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_plha_01
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_0", op="==", value=522,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_1", op="==", value=1851, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_0", op="==", value=482,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_1", op="==", value=2273, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="==", value=71,   tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="<=", value=75,   tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op=">",  value=70,   tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

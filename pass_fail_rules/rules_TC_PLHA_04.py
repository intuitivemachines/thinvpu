import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_plha_04
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_04_Test_0001.csv", time='thinvpu_time', log_var="img_1_num_features", op="==",  value=2500, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_04_Test_0001.csv", time='thinvpu_time', log_var="img_2_num_features", op=">=",  value=1500, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_04_Test_0001.csv", time='thinvpu_time', log_var="lowes_features",     op=">=",  value=800,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_04_Test_0001.csv", time='thinvpu_time', log_var="num_3D_points",      op=">=",  value=2000, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

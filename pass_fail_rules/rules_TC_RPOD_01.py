import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_rpod_01
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_0", op="==", value=782, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_0_1", op="==", value=700, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_0", op="==", value=40,  tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="keypoint_arr_1_1", op="==", value=311, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="==", value=113, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op="<=", value=120, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_RPOD_01_Test_0001.csv", time='thinvpu_time', log_var="desc_arr_0_0"    , op=">",  value=100, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

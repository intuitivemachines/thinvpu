import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_sm_03
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_03_Test_0001.csv", time='thinvpu_time', log_var="features_im1",     op=">=",  value=15, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_03_Test_0001.csv", time='thinvpu_time', log_var="features_im2",     op="==",  value=25, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_SM_03_Test_0001.csv", time='thinvpu_time', log_var="features_matched", op=">=",  value=20, tolerance= "none")
   
    return rule_set.of_type(sim_type)
    

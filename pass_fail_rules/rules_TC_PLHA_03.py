import rules_helpers as rh
def rules(sim_type="thinvpu"):
    rule_set=rh.RuleSet()
    # =================
    # Rules set tc_plha_03
    # =================
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_03_Test_0001.csv", time='thinvpu_time', log_var="superpixel_area_input", op="==",  value=100, tolerance= "none")
    rule_set.new_rule(log_type = 'thinvpu', log="TC_PLHA_03_Test_0001.csv", time='thinvpu_time', log_var="num_superpixels",       op=">=",  value=500, tolerance= "none")
    
    return rule_set.of_type(sim_type)
    

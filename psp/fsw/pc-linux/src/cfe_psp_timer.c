/************************************************************************************************
** File:  cfe_psp_timer.c
**
**
**      Copyright (c) 2004-2011, United States Government as represented by 
**      Administrator for The National Aeronautics and Space Administration. 
**      All Rights Reserved.
**
**      This is governed by the NASA Open Source Agreement and may be used,
**      distributed and modified only pursuant to the terms of that agreement.
**
**
** Purpose:
**   This file contains glue routines between the cFE and the OS Board Support Package ( BSP ).
**   The functions here allow the cFE to interface functions that are board and OS specific
**   and usually dont fit well in the OS abstraction layer.
**
** History:
**   2005/06/05  K.Audra    | Initial version,
**
*************************************************************************************************/

/*
**  Include Files
*/


/*
** cFE includes
*/
#include "common_types.h"
#include "osapi.h"

/*
**  System Include Files
*/
#include <stdio.h>
#include <stdlib.h>

/*
** Types and prototypes for this module
*/
#include "cfe_psp.h"
#include "cfe_time_msg.h"

void CFE_PSP_Init1HzTimer(void);
void CFE_PSP_PPSHandler(int num);

/* External Declarations
**
*/
extern void CFE_TIME_Local1HzISR(void);
extern void CFE_TIME_Tone1HzISR(void);


/******************* Macro Definitions ***********************/

#define CFE_PSP_TIMER_TICKS_PER_SECOND       1000000     /* Resolution of the least significant 32 bits of the 64 bit
                                                           time stamp returned by OS_BSPGet_Timebase in timer ticks per second.
                                                           The timer resolution for accuracy should not be any slower than 1000000
                                                           ticks per second or 1 us per tick */
#define CFE_PSP_TIMER_LOW32_ROLLOVER         1000000     /* The number that the least significant 32 bits of the 64 bit
                                                           time stamp returned by OS_BSPGet_Timebase rolls over.  If the lower 32
                                                           bits rolls at 1 second, then the OS_BSP_TIMER_LOW32_ROLLOVER will be 1000000.
                                                           if the lower 32 bits rolls at its maximum value (2^32) then
                                                           OS_BSP_TIMER_LOW32_ROLLOVER will be 0. */


/******************************************************************************
**  Function:  CFE_PSP_Init1HzTimer()
**
**  Purpose: Initializes the 1Hz Timer and connects it to the cFE TIME 1Hz routine
**  		 via the AuxClkISR
**
**  Arguments: none
******************************************************************************/
// Use GPIO Pin 17, which is Pin 0 for wiringPi library
#define BUTTON_PIN 0

void CFE_PSP_PPSHandler();

void CFE_PSP_Init1HzTimer(void)
{
#if 0 // GPIO HW SPECIFIC< FIXME FOR SG-100
  if (wiringPiSetup () < 0) {
     printf ("Unable to setup wiringPi\n");
		      return;
  }
    // set Pin 17/0 generate an interrupt on high-to-low transitions
    // and attach myInterrupt() to the interrupt
    if ( wiringPiISR (BUTTON_PIN, INT_EDGE_FALLING, &CFE_PSP_PPSHandler) < 0 ) {
          printf ("Unable to setup ISR:\n");
                   return;
    }
#endif
#if 0
    ppsIntConnect((VOIDFUNCPTR)CFE_PSP_PPSHandler);
    ppsIntEnable();
#endif
}/* end CFE_PSP_Init1HzTimer */

extern int dbt2;

void CFE_PSP_PPSHandler(int num)
{
	OS_time_t pps_current_time={0,0};
	unsigned long long pps_time_diff_ll=0;
	unsigned long long pps_current_time_ll=0;
	static unsigned long long pps_prev_time_ll=0;
	
	int32 IntFlags;
	
    char str[80];

    printf("Got PPS !!!!\n");

    /*
    ** Ensure that the change is made without interruption...
    */
    IntFlags = OS_IntLock();
    
    CFE_PSP_GetTime(&pps_current_time);    
    pps_current_time_ll = (unsigned long long)((unsigned long long)pps_current_time.seconds*1000000)+((unsigned long long)pps_current_time.microsecs);
    
    /* only allow Tone ISR during non-rollover; if rollover occurs, will correct after 1 cycle */
    if (pps_current_time_ll  > pps_prev_time_ll)
    {
    	pps_time_diff_ll = pps_current_time_ll-pps_prev_time_ll;
	
	
		if (pps_time_diff_ll > (1000000 - (CFE_TIME_CFG_TONE_LIMIT*2)))
		{
			if (dbt2==1)
			{
				  sprintf(str,"PPS: pPulse good...pps_time_diff_ll:[%llu]\n",pps_time_diff_ll);
				  OS_printf(str);
			}
			/* Call PPS Tone 1HZ ISR Function */
			CFE_TIME_Tone1HzISR();
		}
		else
		{
			if (dbt2==1)
			{
				  sprintf(str,"PPS: pPulse BAD...pps_time_diff_ll:[%llu]\n",pps_time_diff_ll);
				  OS_printf(str);
			}
			
		}
		
    }

	pps_prev_time_ll = pps_current_time_ll;
    


   /*
    ** Release interrupt lock...
    */
    OS_IntUnlock(IntFlags);
}

/******************************************************************************
**  Function:  CFE_PSP_GetTime()
**
**  Purpose: Gets the value of the time from the hardware
**
**  Arguments: LocalTime - where the time is returned through
******************************************************************************/

void CFE_PSP_GetTime( OS_time_t *LocalTime)
{

    /* since we don't have a hardware register to access like the mcp750,
     * we use a call to the OS to get the time */

    OS_GetLocalTime(LocalTime);

}/* end CFE_PSP_GetLocalTime */



/******************************************************************************
**  Function:  CFE_PSP_Get_Timer_Tick()
**
**  Purpose:
**    Provides a common interface to system clock tick. This routine
**    is in the BSP because it is sometimes implemented in hardware and
**    sometimes taken care of by the RTOS.
**
**  Arguments:
**
**  Return:
**  OS system clock ticks per second
*/
uint32 CFE_PSP_Get_Timer_Tick(void)
{
   /* SUB -add function call code*/
   return (0);
}

/******************************************************************************
**  Function:  CFE_PSP_GetTimerTicksPerSecond()
**
**  Purpose:
**    Provides the resolution of the least significant 32 bits of the 64 bit
**    time stamp returned by OS_BSPGet_Timebase in timer ticks per second.
**    The timer resolution for accuracy should not be any slower than 1000000
**    ticks per second or 1 us per tick
**
**  Arguments:
**
**  Return:
**    The number of timer ticks per second of the time stamp returned
**    by CFE_PSP_Get_Timebase
*/
uint32 CFE_PSP_GetTimerTicksPerSecond(void)
{
    return(CFE_PSP_TIMER_TICKS_PER_SECOND);
}

/******************************************************************************
**  Function:  CFE_PSP_GetTimerLow32Rollover()
**
**  Purpose:
**    Provides the number that the least significant 32 bits of the 64 bit
**    time stamp returned by CFE_PSP_Get_Timebase rolls over.  If the lower 32
**    bits rolls at 1 second, then the CFE_PSP_TIMER_LOW32_ROLLOVER will be 1000000.
**    if the lower 32 bits rolls at its maximum value (2^32) then
**    CFE_PSP_TIMER_LOW32_ROLLOVER will be 0.
**
**  Arguments:
**
**  Return:
**    The number that the least significant 32 bits of the 64 bit time stamp
**    returned by CFE_PSP_Get_Timebase rolls over.
*/
uint32 CFE_PSP_GetTimerLow32Rollover(void)
{
    return(CFE_PSP_TIMER_LOW32_ROLLOVER);
}

/******************************************************************************
**  Function:  CFE_PSP_Get_Timebase()
**
**  Purpose:
**    Provides a common interface to system timebase. This routine
**    is in the BSP because it is sometimes implemented in hardware and
**    sometimes taken care of by the RTOS.
**
**  Arguments:
**
**  Return:
**  Timebase register value
*/
void CFE_PSP_Get_Timebase(uint32 *Tbu, uint32* Tbl)
{
   OS_time_t        time;

   OS_GetLocalTime(&time);
   *Tbu = time.seconds;
   *Tbl = time.microsecs;
}

/******************************************************************************
**  Function:  CFE_PSP_Get_Dec()
**
**  Purpose:
**    Provides a common interface to decrementer counter. This routine
**    is in the PSP because it is sometimes implemented in hardware and
**    sometimes taken care of by the RTOS.
**
**  Arguments:
**
**  Return:
**  Timebase register value
*/

uint32 CFE_PSP_Get_Dec(void)
{
   /* SUB -add function call code*/
   return(0);
}


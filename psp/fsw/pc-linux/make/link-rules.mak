###############################################################################
# File: link-rules.mak
#
# Purpose:
#   Makefile for linking code and producing the cFE Core executable image.
#
# History:
#
###############################################################################
##
## Executable target. This is target specific
##
EXE_TARGET=core-linux.bin

CORE_INSTALL_FILES = $(EXE_TARGET)


##
## Linker flags that are needed
##
LDFLAGS = -Wl,-export-dynamic

##
## Libraries to link in
##
LIBS =  -lm -lpthread -ldl -lrt
##
## Uncomment the following line to link in C++ standard libs
## LIBS += -lstdc++
## 

##
## Libraries required for OPENCV_LIBS
##
OPENCV_LIBS = -lstdc++ -I/usr/local/include/opencv2/opencv -I/usr/local/include/opencv2 -L/usr/local/lib -L/usr/local/share/OpenCV/3rdparty/lib  -lopencv_stitching -lopencv_video -lopencv_saliency -lopencv_shape -lopencv_surface_matching -lopencv_ximgproc -lopencv_calib3d -lopencv_imgcodecs -lopencv_features2d -lopencv_imgproc -lopencv_flann -lopencv_core -ljpeg -lpng

##
## cFE Core Link Rule
## 
$(EXE_TARGET): $(CORE_OBJS)
	$(COMPILER) $(DEBUG_FLAGS) -o $(EXE_TARGET) $(CORE_OBJS) $(LDFLAGS) $(LIBS)
	
##
## Application Link Rule
##
$(APPTARGET).$(APP_EXT): $(OBJS) $(CPP_OBJS)
	$(COMPILER) -ggdb -shared $(OBJS) $(CPP_OBJS) $(LDFLAGS) $(LIBS) $(OPENCV_LIBS) -o $@ 
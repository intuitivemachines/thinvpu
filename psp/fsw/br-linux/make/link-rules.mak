###############################################################################
# File: link-rules.mak
#
# Purpose:
#   Makefile for linking code and producing the cFE Core executable image.
#
# History:
#
###############################################################################
##
## Executable target. This is target specific
##
EXE_TARGET=core-linux.bin

CORE_INSTALL_FILES = $(EXE_TARGET)


##
## Linker flags that are needed
##
LDFLAGS = -Wl,-export-dynamic

##
## Libraries to link in
##
LIBS =  -lm -lpthread -ldl -lrt
##
## Uncomment the following line to link in C++ standard libs
## LIBS += -lstdc++
## 

##
## Libraries required for OPENCV_LIBS
##
OPENCV_LIB_BASE=$(HOME)/br/Linux/buildroot-2017.11/output/build/opencv3-3.3.0
OPENCV_LIBS = -L$(OPENCV_LIB_BASE)/buildroot-build/lib -L$(OPENCV_LIB_BASE)/3rdparty -lopencv_stitching -lopencv_shape -lopencv_calib3d -lopencv_imgcodecs -lopencv_features2d -lopencv_imgproc -lopencv_flann -lopencv_core 

##
## cFE Core Link Rule
## 
$(EXE_TARGET): $(CORE_OBJS)
	$(COMPILER) $(DEBUG_FLAGS) -o $(EXE_TARGET) $(CORE_OBJS) $(LDFLAGS) $(LIBS)
	
##
## Application Link Rule
##
$(APPTARGET).$(APP_EXT): $(OBJS) $(CPP_OBJS)
	$(COMPILER) -ggdb -shared $(OBJS) $(CPP_OBJS) $(LDFLAGS) $(LIBS) $(OPENCV_LIBS) -o $@ 
import struct, sys, os, glob
import decom_TC_OD_01_A, decom_TC_OD_01_B, decom_TC_OD_02, decom_TC_OD_03, decom_TC_OD_04
import decom_TC_PLHA_01, decom_TC_PLHA_02, decom_TC_PLHA_03, decom_TC_PLHA_04 
import decom_TC_RPOD_01, decom_TC_RPOD_02, decom_TC_RPOD_03, decom_TC_RPOD_04 
import decom_TC_SM_01, decom_TC_SM_02, decom_TC_SM_03, decom_TC_SM_04

log_dir = "/home/lander/code/test/thinvpu-testbed/build/thinvpu-x86/exe/cf/log/"

def main():

    # Decom all OD_01_A Binary Logs
    TC_OD_01_A_files = list(glob.glob(log_dir+"TC_OD_01_A*.bin"))
    for item in TC_OD_01_A_files:
        decom_TC_OD_01_A.main(item)

    # Decom all OD_01_B Binary Logs
    TC_OD_01_B_files = list(glob.glob(log_dir+"TC_OD_01_B*.bin"))
    for item in TC_OD_01_B_files:
        decom_TC_OD_01_B.main(item)

    # Decom all OD_02 Binary Logs
    TC_OD_02_files = list(glob.glob(log_dir+"TC_OD_02*.bin"))
    for item in TC_OD_02_files:
        decom_TC_OD_02.main(item)

    # Decom all OD_03 Binary Logs
    TC_OD_03_files = list(glob.glob(log_dir+"TC_OD_03*.bin"))
    for item in TC_OD_03_files:
        decom_TC_OD_03.main(item)

    # Decom all OD_04 Binary Logs
    TC_OD_04_files = list(glob.glob(log_dir+"TC_OD_04*.bin"))
    for item in TC_OD_04_files:
        decom_TC_OD_04.main(item)

    # Decom all RPOD_01 Binary Logs
    TC_RPOD_01_files = list(glob.glob(log_dir+"TC_RPOD_01*.bin"))
    for item in TC_RPOD_01_files:
        decom_TC_RPOD_01.main(item)
    
    # Decom all RPOD_02 Binary Logs
    TC_RPOD_02_files = list(glob.glob(log_dir+"TC_RPOD_02*.bin"))
    for item in TC_RPOD_02_files:
        decom_TC_RPOD_02.main(item)

    # Decom all RPOD_03 Binary Logs
    TC_RPOD_03_files = list(glob.glob(log_dir+"TC_RPOD_03*.bin"))
    for item in TC_RPOD_03_files:
        decom_TC_RPOD_03.main(item)

    # Decom all RPOD_04 Binary Logs
    TC_RPOD_04_files = list(glob.glob(log_dir+"TC_RPOD_04*.bin"))
    for item in TC_RPOD_04_files:
        decom_TC_RPOD_04.main(item)

    # Decom all PLHA_01 Binary Logs
    TC_PLHA_01_files = list(glob.glob(log_dir+"TC_PLHA_01*.bin"))
    for item in TC_PLHA_01_files:
        decom_TC_PLHA_01.main(item)

    # Decom all PLHA_02 Binary Logs
    TC_PLHA_02_files = list(glob.glob(log_dir+"TC_PLHA_02*.bin"))
    for item in TC_PLHA_02_files:
        decom_TC_PLHA_02.main(item)

    # Decom all PLHA_03 Binary Logs
    TC_PLHA_03_files = list(glob.glob(log_dir+"TC_PLHA_03*.bin"))
    for item in TC_PLHA_03_files:
        decom_TC_PLHA_03.main(item)

    # Decom all PLHA_04 Binary Logs
    TC_PLHA_04_files = list(glob.glob(log_dir+"TC_PLHA_04*.bin"))
    for item in TC_PLHA_04_files:
        decom_TC_PLHA_04.main(item)

    # Decom all SM_01 Binary Logs
    TC_SM_01_files = list(glob.glob(log_dir+"TC_SM_01*.bin"))
    for item in TC_SM_01_files:
        decom_TC_SM_01.main(item)

    # Decom all SM_02 Binary Logs
    TC_SM_02_files = list(glob.glob(log_dir+"TC_SM_02*.bin"))
    for item in TC_SM_02_files:
        decom_TC_SM_02.main(item)

#    # Decom all SM_03 Binary Logs
    TC_SM_03_files = list(glob.glob(log_dir+"TC_SM_03*.bin"))
    for item in TC_SM_03_files:
        decom_TC_SM_03.main(item)

    # Decom all SM_04 Binary Logs
    TC_SM_04_files = list(glob.glob(log_dir+"TC_SM_04*.bin"))
    for item in TC_SM_04_files:
        decom_TC_SM_04.main(item)

if __name__=="__main__": 
    main() 
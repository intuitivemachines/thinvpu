import struct, sys, os

tc_name = "TC_RPOD_04"

def print_header(out):
    out.write('seconds,subseconds,uiCounter,fElapsedTime,fRateTime,time_tag,test_active,test_complete,test_count,spare')
    out.write(',min_0,max_0,min_loc_x_0,min_loc_y_0,max_loc_x_0,max_loc_y_0')
    out.write(',min_1,max_1,min_loc_x_1,min_loc_y_1,max_loc_x_1,max_loc_y_1')
    out.write(',min_2,max_2,min_loc_x_2,min_loc_y_2,max_loc_x_2,max_loc_y_2')
    out.write(',min_3,max_3,min_loc_x_3,min_loc_y_3,max_loc_x_3,max_loc_y_3')
    out.write(',min_4,max_4,min_loc_x_4,min_loc_y_4,max_loc_x_4,max_loc_y_4')
    out.write(',min_5,max_5,min_loc_x_5,min_loc_y_5,max_loc_x_5,max_loc_y_5\n')

def main(filestring):
    outfile = os.path.splitext(filestring)[0]+'.csv'
    print tc_name + 'Input File:  ' + filestring
    print tc_name + 'Output File: ' + outfile

    try:
        f = open(filestring, 'rb')
        out = open(outfile, 'w')
        print_header(out)
        data = f.read(6)
        while data != "":
            data = f.read(70)
            if len(data) == 70:
                unpacked = struct.unpack('<IHIffdHHHHBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB',data)
                for item in unpacked:
                    out.write(str(item))
                    out.write(',')
                out.seek(-1,os.SEEK_CUR)
                out.write("\n")
    except IOError:
        print tc_name + ' Error - file could not be opened'
        sys.exit()
    print 'TC_RPOD_04 Data Decom Complete....'

if __name__=="__main__": 
    # Verify number of arguments
    if len(sys.argv) < 2 or len(sys.argv) > 2:
        print tc_name + ' Error - Invalid number of arguments....'
        print tc_name + ' Usage - python ' + os.path.basename(__file__) + " /home/filepath/filename.bin"
        sys.exit()
    main(sys.argv[1]) 
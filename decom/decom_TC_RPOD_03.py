import struct, sys, os
import decom_utils

tc_name = "TC_RPOD_03"

def main(filestring):

    # Set file variables
    print tc_name + ' Opening Binary File....'
    outfile = os.path.splitext(filestring)[0]+'.csv'
    print tc_name + ' Input File:  ' + filestring
    print tc_name + ' Output File: ' + outfile

    try:
        f = open(filestring, 'rb')
        out = open(outfile, 'w')
        print tc_name + ' Data Decom Started....'
        decom_utils.print_header(out)
        decom_utils.print_hugh_circle_header(out)
        out.write('\n') # End of row
        decom_utils.parse_ccsds_header(tc_name, f, out)
        decom_utils.parse_testcase_meta(tc_name, f, out)
        decom_utils.parse_hugh_cirlce(tc_name, f, out)
        out.write('\n') # End of row
        print tc_name + ' Data Decom Complete....'
    except IOError:
        print tc_name + ' Error - file could not be opened'
        sys.exit()

if __name__=="__main__": 
    # Verify number of arguments
    if len(sys.argv) < 2 or len(sys.argv) > 2:
        print tc_name + ' Error - Invalid number of arguments....'
        print tc_name + ' Usage - python ' + os.path.basename(__file__) + " /home/filepath/filename.bin"
        sys.exit()
    main(sys.argv[1]) 


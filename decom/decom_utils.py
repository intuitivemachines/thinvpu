import struct, sys, os

sizeof_float = 4

def print_header(out):
    out.write('seconds,subseconds,uiCounter,fElapsedTime,fRateTime,time_tag,test_active,test_complete,test_count,spare')

def print_keypoint_header(num_rows, num_cols, out):
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            var_string = ',keypoint_arr_' + str(x) + "_" + str(y)
            out.write(var_string)

def print_desc_header(num_rows, num_cols, out):
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            var_string = ',desc_arr_' + str(x) + "_" + str(y)
            out.write(var_string)

def print_canny_header(num_rows, num_cols, out):
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            var_string = ',canny_arr_' + str(x) + "_" + str(y)
            out.write(var_string)

def print_feature_matcher_header(out):
    out.write(', img_1_num_features, img_2_num_features, lowes_features')

def print_3D_point_header(out):
    out.write(', img_1_num_features, img_2_num_features, lowes_features, num_3D_points')

def print_homography_header(out):
    out.write(', h_matix_0_1,h_matix_0_2,h_matix_0_3,h_matix_1_1,h_matix_1_2,h_matix_1_3,h_matix_2_1,h_matix_2_2,h_matix_2_3')

def print_hugh_circle_header(out):
    out.write(', circle_diameter, num_circles, centroid_point_x, centroid_point_y')

def print_superpixels_header(out):
    out.write(', superpixel_area_input, num_superpixels')

def print_optical_flow_header(out):
    out.write(', features_im1, features_im2, features_matched')

def parse_ccsds_header(tc_name, file, out):
    print tc_name + ' Parsing CCSDS Header....'
    data = file.read(6) # Skip Primary Header
    data = file.read(6) # Read Secondary Header (Timestamp)
    unpacked = struct.unpack('<IH',data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_testcase_meta(tc_name, file, out):
    print tc_name + ' Parsing Test Case Metadata....'
    data = file.read(28) 
    unpacked = struct.unpack('<IffdHHHH',data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_keypoint(tc_name,num_rows, num_cols, file, out):
    print tc_name + ' Parsing Keypoints....'
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            data = file.read(sizeof_float)
            unpacked = struct.unpack('<f',data)
            for item in unpacked:
                out.write(str(item))
                out.write(',')

def parse_descriptors(tc_name,num_rows,num_cols, file, out):
    print tc_name + ' Parsing Descriptor Array....'
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            data = file.read(1)
            unpacked = struct.unpack('<B',data)
            for item in unpacked:
                out.write(str(item))
                out.write(',')

def parse_canny(tc_name,num_rows,num_cols, file, out):
    print tc_name + ' Parsing Canny Array....'
    for x in range(0,num_rows):
        for y in range(0,num_cols):
            data = file.read(1)
            unpacked = struct.unpack('<B',data)
            for item in unpacked:
                out.write(str(item))
                out.write(',')

def parse_feature_matcher(tc_name, file, out):
    print tc_name + ' Parsing Orb Feature Matcher....'
    data = file.read(6)
    unpacked = struct.unpack('<HHH', data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_homography(tc_name,file, out):
    print tc_name + ' Parsing Homography....'
    data = file.read(74)
    unpacked = struct.unpack('<xxddddddddd',data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_hugh_cirlce(tc_name, file, out):
    print tc_name + ' Parsing Hugh Cirlce....'
    data = file.read(8)
    unpacked = struct.unpack('<HHHH', data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_superpixels(tc_name, file, out):
    print tc_name + ' Parsing Superpixels....'
    data = file.read(8)
    unpacked = struct.unpack('<HH', data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_3D_points(tc_name, file, out):
    print tc_name + ' Parsing 3D Points....'
    data = file.read(8)
    unpacked = struct.unpack('<HHHH', data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')

def parse_optical_flow(tc_name, file, out):
    print tc_name + ' Parsing Optical Flow....'
    data = file.read(6)
    unpacked = struct.unpack('<HHH', data)
    for item in unpacked:
        out.write(str(item))
        out.write(',')
#! /bin/bash
if [ "${#}" -lt 1 ]; then
    echo "Usage: incr_build_run_test <xml_output_file>"
    exit
fi

cd build
make install
status=$?
cd ..
if [ $status -eq 0 ]; then  # only run tests if make succeeded
    ./ThinVPUTestRunner ${1}
fi

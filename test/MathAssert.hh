/******************************************************************************
 ** Macros to assist with CPPUNIT asserts upon vectors and matrices containing
 ** floating point values
 ******************************************************************************/

#ifndef MATHASSERT_HH_
#define MATHASSERT_HH_

#include <cppunit/extensions/HelperMacros.h>

/**
 * Compare two 3-element floating-point vectors w/ provided epsilon acceptance criteria
 */
#define CPPUNIT_ASSERT_VECTOR3_EQUAL( expected, actual, epsilon ) \
        CPPUNIT_ASSERT_VECTORN_EQUAL( 3, expected, actual, epsilon );

/**
 * Compare two N-element floating-point vectors w/ provided epsilon acceptance criteria
 */
#define CPPUNIT_ASSERT_VECTORN_EQUAL( size, expected, actual, epsilon ) \
        for ( int i = 0; i < size; i++ ) \
        { \
                CPPUNIT_ASSERT_DOUBLES_EQUAL( expected[i], actual[i], epsilon ); \
        }

/**
 * Compare two 3-element floating-point vectors w/ the provided acceptance criteria,
 * displays the provided message upon non-acceptance.
 */
#define CPPUNIT_ASSERT_VECTOR3_EQUAL_MESSAGE( message, expected, actual, epsilon ) \
        CPPUNIT_ASSERT_VECTORN_EQUAL_MESSAGE( message, 3, expected, actual, epsilon );

/**
 * Compare two N-element floating-point vectors w/ the provided epsilon acceptance criteria,
 * displays the provided message upon non-acceptance.
 */
#define CPPUNIT_ASSERT_VECTORN_EQUAL_MESSAGE( message, size, expected, actual, epsilon ) \
        for( int i = 0; i < size; i++ ) \
        { \
                CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE( message, expected[i], actual[i], epsilon ); \
        }

/**
 * Compare two 3x3 floating-point matrices w/ the provided epsilon acceptance criteria.
 */
#define CPPUNIT_ASSERT_MATRIX3X3_EQUAL( expected, actual, epsilon ) \
        CPPUNIT_ASSERT_MATRIXMXN_EQUAL( 3, 3, expected, actual, epsilon );

/**
 * Compare two MxN floating-point matrices w/ the provided epsilon acceptance criteria.
 */
#define CPPUNIT_ASSERT_MATRIXMXN_EQUAL( m, n, expected, actual, epsilon ) \
        for ( int i = 0; i < m; i++ ) \
        { \
                for ( int j = 0; j < n; j++ ) \
                { \
                        CPPUNIT_ASSERT_DOUBLES_EQUAL( expected[i][j], actual[i][j], epsilon ); \
                } \
        }

/**
 * Compare two 3x3 floating-point matrices w/ the provided epsilon acceptance criteria,
 * displays the provided message upon non-acceptance.
 */
#define CPPUNIT_ASSERT_MATRIX3X_EQUAL_MESSAGE( message, expected, actual, epsilon )\
        CPPUNIT_ASSERT_MATRIXMXN_EQUAL_MESSAGE( message, 3, 3, expected, actual, epsilon );

/**
 * Compare two MxN floating-point matrices w/ the provided epsilon acceptance criteria,
 * displays the provided message upon non-acceptance.
 */
#define CPPUNIT_ASSERT_MATRIXMXN_EQUAL_MESSAGE( message, m, n, expected, actual, epsilon ) \
    for ( int i = 0; i < m; i++ ) \
        { \
        for ( int j = 0; j < n; j++ ) \
                { \
                CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE( message, expected[i][j], actual[i][j], epsilon ); \
                } \
        }

#endif /* MATHASSERT_HH_ */

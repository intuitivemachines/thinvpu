/**
 * Exercises all ThinVPU unit tests that have been registered with the testing Singleton.  Records XUnit-compliant XML
 * test report output for all test results.
 */

#include <iostream>
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

using namespace CppUnit;

const char* SEPARATOR = "################################################################################";

int main( int argc, char* argv[] )
{
        if ( argc != 2 )
        {
                std::cout << "Usage: ThinVPUTestRunner <outputXmlFilename>" << std::endl;
                return 0;
        }

        TestResult result;

        TestResultCollector collectedResults;
        result.addListener( &collectedResults );

        BriefTestProgressListener progress;
        result.addListener( &progress );

        TestRunner testRunner;

        // retrieve tests from Singleton registry
        testRunner.addTest( TestFactoryRegistry::getRegistry().makeTest() );

        testRunner.run( result );

        std::cout.flush();

        std::cout << std::endl << SEPARATOR << std::endl;
        std::cout << "TEST RESULTS SUMMARY" << std::endl;
        std::cout << SEPARATOR << std::endl;

        CompilerOutputter compilerOutputter( &collectedResults, std::cerr );
        compilerOutputter.write();

        std::ofstream xmlFileOut( argv[1] );
        XmlOutputter xmlOut( &collectedResults, xmlFileOut );
        xmlOut.write();

        return collectedResults.wasSuccessful() ? 0 : 1;
}


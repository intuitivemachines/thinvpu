import numpy as np 
import cv2 

MIN_MATCH_COUNT = 10

old_frame = cv2.imread("old.png",0)
new_frame = cv2.imread("new.png",0)

# params for corner detection 
feature_params = dict(maxCorners = 100, qualityLevel = 0.375, minDistance = 7, blockSize = 7 ) 

# Parameters for lucas kanade optical flow 
lk_params = dict( winSize = (25, 25), maxLevel = 20, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)) 

# Create some random colors 
color = np.random.randint(0, 255, (100, 3)) 

# Take first frame and find corners in it 
p0 = cv2.goodFeaturesToTrack(old_frame, mask = None,**feature_params)
print(len(p0))

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame) 

# calculate optical flow 
p1, st, err = cv2.calcOpticalFlowPyrLK(old_frame,new_frame,p0, None,**lk_params)
print(len(p1))
# Select good points 
good_new = p1[st == 1] 
good_old = p0[st == 1] 
print(len(good_new))
print(len(good_old))
# draw the tracks
for i, (new, old) in enumerate(zip(good_new,good_old)): 
    a, b = new.ravel() 
    c, d = old.ravel()
    a = int(a)
    b = int(b)
    c = int(c)
    d = int(d)
    mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2) 
    frame = cv2.circle(new_frame, (a, b), 5, color[i].tolist(), -1) 
img = cv2.add(frame, mask)
cv2.imwrite('TC-SM-03.png',img) 
# Updating Previous frame and points 
old_frame = new_frame.copy() 
p0 = good_new.reshape(-1, 1, 2) 

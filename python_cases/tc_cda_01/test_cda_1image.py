# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 18:36:32 2020

@author: jgetchius
"""

# Dependencies
import cv2
import numpy as np
import copy
from matplotlib import pyplot as plt
import crater_detect as cd

###############################################################################
#    TEST HARNESS
###############################################################################
# Clean up the space
cv2.destroyAllWindows()

# Parameters
debug_plots = False # Turning this on displays more details on the output img

# OpenCV Initialization
Internal = cd.InternalClass()
u_sun = np.array([0,1])

# Load an image with color channels.  CDA converts as necessary
imgo= cv2.imread('test_image.jpg', cv2.IMREAD_COLOR)
imagein = copy.deepcopy(imgo)

# Direction of travel of the sun's rays in the image frame
# This is the negative of the unit vector to the sun
u_sun = np.array([0,1])

# Adjust the tuning parameters
Internal.params.light_thresh_mult = 1.5 # standard deviation for specular regions
Internal.params.shadow_thresh_mult = 1.5 #standard deviation for shadow regions
Internal.params.area_frac_threshold = .7 #minimum fraction of found features area to fit circle area
Internal.params.max_shadow_contour_area = 1000*1000 #maximum shadow feature area
Internal.params.max_light_contour_area = 1000*1000 #maximum specular feature area
Internal.params.min_light_contour_area = 10 #minimum specular feature area
Internal.params.min_shadow_contour_area = 500 #minimum shadow feature area

# Run the CDA algorithm
shadows, lights, found_craters, center_list = cd.detectCrater( imagein, Internal, u_sun, False)

# Display the output image and the original image
framec = cv2.resize(imagein, (750, 750), interpolation = cv2.INTER_AREA) 
cv2.imshow('Out Image', framec)
imgo = cv2.resize(imgo, (750, 750), interpolation = cv2.INTER_AREA) 
cv2.imshow('Original Image', imgo)

# Write the images out
cv2.imwrite('outimage.png', framec )

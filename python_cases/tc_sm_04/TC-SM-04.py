import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('old.png',0)
img_hist = cv.equalizeHist(img)
# performs canny edge detection
edges = cv.Canny(img_hist,210,220)

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
cv.imwrite('TC-SM-04-output.png', edges)
plt.show()

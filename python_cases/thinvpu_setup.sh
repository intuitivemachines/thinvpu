#!/bin/sh

#================================
# Script Runner
#===============================

# Instructions:
# use command: ./thinvpu_setup.sh
# This will install all dependencies for python3

# Install dependencies
pip3 install numpy
pip3 install matplotlib
pip3 install opencv-contrib-python==3.3.0.9
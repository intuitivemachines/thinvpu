import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('OD_ORB.png',0)

# Initiate STAR detector
orb = cv.ORB_create()

# find the keypoints with ORB
kp = orb.detect(img,None)

print("Number of Key Points",len(kp))
print("Centroid X/Y of the first 3  Key Points:")
print(kp[0].pt)
print(kp[1].pt)
print(kp[2].pt)
# compute the descriptors with ORB
kp, des = orb.compute(img, kp)
print("Descriptor of the first 3  Key Points:")
print(des[0])
print(des[1])
print(des[2])


# draw only keypoints location,not size and orientation
img2 = cv.drawKeypoints(img,kp,color=(0,255,0), flags=0, outImage = None)
cv.imwrite('TC-OD-01-A-output.png', img2)


plt.imshow(img2),plt.show()

img3 = cv.equalizeHist(img)
cv.imwrite('OD_ORB_hist.png', img3)

# Initiate STAR detector
orb = cv.ORB_create()

# find the keypoints with ORB
kp3 = orb.detect(img3,None)

print("Number of Key Points",len(kp3))
print("Centroid X/Y of the first 3  Key Points:")
print(kp[0].pt)
print(kp[1].pt)
print(kp[2].pt)
# compute the descriptors with ORB
kp3, des3 = orb.compute(img3, kp3)
print("Descriptor of the first 3  Key Points:")
print(des[0])
print(des[1])
print(des[2])


# draw only keypoints location,not size and orientation
img4 = cv.drawKeypoints(img3,kp3,color=(0,255,0), flags=0, outImage = None)
cv.imwrite('TC-OD-01-B-output.png', img4)

plt.imshow(img4),plt.show()
#################################################################################
#
#
#
#   Steps to perform SfM:
#
#   1. Look for ORB features from two images of a scene captured from different angle.
#   2. Compute local matches and find closest neighbors.
#   3. Filter out bad neighbors by comparing distance (Lowes criteria).
#   4. Draw remaining matches on the image and output it.
#   5. Find inlier matches by calculating the essential matrix and mask with RANSAC.
#   6. Find rotation and translation matrices of one image.
#   7. Compute 3D point cloud and plot the graph.
#
#
#
##################################################################################
# Import required libraries
import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import csv

MIN_MATCH_COUNT = 10
features2detect = 2500

#TODO: Load Different Image Pairs
#dir_name = "input_data/"
#img_pairs = [(dir_name + "a1.png", dir_name + "a2.png"),
#             (dir_name + "b1.png", dir_name + "b2.png"),
#             (dir_name + "c1.png", dir_name + "c2.png")]

img_pairs = [("FLB.JPG", "FRB.JPG")]

#img_pairs = [("rpi_img_no_pert/nopert80.png", "rpi_img_no_pert/nopert104.png")]

counter = 0

#TODO: Replace K with given Intrinsic Matrix FOV 33.3 x = 2160 y = 2560
K = np.array([[3611.27, 0., 1080],
              [0., 4280.03, 1280],
              [0.,   0.,   1.]])

for img_pair_1, img_pair_2 in img_pairs:
    counter += 1
    img1=cv2.imread(img_pair_1,0)
    img2=cv2.imread(img_pair_2,0)

       
    ###############################
    #1----ORB feature matching---#
    ###############################

    #detect orb features for both images
    orb = cv2.ORB_create(nfeatures = features2detect)
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)
    print('Key points img 1:',len(kp1))
    print('Key points img 2:',len(kp2))

    #use flann to perform feature matching
    FLANN_INDEX_LSH = 6
    index_params= dict(algorithm = FLANN_INDEX_LSH,
                       table_number = 6, # 12
                       key_size = 12,     # 20
                       multi_probe_level = 1) #2

    search_params = dict(checks=100)
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    print('Number of matches:',len(matches))

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    if len(good)>MIN_MATCH_COUNT:
        p1 = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        p2 = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       flags = 2)

    img_orbmatch = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

    cv2.imwrite('orb_match_' + str(counter) + '.png',img_orbmatch)
    print('Lowes number of good matches:',len(good))
    #########################
    #2----essential matrix--#
    #########################
    E, mask = cv2.findEssentialMat(p1, p2, K, cv2.RANSAC, 0.999, 1.0);

    matchesMask = mask.ravel().tolist()

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)

    img_inliermatch = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    cv2.imwrite('inlier_match_' + str(counter) + '.png',img_inliermatch)
    #print("Essential matrix:")
    #print(E)

    ####################
    #3----recoverpose--#
    ####################

    points, R, t, mask = cv2.recoverPose(E, p1, p2)
    #print("Rotation:")
    #print(R)
    #print("Translation:")
    #print(t)
    p1_tmp = np.expand_dims(np.squeeze(p1), 0)
    p1_tmp = np.ones([3, p1.shape[0]])
    p1_tmp[:2,:] = np.squeeze(p1).T
    p2_tmp = np.ones([3, p2.shape[0]])
    p2_tmp[:2,:] = np.squeeze(p2).T
    #print((np.dot(R, p2_tmp) + t) - p1_tmp)

    #######################
    #4----triangulation---#
    #######################

    #calculate projection matrix for both camera
    M_r = np.hstack((R, t))
    M_l = np.hstack((np.eye(3, 3), np.zeros((3, 1))))

    P_l = np.dot(K,  M_l)
    P_r = np.dot(K,  M_r)

    # undistort points
    p1 = p1[np.asarray(matchesMask)==1,:,:]
    p2 = p2[np.asarray(matchesMask)==1,:,:]
    p1_un = cv2.undistortPoints(p1,K,None)
    p2_un = cv2.undistortPoints(p2,K,None)
    p1_un = np.squeeze(p1_un)
    p2_un = np.squeeze(p2_un)

    #triangulate points this requires points in normalized coordinate
    point_4d_hom = cv2.triangulatePoints(P_l, P_r, p1_un.T, p2_un.T)
    point_3d = point_4d_hom / np.tile(point_4d_hom[-1, :], (4, 1))
    point_3d = point_3d[:3, :].T
    print('Number of 3D points:',len(point_3d))

    #############################
    #5----output 3D pointcloud--#
    #############################
    #TODO: Display 3D points
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    for x, y, z in point_3d:
        ax.scatter(x, y, z, c="g", marker="o")

    plt.show()
    fig.savefig('3-D_' + str(counter) + '.png')

    with open('points.csv','w',newline='') as file:
        writer = csv.writer(file)
        writer.writerow(point_3d[:,0])
        writer.writerow(point_3d[:,1])
        writer.writerow(point_3d[:,2])
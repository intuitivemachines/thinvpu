import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('RPOD_ORB.png',0)

# Initiate STAR detector
orb = cv.ORB_create()

# find the keypoints with ORB
kp = orb.detect(img,None)

print("Number of Key Points",len(kp))
print("Centroid X/Y of the first 3  Key Points:")
print(kp[0].pt)
print(kp[1].pt)
print(kp[2].pt)
# compute the descriptors with ORB
kp, des = orb.compute(img, kp)
print("Descriptor of the first 3  Key Points:")
print(des[0])
print(des[1])
print(des[2])


# draw only keypoints location,not size and orientation
img2 = cv.drawKeypoints(img,kp,color=(0,255,0), flags=0, outImage = None)
cv.imwrite('TC-RPOD-01-output.png', img2)

plt.imshow(img2),plt.show()
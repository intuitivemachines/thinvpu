import numpy as np
import cv2 as cv

# Load Image
img = cv.imread('pert95.png',0)
# Define average area of Superpixels
sp_size = 100
# Superpixels
super_pixels = cv.ximgproc.createSuperpixelSLIC(img.copy(),cv.ximgproc.SLICO,sp_size)
num = super_pixels.getNumberOfSuperpixels()
print("Number of super pixels = "+str(num))
none = super_pixels.iterate(10)
none = super_pixels.enforceLabelConnectivity(25)
sp_mask = super_pixels.getLabelContourMask(img.copy(), 1)
cv.imwrite('TC-PLHA-03-output.png', sp_mask)
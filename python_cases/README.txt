ThinVPU setup
1. Install dependencies for python3
    - Use the setup script only once to install needed libraries
    - run command: ./thinvpu_setup.sh

2. Run cases individually
    - Go into case directory (example: python_cases/tc_od_01)
    - run command: python3 case.py (example: python3 tc_od_01.py)

3. Interrogate output
    - Each case will output a processed image
    - Additional informaiton on the process can be found in the terminal

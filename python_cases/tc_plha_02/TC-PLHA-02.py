import cv2
import numpy as np

MIN_MATCH_COUNT = 10
features2detect = 2500

img_pairs = [("pert95.png", "pert97.png")]

counter = 0

for img_pair_1, img_pair_2 in img_pairs:
    counter += 1
    img1=cv2.imread(img_pair_1,0)
    img2=cv2.imread(img_pair_2,0)

       
    ###############################
    #1----ORB feature matching---#
    ###############################

    #detect orb features for both images
    orb = cv2.ORB_create(nfeatures = features2detect)
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)
    print('Key points img 1:',len(kp1))
    print('Key points img 2:',len(kp2))

	# create BFMatcher object
    bf = cv2.BFMatcher()
	# Match descriptors.
    matches = bf.knnMatch(des1,des2, k=2)


    print('Number of matches:',len(matches))

    #store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    if len(good)>MIN_MATCH_COUNT:
        p1 = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        p2 = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       flags = 2)

    img_orbmatch = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

    cv2.imwrite('orb_match_' + str(counter) + '.png',img_orbmatch)
    print('Lowes number of good matches:',len(good))
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
MIN_MATCH_COUNT = 10
features2detect = 2500
img1 = cv.imread('im1.png',0)   # queryImage
img2 = cv.imread('im2.png',0)   # trainImage
# Initiate ORB detector
orb = cv.ORB_create(nfeatures = features2detect)
# find the keypoints and descriptors with ORB
kp1, des1 = orb.detectAndCompute(img1,None)
kp2, des2 = orb.detectAndCompute(img2,None)

print('Key points img 1:',len(kp1))
print('Key points img 2:',len(kp2))

# create BFMatcher object
bf = cv.BFMatcher()
# Match descriptors.
matches = bf.knnMatch(des1,des2, k=2)

print('Number of matches:',len(matches))
# store all the good matches as per Lowe's ratio test.
good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
    M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    print(M)
    # output homography transformation
    np.savetxt("Homography_Trans.csv", M, delimiter=",")
else:
    print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
    matchesMask = None

draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       flags = 2)
img_orbmatch = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
cv.imwrite('orb_match_1.png',img_orbmatch)
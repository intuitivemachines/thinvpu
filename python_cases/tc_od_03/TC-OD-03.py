import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('OD_ORB.png',0)

# performs canny edge detection
edges = cv.Canny(cv.equalizeHist(img),100,200)
print('Number of canny edges:',len(edges))

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
cv.imwrite('TC-OD-03-output.png', edges)
plt.show()
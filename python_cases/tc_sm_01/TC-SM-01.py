import numpy as np
import cv2 as cv

# Load Image
img = cv.imread('SM01.png',0)
# Define average area of Superpixels
sp_size = 100
# Superpixels
super_pixels = cv.ximgproc.createSuperpixelSLIC(img.copy(),cv.ximgproc.SLICO,sp_size)
num = super_pixels.getNumberOfSuperpixels()
print("Number of super pixels = "+str(num))
none = super_pixels.iterate(10)
none = super_pixels.enforceLabelConnectivity(25)
sp_mask = super_pixels.getLabelContourMask(img.copy(), 1)
cv.imwrite('TC-SM-01-output.png', sp_mask)
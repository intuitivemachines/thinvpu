import cv2
import numpy as np

img = cv2.imread('atv_esa7.jpg',0)
img = cv2.medianBlur(img,5)
cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20, param1=255,param2=160,minRadius=1300,maxRadius=1800)
circles = np.uint16(np.around(circles))
for i in circles[0,:]:
	# draw the outer circle
	cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),30)
	# draw the center of the circle
	cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),40)
	print("Centroid XY ("+str(i[0])+", "+str(i[1])+")")

cv2.imwrite('TC-RPOD-03-output.png', cimg)
import cv2 as cv

# load image and template
img = cv.imread('tm1.png', cv.IMREAD_COLOR)
templ = cv.imread('tm2.png', cv.IMREAD_COLOR)

# Match template algorithms
SQD = cv.matchTemplate(img, templ, cv.TM_SQDIFF, None, None)
SQD_NORM = cv.matchTemplate(img, templ, cv.TM_SQDIFF_NORMED)
COR = cv.matchTemplate(img, templ, cv.TM_CCORR)
COR_NORM = cv.matchTemplate(img, templ, cv.TM_CCORR_NORMED, None, None)
COF = cv.matchTemplate(img, templ, cv.TM_CCOEFF)
COF_NORM = cv.matchTemplate(img, templ, cv.TM_CCOEFF_NORMED)
templates = [SQD, SQD_NORM, COR, COR_NORM, COF, COF_NORM]
algo_templ_name = ['SQDIFF', 'SQDIFF_NORMED', 'CCORR', 'CCORR_NORMED', 'CCOEFF', 'CCOEFF_NORMED']

for i, temp in enumerate(templates):
    # normalize
    cv.normalize(temp, temp, 0, 1, cv.NORM_MINMAX, -1)

    # best_match
    _minVal, _maxVal, minLoc, maxLoc = cv.minMaxLoc(temp, None)

    # match_loc 
    # TM_SQDIFF = 0 
    # TM_SQDIFF_NORMED = 1
    if (i == 0 or i == 1):
        matchLoc = minLoc
    else:
        matchLoc = maxLoc
    
    print(algo_templ_name[i], _minVal, _maxVal, minLoc, maxLoc)
    cv.rectangle(img, matchLoc, (matchLoc[0] + templ.shape[0], matchLoc[1] + templ.shape[1]), (0,255,0), 2, 8, 0 )
    cv.imwrite('TC-RPOD-04-output_'+str(i)+'_'+str(algo_templ_name[i])+'.png',img)

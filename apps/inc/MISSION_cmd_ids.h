/*=======================================================================================
** File Name:  MISSION_cmd_ids.h
**
** Title:  Command Message ID Header File for ThinVPU
**
** $Author:    Auto-generated by Python script
** $Revision:  $
** $Date:      2015-01-06
**
** Purpose:  This header file contains command message Ids defined for all CFS applications
**           built for the mission.
**
**=====================================================================================*/

#ifndef _MISSION_CMD_IDS_H_
#define _MISSION_CMD_IDS_H_

/*
** Notes:
**     The following value ranges are available for project use of command IDs:
**         0x1822 - 0x185F, 0x1870 - 0x188F, 0x18C0 - 0x18FF
**
**     Do not use other values outside these range.
**     They have been used by CFE and the built-in CFS applications.
*/

#include "cfe_sb.h"


typedef struct
{
    int MsgId;
    int PipeRate;
    char PipeName[30];
    CFE_SB_PipeId_t PipeId;
} MISSION_app_pipes_T;


#define APP_GET_Q_RATE_DEPTH(pipe_rate,app_rate) \
        (((int)(pipe_rate/app_rate))+2)


/* Default MID's for common app rates. */

#define SCH_1HZ_WAKEUP_MID         0x1850
#define SCH_5HZ_WAKEUP_MID         0x1851
#define SCH_10HZ_WAKEUP_MID        0x1852
#define SCH_25HZ_WAKEUP_MID        0x1853
#define SCH_50HZ1_WAKEUP_MID       0x1854
#define SCH_50HZ2_WAKEUP_MID       0x1855
#define SCH_100HZ_WAKEUP_MID       0x1856
#define SCH_1HZ_SEND_HK_MID        0x1857
#define SCH_5HZ_SEND_HK_MID        0x1858


/* Placeholder MID's for manual app scheduling */
/*     vs. default prioritized rate groups.    */

#define TC_XXXX_01_CMD_MID        0x1940
#define TC_XXXX_01_WAKEUP_MID     0x1941
#define TC_XXXX_01_CMD_MID        0x1942

#define TC_RPOD_01_CMD_MID        0x1943
#define TC_RPOD_01_WAKEUP_MID     0x1944
#define TC_RPOD_01_CMD_MID        0x1945

#define TC_RPOD_02_CMD_MID        0x1946
#define TC_RPOD_02_WAKEUP_MID     0x1947
#define TC_RPOD_02_CMD_MID        0x1948

#define TC_RPOD_03_CMD_MID        0x1949
#define TC_RPOD_03_WAKEUP_MID     0x194A
#define TC_RPOD_03_CMD_MID        0x194B

#define TC_RPOD_04_CMD_MID        0x194C
#define TC_RPOD_04_WAKEUP_MID     0x194D
#define TC_RPOD_04_CMD_MID        0x194E

#define TC_OD_01_A_CMD_MID        0x1950  
#define TC_OD_01_A_WAKEUP_MID     0x1951  
#define TC_OD_01_A_CMD_MID        0x1952  

#define TC_OD_01_B_CMD_MID        0x1953
#define TC_OD_01_B_WAKEUP_MID     0x1954
#define TC_OD_01_B_CMD_MID        0x1955

#define TC_OD_02_CMD_MID          0x1956
#define TC_OD_02_WAKEUP_MID       0x1957
#define TC_OD_02_CMD_MID          0x1958

#define TC_OD_03_CMD_MID          0x1959
#define TC_OD_03_WAKEUP_MID       0x195A
#define TC_OD_03_CMD_MID          0x195B

#define TC_OD_04_CMD_MID          0x195C
#define TC_OD_04_WAKEUP_MID       0x195D
#define TC_OD_04_CMD_MID          0x195E

#define TC_PLHA_01_CMD_MID        0x1960
#define TC_PLHA_01_WAKEUP_MID     0x1961
#define TC_PLHA_01_CMD_MID        0x1962

#define TC_PLHA_02_CMD_MID        0x1963
#define TC_PLHA_02_WAKEUP_MID     0x1964
#define TC_PLHA_02_CMD_MID        0x1965

#define TC_PLHA_03_CMD_MID        0x1966
#define TC_PLHA_03_WAKEUP_MID     0x1967
#define TC_PLHA_03_CMD_MID        0x1968

#define TC_PLHA_04_CMD_MID        0x1969
#define TC_PLHA_04_WAKEUP_MID     0x196A
#define TC_PLHA_04_CMD_MID        0x196B

#define TC_SM_01_CMD_MID          0x196C
#define TC_SM_01_WAKEUP_MID       0x196D
#define TC_SM_01_CMD_MID          0x196E

#define TC_SM_02_CMD_MID          0x1970
#define TC_SM_02_WAKEUP_MID       0x1971
#define TC_SM_02_CMD_MID          0x1972

#define TC_SM_03_CMD_MID          0x1973
#define TC_SM_03_WAKEUP_MID       0x1974
#define TC_SM_03_CMD_MID          0x1975

#define TC_SM_04_CMD_MID          0x1976
#define TC_SM_04_WAKEUP_MID       0x1977
#define TC_SM_04_CMD_MID          0x1978

#define TCM_SEND_HK_MID           0x19C6
#define TCM_WAKEUP_MID            0x19C7
#define TCM_CMD_MID               0x19C8

#endif /* _MISSION_CMD_IDS_H_ */

/*=======================================================================================
** End of file MISSION_cmd_ids.h
**=====================================================================================*/


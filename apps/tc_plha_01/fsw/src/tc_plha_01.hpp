#ifndef __TC_PLHA_01CLASS_H__
#define __TC_PLHA_01CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_plha_01_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_plha_01
{
public:
  cv::Mat image;                        // Orignal Image
  int load_img(const char*);            // Load image
  int orb_detector(TC_PLHA_01_OUT_T *); // Orb detection on loaded Image
  virtual ~tc_plha_01();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_PLHA_01CLASS_H__ */

    
/* ---------- end of tc_plha_01_class.hpp ---------- */
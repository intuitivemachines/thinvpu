#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_plha_01_cv.hpp"
#include "tc_plha_01.hpp"
#include "tc_plha_01_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_plha_01_driver(TC_PLHA_01_OUT_T *tc_plha_01_out_ptr,const char *fileString)
{
    tc_plha_01 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.orb_detector(tc_plha_01_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
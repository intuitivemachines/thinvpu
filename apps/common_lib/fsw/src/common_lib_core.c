#include "common_lib_core.h"
#include "common/utils/logger.h"
#include "osapi.h"

int32 Common_LibInit( void )
{
    
    LOG_INFO( "Common Lib Initialized.  Version %d.%d.%d.%d",
    	       COMMON_LIB_MAJOR_VERSION,
    		   COMMON_LIB_MINOR_VERSION,
    		   COMMON_LIB_REVISION,
               COMMON_LIB_MISSION_REV );

    return OS_SUCCESS;
 
}  /* End Common_LibInit */
/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 * 
 * @file
 * @author James Blakeslee <james@intuitivemachines.com>
 * 
 * @section DESCRIPTION
 * Logging infrastructure for various platform implementations
 * 
 * Selected at compile-time by the following definitions, this logging 
 * framework provides three distinct logging implementations:
 * 
 * LOGGER_CONSOLE : printf-based implementation, intended for stand-alone
 *                  execution w/ minimal dependencies (i.e. unit testing)
 *
 * LOGGER_CFE     : Leverages cFE APIs for Executive and Event Services
 *                  reporting.  Intended for CFS/Linux deployments.
 *
 * LOGGER_VXWORKS : (*FUTURE*) Leverages VxWorks OS-level logging infrastructure.
 *****************************************************************************/

#include "common/utils/logger.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "osapi.h"
#include "cfe_evs.h"
#include "cfe_time.h"
#include "common_types.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

/* character-array representation of levels for fast conversion */
const char LOGGER_LEVEL_STRING[NUM_LOGGER_MESSAGE_LEVELS][8] =
{
    "TRACE",    /* TRACE */
    "DEBUG",    /* DEBUG */
    "INFO",     /* INFO */
    "WARNING",  /* WARNING */
    "ERROR",    /* ERROR */
    "SEVERE",   /* SEVERE */
    "FATAL"     /* FATAL */
};

/* map IM logging levels to more limited cFE supported levels */
const short CFE_LEVEL_MAPPING[NUM_LOGGER_MESSAGE_LEVELS] =
{
    CFE_EVS_DEBUG,        /* TRACE */
    CFE_EVS_DEBUG,        /* DEBUG */
    CFE_EVS_INFORMATION,  /* INFO */
    CFE_EVS_INFORMATION,  /* WARNING */
    CFE_EVS_ERROR,        /* ERROR */
    CFE_EVS_ERROR,        /* SEVERE */
    CFE_EVS_CRITICAL      /* FATAL */
};

/* local function prototypes */
void logger_osprintf( const LOGGER_LEVEL level, const uint16 eid, const char* file, const char* func, const uint16 line, const char* msg );

/* conditional implementation of logger based upon compiler definition */
#if LOGGER_IMPL == LOGGER_CFE

/**
 * cFE logger which leverages cFE Executive and Event Services
 * 
 * @param level severity level of the message to log
 * @param eid error identifier for the message, uint16 type limits EID to 65,536 max
 * @param use_eid flag indicating if EID was provided by the client
 * @param file source file from which the message originated
 * @param func function from which the message originated
 * @param line line number in source file from which the message originated, uint16 type limits source file length to 65,536 lines
 * @param format C-style format specifier string of the message
 * @param va_args optional and variable number of supplementary arguments corresponding to each format specifier
 */
void logger_message_impl( const LOGGER_LEVEL level, const uint16 eid, const osalbool use_eid, const char* file, const char* func, const uint16 line, const char* format, ... )
{
    LOGGER_LEVEL __level = level;
    va_list      argptr;
    char         msg_buffer[OS_BUFFER_SIZE];
    char         log_buffer[OS_BUFFER_SIZE];
    osalbool     is_logged = FALSE;

    /* protect subsequent label lookup against out of bounds levels */
    if ( level < LOGGER_LEVEL_TRACE )
        __level = LOGGER_LEVEL_TRACE;
    else if ( level > LOGGER_LEVEL_FATAL )
        __level = LOGGER_LEVEL_FATAL;

    va_start( argptr, format );
    vsnprintf( msg_buffer, OS_BUFFER_SIZE, format, argptr );
    va_end( argptr) ;

    /* create entry */
    snprintf( 
              log_buffer,
              OS_BUFFER_SIZE,                 /* truncate if necessary to prevent overruns */
              LOGGER_CFE_ENTRY_FORMAT,        /* cFE log entry format */
              LOGGER_LEVEL_STRING[__level],   /* level string lookup */
              eid,                            /* pass-through eid from argument */
              file,                           /* pass-through file name from argument */
              func,                           /* pass-through function name from argument */
              line,                           /* pass-through line number from argument */
              msg_buffer                      /* append message portion */
    );

    /* enforce newline on truncated messages */
    log_buffer[OS_BUFFER_SIZE-2] = LOGGER_NEWLINE;

    if ( ( use_eid == TRUE ) && ( __level >= LOGGER_CFE_EVENT_THRESHOLD ) )
    {
        is_logged = TRUE;
        /* EID was supplied and this message is above the event threshold, publish message portion only to cFE EVS */
        CFE_EVS_SendEvent( eid, CFE_LEVEL_MAPPING[__level], msg_buffer );
    }

    if ( __level >= LOGGER_CFE_SYSLOG_THRESHOLD )
    {
        is_logged = TRUE;
        /* Above the severity threshold, write to the cFE ES System Log */
        CFE_ES_WriteToSysLog( log_buffer );
    }

    if ( ( ( LOGGER_CFE_PRINTF_FALLTHRU == TRUE ) && ( is_logged == FALSE ) ) || ( LOGGER_CFE_FORCE_PRINTF == TRUE ) )
        /* if configured, fallback to an OSAL compliant printf to registered consoles for entries which weren't previously logged */
        logger_osprintf( __level, eid, file, func, line, msg_buffer );
}

#else
#if LOGGER_IMPL == LOGGER_VXWORKS

/* TODO: placeholder for VxWorks system logger */

/* TODO: not yet implemented, compilation will fail */

/* TODO: VxWorks may not have all C pre-processor definitions available (especially __FUNCTION__, need to investigate) */

#else

/**
 * Direct to console logger
 * 
 * @param level severity level of the message to log
 * @param eid error identifier for the message, uint16 type limits EID to 65,536 max
 * @param use_eid flag indicating if EID was provided by the client
 * @param file source file from which the message originated
 * @param func function from which the message originated
 * @param line line number in source file from which the message originated, uint16 type limits source file length to 64K lines
 * @param format C-style format specifier string of the message
 * @param va_args optional and variable number of supplementary arguments corresponding to each format specifier
 */
void logger_message_impl( const LOGGER_LEVEL level, const uint16 eid, const osalbool use_eid, const char* file, const char* func, const uint16 line, const char* format, ... )
{
    LOGGER_LEVEL __level = level;
    va_list      argptr;
    char         msg_buffer[OS_BUFFER_SIZE];

    /* protect subsequent label lookup against out of bounds levels */
    if ( level < LOGGER_LEVEL_TRACE )
        __level = LOGGER_LEVEL_TRACE;
    else if ( level > LOGGER_LEVEL_FATAL )
        __level = LOGGER_LEVEL_FATAL;

    /* create the message portion */
    va_start( argptr, format );
    vsnprintf( msg_buffer, OS_BUFFER_SIZE, format, argptr );
    va_end( argptr );

    logger_osprintf( __level, eid, file, func, line, msg_buffer );
}

#endif

#endif

/**
 * Logs via OSAL OS_printf command
 * 
 * @param level severity level of the message to log
 * @param eid error identifier for the message, uint16 type limits EID to 65,536 max
 * @param file source file from which the message originated
 * @param func function from which the message originated
 * @param line line number in source file from which the message originated, uint16 type limits source file length to 64K lines
 * @param msg message portion of the log entry
 */
void logger_osprintf( const LOGGER_LEVEL level, const uint16 eid, const char* file, const char* func, const uint16 line, const char* msg )
{
    char log_buffer[OS_BUFFER_SIZE];
    char datetime_str[LOGGER_CFE_TIME_PRINT_SIZE];
    CFE_TIME_SysTime_t curr_time = CFE_TIME_GetTime();
    CFE_TIME_Print( datetime_str, curr_time ); 

    /* create entry including timestamp portion */
    snprintf(
              log_buffer,
              OS_BUFFER_SIZE,                 /* truncate if necessary to prevent overruns */
              LOGGER_CONSOLE_ENTRY_FORMAT,    /* console log entry format */
              datetime_str,                   /* time string printed via cFE */
              LOGGER_LEVEL_STRING[level],     /* level string lookup */
              eid,                            /* pass-through eid from argument */
              file,                           /* pass-through file name from argument */
              func,                           /* pass-through function name from argument */
              line,                           /* pass-through line number from argument */
              msg                             /* append message portion */
    );

    /* enforce newline on truncated messages */
    log_buffer[OS_BUFFER_SIZE-2] = LOGGER_NEWLINE;

    /* log message to console via OSAL */
    OS_printf( "%s", log_buffer );
}

/* intentionally no fallthrough else case.  If no logger implemenation is selected, compilation will fail */

#ifdef __cplusplus
}
#endif
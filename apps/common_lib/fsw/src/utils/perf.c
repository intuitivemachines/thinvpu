/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 * 
 * @file
 * @author Brian Butcher <bbutcher@intuitivemachines.com>
 * 
 * @section DESCRIPTION
 * Performance logging utility functions
 *****************************************************************************/

#include "common/utils/perf.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"

/**
 * Calculates elapsed and rate time of execution 
 * 
 * @param dEnterTime Application wakeup timestamp
 * @param pPrevExecTime Pointer to timestamp from previous cycle
 * @param pRate Pointer to rate time 
 * @param pElapsed Pointer to elapsed
 */
void perf_calc_times( double dEnterTime, double* pPrevExecTime, float* pRate, float* pElapsed)
{
    ERROR_NULL_CHK( pPrevExecTime );
    ERROR_NULL_CHK( pRate );
    ERROR_NULL_CHK( pElapsed );
    
    // Local variables
    int32 rv;
    double  dExitTime  = 0.0;

    rv = perf_get_time_seconds(&dExitTime);
    ERROR_RV_CSTM_CHK_RTRN(rv, PT_GT_ERR);

    rv = perf_calc_rate(dEnterTime, 
                        *pPrevExecTime, 
                        pRate 
                       );
    ERROR_RV_CSTM_CHK_RTRN(rv, PT_CALC_ERR);

    rv = perf_calc_elapsed(dEnterTime, dExitTime, pElapsed);
    ERROR_RV_CSTM_CHK_RTRN(rv, PT_ELPSD_ERR);

    rv = perf_set_prev_time(dEnterTime, pPrevExecTime);
    ERROR_RV_CSTM_CHK_RTRN(rv, PT_ST_ERR);
}

/**
 * Calculates current time in seconds format
 * 
 * @param pTimeTag Application timestamp to store time into
 */
PERF_TIME_STATUS_E perf_get_time_seconds(double* pTimeTag)
{
    ERROR_NULL_CHK( pTimeTag );

    CFE_TIME_SysTime_t cfeTime;
    double tempTime;
    cfeTime = CFE_TIME_GetTime();
    tempTime = cfeTime.Seconds +
                (double)  CFE_TIME_Sub2MicroSecs( cfeTime.Subseconds) / 1000000.0;
    if(tempTime > 0.0)
    {
        *pTimeTag = tempTime;
        return PT_SUCCESS;
    }
    else
    {
        return PT_GT_ERR;
    }
}

/**
 * Sets previous execution time
 * 
 * @param dEnterTime Application wakeup timestamp
 * @param pPrevExecTime Pointer to timestamp from previous cycle
 */
PERF_TIME_STATUS_E perf_set_prev_time(double dEnterTime, double* pPrevExecTime)
{
    ERROR_NULL_CHK( pPrevExecTime );


    if( dEnterTime > 0.0 )
    {
        *pPrevExecTime = dEnterTime;
        return PT_SUCCESS;
    }
    else
    {
        return PT_ST_ERR;
    }
}

/**
 * Calculates rate time of execution 
 * 
 * @param dEnterTime Application wakeup timestamp
 * @param dPrevExecTime Timestamp from previous cycle
 * @param pRate Pointer to rate time 
 */
PERF_TIME_STATUS_E perf_calc_rate(double dEnterTime, double pPrevExecTime, float* pRate)
{
    ERROR_NULL_CHK( pRate );   

    if( (pPrevExecTime > 0.0) && (dEnterTime > 0.0) )
    {
        *pRate = (float) (dEnterTime - pPrevExecTime);
    }
    return PT_SUCCESS;                 
}

/**
 * Calculates elapsed time of execution 
 * 
 * @param dEnterTime Application wakeup timestamp
 * @param dExitTime Timestamp from end of exec process
 * @param pElapsed Pointer to elapsed time 
 */
PERF_TIME_STATUS_E perf_calc_elapsed(double dEnterTime, double dExitTime, float* pElapsed)
{
    ERROR_NULL_CHK( pElapsed );

    if( (dExitTime > 0.0)  && (dEnterTime > 0.0) )
    {
        *pElapsed = (float) (dExitTime - dEnterTime);
        return PT_SUCCESS;                 
    }
    else
    {
        return PT_ELPSD_ERR;
    }
    
}
#include "mock_logger_cfe.h"

// mocked cFE functions for logger testing

#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

void CFE_TIME_Print( char* str, CFE_TIME_SysTime_t ignored )
{
    // yyyy-ddd-hh:mm:ss.xxxxx\0
    time_t raw = time( NULL );
    struct tm *current = localtime( &raw );
    struct timeval tv;

    gettimeofday( &tv, NULL );

    sprintf(
        str, 
        "%4d-%03d-%02d:%02d:%02d.%05d",
        current->tm_year + 1900,
        current->tm_yday,
        current->tm_hour,
        current->tm_min,
        current->tm_sec,
        (int)tv.tv_usec
    );
}

CFE_TIME_SysTime_t CFE_TIME_GetTime( void )
{
    struct timeval tv;
    gettimeofday( &tv, NULL );
    
    CFE_TIME_SysTime_t time;
    time.Seconds = tv.tv_sec;
    time.Subseconds = tv.tv_usec;
    return time;
}

uint32 CFE_TIME_Sub2MicroSecs(uint32 SubSeconds)
{
    uint32 MicroSeconds;
	
    /* 0xffffdf00 subseconds = 999999 microseconds, so anything greater 
     * than that we set to 999999 microseconds, so it doesn't get to
     * a million microseconds */
    
	if (SubSeconds > 0xffffdf00)
	{
			MicroSeconds = 999999;
	}
    else
    {
        /*
        **  Convert a 1/2^32 clock tick count to a microseconds count
        **
        **  Conversion factor is  ( ( 2 ** -32 ) / ( 10 ** -6 ) ).
        **
        **  Logic is as follows:
        **    x * ( ( 2 ** -32 ) / ( 10 ** -6 ) )
        **  = x * ( ( 10 ** 6  ) / (  2 ** 32 ) )
        **  = x * ( ( 5 ** 6 ) ( 2 ** 6 ) / ( 2 ** 26 ) ( 2 ** 6) )
        **  = x * ( ( 5 ** 6 ) / ( 2 ** 26 ) )
        **  = x * ( ( 5 ** 3 ) ( 5 ** 3 ) / ( 2 ** 7 ) ( 2 ** 7 ) (2 ** 12) )
        **
        **  C code equivalent:
        **  = ( ( ( ( ( x >> 7) * 125) >> 7) * 125) >> 12 )
        */   

    	MicroSeconds = (((((SubSeconds >> 7) * 125) >> 7) * 125) >> 12);
    

        /* if the Subseconds % 0x4000000 != 0 then we will need to
         * add 1 to the result. the & is a faster way of doing the % */  
	    if ((SubSeconds & 0x3ffffff) != 0)
    	{
	    	MicroSeconds++;
    	}
    
        /* In the Micro2SubSecs conversion, we added an extra anomaly
         * to get the subseconds to bump up against the end point,
         * 0xFFFFF000. This must be accounted for here. Since we bumped
         * at the half way mark, we must "unbump" at the same mark 
         */
        if (MicroSeconds > 500000)
        {
            MicroSeconds --;
        }
        
    } /* end else */
    
    return(MicroSeconds);
}
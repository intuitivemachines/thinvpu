// mocked cFE elements which are depended upon by the logging implementations

#ifndef __MOCK_LOGGER_CFE
#define __MOCK_LOGGER_CFE

#define CFE_EVS_DEBUG            0x01
#define CFE_EVS_INFORMATION      0x02
#define CFE_EVS_ERROR            0x03
#define CFE_EVS_CRITICAL         0x04

typedef struct
{
  int  Seconds;
  int  Subseconds;
} CFE_TIME_SysTime_t;

typedef unsigned char                         osalbool;
typedef signed char                           int8;
typedef short int                             int16;
typedef int                                   int32;
typedef long int                              int64;
typedef unsigned char                         uint8;
typedef unsigned short int                    uint16;
typedef unsigned int                          uint32;
typedef unsigned long int                     uint64;

#define TRUE   1
#define FALSE  0

#define OS_BUFFER_SIZE  172

#define OS_SUCCESS  0

/* mocked functions */
void CFE_TIME_Print( char*, CFE_TIME_SysTime_t );
CFE_TIME_SysTime_t CFE_TIME_GetTime( void );
uint32 CFE_TIME_Sub2MicroSecs(uint32 SubSeconds);

#endif
/******************************************************************************
 * Filename: byteswap.h
 * Author: James Blakeslee
 * Copyright � 2019, Intuitive Machines, LLC
 *
 * Header for float/double byte-swapping routines
 *****************************************************************************/
#ifndef BYTESWAP_H_
#define BYTESWAP_H_

/* function prototypes */
float htonf( const float host_val );
float ntohf( const float network_val );

double htond( double host_val );
double ntohd( double network_val );

#endif /* BYTESWAP_H_ */

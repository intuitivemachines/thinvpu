/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 *
 * @file
 * @author James Blakeslee <james@intuitivemachines.com>
 *
 * @section DESCRIPTION
 * Header for miscellaneous utility functions
 *****************************************************************************/

#ifndef MISC_H_
#define MISC_H_

#include <stddef.h>

int convert_to_hex_string( char* dest, const size_t dest_len, const unsigned char *buf, const size_t buf_len );

#endif  /* MISC_H_ */

/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 *
 * @file
 * @author Brian Butcher <bbutcher@intuitivemachines.com>
 *
 * @section DESCRIPTION
 * Header for performance utility functions
 *****************************************************************************/

#ifndef PERF_H_
#define PERF_H_

#include "cfe_time.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"

typedef enum
{
    PT_SUCCESS      =  0,
    PT_GT_ERR       = -1,
    PT_RATE_ERR     = -2,
    PT_ELPSD_ERR    = -3,
    PT_ST_ERR       = -4,
    PT_CALC_ERR     = -5
} PERF_TIME_STATUS_E;

void perf_calc_times( double dEnterTime, double* pPrevExecTime, float* pRate, float* pElapsed);

PERF_TIME_STATUS_E perf_get_time_seconds( double* pTimeTag );
PERF_TIME_STATUS_E perf_set_prev_time(double pEnterTime, double* pPrevExecTime);
PERF_TIME_STATUS_E perf_calc_rate( double dEnterTime, double pPrevExecTime, float* pRate);
PERF_TIME_STATUS_E perf_calc_elapsed(double dEnterTime, double dExitTime, float* pElapsed);

#endif  /* PERF_H_ */

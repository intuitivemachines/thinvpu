/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 *
 * @file
 * @author James Blakeslee <james@intuitivemachines.com>
 *
 * @section DESCRIPTION
 * Header for level-based logging infrastructure
 * 
 * @section API
 * Logging API is as follows:
 * 
 *   All arguments support a C-style format specifier string with an optional
 *   and variable set of supplemental arguments corresponding to format
 *   specifiers.  Following API calls are listed in decreasing (most to least)
 *   severity level of the message:
 * 
 *     LOG_FATAL( char* format, ... );    // log a message at fatal level
 *     LOG_SEVERE( char* format, ... );   // log a message at severe level
 *     LOG_ERROR( char* format, ... );    // log a message at error level
 *     LOG_WARNING( char* format, ... );  // log a message at warning level
 *     LOG_INFO( char* format, ... );     // log a message at info level
 *     LOG_DEBUG( char* format, ... );    // log a message at debug level
 *     LOG_TRACE( char* format, ... );    // log a message at trace level
 * 
 *     LOG_EID_FATAL( int err_id, char* format, ... );    // log a message w/ an event id at fatal level
 *     LOG_EID_SEVERE( int err_id, char* format, ... );   // log a message w/ an event id at severe level
 *     LOG_EID_ERROR( int err_id, char* format, ... );    // log a message w/ an event id at error level
 *     LOG_EID_WARNING( int err_id, char* format, ... );  // log a message w/ an event id at warning level
 *     LOG_EID_INFO( int err_id, char* format, ... );     // log a message w/ an event id at info level
 *     LOG_EID_DEBUG( int err_id, char* format, ... );    // log a message w/ an event id at debug level
 *     LOG_EID_TRACE( int err_id, char* format, ... );    // log a message w/ an event id at trace level
 * 
 * @section CONFIGURATION
 * Logging filtering levels is set at compile-time via #define LOGGER_MESSAGE_FILTER
 *****************************************************************************/

#ifndef LOGGER_H_
#define LOGGER_H_

#include "common_types.h"

/* filtering levels avaialble for compile-time selection */
#define LVL_TRACE    0
#define LVL_DEBUG    1
#define LVL_INFO     2
#define LVL_WARNING  3
#define LVL_ERROR    4
#define LVL_SEVERE   5
#define LVL_FATAL    6

/* available logging implementations */
#define LOGGER_CFE      0
#define LOGGER_VXWORKS  1
#define LOGGER_CONSOLE  2

/* default logging implementation to the console logger */
#ifndef LOGGER_IMPL
#define LOGGER_IMPL                  LOGGER_CONSOLE
#endif

/* maximum level of compiled-in logging messages */
#ifndef LOGGER_MESSAGE_FILTER
#define LOGGER_MESSAGE_FILTER        LVL_TRACE   /* default level filter to all messages (TRACE) */
#endif

/* specific to cFE logging implementations */
#define LOGGER_CFE_EVENT_THRESHOLD   LVL_INFO    /* messages at this level or higher in severity will be sent to cFE EVS */
#define LOGGER_CFE_SYSLOG_THRESHOLD  LVL_SEVERE  /* messages at this level or higher in severity will be sent to cFE ES System Log */
#define LOGGER_CFE_PRINTF_FALLTHRU   TRUE        /* toggles whether messages that don't get sent to EVS or ES are printed to the console */
#define LOGGER_CFE_FORCE_PRINTF      FALSE       /* forces messages that are sent to EVS or ES to also be printed to the console */
#define LOGGER_CFE_DEFAULT_EID       0           /* used for messages sent to EVS w/o supplying an EID */

/* no-op */
#define LOGGER_NO_OP (void)0;

/* extract source file from full path of __FILE__ absolute reference */
#include <string.h>

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

#define __FILENAME__ ( strrchr( __FILE__, '/' ) ? strrchr( __FILE__, '/' ) + 1 : __FILE__ )

#define LOGGER_MESSAGE( __LEVEL__, ... )                logger_message_impl( __LEVEL__, LOGGER_CFE_DEFAULT_EID, FALSE, __FILENAME__, __FUNCTION__, __LINE__, __VA_ARGS__ )
#define LOGGER_EID_MESSAGE( __LEVEL__, __CODE__, ... )  logger_message_impl( __LEVEL__, __CODE__, TRUE, __FILENAME__, __FUNCTION__, __LINE__, __VA_ARGS__ )

/* avoid processor overhead by only compiling in logging handlers at or below the threshold threshold */

/* log all messages */
#if LOGGER_MESSAGE_FILTER == LVL_TRACE
#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_ERROR, __VA_ARGS__ )
#define LOG_WARNING( ... )                LOGGER_MESSAGE( LOGGER_LEVEL_WARNING, __VA_ARGS__ )
#define LOG_INFO( ... )                   LOGGER_MESSAGE( LOGGER_LEVEL_INFO, __VA_ARGS__ )
#define LOG_DEBUG( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_DEBUG, __VA_ARGS__ )
#define LOG_TRACE( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_TRACE, __VA_ARGS__ )

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_ERROR, __CODE__, __VA_ARGS__ )
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_EID_MESSAGE( LOGGER_LEVEL_WARNING, __CODE__, __VA_ARGS__ )
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_EID_MESSAGE( LOGGER_LEVEL_INFO, __CODE__, __VA_ARGS__ )
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_DEBUG, __CODE__, __VA_ARGS__ )
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_TRACE, __CODE__, __VA_ARGS__ )

/* log FATAL, SEVERE, ERROR, WARNING, INFO, DEBUG messages */
#elif LOGGER_MESSAGE_FILTER == LVL_DEBUG

#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_ERROR, __VA_ARGS__ )
#define LOG_WARNING( ... )                LOGGER_MESSAGE( LOGGER_LEVEL_WARNING, __VA_ARGS__ )
#define LOG_INFO( ... )                   LOGGER_MESSAGE( LOGGER_LEVEL_INFO, __VA_ARGS__ )
#define LOG_DEBUG( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_DEBUG, __VA_ARGS__ )
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_ERROR, __CODE__, __VA_ARGS__ )
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_EID_MESSAGE( LOGGER_LEVEL_WARNING, __CODE__, __VA_ARGS__ )
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_EID_MESSAGE( LOGGER_LEVEL_INFO, __CODE__, __VA_ARGS__ )
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_DEBUG, __CODE__, __VA_ARGS__ )
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP

/* log FATAL, SEVERE, ERROR, WARNING, INFO messages */
#elif LOGGER_MESSAGE_FILTER == LVL_INFO
#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_ERROR, __VA_ARGS__ )
#define LOG_WARNING( ... )                LOGGER_MESSAGE( LOGGER_LEVEL_WARNING, __VA_ARGS__ )
#define LOG_INFO( ... )                   LOGGER_MESSAGE( LOGGER_LEVEL_INFO, __VA_ARGS__ )
#define LOG_DEBUG( ... )                  LOGGER_NO_OP
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_ERROR, __CODE__, __VA_ARGS__ )
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_EID_MESSAGE( LOGGER_LEVEL_WARNING, __CODE__, __VA_ARGS__ )
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_EID_MESSAGE( LOGGER_LEVEL_INFO, __CODE__, __VA_ARGS__ )
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP


/* log FATAL, SEVERE, ERROR, WARNING messages */
#elif LOGGER_MESSAGE_FILTER == LVL_WARNING
#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_ERROR, __VA_ARGS__ )
#define LOG_WARNING( ... )                LOGGER_MESSAGE( LOGGER_LEVEL_WARNING, __VA_ARGS__ )
#define LOG_INFO( ... )                   LOGGER_NO_OP
#define LOG_DEBUG( ... )                  LOGGER_NO_OP
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_ERROR, __CODE__, __VA_ARGS__ )
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_EID_MESSAGE( LOGGER_LEVEL_WARNING, __CODE__, __VA_ARGS__ )
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_NO_OP
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP

/* log FATAL, SEVERE, ERROR messages */
#elif LOGGER_MESSAGE_FILTER == LVL_ERROR

#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_ERROR, __VA_ARGS__ )
#define LOG_WARNING( ... )                LOGGER_NO_OP
#define LOG_INFO( ... )                   LOGGER_NO_OP
#define LOG_DEBUG( ... )                  LOGGER_NO_OP
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_ERROR, __CODE__, __VA_ARGS__ )
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_NO_OP
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_NO_OP
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP

/* log FATAL, SEVERE messages */
#elif LOGGER_MESSAGE_FILTER == LVL_SEVERE 

#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_MESSAGE( LOGGER_LEVEL_SEVERE, __VA_ARGS__ )
#define LOG_ERROR( ... )                  LOGGER_NO_OP
#define LOG_WARNING( ... )                LOGGER_NO_OP
#define LOG_INFO( ... )                   LOGGER_NO_OP
#define LOG_DEBUG( ... )                  LOGGER_NO_OP
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_EID_MESSAGE( LOGGER_LEVEL_SEVERE, __CODE__, __VA_ARGS__ )
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_NO_OP
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_NO_OP
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP

/* only log FATAL messages */
#else

#define LOG_FATAL( ... )                  LOGGER_MESSAGE( LOGGER_LEVEL_FATAL, __VA_ARGS__ )
#define LOG_SEVERE( ... )                 LOGGER_NO_OP
#define LOG_ERROR( ... )                  LOGGER_NO_OP
#define LOG_WARNING( ... )                LOGGER_NO_OP
#define LOG_INFO( ... )                   LOGGER_NO_OP
#define LOG_DEBUG( ... )                  LOGGER_NO_OP
#define LOG_TRACE( ... )                  LOGGER_NO_OP

#define LOG_EID_FATAL( __CODE__, ... )    LOGGER_EID_MESSAGE( LOGGER_LEVEL_FATAL, __CODE__, __VA_ARGS__ )
#define LOG_EID_SEVERE( __CODE__, ... )   LOGGER_NO_OP
#define LOG_EID_ERROR( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_WARNING( __CODE__, ... )  LOGGER_NO_OP
#define LOG_EID_INFO( __CODE__, ... )     LOGGER_NO_OP
#define LOG_EID_DEBUG( __CODE__, ... )    LOGGER_NO_OP
#define LOG_EID_TRACE( __CODE__, ... )    LOGGER_NO_OP

#endif

typedef enum
{
    LOGGER_LEVEL_TRACE   = 0,

    LOGGER_LEVEL_DEBUG   = 1,

    LOGGER_LEVEL_INFO    = 2,

    LOGGER_LEVEL_WARNING = 3,

    LOGGER_LEVEL_ERROR   = 4,

    LOGGER_LEVEL_SEVERE  = 5,

    LOGGER_LEVEL_FATAL   = 6,

    NUM_LOGGER_MESSAGE_LEVELS

} LOGGER_LEVEL;

/* function prototypes */
void logger_message_impl( const LOGGER_LEVEL level, const uint16 eid, const osalbool use_eid, const char* file, const char* func, const uint16 line, const char* format, ... );

/* constants */

/* include timestamp, packs EID and line tight, but due to typing, max width is 5 (65536 value) and 5 (65536 value) respectively */
#define LOGGER_CONSOLE_ENTRY_FORMAT  "%.24s : %.8s : %1d : %.24s : %.64s() : %1d : %s\n"

#define LOGGER_CFE_ENTRY_FORMAT      ": %.8s : %1d : %.24s : %.64s() : %1d : %s\n"  /* no timestamp, cFE includes timestamp in ES Sys Log API */
#define LOGGER_CFE_TIME_PRINT_SIZE   25

#define LOGGER_NEWLINE               '\n'

#ifdef __cplusplus
}
#endif

#endif  /* LOGGER_H_ */

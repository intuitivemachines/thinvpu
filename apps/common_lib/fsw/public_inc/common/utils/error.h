/******************************************************************************
 * Copyright © 2019, Intuitive Machines, LLC
 *
 * @file
 * @author James Blakeslee <james@intuitivemachines.com>
 *
 * @section DESCRIPTION
 * Error checking routines
 * 
 * @section API
 * Error checking API is as follows:
 * 
 * // null pointer checks
 * ERROR_NULL_CHK( PTR );                              // check for NULL pointer, log an error message w/ event id
 * ERROR_NULL_CHK_RTRN( PTR );                         // check for NULL pointer, log an error message w/ event id, and returns APP_ERR_FAIL
 * ERROR_NULL_CHK_RTRN_CODE( PTR, CODE );              // check for NULL pointer, log an error message w/ event id, and returns supplied code
 *
 * // function return value checks -- requires function returns to adhere to APP_ERR_OK, APP_ERR_FAIL for SUCCESS/FAILURE
 * ERROR_RV_CHK( RETVAL );                             // check return value of a function, if return value indicates failure, logs an error message w/ event id
 * ERROR_RV_CHK_RTRN( RETVAL );                        // check return value of a function, if return value indicates failure, logs an error message w/ event id and returns APP_ERR_FAIL
 * ERROR_RV_CHK_RTRN_CODE( RETVAL, CODE );             // check return value of a function, if return value indicates failure, logs an error message w/ event id and returns supplied code
 * ERROR_RV_CSTM_CHK_RTRN( RETVAL, APP_FAIL_CODE )     // check return value of a function against a custom APP_FAIL_CODE, logs an error message w/ event id
 * 
 * // value comparison checks
 * ERROR_VAL_CHK( EXPECTED, ACTUAL );                  // check for expected == actual, on failure, logs an error message w/ event id
 * ERROR_VAL_CHK_RTRN( EXPECTED, ACTUAL );             // check for expected == actual, on failure, logs an error message w/ event id and returns APP_ERR_FAIL
 * ERROR_VAL_CHK_RTRN_CODE( EXPECTED, ACTUAL, CODE );  // check for expected == actual, on failure, logs an error message w/ event id and returns supplied code
 *
 * // expression/assertion checks
 * ERROR_EXPR_CHK( EXPR );                             // if expression evaluates to false, log an error message w/ event id
 * ERROR_EXPR_CHK_RTRN( EXPR );                        // if expression evaluates to false, log an error message w/ event id and return APP_ERR_FAIL
 * ERROR_EXPR_CHK_RTRN_CODE( EXPR, CODE );             // if expression evaluates to false, log an error message w/ event id and return supplied code
 *****************************************************************************/
#ifndef ERROR_H_
#define ERROR_H_

#include "common/utils/logger.h"

#define APP_ERR_FAIL  -1  /* ubiquitous return code to be used to indicate failed execution */
#define APP_ERR_OK     0  /* ubiquitous return code to be used to indicate successful execution */

/*
 * Universal error codes, leveraged by the error-handling framework which don't need app-specific
 * EIDs defined across the private_ids.h of each application
 * 
 * This implementaiton reserves 255 universal EIDs for common usage across applications:
 * 
 *     1111 1111 xxxx xxxx
 */
#define APP_EID_PREFIX_MASK           ( 0xFF00 )
#define APP_NULL_POINTER_ERR_EID      ( 0x0001 | APP_EID_PREFIX_MASK )
#define APP_RET_VAL_CHK_FAIL_ERR_EID  ( 0x0002 | APP_EID_PREFIX_MASK )
#define APP_VALUE_CHK_FAIL_ERR_EID    ( 0x0003 | APP_EID_PREFIX_MASK )
#define APP_EXPR_CHK_FAIL_ERR_EID     ( 0x0004 | APP_EID_PREFIX_MASK )

/**
 * Macro for checking for NULL pointers
 * 
 * Logs an error message w/ EID when a NULL pointer is present
 */
#define ERROR_NULL_CHK( PTR ) \
{ \
    if ( !( PTR ) ) \
    { \
        LOG_EID_ERROR( APP_NULL_POINTER_ERR_EID, "NULL pointer found" ); \
    } \
}

/**
 * Macro for checking for NULL pointers, returns on error
 * 
 * Logs an error message w/ EID when a NULL pointer is present and returns APP_ERR_FAIL identifier
 */
#define ERROR_NULL_CHK_RTRN( PTR ) ERROR_NULL_CHK_RTRN_CODE( PTR, APP_ERR_FAIL )

/**
 * Macro for checking for NULL pointers, returns supplied code on error
 * 
 * Logs an error message w/ EID when a NULL pointer is present and returns supplied identifier
 */
#define ERROR_NULL_CHK_RTRN_CODE( PTR, CODE ) \
{ \
    if ( !( PTR ) ) \
    { \
        LOG_EID_ERROR( APP_NULL_POINTER_ERR_EID, "NULL pointer found" ); \
        return CODE; \
    } \
}


/**
 * Macro for checking for understood failure return code (APP_ERR_FAIL) from a function return value
 * 
 * Logs an error message w/ EID when the return value matches APP_ERR_FAIL
 */
#define ERROR_RV_CHK( RETVAL ) \
{ \
    if ( RETVAL == APP_ERR_FAIL ) \
        LOG_EID_ERROR( APP_RET_VAL_CHK_FAIL_ERR_EID, "Return value indicates failure" ); \
}

/**
 * Macro for checking for understood failure return code (APP_ERR_FAIL) from a function return value
 * 
 * Logs an error message w/ EID when the return value matches APP_ERR_FAIL and returns APP_ERR_FAIL identifier
 */
#define ERROR_RV_CHK_RTRN( RETVAL ) ERROR_RV_CHK_RTRN_CODE( RETVAL, APP_ERR_FAIL )

/**
 * Macro for checking for understood failure return code (APP_ERR_FAIL) from a function return value
 * 
 * Logs an error message w/ EID when the return value matches APP_ERR_FAIL and returns supplied code
 */
#define ERROR_RV_CHK_RTRN_CODE( RETVAL, CODE ) \
{ \
    if ( RETVAL == APP_ERR_FAIL ) \
    { \
        LOG_EID_ERROR( APP_RET_VAL_CHK_FAIL_ERR_EID, "Return value indicates failure" ); \
        return CODE; \
    } \
}

/**
 * Macro for checking for custom failure return code (APP_FAIL_CODE) from a function return value
 * 
 * Logs an error message w/ EID when the return value matches APP_ERR_FAIL
 */
#define ERROR_RV_CSTM_CHK_RTRN( RETVAL, APP_FAIL_CODE ) \
{ \
    if ( RETVAL == APP_FAIL_CODE ) \
        LOG_EID_ERROR( APP_RET_VAL_CHK_FAIL_ERR_EID, "Return value indicates failure" ); \
}

/**
 * Macro for checking expected vs. actual values
 * 
 * Logs error message w/ EID on failed comparisons
 * 
 * Warning: not for floating-point or pointer types
 */
#define ERROR_VAL_CHK( EXPECTED, ACTUAL ) \
{ \
    if ( EXPECTED != ACTUAL ) \
        LOG_EID_ERROR( APP_VALUE_CHK_FAIL_ERR_EID, "actual value: %d, does not match expected: %d", ACTUAL, EXPECTED ); \
}

/**
 * Macro for checking expected vs. actual values, returns on error
 * 
 * Logs error message w/ EID on failed comparisons and returns APP_ERR_FAIL identifier
 * 
 * Warning: not for floating-point or pointer types
 */
#define ERROR_VAL_CHK_RTRN( EXPECTED, ACTUAL ) ERROR_VAL_CHK_RTRN_CODE( EXPECTED, ACTUAL, APP_ERR_FAIL )

/**
 * Macro for checking expected vs. actual values, returns supplied code on error
 * 
 * Logs error message w/ EID on failed comparisons and returns supplied identifier
 * 
 * Warning: not for floating-point or pointer types
 */
#define ERROR_VAL_CHK_RTRN_CODE( EXPECTED, ACTUAL, CODE ) \
{ \
    if ( EXPECTED != ACTUAL ) \
    { \
        LOG_EID_ERROR( APP_VALUE_CHK_FAIL_ERR_EID, "actual value: %d, does not match expected: %d", ACTUAL, EXPECTED ); \
        return CODE; \
    } \
}


/**
 * Macro for assertion tests
 * 
 * Logs error message w/ EID when an expression evaluates to false
 */
#define ERROR_EXPR_CHK( EXPR ) \
{ \
    if ( !( EXPR ) ) \
        LOG_EID_ERROR( APP_EXPR_CHK_FAIL_ERR_EID, "expression evaluated to false" ); \
}

/**
 * Macro for expression-based tests, returns on error
 * 
 * Logs error message w/ EID when an expression evaluates to false and returns APP_ERR_FAIL identifier
 */
#define ERROR_EXPR_CHK_RTRN( EXPR ) ERROR_EXPR_CHK_RTRN_CODE( EXPR, APP_ERR_FAIL )

/**
 * Macro for expression-based tests, returns supplied code on error
 * 
 * Logs error message w/ EID when an expression evaluates to false and returns supplied identifier
 */
#define ERROR_EXPR_CHK_RTRN_CODE( EXPR, CODE ) \
{ \
    if ( !( EXPR ) ) \
    { \
        LOG_EID_ERROR( APP_EXPR_CHK_FAIL_ERR_EID, "expression evaluted to false" ); \
        return CODE; \
    } \
}

#endif // ERROR_H_

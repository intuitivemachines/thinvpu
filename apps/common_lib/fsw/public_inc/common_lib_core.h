/**
 * Copyright 2019 Intuitive Machines, LLC. All rights reserved.
 * 
 * @file
 * @author  James Blakeslee <james@intuitivemachines.com>
 * 
 * @section DESCRIPTION
 * Prototypes and defines for common library functions
 */

#ifndef _COMMON_LIB_H_
#define _COMMON_LIB_H_

#include "common_types.h"

/*
 ******************* Includes ********************
 */


/* -------------- end of includes --------------*/

/*
 ******************* Defines ********************
 */
#define COMMON_LIB_MAJOR_VERSION 1
#define COMMON_LIB_MINOR_VERSION 0
#define COMMON_LIB_REVISION 0
#define COMMON_LIB_MISSION_REV 0

/* -------------- end of defines ---------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************* Function Prototypes ********************
 */
int32 Common_LibInit( void );

/* ---------- end of function prototypes ----------*/

#ifdef __cplusplus
}
#endif

#endif // _COMMON_LIB_H_ scope ends

/* ---------- end of common_lib.h ----------*/


#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_od_02_cv.hpp"
#include "tc_od_02.hpp"
#include "tc_od_02_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_od_02_driver(TC_OD_02_OUT_T *tc_od_02_out_ptr,const char *fileString)
{   
    LOG_INFO("TC_OD_02_DRIVER File Name %s", fileString);
    tc_od_02 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.process_img(tc_od_02_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
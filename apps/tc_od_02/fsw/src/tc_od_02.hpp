#ifndef __TC_OD_02CLASS_H__
#define __TC_OD_02CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_od_02_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_od_02
{
public:
  cv::Mat tc_od_02_img_1;
  cv::Mat tc_od_02_img_2;
  int load_img(const char*);
  int process_img(TC_OD_02_OUT_T*);
  virtual ~tc_od_02();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_02CLASS_H__ */

    
/* ---------- end of tc_od_02_class.hpp ---------- */
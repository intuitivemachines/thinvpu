/*=======================================================================================
** File Name:  sch_tblidx.h
**
** Title:  Table indicies into the CFS scheduler's message table for ThinVPU
**
** $Author:    Shaun Stewart
** $Revision:  $
** $Date:      2018-10-30
**
** Purpose:  This header file contains table indicies into the message table.
**
**=====================================================================================*/

#ifndef _SCH_TBL_IDX_H_
#define _SCH_TBL_IDX_H_

/*
** Pragmas
*/

/*
** Include Files
*/

/*
** Local Defines
*/
#define CFE_ES_SEND_HK_MIDX        1
#define CFE_EVS_SEND_HK_MIDX       2
#define CFE_SB_SEND_HK_MIDX        3
#define CFE_TBL_SEND_HK_MIDX       4
#define CFE_TIME_SEND_HK_MIDX      5

#define CS_SEND_HK_MIDX            6
#define DS_SEND_HK_MIDX            7
#define FM_SEND_HK_MIDX            8
#define HK_SEND_HK_MIDX            9
#define HS_SEND_HK_MIDX           10
#define LC_SEND_HK_MIDX           11
#define MD_SEND_HK_MIDX           12
#define MM_SEND_HK_MIDX           13
#define SC_SEND_HK_MIDX           14
#define SCH_SEND_HK_MIDX          15

#define CS_WAKEUP_MIDX            20
#define SC_WAKEUP_MIDX            21
#define LC_WAKEUP_MIDX            22
#define DS_WAKEUP_MIDX            23
#define MD_WAKEUP_MIDX            24
#define CF_WAKEUP_MIDX            25
#define CF_SEND_HK_MIDX           26
#define HS_WAKEUP_MIDX            27
#define HS_SEND_HK_MIDX           8
#define HS_WAKEUP_MIDX            9

#define CI_SEND_HK_MIDX           30
#define TO_SEND_HK_MIDX           31
#define CI_WAKEUP_MIDX            32
#define TO_WAKEUP_MIDX            33
#define SBN_WAKEUP_MIDX           34

/* Placeholder messages for manual app scheduling */
/*     vs. default prioritized rate groups.       */

#define SCH_1HZ_WAKEUP_MIDX       90
#define SCH_5HZ_WAKEUP_MIDX       91
#define SCH_10HZ_WAKEUP_MIDX      92
#define SCH_25HZ_WAKEUP_MIDX      93
#define SCH_50HZ1_WAKEUP_MIDX     94
#define SCH_50HZ2_WAKEUP_MIDX     95
#define SCH_100HZ_WAKEUP_MIDX     96

#define SCH_1HZ_SEND_HK_MIDX     100
#define SCH_5HZ_SEND_HK_MIDX     101

#endif /* _SCH_TBL_IDX_H_ */

/*=======================================================================================
** End of file sch_tblidx.h
**=====================================================================================*/


/*
** $Id: sch_def_schtbl.c 1.3 2017/06/21 15:29:50EDT mdeschu Exp  $
**
**  Copyright (c) 2007-2014 United States Government as represented by the
**  Administrator of the National Aeronautics and Space Administration.
**  All Other Rights Reserved.
**
**  This software was created at NASA's Goddard Space Flight Center.
**  This software is governed by the NASA Open Source Agreement and may be
**  used, distributed and modified only pursuant to the terms of that
**  agreement.
**
** Purpose: Scheduler (SCH) default schedule table data
**
** Author:
**
** Notes:
**
** Modification History:
**   MM/DD/YY  SCR/SDR     Author          DESCRIPTION
**   --------  ----------  -------------   -----------------------------
**   12/06/12  1129        B. Butcher      Added new 1/8 Hz Packets
**   10/26/18  ---         S. Stewart      Initial slot assigments
*/

/*************************************************************************
**
** Include section
**
**************************************************************************/

#include "cfe.h"
#include "cfe_tbl_filedef.h"
#include "sch_platform_cfg.h"
#include "sch_msgdefs.h"
#include "sch_tbldefs.h"

#include "sch_tblidx.h"

/*
** Schedule Table "group" definitions
*/
#define SCH_GROUP_NONE   0x00000000
#define SCH_GROUP_CDH    0x00000001    /* All C&DH Messages */
#define SCH_GROUP_GNC    0x00000002    /* All GNC  Messages */
#define SCH_GROUP_MISC   0x00000004
#define SCH_GROUP_HI     0x00000008
#define SCH_NAV          0x00000010
#define SCH_GUID         0x00000020
#define SCH_CNTRL        0x00000040

#define SCH_100HZ        0x01000000
#define SCH_50HZ         0x02000000
#define SCH_25HZ         0x04000000
#define SCH_10HZ         0x08000000
#define SCH_5HZ          0x21000000
#define SCH_1HZ          0x22000000

/* Define sub multi-groups           */
#define SCH_GROUP_CFS_HK      (  (0x000100) | SCH_GROUP_CDH)    /* CFS HK Messages          */
#define SCH_GROUP_CFE_HK      (  (0x000200) | SCH_GROUP_CDH)    /* cFE HK Messages          */
#define SCH_GROUP_GNC_HK      (  (0x000400) | SCH_GROUP_GNC)    /* GNC HK Messages          */

/*
** Default schedule table data
*/
SCH_ScheduleEntry_t SCH_DefaultScheduleTable[SCH_TABLE_ENTRIES] =
{

/*
** Structure definition...
**
**    uint8    EnableState  -- SCH_UNUSED, SCH_ENABLED, SCH_DISABLED
**    uint8    Type         -- 0 or SCH_ACTIVITY_SEND_MSG
**    uint16   Frequency    -- how many seconds between Activity execution
**    uint16   Remainder    -- seconds offset to perform Activity
**    uint16   MessageIndex -- Message Index into Message Definition table
**    uint32   GroupData    -- Group and Multi-Group membership definitions
*/

  /* slot #0 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_5HZ_WAKEUP_MIDX, SCH_5HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_1HZ_SEND_HK_MIDX, SCH_1HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




    /* slot #1 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, DS_SEND_HK_MIDX, SCH_1HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #2 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #3 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, DS_WAKEUP_MIDX,  SCH_1HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #4 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #5 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #6 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #7 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #8 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #9 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #10 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #11 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #12 */
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #13 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #14 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #15 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #16 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #17 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #18 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #19 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #20 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_5HZ_WAKEUP_MIDX, SCH_5HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #21 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  {  SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #22 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #23 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  4,  3, CFE_TIME_SEND_HK_MIDX, SCH_GROUP_CFE_HK },   /* TIME HK Request */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #24 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #25 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #26 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #27 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #28 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #29 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #30 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #31 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #32 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #33 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #34 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #35 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #36 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #37 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #38 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #39 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #40 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_5HZ_WAKEUP_MIDX, SCH_5HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #41 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},




  /* slot #42 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #43 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  4,  3, CFE_ES_SEND_HK_MIDX, SCH_GROUP_CFE_HK },   /* ES HK Request */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #44 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #45 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #46 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #47 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #48 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #49 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #50 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #51 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #52 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #53 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #54 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #55 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #56 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #57 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #58 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #59 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #60 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_5HZ_WAKEUP_MIDX, SCH_5HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #61 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #62 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #63 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #64 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #65 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #66 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #67 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #68 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #69 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #70 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #71 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #72 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #73 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #74 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #75 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #76 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #77 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #78 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #79 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #80 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_5HZ_WAKEUP_MIDX, SCH_5HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #81 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_1HZ_WAKEUP_MIDX, SCH_1HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #82 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #83 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #84 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #85 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #86 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #87 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #88 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #89 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #90 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #91 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_SEND_HK_MIDX, SCH_10HZ },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, HS_WAKEUP_MIDX,  SCH_10HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE},
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },





  /* slot #92 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #93 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #94 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #95 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #96 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_25HZ_WAKEUP_MIDX, SCH_25HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #97 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #98 */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ1_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },




  /* slot #99 - Left Empty to allow Scheduler to Easily Resynchronize with 1 Hz */
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_100HZ_WAKEUP_MIDX, SCH_100HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_ENABLED, SCH_ACTIVITY_SEND_MSG,  1,  0, SCH_50HZ2_WAKEUP_MIDX, SCH_50HZ },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE },
  { SCH_UNUSED,   0,      0,  0, 0,  SCH_GROUP_NONE }
};

/*
** Table file header
*/
CFE_TBL_FILEDEF(SCH_DefaultScheduleTable, SCH.SCHED_DEF, SCH schedule table, sch_def_schtbl.tbl)

/*************************************************************************
**
** File data
**
**************************************************************************/

/*
** (none)
*/

/*************************************************************************
**
** Local function prototypes
**
**************************************************************************/

/*
** (none)
*/

/************************/
/*  End of File Comment */
/************************/


/************************************************************************
** File:
**   $Id: sch_app_rates.h 1.0 2012/5/16 20:08:00BST jb Exp  $
**
** Purpose: 
**  The CFS Scheduler (SCH) Application Rates defined header file
**
** Notes:
**
** $Log: sch_msgids.h  $
** Revision 1.1 2012/5/16 20:08:00BST jbusa
** Initial revision
**
*************************************************************************/
#ifndef _sch_app_rates_h_
#define _sch_app_rates_h_

#define MP_RATE_DS              5
#define MP_RATE_TC_XXXX_01      1
#define MP_RATE_TC_RPOD_01      1
#define MP_RATE_TC_RPOD_02      1
#define MP_RATE_TC_RPOD_03      1
#define MP_RATE_TC_RPOD_04      1
#define MP_RATE_TC_OD_01_A      1
#define MP_RATE_TC_OD_01_B      1
#define MP_RATE_TC_OD_02        1
#define MP_RATE_TC_OD_03        1
#define MP_RATE_TC_OD_04        1
#define MP_RATE_TC_PLHA_01      1
#define MP_RATE_TC_PLHA_02      1
#define MP_RATE_TC_PLHA_03      1
#define MP_RATE_TC_PLHA_04      1
#define MP_RATE_TC_SM_01        1
#define MP_RATE_TC_SM_02        1
#define MP_RATE_TC_SM_03        1
#define MP_RATE_TC_SM_04        1
#define MP_RATE_TCM             1


#endif /* _sch_app_rates_h_ */

/************************/
/*  End of File Comment */
/************************/

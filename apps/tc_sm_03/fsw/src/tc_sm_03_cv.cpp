#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_sm_03_cv.hpp"
#include "tc_sm_03.hpp"
#include "tc_sm_03_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_sm_03_driver(TC_SM_03_OUT_T *tc_sm_03_out_ptr,const char *fileString)
{   
    LOG_INFO("TC_SM_03_DRIVER File Name %s", fileString);
    tc_sm_03 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.process_img(tc_sm_03_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
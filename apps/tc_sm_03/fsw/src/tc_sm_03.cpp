#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/video/tracking.hpp>
#include "tc_sm_03.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_sm_03::load_img(const char *filepath)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    char cTranslatedPath2[OS_MAX_LOCAL_PATH_LEN];

    OS_TranslatePath("/cf/apps/old.png", cTranslatedPath);
    OS_TranslatePath("/cf/apps/new.png", cTranslatedPath2);

    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);

    tc_sm_03_img_1 = imread(cTranslatedPath, IMREAD_GRAYSCALE);
    tc_sm_03_img_2 = imread(cTranslatedPath2, IMREAD_GRAYSCALE);

    // Check image loaded correctly
    if(tc_sm_03_img_1.empty()||tc_sm_03_img_2.empty())
    {
        LOG_INFO("Warning Empty image found in TC_SM_02");
        return -1;
    }
    return 0;
}

int tc_sm_03::process_img(TC_SM_03_OUT_T *tc_sm_03_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];

    // Create some random colors
    vector<Scalar> colors;
    RNG rng;
    int feat_im1; 
    int feat_im2;
    int feat_match; 
    
    for(int i = 0; i < 100; i++)
    {
        int r = rng.uniform(0, 256);
        int g = rng.uniform(0, 256);
        int b = rng.uniform(0, 256);
        colors.push_back(Scalar(r,g,b));
    }
    
    // Create frames from first image
    Mat old_frame = tc_sm_03_img_1.clone();
    Mat old_feature_frame = tc_sm_03_img_1.clone();
    
    // Convert frist frame from 8UC1 to 32FC1 for corner detection 
    old_feature_frame.convertTo(old_feature_frame,CV_32FC1, 1.0/255.0);
    
    // Take first frame and find corners in it
    vector<Point2f> prev_frame_corners, new_frame_feature;
    vector<Point2f> next_frame_corners;
    int maxCorners = 100;
    double qualityLevel = 0.375;
    double minDistance = 7;
    int blockSize = 7;
    bool useHarrisDetector = false;
    double k = 0.04;
    goodFeaturesToTrack(old_feature_frame, prev_frame_corners, maxCorners, qualityLevel, minDistance, Mat(), blockSize,useHarrisDetector, k);
    LOG_INFO("Number of prev frame corners detected: %d", prev_frame_corners.size());
    
    // Create a mask image for drawing purposes
    Mat mask = Mat::zeros(old_frame.size(), old_frame.type());

    while(true)
    {   
        // No video feed, create the frames
        Mat prev_frame = tc_sm_03_img_1.clone();
        Mat next_frame = tc_sm_03_img_2.clone();

        // calculate optical flow
        vector<uchar> status;
        vector<float> err;
        TermCriteria criteria = TermCriteria((TermCriteria::COUNT) + (TermCriteria::EPS), 10, 0.03);
        Size winSize = Size(25,25);
        int maxLevel = 20;
        calcOpticalFlowPyrLK(prev_frame, next_frame, prev_frame_corners, next_frame_corners, status, err, winSize, maxLevel, criteria);
        vector<Point2f> good_new;

        // Select Good points
        for(int i = 0; i < prev_frame_corners.size(); i++)
        {   
            if( (int)status[i] == 1) {
                good_new.push_back(next_frame_corners[i]);
                // Draw the tracks
                line(mask,next_frame_corners[i], prev_frame_corners[i], colors[i], 2);
                circle(next_frame, next_frame_corners[i], 5, colors[i], -1);
            }
        }
        Mat img;
        add(next_frame, mask, img);
        OS_TranslatePath("/cf/vpu/1_tc_sm_03_output.png", cTranslatedPath);
        imwrite(cTranslatedPath, img);
        LOG_INFO("Number of detected features im1 %d", prev_frame_corners.size());
        LOG_INFO("Number of detected features im2 %d", next_frame_corners.size());
        // Now update the previous frame and previous points
        prev_frame = next_frame.clone();
        prev_frame_corners = good_new;
        LOG_INFO("Number of corretly Matched Features: %d", good_new.size());

        // Capture numbers to be logged
        feat_im1 =  prev_frame_corners.size();
        feat_im2 =  next_frame_corners.size();
        feat_match = good_new.size();

        // No video feed, run only once
        break;
    }

    if(tc_sm_03_out_ptr != NULL)
    {
        tc_sm_03_out_ptr->features_im1 = static_cast<int>(feat_im1);
        tc_sm_03_out_ptr->features_im2 = static_cast<int>(feat_im2);
        tc_sm_03_out_ptr->matched_features = static_cast<int>(feat_match);
    }
    return 0;
}

// Class Deconstructor
tc_sm_03::~tc_sm_03() = default;

#ifdef __cplusplus
}
#endif
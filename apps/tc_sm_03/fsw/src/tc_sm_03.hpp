#ifndef __TC_SM_03CLASS_H__
#define __TC_SM_03CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_sm_03_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_sm_03
{
public:
  cv::Mat tc_sm_03_img_1;
  cv::Mat tc_sm_03_img_2;
  int load_img(const char*);
  int process_img(TC_SM_03_OUT_T*);
  virtual ~tc_sm_03();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_SM_03CLASS_H__ */

    
/* ---------- end of tc_sm_03_class.hpp ---------- */
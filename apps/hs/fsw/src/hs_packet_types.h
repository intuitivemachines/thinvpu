/* Copyright 2019 Intuitive Machines, LLC. All rights reserved.
 */
#ifndef HS_PACKET_TYPES_H_
#define HS_PACKET_TYPES_H_
#include "common_types.h"
#include "common/interfaces/fsw_sim/constants.h"
#include "hs_types.h"

typedef struct hs_rx_packet_struct
{
    float TempSensorData[NUM_TEMP_SENSORS];

} HS_RX_PKT_T;



void HS_RX_XLATE( HSIO_OUT_T *pDest, HS_RX_PKT_T *pSrc);

#endif
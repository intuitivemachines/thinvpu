// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
** File Name: hs_sock_drv.h
**
** Title:    Health & Safety Socket Driver
**
** Programmer(s): Miguel Rosales
**
** Purpose:
**    Serves as the device driver to exchange data with the Health & Safety Model
**    of the Trick Sumulation.
**
** Functions Contained:
**    hs_sock_drv_init()
**    hs_sock_drv_read()
**
** Limitations, Assumptions, External Events, and Notes:
**    See each function's prolog.
**
** Modification History:
**    MM-DD-YYYY  SCR*  AUTHOR   DESCRIPTION
**    ----------- ----  -------- ----------------------------------------------
**    12-21-2014  612   R.Ferguson  Initial Development
**    01-08-2020  --    M. Rosales  Configured for lander HS device
**
**==============================================================================
*/

#ifndef _HS_SOCK_DRV_H
#define _HS_SOCK_DRV_H

#include "common/interfaces/fsw_sim/constants.h"
#include "hs_types.h"
#include "hs_packet_types.h"
#include "stdio.h"
#include <string.h>
#include <arpa/inet.h>


int hs_sock_drv_init(void);

int hs_sock_drv_read( HSIO_OUT_T *hs_data_ptr );


#endif


// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
**
** Purpose:   
**    Serves as the device driver to exchange data with the Health & Safety Model 
**    of the Trick Simulation.
**
** Functions Contained:
**    hs_sock_drv_init()
**    hs_sock_drv_read()
**    hs_sock_rx_transformer()
**
**    Note: This implementation follows the adio_ignus_fumo_drv.c implementation
**          for the initial engine smoke test.
**
**==============================================================================
*/

/*
** Include files 
*/
#include "hs_sock_drv.h"
#include "common_types.h"
#include "common/utils/error.h"
#include "common/io/socket/udp_socket2.h"
#include "common/utils/logger.h"
#include <arpa/inet.h>

/* constants */
// TODO: Similar to ADIO, just hard code in the driver for now- we don't need to I-Load socket configs for Ignis/Fumo

/* Only included receive below, no transmit expected to IMU */

/* receive from Sim */
/* receive from other source(s) - goes below */

/* local function prototypes */
HS_ERR_T HS_sock_rx_transformer( HSIO_OUT_T* dest, HS_RX_PKT_T* src );

HS_RX_PKT_T hs_io_out[HS_RING_BUFFER_SIZE];

/*==============================================================================
** Name:    hs_sock_drv_init()
**
** Purpose: 
**    Initializes the device driver
**
** Returns: int
**    0 = good execution
**   -1 = error encountered
**
** Routines Called:       none  
** Called By:             hs_exec()
** Global Inputs/Reads:   none
** Global Outputs/Writes: none
**
** Limitations, Assumptions, External Events, and Notes:
**
**==============================================================================
*/
HS_ERR_T hs_sock_drv_init()
{
    LOG_TRACE( "Initializing FSW HS UDP" );
    if (GetReadSocket(HS_UDP_OUT_ID) >= 0){
        LOG_EID_TRACE( (uint16)ERR_HS_OK, "Port Allocated FSW HS UDP" );
        return ERR_HS_OK;
    }
    else {
        LOG_EID_TRACE( (uint16)ERR_HS_INIT_ERROR, "Port Allocation failed FSW HS UDP" );
        return ERR_HS_INIT_ERROR;
    }
    return ERR_HS_OK;
}

/*==============================================================================
** Name:    hs_sock_drv_read()
**
** Purpose: 
**    Read data from device
**
** Arguments:
**  Input:
**    readCode                Type of read to perform from device
**
**  Output:
**    hs_data_ptr        Receives the data from the device
**
** Returns: int
**    0 = good execution
**   -1 = error encountered
**
** Routines Called:       none  
** Called By:             hs_exec()
** Global Inputs/Reads:   none
** Global Outputs/Writes: none
**
**==============================================================================
*/
int hs_sock_drv_read(
      HSIO_OUT_T   *pHsIoOut   /* Holds data from Read */
   )
{
    ERROR_NULL_CHK_RTRN( pHsIoOut );
    LOG_TRACE( "Reading FSW HS UDP" );
    memset(&hs_io_out, 0, sizeof(hs_io_out));
    int32 bytesRead = ReadFromSocket(&hs_io_out, sizeof(HS_RX_PKT_T), HS_RING_BUFFER_SIZE, HS_UDP_OUT_ID);
    if (bytesRead < 0)
        return ERR_HS_READ_ERROR;
    if (bytesRead > 0)
        return hs_sock_rx_transformer(pHsIoOut, &hs_io_out);
    return ERR_HS_OK;
}

/*==============================================================================
** Name:    hs_sock_rx_transformer()
**
** Purpose: transforms an externally received packet originally from IMU sensor into internal IMU IO App type
**
** Arguments:
**  Input:
**    pSrc          pointer to packet received from external source (IMU via socket)
**
**  Output:
**    pDest         pointer to transformed internal structure
**
** Returns: status of operation, non-zero indicates error occured: \
**    ERR_IMU_OK                         -- no error \
**    ERR_IMU_NULL                       -- null pointer was found on input arguments, no transformation performed \
**    ERR_IMU_IF_VERSION_LENGTH_MISMATCH -- version length broadcast by external source does not match expected \
**    ERR_IMU_IF_VERSION_MISMATCH        -- version broadcast by external source does not match expected
**
** Routines Called:       none  
** Called By:             imu_sock_drv_read()
**
** Note:    byte swapping is not incorportated in this implementation. 
**          Todo: Determine if byte swapping is needed for IMU sock driver
**
/*==============================================================================*/

HS_ERR_T hs_sock_rx_transformer( HSIO_OUT_T* pDest, HS_RX_PKT_T pSrc[HS_RING_BUFFER_SIZE] )
{
    int32  lastindex                            = 0;
    static double    lastTimeStamp              = 0;   
    static int32     previousDataCycleCounter   = 0;
    int32            currentDataCycleCounter    = 0;
    static osalbool  bIsFirst                   = TRUE;

    ERROR_NULL_CHK_RTRN( pDest );
    ERROR_NULL_CHK_RTRN( pSrc );

    return ERR_HS_OK;
}
/* ---------- end of hs_sock_drv.c ----------*/

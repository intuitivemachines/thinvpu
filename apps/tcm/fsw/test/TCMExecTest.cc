/*==============================================================================
** File Name: TCMExecTest.cc
**
** Title: Unit Tests for TCM Executive Layer
**
** Author: James Blakeslee
** Date: 2019-01-08
**============================================================================*/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestFixture.h>

#include "MathAssert.hh"  // convenience macros for matrix and vector comparisons

extern "C"
{
#include "tcm_exec.h"
}

class TCMExecTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE( TCMExecTest );
    CPPUNIT_TEST( testMyTest1 );  // TODO: add unit tests here
    CPPUNIT_TEST( testMyTest2 );  // TODO: add unit tests here
    CPPUNIT_TEST_SUITE_END();

    public:
        void setUp();
        void tearDown();
        void testMyTest1();  // TODO: public visibility for all unit test functions
        void testMyTest2();  // TODO: public visibility for all unit test functions
};

/* called on instantiation */
void TCMExecTest::setUp()
{
}

/* called after tests are complete (falls out-of-scope) */
void TCMExecTest::tearDown()
{
}

/* TODO: short test description */
void TCMExecTest::testMyTest1()
{
}

/* TODO: short test description */
void TCMExecTest::testMyTest2()
{
}

CPPUNIT_TEST_SUITE_REGISTRATION( TCMExecTest );

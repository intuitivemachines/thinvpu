// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
**
** Title:  Function Definitions for TCM Application
** Purpose:  This source file contains all necessary function definitions to run TCM
**           application.
**
** Functions Defined:
**    TCM_InitEvent()
**    TCM_InitPipe()
**    TCM_InitData()
**    TCM_InitApp()
**    TCM_CleanupCallback()
**    TCM_RecvMsg()
**    TCM_ProcessNewData()
**    TCM_ProcessNewCmds()
**    TCM_ProcessNewAppCmds()
**    TCM_ReportHousekeeping()
**    TCM_CheckStatusOfTables()
**    TCM_SendOutData()
**    TCM_VerifyCmdLength()
**    TCM_AppMain()
**    TCM_InitCDSData()
**    TCM_SetCDSData()
**
**=====================================================================================*/

/*
** Pragmas
*/

/*
** Include Files
*/
#include <string.h>
#include "tcm_app.h"
#include "common_types.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/

/*
** External Global Function Prototypes
*/
extern int schSetTime;

/*
** Global Variables
*/
TCM_AppData_t  g_TCM_AppData;

MISSION_app_pipes_T tcm_pipes[TCM_TOTAL_DATAPIPES] =
{
  //MsgId                        PipeRate                 PipeName                 Pipeid
  {TC_RPOD_01_OUT_DATA_MID,      MP_RATE_TC_RPOD_01,     "tcm_rpod_01_pipe",      0},
  {TC_RPOD_02_OUT_DATA_MID,      MP_RATE_TC_RPOD_02,     "tcm_rpod_02_pipe",      0},
  {TC_RPOD_03_OUT_DATA_MID,      MP_RATE_TC_RPOD_03,     "tcm_rpod_03_pipe",      0},
  {TC_RPOD_04_OUT_DATA_MID,      MP_RATE_TC_RPOD_04,     "tcm_rpod_04_pipe",      0},
  {TC_OD_01_A_OUT_DATA_MID,      MP_RATE_TC_OD_01_A,     "tcm_od_01_a_pipe",      0},
  {TC_OD_01_B_OUT_DATA_MID,      MP_RATE_TC_OD_01_B,     "tcm_od_01_b_pipe",      0},
  {TC_OD_02_OUT_DATA_MID,        MP_RATE_TC_OD_02,       "tcm_od_02_pipe",        0},
  {TC_OD_03_OUT_DATA_MID,        MP_RATE_TC_OD_03,       "tcm_od_03_pipe",        0},
  {TC_OD_04_OUT_DATA_MID,        MP_RATE_TC_OD_04,       "tcm_od_04_pipe",        0},
  {TC_PLHA_01_OUT_DATA_MID,      MP_RATE_TC_PLHA_01,     "tcm_plha_01_pipe",      0},
  {TC_PLHA_02_OUT_DATA_MID,      MP_RATE_TC_PLHA_02,     "tcm_plha_02_pipe",      0},
  {TC_PLHA_03_OUT_DATA_MID,      MP_RATE_TC_PLHA_03,     "tcm_plha_03_pipe",      0},
  {TC_PLHA_04_OUT_DATA_MID,      MP_RATE_TC_PLHA_04,     "tcm_plha_04_pipe",      0},
  {TC_SM_01_OUT_DATA_MID,        MP_RATE_TC_SM_01,       "tcm_sm_01_pipe",        0},
  {TC_SM_02_OUT_DATA_MID,        MP_RATE_TC_SM_02,       "tcm_sm_02_pipe",        0},
  {TC_SM_03_OUT_DATA_MID,        MP_RATE_TC_SM_03,       "tcm_sm_03_pipe",        0},
  {TC_SM_04_OUT_DATA_MID,        MP_RATE_TC_SM_04,       "tcm_sm_04_pipe",        0}
};

/*
** Local Variables
*/

/*
** Local Function Definitions
*/

/*=====================================================================================
** Name: TCM_InitEvent
**
** Purpose: To initialize and register event table for TCM application
**
** Routines Called:
**    CFE_EVS_Register
**    CFE_ES_WriteToSysLog
**
** Called By:
**    TCM_InitApp
**
** Global Outputs/Writes:
**    g_TCM_AppData.EventTbl
**=====================================================================================*/
int32 TCM_InitEvent()
{
    int32  iStatus=CFE_SUCCESS;

    /* Create the event table */
    memset((void*)g_TCM_AppData.EventTbl, 0x00, sizeof(g_TCM_AppData.EventTbl));

    g_TCM_AppData.EventTbl[0].EventID = TCM_RESERVED_EID;
    g_TCM_AppData.EventTbl[1].EventID = TCM_INF_EID;
    g_TCM_AppData.EventTbl[2].EventID = TCM_INIT_INF_EID;
    g_TCM_AppData.EventTbl[3].EventID = TCM_ILOAD_INF_EID;
    g_TCM_AppData.EventTbl[4].EventID = TCM_CDS_INF_EID;
    g_TCM_AppData.EventTbl[5].EventID = TCM_CMD_INF_EID;

    g_TCM_AppData.EventTbl[ 6].EventID = TCM_ERR_EID;
    g_TCM_AppData.EventTbl[ 7].EventID = TCM_INIT_ERR_EID;
    g_TCM_AppData.EventTbl[ 8].EventID = TCM_ILOAD_ERR_EID;
    g_TCM_AppData.EventTbl[ 9].EventID = TCM_CDS_ERR_EID;
    g_TCM_AppData.EventTbl[10].EventID = TCM_CMD_ERR_EID;
    g_TCM_AppData.EventTbl[11].EventID = TCM_PIPE_ERR_EID;
    g_TCM_AppData.EventTbl[12].EventID = TCM_MSGID_ERR_EID;
    g_TCM_AppData.EventTbl[13].EventID = TCM_MSGLEN_ERR_EID;

    /* Register the table with CFE */
    iStatus = CFE_EVS_Register(g_TCM_AppData.EventTbl,
                               TCM_EVT_CNT, CFE_EVS_BINARY_FILTER);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to register with EVS (0x%08X)\n", (unsigned int)iStatus);
    }

    return (iStatus);
}

/*=====================================================================================
** Name: TCM_InitPipe
**
** Purpose: To initialize all message pipes and subscribe to messages for TCM application
**
** Routines Called:
**    CFE_SB_CreatePipe
**    CFE_SB_Subscribe
**    CFE_ES_WriteToSysLog
**
** Called By:
**    TCM_InitApp
**
** Global Outputs/Writes:
**    g_TCM_AppData.usSchPipeDepth
**    g_TCM_AppData.cSchPipeName
**    g_TCM_AppData.SchPipeId
**    g_TCM_AppData.usCmdPipeDepth
**    g_TCM_AppData.cCmdPipeName
**    g_TCM_AppData.CmdPipeId
**    g_TCM_AppData.usTlmPipeDepth
**    g_TCM_AppData.cTlmPipeName
**    g_TCM_AppData.TlmPipeId
**=====================================================================================*/
int32 TCM_InitPipe()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init schedule pipe */
    g_TCM_AppData.usSchPipeDepth = TCM_SCH_PIPE_DEPTH;
    memset((void*)g_TCM_AppData.cSchPipeName, '\0', sizeof(g_TCM_AppData.cSchPipeName));
    strncpy(g_TCM_AppData.cSchPipeName, "TCM_SCH_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to Wakeup messages */
    iStatus = CFE_SB_CreatePipe(&g_TCM_AppData.SchPipeId,
                                 g_TCM_AppData.usSchPipeDepth,
                                 g_TCM_AppData.cSchPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        iStatus = CFE_SB_Subscribe(SCH_25HZ_WAKEUP_MID, g_TCM_AppData.SchPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Sch Pipe failed to subscribe to SCH_1HZ_WAKEUP_MID. (0x%08X)\n", (unsigned int)iStatus);
			return (iStatus);
        }

        iStatus = CFE_SB_Subscribe(SCH_1HZ_SEND_HK_MID, g_TCM_AppData.SchPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Sch Pipe failed to subscribe to SCH_1HZ_SEND_HK_MID. (0x%08X)\n", (unsigned int)iStatus);
			return (iStatus);
        }
    }
    else
    {
        CFE_ES_WriteToSysLog("TCM - Failed to create SCH pipe (0x%08X)\n", (unsigned int)iStatus);
		return (iStatus);
    }

    /* Init command pipe */
    g_TCM_AppData.usCmdPipeDepth = TCM_CMD_PIPE_DEPTH ;
    memset((void*)g_TCM_AppData.cCmdPipeName, '\0', sizeof(g_TCM_AppData.cCmdPipeName));
    strncpy(g_TCM_AppData.cCmdPipeName, "TCM_CMD_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to command messages */
    iStatus = CFE_SB_CreatePipe(&g_TCM_AppData.CmdPipeId,
                                 g_TCM_AppData.usCmdPipeDepth,
                                 g_TCM_AppData.cCmdPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        /* Subscribe to command messages */
        iStatus = CFE_SB_Subscribe(TCM_CMD_MID, g_TCM_AppData.CmdPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - CMD Pipe failed to subscribe to TCM_CMD_MID. (0x%08X)\n", (unsigned int)iStatus);
			return (iStatus);
        }

    }
    else
    {
        CFE_ES_WriteToSysLog("TCM - Failed to create CMD pipe (0x%08X)\n", (unsigned int)iStatus);
		return (iStatus);
    }

    /* Init telemetry pipes */

    for (int pipes=0; pipes<TCM_TOTAL_DATAPIPES; pipes++) {

        /* Subscribe to telemetry messages on each telemetry pipe */
        iStatus = CFE_SB_CreatePipe(
                      &tcm_pipes[pipes].PipeId,
                      APP_GET_Q_RATE_DEPTH(tcm_pipes[pipes].PipeRate,MP_RATE_TCM),
                      tcm_pipes[pipes].PipeName);

        if (iStatus == CFE_SUCCESS)
        {
            /*
            ** Subscribe to Out data packets
            */
    	    iStatus = CFE_SB_SubscribeEx(
                tcm_pipes[pipes].MsgId,
                tcm_pipes[pipes].PipeId,
				CFE_SB_Default_Qos,
                APP_GET_Q_RATE_DEPTH(tcm_pipes[pipes].PipeRate,MP_RATE_TCM));
            if ( iStatus != CFE_SUCCESS )
            {
                CFE_ES_WriteToSysLog("TCM App: Error Subscribing to \
                        %s Outut RC = 0x%08X\n", tcm_pipes[pipes].PipeName, (unsigned int)iStatus);
                return (iStatus);
            }
        }
        else
        {
            CFE_ES_WriteToSysLog("TCM App: Error Creating %s, \
                                 RC = 0x%08X\n", tcm_pipes[pipes].PipeName, (unsigned int)iStatus);

            return (iStatus);
        }

    }

    return (iStatus);
}

/*=====================================================================================
** Name: TCM_InitData
**
** Purpose: To initialize global variables used by TCM application
**
** Routines Called:
**    CFE_SB_InitMsg
**
** Called By:
**    TCM_InitApp
**
** Global Outputs/Writes:
**    g_TCM_AppData.InData
**    g_TCM_AppData.OutPacket
**    g_TCM_AppData.HkTlm
**=====================================================================================*/
int32 TCM_InitData()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init input data */
    memset((void*)&g_TCM_AppData.InData, 0x00, sizeof(g_TCM_AppData.InData));

    /* Init execution timer */
    g_TCM_AppData.prevExecTime = 0.0;

    /* Init output data */
    memset((void*)&g_TCM_AppData.OutPacket, 0x00, sizeof(g_TCM_AppData.OutPacket));
    CFE_SB_InitMsg(&g_TCM_AppData.OutPacket,
                   TCM_OUT_DATA_MID, sizeof(g_TCM_AppData.OutPacket), TRUE);

    /* Init housekeeping packet */
    memset((void*)&g_TCM_AppData.HkTlm, 0x00, sizeof(g_TCM_AppData.HkTlm));
    CFE_SB_InitMsg(&g_TCM_AppData.HkTlm,
                   TCM_HK_TLM_MID, sizeof(g_TCM_AppData.HkTlm), TRUE);

    /*
     ** Create Critical Data Store (CDS)
     */

    /* Default CDS as enabled */
    g_TCM_AppData.CDSState = TRUE;

    /* Register CDS memory */
    //iStatus = CFE_ES_RegisterCDS(&g_TCM_AppData.MyCDSHandle, sizeof(TCM_CDS_RESTART_T), TCM_CDSNAME); // TODO: Uncomment once CDS struct is filled

    /* Initialize/Restore CDS memory */
    if (iStatus == CFE_ES_CDS_ALREADY_EXISTS)
    {
        /*
         * Critical Data Store already existed! THIS is a RESTART!
         */

        /* Announce */
        CFE_EVS_SendEvent(TCM_CDS_RESTORE_ERR_EID, CFE_EVS_INFORMATION, "TCM: !!!CPU Reset Detected!!!");

        /* Get copy of CDS contents from CDS and transfer to CDS staging area */
    	iStatus = CFE_ES_RestoreFromCDS(&g_TCM_AppData.CDSData, g_TCM_AppData.MyCDSHandle);

        if (iStatus == CFE_SUCCESS)
        {
            /* Copy from CDS staging area to TCM Exec-Layer inputs */
            // TODO: Copy CDS staging area

            /* Command the TCM Exec-Layer to process the restart data */
            // TODO: Command Exec-Layer to process data restart

            /* Announce Restored Values */
            // TODO: Announce restored values
        }
        else
    	{
            /* Report error restoring data */
            CFE_EVS_SendEvent(TCM_CDS_RESTORE_ERR_EID, CFE_EVS_INFORMATION,
                              "TCM: Failed to restore data from CDS (Err=0x%08x).", (unsigned int)iStatus);

            /* If data could not be retrieved, initialize data */
            TCM_InitCDSData();
        }

        iStatus = CFE_SUCCESS;
    }
    else if (iStatus == CFE_SUCCESS)
    {
        /* If CDS did not previously exist, initialize data */
        TCM_InitCDSData();
        CFE_EVS_SendEvent(TCM_CDS_RESTORE_ERR_EID, CFE_EVS_INFORMATION, "TCM - Initialized CDS");
    }
    else
    {
        /* CDS Memory Error Encountered: Disable CDS usage */
        g_TCM_AppData.CDSState = FALSE;

        /* Initialize values anyway (they will not be saved) */
        TCM_InitCDSData();
    }

    g_TCM_AppData.StateData.active_test_case = TCM_ID_RPOD_01;
    g_TCM_AppData.StateData.next_test_case = TCM_ID_RPOD_01;
    g_TCM_AppData.OutPacket.OutData.test_state.active_test_case = TCM_ID_RPOD_01;

    return (iStatus);
}

/*=====================================================================================
** Name: TCM_InitApp
**
** Purpose: To initialize all data local to and used by TCM application
**
** Routines Called:
**    CFE_ES_RegisterApp
**    CFE_ES_WriteToSysLog
**    CFE_EVS_SendEvent
**    OS_TaskInstallDeleteHandler
**    TCM_InitEvent
**    TCM_InitPipe
**    TCM_InitData
**
** Called By:
**    TCM_AppMain
**=====================================================================================*/
int32 TCM_InitApp()
{
    int32  iStatus=CFE_SUCCESS;

    g_TCM_AppData.uiRunStatus = CFE_ES_APP_RUN;

    iStatus = CFE_ES_RegisterApp();
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to register the app (0x%08X)\n", (unsigned int)iStatus);
		CFE_ES_WriteToSysLog("TCM - Application failed to initialize\n");
		return iStatus;
    }

    if ((TCM_InitEvent()     != CFE_SUCCESS) ||
        (TCM_InitPipe()      != CFE_SUCCESS) ||
        (TCM_InitData()      != CFE_SUCCESS) ||
        (TCM_InitILoadTbl()  != CFE_SUCCESS) 
       )
    {
        iStatus = -1;
		CFE_ES_WriteToSysLog("TCM - Application failed to initialize\n");
		return iStatus;
    }

    /* Install the cleanup callback */
    OS_TaskInstallDeleteHandler((void*)&TCM_CleanupCallback);


  if (iStatus == CFE_SUCCESS) {
	CFE_EVS_SendEvent(TCM_INIT_INF_EID, CFE_EVS_INFORMATION,
                      "TCM - Application initialized");
  } else {
    CFE_ES_WriteToSysLog("TCM - Application failed to initialize\n");
  }

  return iStatus;
}

/*=====================================================================================
** Name: TCM_CleanupCallback
**
** Purpose: To handle any neccesary cleanup prior to application exit
**
** Called By:
**    TCM_InitApp
**=====================================================================================*/
void TCM_CleanupCallback()
{
    /* No memory cleanup code currently needed. Function intentionally left blank.
     Callback should be maintained, since it is registered with CFS. 
     This registered callback provides a placeholder in case future cleanup needs are identified.*/
}

/*=====================================================================================
** Name: TCM_RcvMsg
**
** Purpose: To receive and process messages for TCM application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    CFE_ES_PerfLogEntry
**    CFE_ES_PerfLogExit
**    TCM_ProcessNewCmds
**    TCM_ProcessNewData
**    TCM_SendOutData
**    TCM_SetCDSData
**
** Called By:
**    TCM_Main
**
** Global Inputs/Reads:
**    g_TCM_AppData.SchPipeId
**
** Global Outputs/Writes:
**    g_TCM_AppData.uiRunStatus
**=====================================================================================*/
int32 TCM_RcvMsg(int32 iBlocking)
{
    int32           iStatus=CFE_SUCCESS;
    CFE_SB_Msg_t*   MsgPtr=NULL;
    CFE_SB_MsgId_t  MsgId;
    double          dEnterTime = 0.0;

	LOG_TRACE( "at top of TCM_RcvMsg" );

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(TCM_MAIN_TASK_PERF_ID);

    /* Wait for WakeUp messages from scheduler */
    iStatus = CFE_SB_RcvMsg(&MsgPtr, g_TCM_AppData.SchPipeId, iBlocking);

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(TCM_MAIN_TASK_PERF_ID);

    if (iStatus == CFE_SUCCESS)
    {
        MsgId = CFE_SB_GetMsgId(MsgPtr);
        switch (MsgId)
        {
            case SCH_25HZ_WAKEUP_MID:
                if( perf_get_time_seconds(&dEnterTime)  != PT_SUCCESS )
                {
                    LOG_ERROR("Error setting dEnterTime for exec call");
                }
                TCM_ProcessNewCmds();
                TCM_ProcessNewData();

                LOG_TRACE( "This is a debug print in tcm_app before tcm_exec call" );

                /* Manage test case execution */
                TCM_TestCaseExec();

                /* Check for table load and runtime dump request */
                TCM_CheckStatusOfTables();

                perf_calc_times(dEnterTime,
                                &g_TCM_AppData.prevExecTime,
                                &g_TCM_AppData.OutPacket.fRateTime,
                                &g_TCM_AppData.OutPacket.fElapsedTime
                                );
                g_TCM_AppData.OutPacket.uiCounter++;

                /* Publish new TCM outputs to Message Bus */
                TCM_SendOutData();

                break;

            case SCH_1HZ_SEND_HK_MID:
                TCM_ReportHousekeeping();
                break;

            default:
                CFE_EVS_SendEvent(TCM_MSGID_ERR_EID, CFE_EVS_ERROR,
                                  "TCM - Recvd invalid SCH msgId (0x%08X)", MsgId);
        }
    }
    else if (iStatus == CFE_SB_NO_MESSAGE)
    {
        /* If there's no incoming message, you can do something here, or do nothing */
    }
    else
    {
        /* This is an example of exiting on an error.
        ** Note that a SB read error is not always going to result in an app quitting.
        */
        CFE_EVS_SendEvent(TCM_PIPE_ERR_EID, CFE_EVS_ERROR,
                         "TCM: SB pipe read error (0x%08X), app will exit", (unsigned int)iStatus);
        g_TCM_AppData.uiRunStatus= CFE_ES_APP_ERROR;
    }

    return (iStatus);
}

/*=====================================================================================
** Name: TCM_ProcessNewData
**
** Purpose: To process incoming data subscribed by TCM application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_ProcessNewData()
{
    /* locals */
    int                pipes;
    int                msgsleft;
    CFE_SB_MsgPtr_t    TlmMsgPtr;
    CFE_SB_MsgId_t     TlmMsgId;

    /* process all data pipes */
    for (pipes=0; pipes<TCM_TOTAL_DATAPIPES; pipes++)
    {
        msgsleft = CFE_SB_GetLatestMsg(&TlmMsgPtr,  tcm_pipes[pipes].PipeId);
        if (msgsleft > 0)
        {
            TlmMsgId = CFE_SB_GetMsgId(TlmMsgPtr);
            switch (TlmMsgId)
            {
                case TC_RPOD_01_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sRPOD01_In,TlmMsgPtr,sizeof(TC_RPOD_01_OutPacket_t));
                    break;
                
                case TC_RPOD_02_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sRPOD02_In,TlmMsgPtr,sizeof(TC_RPOD_02_OutPacket_t));
                    break;

                case TC_RPOD_03_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sRPOD03_In,TlmMsgPtr,sizeof(TC_RPOD_03_OutPacket_t));
                    break;

                case TC_RPOD_04_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sRPOD04_In,TlmMsgPtr,sizeof(TC_RPOD_04_OutPacket_t));
                    break;

                case TC_OD_01_A_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sOD01A_In,TlmMsgPtr,sizeof(TC_OD_01_A_OutPacket_t));
                    break;
                
                case TC_OD_01_B_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sOD01B_In,TlmMsgPtr,sizeof(TC_OD_01_B_OutPacket_t));
                    break;

                case TC_OD_02_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sOD02_In,TlmMsgPtr,sizeof(TC_OD_02_OutPacket_t));
                    break;

                case TC_OD_03_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sOD03_In,TlmMsgPtr,sizeof(TC_OD_03_OutPacket_t));
                    break;

                case TC_OD_04_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sOD04_In,TlmMsgPtr,sizeof(TC_OD_04_OutPacket_t));
                    break;

                case TC_PLHA_01_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sPLHA01_In,TlmMsgPtr,sizeof(TC_PLHA_01_OutPacket_t));
                    break;

                case TC_PLHA_02_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sPLHA02_In,TlmMsgPtr,sizeof(TC_PLHA_02_OutPacket_t));
                    break;
                
                case TC_PLHA_03_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sPLHA03_In,TlmMsgPtr,sizeof(TC_PLHA_03_OutPacket_t));
                    break;
                
                case TC_PLHA_04_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sPLHA04_In,TlmMsgPtr,sizeof(TC_PLHA_04_OutPacket_t));
                    break;
                
                case TC_SM_01_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sSM01_In,TlmMsgPtr,sizeof(TC_SM_01_OutPacket_t));
                    break;
                
                case TC_SM_02_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sSM02_In,TlmMsgPtr,sizeof(TC_SM_02_OutPacket_t));
                    break;

                case TC_SM_03_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sSM03_In,TlmMsgPtr,sizeof(TC_SM_03_OutPacket_t));
                    break;

                case TC_SM_04_OUT_DATA_MID:
                    memcpy (&g_TCM_AppData.InData.sSM04_In,TlmMsgPtr,sizeof(TC_SM_04_OutPacket_t));
                    break;

                default:
                    CFE_EVS_SendEvent(TCM_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "TCM Invalid Data Pipe Message, msgId: (0x%08X)", TlmMsgId);
                    break;
            }
        }
    }

}

/*=====================================================================================
** Name: TCM_ProcessNewCmds
**
** Purpose: To process incoming command messages for TCM application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    TCM_ProcessCommandPacket
**
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_ProcessNewCmds()
{
    int iStatus = CFE_SUCCESS;
    CFE_SB_Msg_t*   CmdMsgPtr=NULL;
    CFE_SB_MsgId_t  CmdMsgId;

    /* Process command messages till the pipe is empty */
    while (1)
    {
        iStatus = CFE_SB_RcvMsg(&CmdMsgPtr, g_TCM_AppData.CmdPipeId, CFE_SB_POLL);
        if(iStatus == CFE_SUCCESS)
        {
            CmdMsgId = CFE_SB_GetMsgId(CmdMsgPtr);
            switch (CmdMsgId)
            {
                case TCM_CMD_MID:
                    TCM_ProcessCommandPacket(CmdMsgPtr);
                    break;

                default:
                    CFE_EVS_SendEvent(TCM_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "TCM - Recvd invalid CMD msgId (0x%08X)", CmdMsgId);
                    break;
            }
        }
        else if (iStatus == CFE_SB_NO_MESSAGE)
        {
            break;
        }
        else
        {
            CFE_EVS_SendEvent(TCM_PIPE_ERR_EID, CFE_EVS_ERROR,
                  "TCM: CMD pipe read error (0x%08X)", iStatus);
            g_TCM_AppData.uiRunStatus = CFE_ES_APP_ERROR;
            break;
        }
    }
}

/*=====================================================================================
** Name: TCM_ProcessCommandPacket
**
** Purpose: To process command messages targeting TCM application
**
** Routines Called:
**    CFE_SB_GetCmdCode
**    CFE_EVS_SendEvent
**
** Called By:
**    TCM_ProcessNewCmds
**
** Global Outputs/Writes:
**    g_TCM_AppData.HkTlm.usCmdCnt
**    g_TCM_AppData.HkTlm.usCmdErrCnt
**=====================================================================================*/
void TCM_ProcessCommandPacket(CFE_SB_Msg_t* MsgPtr)
{
    uint32 uiCmdCode = 0;

    if (MsgPtr != NULL)
    {
        uiCmdCode = CFE_SB_GetCmdCode(MsgPtr);
        switch (uiCmdCode)
        {
            case TCM_NOOP_CC:
                g_TCM_AppData.HkTlm.usCmdCnt++;
                CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TCM - Recvd NOOP cmd (%ld)", uiCmdCode);
                break;

            case TCM_RESET_CC:
                g_TCM_AppData.HkTlm.usCmdCnt = 0;
                g_TCM_AppData.HkTlm.usCmdErrCnt = 0;
                CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TCM - Recvd RESET cmd (%ld)", uiCmdCode);
                break;

            case TCM_GET_DOLILU_CC:
                CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TCM: Recvd TCM_GET_DOLILU_CC ground command");
                //ILOAD_GetiLoad(MsgPtr, &tcm_iload_data);
                break;

            case TCM_SET_DOLILU_CC:
                CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TCM: Recvd TCM_SET_DOLILU_CC ground command");
                //ILOAD_SetiLoad(MsgPtr, &tcm_iload_data);
                break;

            default:
                g_TCM_AppData.HkTlm.usCmdErrCnt++;
                CFE_EVS_SendEvent(TCM_MSGID_ERR_EID, CFE_EVS_ERROR, "ERROR - TCM - Recvd invalid cmdId (%ld)", uiCmdCode);
                break;
        }
    }
}

/*====================================================================
** Name:     TCM_CheckStatusOfTables()
**
** Purpose:  Check the status of the I-load and Maneuver tables
**
** Assumptions, External Events, and Notes:
**  1.   NOT called in the Trick-sim instantiation
**
** Routines Called:
**   CFE_TBL_GetStatus()
**   CFE_TBL_Validate()
**   CFE_TBL_ReleaseAddress()
**   CFE_TBL_Update()
**   CFE_TBL_GetAddress()
**   CFE_ES_WriteToSysLog()
**   CFE_EVS_SendEvent()
**
** Global Inputs:
**   TCM_iLoadTblHandle  - Handle to TCM I-load tables
**
** Global Outputs:
**   tcm_iload_data               - TCM's I-loads
**
**====================================================================*/
void TCM_CheckStatusOfTables(void)
{
    int32   Status = CFE_SUCCESS;
    int32   ii     = 0;
    
    /* Loop through all Payload I-loads tables */
    for(ii = 0; ii < TCM_NUM_ILOAD_TBLS; ii++)
    {
        /* Determine if the iload table has a validation or update that needs to be performed */
        Status = CFE_TBL_GetStatus(g_TCM_AppData.ILoadTblHdl[ii]);

        if (Status == CFE_TBL_INFO_VALIDATION_PENDING)
        {
            LOG_INFO( "TCM: Validating the I-Load table %d", ii );
            
            /* Validate the specified table */
            Status = CFE_TBL_Validate(g_TCM_AppData.ILoadTblHdl[ii]);
            if (Status != CFE_SUCCESS)
            {
                CFE_ES_WriteToSysLog("TCM - Failed to validate I-Load table %d (0x%08X)\n", (unsigned int)ii, (unsigned int)Status);
                return;
            }
        }
        else if (Status == CFE_TBL_INFO_UPDATE_PENDING)
        {
            LOG_INFO( "TCM: Updating the I-Load table %d", ii );

            /* release address must be called for update to take */
            CFE_TBL_ReleaseAddress(g_TCM_AppData.ILoadTblHdl[ii]);

            /* Update the iload table */
            CFE_TBL_Update(g_TCM_AppData.ILoadTblHdl[ii]);

            /* Get address of the newly updated iload table */
            CFE_TBL_GetAddress((void*)&g_TCM_AppData.ILoadTblPtr[ii], g_TCM_AppData.ILoadTblHdl[ii]);

            /* Set new parameter values */
            TCM_ProcessNewILoadTbl(ii);

        }
        else if (Status != CFE_SUCCESS)
        {
            CFE_EVS_SendEvent(99, CFE_EVS_ERROR,
                "Unexpected CFE_TBL_GetStatus return (0x%08X) for TCM I-Load Table %d", (unsigned int)Status, (unsigned int)ii);
        }
    }

    return;

}   /* end TCM_CheckStatusOfTables */

/*=====================================================================================
** Name: TCM_ReportHousekeeping
**
** Purpose: To send housekeeping message
**
** Called By:
**    TCM_ProcessNewCmds
**=====================================================================================*/
void TCM_ReportHousekeeping()
{
    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.HkTlm);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.HkTlm);
}

/*=====================================================================================
** Name: TCM_SendOutData
**
** Purpose: To publish 1-Wakeup cycle output data
**
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_SendOutData()
{
    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.OutPacket);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.OutPacket);
}

/*=====================================================================================
** Name: TCM_NextTestCase
**
** Purpose: To incremeent next test case after desired delay in between tests
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_NextTestCase()
{   
    /* Use strikeCount to add delay between tests */
    static int strikeCount  = 0;
    int strikeTarget = g_TCM_AppData.InData.tcm_iload[0].delay_between_tests * 25;

    /* Set to 500 which is 25HZ * 20 seconds */
    if(strikeCount == strikeTarget)
    {
        g_TCM_AppData.StateData.active_test_case++;
        g_TCM_AppData.OutPacket.OutData.test_state.active_test_case = 
                                             g_TCM_AppData.StateData.active_test_case;
        strikeCount = 0;
    }
    else
    {
        /* Set Active in OutData to 0 while we wait for strike count */
        g_TCM_AppData.OutPacket.OutData.test_state.active_test_case = TCM_ID_RESERVED;
        strikeCount++;
    }
}

/*=====================================================================================
** Name: TCM_TestCaseExec
**
** Purpose: Manage test case execution
**
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_TestCaseExec()
{
    boolean bIncrementTest = FALSE;
    static boolean bPrintComplete = TRUE;

    /* All test cases are completed, nothing to do */
    if(g_TCM_AppData.StateData.active_test_case >= NUM_TEST_CASES)
    {
        if(bPrintComplete)
        {
            bPrintComplete = FALSE;
            LOG_INFO("All Test Cases Complete");
        }
        return;
    }

    /* Send command to each app to start test */
    if( g_TCM_AppData.StateData.active_test_case == g_TCM_AppData.StateData.next_test_case)
    {
        TCM_SendCmd(g_TCM_AppData.StateData.next_test_case);
        g_TCM_AppData.StateData.next_test_case++;
    }

    /********************************************************************************
    *  Increment through test cases as then finish                                  *
    ********************************************************************************/
    switch( g_TCM_AppData.StateData.active_test_case )
    {
        case TCM_ID_RPOD_01:
            if(g_TCM_AppData.InData.sRPOD01_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;
        
        case TCM_ID_RPOD_02:
            if(g_TCM_AppData.InData.sRPOD02_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;
        
        case TCM_ID_RPOD_03:
            if(g_TCM_AppData.InData.sRPOD03_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_RPOD_04:
            if(g_TCM_AppData.InData.sRPOD04_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_OD_01_A:
            if(g_TCM_AppData.InData.sOD01A_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;
        
        case TCM_ID_OD_01_B:
            if(g_TCM_AppData.InData.sOD01B_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_OD_02:
            if(g_TCM_AppData.InData.sOD02_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_OD_03:
            if(g_TCM_AppData.InData.sOD03_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_OD_04:
            if(g_TCM_AppData.InData.sOD04_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_PLHA_01:
           if(g_TCM_AppData.InData.sPLHA01_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_PLHA_02:
           if(g_TCM_AppData.InData.sPLHA02_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_PLHA_03:
           if(g_TCM_AppData.InData.sPLHA03_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_PLHA_04:
           if(g_TCM_AppData.InData.sPLHA04_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_SM_01:
           if(g_TCM_AppData.InData.sSM01_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_SM_02:
           if(g_TCM_AppData.InData.sSM02_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_SM_03:
           if(g_TCM_AppData.InData.sSM03_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        case TCM_ID_SM_04:
           if(g_TCM_AppData.InData.sSM04_In.OutData.test_complete == TRUE)
                bIncrementTest = TRUE;
            break;

        default:
            break;
    } // end switch

    if(bIncrementTest)
        TCM_NextTestCase();

} // end TCM_TestCaseExec


/*=====================================================================================
** Name: TCM_SendCmd
**
** Purpose: To publish CI commands to the designated test case app
**
** Called By:
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_SendCmd(TCM_TC_IDS_E test_case)
{
    /********************************************************************************
    *  Process NOOP Commands                                                       *
    ********************************************************************************/
    switch( test_case )
    {
        case TCM_ID_RPOD_01:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_RPOD_01.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_RPOD_01_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_RPOD_01_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;
        
        case TCM_ID_RPOD_02:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_RPOD_02.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_RPOD_02_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_RPOD_02_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;
        
        case TCM_ID_RPOD_03:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_RPOD_03.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_RPOD_03_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_RPOD_03_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_RPOD_04:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_RPOD_04.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_RPOD_04_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_RPOD_04_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_OD_01_A:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_OD_01_A.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_OD_01_A_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_OD_01_A_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_OD_01_B:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_OD_01_B.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_OD_01_B_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_OD_01_B_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_OD_02:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_OD_02.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_OD_02_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_OD_02_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;
        
        case TCM_ID_OD_03:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_OD_03.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_OD_03_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_OD_03_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;
        
        case TCM_ID_OD_04:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_OD_04.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_OD_04_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_OD_04_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_PLHA_01:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_PLHA_01.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_PLHA_01_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_PLHA_01_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;
        
        case TCM_ID_PLHA_02:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_PLHA_02.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_PLHA_02_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_PLHA_02_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_PLHA_03:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_PLHA_03.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_PLHA_03_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_PLHA_03_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_PLHA_04:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_PLHA_04.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_PLHA_04_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_PLHA_04_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_SM_01:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_SM_01.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_SM_01_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_SM_01_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_SM_02:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_SM_02.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_SM_02_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_SM_02_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_SM_03:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_SM_03.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_SM_03_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_SM_03_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        case TCM_ID_SM_04:
            CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "=> Sending Start Cmd from TCM --> TC_SM_04.");
            CFE_SB_InitMsg(&g_TCM_AppData.Cmd, TC_SM_04_CMD_MID, sizeof(g_TCM_AppData.Cmd), TRUE);
            CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            CFE_SB_SetCmdCode((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd, TC_SM_04_START_CC);
            CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TCM_AppData.Cmd);
            break;

        default:
            break;
    } // end switch
} // end TCM_SendCmd

/*=====================================================================================
** Name: TCM_VerifyCmdLength
**
** Purpose: To verify command length for a particular command message
**
** Arguments:
**    CFE_SB_Msg_t*  MsgPtr      - command message pointer
**    uint16         usExpLength - expected command length
**
** Called By:
**    TCM_ProcessNewCmds
**=====================================================================================*/
boolean TCM_VerifyCmdLength(CFE_SB_Msg_t* MsgPtr,
                           uint16 usExpectedLen)
{
    boolean bResult=FALSE;
    uint16  usMsgLen=0;

    if (MsgPtr != NULL)
    {
        usMsgLen = CFE_SB_GetTotalMsgLength(MsgPtr);

        if (usExpectedLen != usMsgLen)
        {
            CFE_SB_MsgId_t MsgId = CFE_SB_GetMsgId(MsgPtr);
            uint16 usCmdCode = CFE_SB_GetCmdCode(MsgPtr);

            CFE_EVS_SendEvent(TCM_MSGLEN_ERR_EID, CFE_EVS_ERROR,
                              "TCM - Rcvd invalid msgLen: msgId=0x%08X, cmdCode=%d, "
                              "msgLen=%d, expectedLen=%d",
                              MsgId, usCmdCode, usMsgLen, usExpectedLen);
            g_TCM_AppData.HkTlm.usCmdErrCnt++;
        }
    }

    return (bResult);
}

/*=====================================================================================
** Name: TCM_AppMain
**
** Purpose: To define TCM application's entry point and main process loop
**
** Routines Called:
**    CFE_ES_RunLoop
**    CFE_ES_ExitApp
**    TCM_InitApp
**    TCM_RcvMsg
**=====================================================================================*/
void TCM_AppMain()
{
    int32  iStatus=CFE_SUCCESS;

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(TCM_MAIN_TASK_PERF_ID);

    /* Perform application initializations */
    if (TCM_InitApp() != CFE_SUCCESS)
    {
        g_TCM_AppData.uiRunStatus = CFE_ES_APP_ERROR;
    }
    else {
        /* Do not perform performance monitoring on startup sync */
        CFE_ES_PerfLogExit(TCM_MAIN_TASK_PERF_ID);
        CFE_ES_WaitForStartupSync(APP_TIMEOUT_DEFAULT_MSEC);
        CFE_ES_PerfLogEntry(TCM_MAIN_TASK_PERF_ID);
    }

    /* Application main loop */
    while (CFE_ES_RunLoop(&g_TCM_AppData.uiRunStatus) == TRUE)
    {
        iStatus = TCM_RcvMsg(CFE_SB_PEND_FOREVER);
    }

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(TCM_MAIN_TASK_PERF_ID);

    /* Exit the application */
    CFE_ES_ExitApp(g_TCM_AppData.uiRunStatus);

    CFE_EVS_SendEvent(TCM_CMD_INF_EID, CFE_EVS_INFORMATION, "End of TCM_AppMain (iStatus = %ld)", iStatus );

}

/**@detail
**======================================================================================
** Name: TCM_InitCDSData
**
** Purpose: To initialize the values being stored in the Critical Data Store (CDS)  <br>
**
** Arguments:             None                                                      <br>
** Returns:               None                                                      <br>
** Called By:             TCM_InitData()                                            <br>
** Routines Called:       CFE_ES_CopyToCDS()                                        <br>
**
** Global Inputs/Reads:   None                                                      <br>
**
** Global Outputs/Writes: g_TCM_AppData.CDSData                                     <br>
**                        g_TCM_AppData.MyCDSHandle                                 <br>
**
** Limitations, Assumptions, External Events, and Notes:                            <br>
**    1. CDS is enabled, and initial creation was successful.                       <br>
**
**======================================================================================
*/
void TCM_InitCDSData()
{
    /* Update CDS staging area with initial values */
    // TODO: Add initial values

    /* Copy the staged data to CDS-proper (if CDS Creation was successful) */
    if (g_TCM_AppData.CDSState == TRUE)
    {
        CFE_ES_CopyToCDS( g_TCM_AppData.MyCDSHandle, &g_TCM_AppData.CDSData );
    }

    return;
} /* end TCM_InitCDSData */

/**@detail
**======================================================================================
** Name: TCM_SetCDSData
**
** Purpose: To set the values being stored in the Critical Data Store (CDS)        <br>
**
** Arguments:             None                                                      <br>
** Returns:               None                                                      <br>
** Called By:             TCM_RecvMsg()                                             <br>
** Routines Called:       CFE_ES_CopyToCDS()                                        <br>
**
** Global Inputs/Reads:   g_TCM_AppData.CDSState                                    <br>
**                        g_TCM_AppData.OutPacket.OutData                           <br>
**                        g_TCM_AppData.InData                                      <br>
**                        g_TCM_AppData.StateData                                   <br>
**
** Global Outputs/Writes: g_TCM_AppData.CDSData                                     <br>
**                        g_TCM_AppData.MyCDSHandle                                 <br>
**
** Limitations, Assumptions, External Events, and Notes:                            <br>
**    1. CDS is enabled, and initial creation was successful.                       <br>
**
**======================================================================================
*/
void TCM_SetCDSData()
{
    /* Update CDS staging area with latest values */
    // TODO: Add latest values

    /* Copy the staged data to CDS-proper (if CDS Creation was successful) */
    if (g_TCM_AppData.CDSState == TRUE)
    {
        CFE_ES_CopyToCDS( g_TCM_AppData.MyCDSHandle, &g_TCM_AppData.CDSData );
    }
    return;
} /* end TCM_SetCDSData */

/*=======================================================================================
** End of file tcm_app.c
**=====================================================================================*/


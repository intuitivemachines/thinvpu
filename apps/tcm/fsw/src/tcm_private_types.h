// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_private_types.h
**
** Title:    Type Header File for TCM Application
**
** Author:   Scott Tamblyn
**
** Purpose:  This header file contains declarations and definitions of all TCM's private
**           data structures and data types.
**
**=====================================================================================*/
    
#ifndef _TCM_PRIVATE_TYPES_H_
#define _TCM_PRIVATE_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"
#include "tcm_types.h"
#include "tc_rpod_01_public.h"
#include "tc_rpod_02_public.h"
#include "tc_rpod_03_public.h"
#include "tc_rpod_04_public.h"
#include "tc_od_01_a_public.h"
#include "tc_od_01_b_public.h"
#include "tc_od_02_public.h"
#include "tc_od_03_public.h"
#include "tc_od_04_public.h"
#include "tc_plha_01_public.h"
#include "tc_plha_02_public.h"
#include "tc_plha_03_public.h"
#include "tc_plha_04_public.h"
#include "tc_sm_01_public.h"
#include "tc_sm_02_public.h"
#include "tc_sm_03_public.h"
#include "tc_sm_04_public.h"
/*
** Local Defines
*/
typedef enum {
    TCM_DATAPIPE_RPOD_01 = 0,
    TCM_DATAPIPE_RPOD_02 = 1,
    TCM_DATAPIPE_RPOD_03 = 2,
    TCM_DATAPIPE_RPOD_04 = 3,
    TCM_DATAPIPE_OD_01_A = 4,
    TCM_DATAPIPE_OD_01_B = 5,
    TCM_DATAPIPE_OD_02   = 6,
    TCM_DATAPIPE_OD_03   = 7,
    TCM_DATAPIPE_OD_04   = 8,
    TCM_DATAPIPE_PLHA_01 = 9,
    TCM_DATAPIPE_PLHA_02 = 10,
    TCM_DATAPIPE_PLHA_03 = 11,
    TCM_DATAPIPE_PLHA_04 = 12,
    TCM_DATAPIPE_SM_01   = 13,
    TCM_DATAPIPE_SM_02   = 14,
    TCM_DATAPIPE_SM_03   = 15,
    TCM_DATAPIPE_SM_04   = 16,
    TCM_TOTAL_DATAPIPES
} tcm_pipes_enum;

/*
** Local Structure Declarations
*/

typedef struct
{
    uint32  counter;

    TCM_EXTL_CMD_T       tcm_extl_cmd;
    TCM_ILOAD_T          tcm_iload[TCM_NUM_ILOAD_TBLS];
    TC_RPOD_01_OutPacket_t      sRPOD01_In;
    TC_RPOD_02_OutPacket_t      sRPOD02_In;
    TC_RPOD_03_OutPacket_t      sRPOD03_In;
    TC_RPOD_04_OutPacket_t      sRPOD04_In;
    TC_OD_01_A_OutPacket_t      sOD01A_In;
    TC_OD_01_B_OutPacket_t      sOD01B_In;
    TC_OD_02_OutPacket_t        sOD02_In;
    TC_OD_03_OutPacket_t        sOD03_In;
    TC_OD_04_OutPacket_t        sOD04_In;
    TC_PLHA_01_OutPacket_t      sPLHA01_In;
    TC_PLHA_02_OutPacket_t      sPLHA02_In;
    TC_PLHA_03_OutPacket_t      sPLHA03_In;
    TC_PLHA_04_OutPacket_t      sPLHA04_In;
    TC_SM_01_OutPacket_t        sSM01_In;
    TC_SM_02_OutPacket_t        sSM02_In;
    TC_SM_03_OutPacket_t        sSM03_In;
    TC_SM_04_OutPacket_t        sSM04_In;
} TCM_InData_t;


#ifdef __cplusplus
}
#endif

#endif /* _TCM_PRIVATE_TYPES_H_ */

/*=======================================================================================
** End of file tcm_private_types.h
**=====================================================================================*/
    

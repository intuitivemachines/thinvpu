// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
**
** Title:  Header File for TCM Application
** Purpose:  To define TCM's internal macros, data types, global variables and
**           function prototypes
**
**=====================================================================================*/

#ifndef _TCM_APP_H_
#define _TCM_APP_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "sch_app_rates.h"

#include "tcm_public.h"
#include "tcm_mission_cfg.h"
#include "tcm_iloads_tbldefs.h"
#include "tcm_iload_utils.h"
#include "common/utils/perf.h"


/*
** Local Defines
*/
#define TCM_SCH_PIPE_DEPTH  10
#define TCM_CMD_PIPE_DEPTH  10
#define TCM_TLM_PIPE_DEPTH  10

/*
** TCM CDS Buffer String Name
*/
#define TCM_CDSNAME      "TCM_CDS"

/*
** Local Structure Declarations
*/
typedef struct
{
    /* CFE Event table */
    CFE_EVS_BinFilter_t  EventTbl[TCM_EVT_CNT];

    /* CFE scheduling pipe */
    CFE_SB_PipeId_t      SchPipeId;
    uint16               usSchPipeDepth;
    char                 cSchPipeName[OS_MAX_API_NAME];

    /* CFE command pipe */
    CFE_SB_PipeId_t      CmdPipeId;
    uint16               usCmdPipeDepth;
    char                 cCmdPipeName[OS_MAX_API_NAME];

    /* CFE telemetry pipe */
    CFE_SB_PipeId_t      TlmPipeId;
    uint16               usTlmPipeDepth;
    char                 cTlmPipeName[OS_MAX_API_NAME];

    /* Critical Data Store (CDS) data */
    CFE_ES_CDSHandle_t   MyCDSHandle; /*< \brief Handle to CDS memory block      */
    TCM_CDS_RESTART_T    CDSData;     /*< \brief Copy of Critical Data           */
    uint8                CDSState;    /*< \brief Status of Critical Data Storing */

    /* Task-related */
    uint32  uiRunStatus;
    double  prevExecTime;

    /* ILoad table-related */
    CFE_TBL_Handle_t     ILoadTblHdl[TCM_NUM_ILOAD_TBLS];
    TCM_ILoadTblEntry_t* ILoadTblPtr[TCM_NUM_ILOAD_TBLS];

    /* Input data - from I/O devices or subscribed from other apps' output data.
       Data structure should be defined in tcm/fsw/mission_inc/tcm_private_types.h */
    TCM_InData_t         InData;

    /* Output data - to be published at the end of a Wakeup cycle.
       Data structure should be defined in tcm/fsw/platform_inc/tcm_platform_cfg.h */
    TCM_OutPacket_t      OutPacket;

    /* Housekeeping telemetry - for downlink only.
       Data structure should be defined in tcm/fsw/platform_inc/tcm_platform_cfg.h */
    TCM_HkTlm_t          HkTlm;

    /* Add declarations for additional private data here */
    TCM_INTERNAL_T       StateData;

    /* Command Types */
    TCM_NoArgCmd_t       Cmd;

} TCM_AppData_t;

/*
** Local Function Prototypes
*/
int32  TCM_InitApp(void);
int32  TCM_InitEvent(void);
int32  TCM_InitData(void);
int32  TCM_InitPipe(void);

void  TCM_AppMain(void);

void  TCM_CleanupCallback(void);

int32  TCM_RcvMsg(int32 iBlocking);

void  TCM_ProcessNewData(void);
void  TCM_ProcessNewCmds(void);
void  TCM_ProcessCommandPacket(CFE_SB_Msg_t*);

void  TCM_ReportHousekeeping(void);
void  TCM_SendOutData(void);
void  TCM_SendCmd(TCM_TC_IDS_E test_case);

void  TCM_InitCDSData(void);
void  TCM_SetCDSData(void);

void  TCM_CheckStatusOfTables(void);

boolean  TCM_VerifyCmdLength(CFE_SB_Msg_t*, uint16);
void TCM_NextTestCase();

#ifdef __cplusplus
}
#endif

#endif /* _TCM_APP_H_ */

/*=======================================================================================
** End of file tcm_app.h
**=====================================================================================*/


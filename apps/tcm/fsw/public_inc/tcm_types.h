// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
**
** Purpose:
**    Contains enumerations, macros and structure definitions for the top-level TCM executive.
**    which are also used by the rest of GN&C.
**
**============================================================================== */

#ifndef _tcm_types_h_
#define _tcm_types_h_

#include "common_types.h"
#include "common/utils/error.h"


#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************
 ** IDENTIFIERS                                               **
 ***************************************************************/

#define MAX_NUM_TRIGGERS      32    /* Maximum number of triggers per test case app           */
#define TCM_NUM_ILOAD_TBLS     1    /* Number of Test Case I-loads tables                   */

/* Note - since there will be a mapping of a test case's cmd ID to their bytestrings
 *        need to make sure these power commands to the PACOIO are unique and do not collide.
 */
#define TCM_CMD_NONE           0    /* TCM identifier for "No Command"                      */
#define TCM_CMD_NOOP          -1    /* TCM identifier for "No-Op Command"                   */


/***************************************************************
 ** TYPEDEFINED ENUMERATIONS                                  **
 ***************************************************************/

/* Commandable Payload App IDs */
/* (Note - enumerated for array use - this order is CRITICAL as it how the I-load tables are ordered in tcm_iloads_tbldefs.h) */
typedef enum
 {
   TCM_ID_RESERVED          = 0,    /* RESERVED                          */
   TCM_ID_RPOD_01           = 1,    /* TC_RPOD_01                        */
   TCM_ID_RPOD_02           = 2,    /* TC_RPOD_02                        */
   TCM_ID_RPOD_03           = 3,    /* TC_RPOD_03                        */
   TCM_ID_RPOD_04           = 4,    /* TC_RPOD_04                        */
   TCM_ID_OD_01_A           = 5,    /* TC_OD_01_A                        */
   TCM_ID_OD_01_B           = 6,    /* TC_OD_01_B                        */
   TCM_ID_OD_02             = 7,    /* TC_OD_02                          */
   TCM_ID_OD_03             = 8,    /* TC_OD_03                          */
   TCM_ID_OD_04             = 9,    /* TC_OD_04                          */
   TCM_ID_PLHA_01           = 10,   /* TC_PLHA_01                        */
   TCM_ID_PLHA_02           = 11,   /* TC_PLHA_02                        */
   TCM_ID_PLHA_03           = 12,   /* TC_PLHA_03                        */
   TCM_ID_PLHA_04           = 13,   /* TC_PLHA_04                        */
   TCM_ID_SM_01             = 14,   /* TC_SM_01                          */
   TCM_ID_SM_02             = 15,   /* TC_SM_02                          */
   TCM_ID_SM_03             = 16,   /* TC_SM_03                          */
   TCM_ID_SM_04             = 17,   /* TC_SM_04                          */
   NUM_TEST_CASES
 } TCM_TC_IDS_E;

/***************************************************************
 ** TYPEDEFINED DATA STRUCTURES                               **
 ***************************************************************/

/*! TCM Critical Data Store (CDS) Restart Data */
typedef struct tcm_types
 {
   /* TBD data */
 } TCM_CDS_RESTART_T; 


/*! TCM External CFS Commands & Inputs */
typedef struct
 {
   uint32                 id;                                          /*!< --  Command ID sCommandCode from TCM CFS Cmd Pkt      */
   uint32                 argc;                                        /*!< --  Integer argument #1 from TCM CFS Command Packet   */
   uint32                 argc2;                                       /*!< --  Integer argument #2 from TCM CFS Command Packet   */
 } TCM_EXTL_CMD_T;

/*! TCM I-loads for Payload: Generic - template for each Payload's I-loads */
typedef struct
 {
   uint32                  delay_between_tests;                         /*!< -- I-loads table ID                                   */
 } TCM_ILOAD_T;

/*! TCM State Data */
typedef struct
 {
   TCM_TC_IDS_E           active_test_case;                            /*!< -- Active Test Case                                   */
   TCM_TC_IDS_E           next_test_case;                              /*!< -- Next Test Case                                     */
 } TCM_STATE_T;

typedef TCM_STATE_T TCM_INTERNAL_T;                                    /*!< -- Also allow alternate name                          */

/* TCM Housekeeping Data */
typedef struct 
 {
   float  time;                                                        /*!< s  Time of current data set                           */
   uint32 spare;
 } TCM_HK_T;


/*! TCM Outputs */
/*! Note: All enumerated members' typdefs are noted in the comments.
 *        Primitive data types are used instead, to conserve memory.
 */
typedef struct
 {
   /* TCM output/telemetry data */
   double                 MET_time;                                    /*!< s  Current MET time relative to vehicle deploy        */
   TCM_STATE_T            test_state;                                              /*!< -- Test case info                                     */
 } TCM_OUT_T;

#ifdef __cplusplus
}
#endif

#endif

/* ---------- end of tcm_types.h ----------*/
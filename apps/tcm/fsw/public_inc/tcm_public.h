// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_public.h
**
** Title:  Public Header File for TCM Application
**
** Author:   Scott Tamblyn
**
** Purpose:  This header file contains declartions and definitions of all TCM's 
**           public structs.
**
**=====================================================================================*/
    
#ifndef _TCM_PUBLIC_H_
#define _TCM_PUBLIC_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"
#include "tcm_types.h"

/*
** Local Structure Declarations
*/
typedef struct
{
    uint8  ucCmdHeader[CFE_SB_CMD_HDR_SIZE];
} TCM_NoArgCmd_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint16  usCmdCnt;
    uint16  usCmdErrCnt;

    TCM_HK_T  HkData;
} TCM_HkTlm_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint32  uiCounter;
    float   fElapsedTime;
    float   fRateTime;

    TCM_OUT_T  OutData;
} TCM_OutPacket_t;

#ifdef __cplusplus
}
#endif

#endif /* _TCM_PUBLIC_H_ */

/*=======================================================================================
** End of file tcm_public.h
**=====================================================================================*/
    

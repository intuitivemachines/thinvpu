// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_private_ids.h
**
** Title:  ID Header File for TCM Application
**
** $Author:    Scott Tamblyn
** $Date:      2015-01-07
**
** Purpose:  This header file contains declarations and definitions of TCM's private IDs.
**
** Modification History:
**   Date       | Author        | Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Scott Tamblyn |   --     | Code Started
**   2015-10-26 |Butcher/Tamblyn|  0589    | TCM CDS storage for computer reset FDIR (initial prototype from Butcher)
**
**=====================================================================================*/
    
#ifndef _TCM_PRIVATE_IDS_H_
#define _TCM_PRIVATE_IDS_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** These message Ids are defined in $(CFS_APP_SRC)/inc/MISSION_cmd_ids.h.
** Note that the header file is auto-generated from the Command & Data Dictionary (CDD).
**     TCM_CMD_MID
**     TCM_SEND_HK_MID
**     TCM_WAKEUP_MID
**
** These command code Ids are defined in $(CFS_APP_SRC)/inc/MISSION_cmd_codes.h.
** Note that the header file is auto-generated from the CDD.
**     TCM_NOOP_CC
**     TCM_RESET_CC
**
** These message Ids are defined in $(CFS_APP_SRC)/inc/MISSION_tlm_ids.h.
** Note that the header file is auto-generated from the CDD.
**     TCM_HK_TLM_MID
**     TCM_OUT_DATA_MID
**
** These performance Ids are defined in $(CFS_APP_SRC)/inc/MISSION_perf_ids.h.
**     TCM_MAIN_TASK_PERF_ID
**     TCM_XXX_PERF_ID
*/

/* Event IDs */
#define TCM_RESERVED_EID         0

#define TCM_INF_EID              1
#define TCM_INIT_INF_EID         2
#define TCM_ILOAD_INF_EID        3
#define TCM_CDS_INF_EID          4
#define TCM_CMD_INF_EID          5

#define TCM_ERR_EID             51
#define TCM_INIT_ERR_EID        52
#define TCM_ILOAD_ERR_EID       53
#define TCM_CDS_ERR_EID         54
#define TCM_CMD_ERR_EID         55
#define TCM_PIPE_ERR_EID        56
#define TCM_MSGID_ERR_EID       57
#define TCM_MSGLEN_ERR_EID      58
#define TCM_CDS_RESTORE_ERR_EID 59
#define TCM_CDS_CORRUPT_ERR_EID 60

#define TCM_EVT_CNT  16


#ifdef __cplusplus
}
#endif

#endif /* _TCM_PRIVATE_IDS_H_ */

/*=======================================================================================
** End of file tcm_private_ids.h
**=====================================================================================*/
    

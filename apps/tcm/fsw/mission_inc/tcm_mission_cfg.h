// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_mission_cfg.h
**
** Title:  Mission Configuration Header File for TCM Application
**
** $Author:    Scott Tamblyn
** $Date:      2015-01-07
**
** Purpose:  This header file contains declartions and definitions of all TCM's
**           mission-specific configurations.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2015-01-07 | Scott Tamblyn | Build #: Code Started
**
**=====================================================================================*/

#ifndef _TCM_MISSION_CFG_H_
#define _TCM_MISSION_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"

#include "MISSION_perf_ids.h"
#include "MISSION_cmd_ids.h"
#include "MISSION_cmd_codes.h"
#include "MISSION_tlm_ids.h"

#include "tcm_private_ids.h"
#include "tcm_private_types.h"


#ifdef __cplusplus
}
#endif

#endif /* _TCM_MISSION_CFG_H_ */

/*=======================================================================================
** End of file tcm_mission_cfg.h
**=====================================================================================*/


// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_iload_utils.c
**
** Title:  Iload Tables' Utilities for TCM Application
**
** Purpose:  This source file contains definitions of ILoad table-related utility
**           function for TCM application.
**
** Limitations, Assumptions, External Events, and Notes:
**    1. One source file per CFS table!
**
**=====================================================================================*/

/*
** Include Files
*/
#include "tcm_app.h"
#include "tcm_iload_utils.h"

/*
** External Global Variables
*/
extern TCM_AppData_t  g_TCM_AppData;


/*
** Function Definitions
*/
    
/*=====================================================================================
** Name: TCM_InitILoadTbl
**
** Purpose: To initialize the TCM's ILoad tables
**
** Routines Called:
**    CFE_TBL_Register
**    CFE_TBL_Load
**    CFE_TBL_Manage
**    CFE_TBL_GetAddress
**    TCM_ValidateILoadTbl
**    TCM_ProcessNewILoadTbl
**=====================================================================================*/
int32 TCM_InitILoadTbl()
{
    int32 iStatus = CFE_SUCCESS;
    int32 ii      = 0;

    /* Loop over all I-loads tables */ 
    for( ii = 0; ii < TCM_NUM_ILOAD_TBLS; ii++ )
    {
        /* Register I-Load table */
        iStatus = CFE_TBL_Register(&g_TCM_AppData.ILoadTblHdl[ii],                           // Table Handle
                                   g_TCM_iload_tbl_name[ii],                                 // Table Name
                                   (sizeof(TCM_ILoadTblEntry_t)),                            // Table Size
                                   CFE_TBL_OPT_DEFAULT,                                      // Table Option (default)
                                   (void*)TCM_ValidateILoadTbl);                             // Local Validation() function to call 
        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to register I-Load table %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }

        /* Load I-Load TBL file into the Registered CFS Table Name (ILoadTblHdl) */
        iStatus = CFE_TBL_Load(g_TCM_AppData.ILoadTblHdl[ii],
                               CFE_TBL_SRC_FILE,
                               g_TCM_iload_tbl_filename[ii]);
        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to load I-Load tbl file %s (Err 0x%08X)\n", g_TCM_iload_tbl_filename[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }
        else
        {
            LOG_INFO("TCM tbl file successfully loaded: %s ", g_TCM_iload_tbl_filename[ii]);
        }

        /* Manage I-Load table */
        iStatus = CFE_TBL_Manage(g_TCM_AppData.ILoadTblHdl[ii]);
        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to manage I-Load table %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }

        /* Make sure I-Load table is accessible by getting reference to it */
        iStatus = CFE_TBL_GetAddress((void *)(&g_TCM_AppData.ILoadTblPtr[ii]), g_TCM_AppData.ILoadTblHdl[ii]);
        if (iStatus != CFE_TBL_INFO_UPDATED)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to get I-Load table address for %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }

        /* Validate I-Load table (verifies updated TblPtr) */
        iStatus = TCM_ValidateILoadTbl(g_TCM_AppData.ILoadTblPtr[ii]);
        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to validate I-Load table %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }

        /* Set new parameter values */
        iStatus = TCM_ProcessNewILoadTbl(ii);
        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TCM - Failed to set I-Load param values in table  %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[ii], (unsigned int)iStatus);
            goto TCM_InitILoadTbl_Exit_Tag;
        }

    }

TCM_InitILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TCM_ReInitILoadTbl
**
** Purpose: To re-initialize the TCM's ILoad tables
**
** Routines Called:
**    CFE_TBL_ReleaseAddress
**    CFE_TBL_Load
**    CFE_TBL_Validate   (registered to call TCM_ValidateILoadTbl)
**    CFE_TBL_GetAddress
**    TCM_ProcessNewILoadTbl
**=====================================================================================*/
int32 TCM_ReInitILoadTbl( uint32 table_id )
{
    int32 iStatus = CFE_SUCCESS;

    /* Release address (to unlock table - required before re-loading)  */
    CFE_TBL_ReleaseAddress(g_TCM_AppData.ILoadTblHdl[table_id]);

    /* Load Specified I-Load TBL file into the Registered CFS Table Name (ILoadTblHdl) */
    /* (Note - CFS Table is already registered at this point) */
    iStatus = CFE_TBL_Load(g_TCM_AppData.ILoadTblHdl[table_id],
                           CFE_TBL_SRC_FILE,
                           g_TCM_iload_tbl_filename[table_id]);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to load I-Load tbl file %s (Err 0x%08X)\n", g_TCM_iload_tbl_filename[table_id], (unsigned int)iStatus);
        goto TCM_ReInitILoadTbl_Exit_Tag;
    }

    /* Get/Update address of the newly updated table */
    iStatus = CFE_TBL_GetAddress((void *)(&g_TCM_AppData.ILoadTblPtr[table_id]), g_TCM_AppData.ILoadTblHdl[table_id]);
    if (iStatus != CFE_TBL_INFO_UPDATED)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to get I-Load table address for %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[table_id], (unsigned int)iStatus);
        goto TCM_ReInitILoadTbl_Exit_Tag;
    }

    /* Validate I-Load table (verifies updated TblPtr) */
    iStatus = TCM_ValidateILoadTbl(g_TCM_AppData.ILoadTblPtr[table_id]);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to validate I-Load table %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[table_id], (unsigned int)iStatus);
        goto TCM_ReInitILoadTbl_Exit_Tag;
    }

    /* Set new parameter values */
    iStatus = TCM_ProcessNewILoadTbl(table_id);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TCM - Failed to set I-Load param values in table  %s (Err 0x%08X)\n", g_TCM_iload_tbl_name[table_id], (unsigned int)iStatus);
        goto TCM_ReInitILoadTbl_Exit_Tag;
    }

TCM_ReInitILoadTbl_Exit_Tag:
    return (iStatus);
}

/*=====================================================================================
** Name: TCM_ValidateILoadTbl
**
** Purpose: To validate the TCM's ILoad tables
**
** Arguments:
**    TCM_ILoadTblEntry_t*  iLoadTblPtr - pointer to the ILoad table
**=====================================================================================*/
int32 TCM_ValidateILoadTbl(TCM_ILoadTblEntry_t* iLoadTblPtr)
{
    int32  iStatus = CFE_SUCCESS;

    if (iLoadTblPtr == NULL)
    {
        iStatus = -1;
        goto TCM_ValidateILoadTbl_Exit_Tag;
    }
    // TODO: Add validation logic

TCM_ValidateILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TCM_ProcessNewILoadTbl
**
** Purpose: To process TCM's new ILoad tables and set ILoad parameters with new values
**=====================================================================================*/
int32 TCM_ProcessNewILoadTbl(uint32 table_id)
{
    int32 iStatus = 0;
    /*
    ** Process new iload table...
    */
    if (table_id >= 0 && table_id < TCM_NUM_ILOAD_TBLS)
    {
        memcpy(&g_TCM_AppData.InData.tcm_iload[table_id], &g_TCM_AppData.ILoadTblPtr[table_id]->iloads, sizeof(TCM_ILOAD_T));
      //LOG_INFO("g_TCM_AppData.InData.tcm_iload[%d].iload_table_self_id = %d", table_id, g_TCM_AppData.InData.tcm_iload[table_id].iload_table_self_id);
    }
    else
    {
        iStatus = -1;
    }
    
    return iStatus;
}
    
/*=======================================================================================
** End of file tcm_iload_utils.c
**=====================================================================================*/
    

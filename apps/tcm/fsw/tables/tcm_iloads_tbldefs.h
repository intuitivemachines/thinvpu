// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_iloads_tbldefs.h
**
** Title:    Header File for TCM Application's "iloads" tables
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TCM's "iloads" tables.
**=====================================================================================*/

#ifndef _TCM_ILOADS_TBLDEFS_H_
#define _TCM_ILOADS_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tcm_mission_cfg.h"
#include "tcm_types.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/
/* Definition for Iload table entry */
typedef struct
{
  TCM_ILOAD_T iloads;
} TCM_ILoadTblEntry_t;

/*
** Global Variables
*/
/* I-loads Table-Filenames */
static char *g_TCM_iload_tbl_filename[TCM_NUM_ILOAD_TBLS] =
                /* [0] */  {  "/cf/apps/tcm_iloads.tbl"    };

/* I-loads Table Names (to be Registered) */
/* Notes - need to keep tablenames less than 17 characters in length, including trailing null character 
 *       - these Table Names need to be the ones specified in the I-load .c files for the "TblName" (2nd arg in the CFE_TBL_FileDef)
 */
static char *g_TCM_iload_tbl_name[TCM_NUM_ILOAD_TBLS] =
                /* [0] */  {  "tcm_iloads"                  };


#endif /* _TCM_ILOADS_TBLDEFS_H_ */

/*=======================================================================================
** End of file tcm_iloads_tbldefs.h
**=====================================================================================*/

// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tcm_iload_utils.h
**
** Title:     I-Load Tables' Utility Header File for TCM Application
**
** Purpose:  To define TCM's ILoad table-related utility functions
**
**=====================================================================================*/

#ifndef _TCM_ILOAD_UTILS_H_
#define _TCM_ILOAD_UTILS_H_

/*
** Include Files
*/
#include "tcm_iloads_tbldefs.h"

/*
** Local Function Prototypes
*/
int32  TCM_InitILoadTbl(void);
int32  TCM_ReInitILoadTbl(uint32 table_id);
int32  TCM_ValidateILoadTbl(TCM_ILoadTblEntry_t*);
int32  TCM_ProcessNewILoadTbl(uint32 table_id);

#endif /* _TCM_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tcm_iload_utils.h
**=====================================================================================*/

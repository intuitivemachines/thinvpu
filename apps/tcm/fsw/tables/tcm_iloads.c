// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
** Name:   tcm_iloads 
**         Note: this file is derived from the TRV tcm_iloads_from_code_table0
**
** Purpose:                                                                 <br>
** To load (initialize) the I-load values into the TCM I-load structure,    <br>
** for use by the TCM Executive.
**
** Limitations, Assumptions, External Events, and Notes:                    <br>
**  - I-load values should be provided in metric units.                     <br>
**
** Algorithm:                                                               <br>
**  - This routine assigns values into the TCM I-load structure,            <br>
**    the address of which is provided as an input argument.                <br>
**
**==============================================================================
*/


/* 
** Include files 
*/
#include "cfe_tbl_filedef.h"
#include "common_types.h"
#include "tcm_iloads_tbldefs.h"

/*
** Table file header
*/
static CFE_TBL_FileDef_t CFE_TBL_FileDef =
{
    /* Content format: ObjName[64], TblName[38], Desc[32], TgtFileName[20], ObjSize 
    **    ObjName - variable name of ILoad table, e.g., TCM_ILoadDefTbl[]
    **    TblName - app's table name, e.g., TCM.ILOAD_TBL, where TCM is the same app name
    **              used in cfe_es_startup.scr, and TCM_defILoadTbl is the same table
    **              name passed in to CFE_TBL_Register()
    **    Desc - description of table in string format
    **    TgtFileName[20] - table file name, compiled as .tbl file extension
    **    ObjSize - size of the entire table
    */

    "tcm_iLoadTable", "TCM.tcm_iloads", "TCM iLoads table",
    "tcm_iloads.tbl", (sizeof(TCM_ILoadTblEntry_t))

};

/*
** Default TCM iLoad table data
*/
/**@detail
*/
TCM_ILoadTblEntry_t tcm_iLoadTable =
{
/****************************************************************
 ** I-LOADS SELF ID                                            **
 ****************************************************************/
.iloads.delay_between_tests = 20    /* Seconds  between tests */

};
/* ---------- end of tcm_iloads_LN1.c ----------*/

// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_sm_04_private_ids.h
**
** Title:  ID Header File for the TC_SM_04 Application
**
** $Author:    Brian Butcher
** $Date:      2015-01-07
**
** Purpose:  This header file contains declarations and definitions of the TC_SM_04's private IDs.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_SM_04_PRIVATE_IDS_H_
#define _TC_SM_04_PRIVATE_IDS_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** These message Ids are defined in $(CFS_APP_SRC)/inc/MISSION_cmd_ids.h.
** Note that the header file is auto-generated from the Command & Data Dictionary (CDD).
**     TC_SM_04_CMD_MID
**     TC_SM_04_SEND_HK_MID
**     TC_SM_04_WAKEUP_MID
**
** These command code Ids are defined in $(CFS_APP_SRC)/inc/MISSION_cmd_codes.h.
** Note that the header file is auto-generated from the CDD.
**     TC_SM_04_NOOP_CC
**     TC_SM_04_RESET_CC
**
** These message Ids are defined in $(CFS_APP_SRC)/inc/MISSION_tlm_ids.h.
** Note that the header file is auto-generated from the CDD.
**     TC_SM_04_HK_TLM_MID
**     TC_SM_04_OUT_DATA_MID
**
** These performance Ids are defined in $(CFS_APP_SRC)/inc/MISSION_perf_ids.h.
**     TC_SM_04_MAIN_TASK_PERF_ID
**     TC_SM_04_XXX_PERF_ID
*/

/* Event IDs */
#define TC_SM_04_RESERVED_EID  0

#define TC_SM_04_INF_EID        1
#define TC_SM_04_INIT_INF_EID   2
#define TC_SM_04_ILOAD_INF_EID  3
#define TC_SM_04_CDS_INF_EID    4
#define TC_SM_04_CMD_INF_EID    5

#define TC_SM_04_ERR_EID         51
#define TC_SM_04_INIT_ERR_EID    52
#define TC_SM_04_ILOAD_ERR_EID   53
#define TC_SM_04_CDS_ERR_EID     54
#define TC_SM_04_CMD_ERR_EID     55
#define TC_SM_04_PIPE_ERR_EID    56
#define TC_SM_04_MSGID_ERR_EID   57
#define TC_SM_04_MSGLEN_ERR_EID  58

#define TC_SM_04_EVT_CNT  14


#ifdef __cplusplus
}
#endif

#endif /* _TC_SM_04_PRIVATE_IDS_H_ */

/*=======================================================================================
** End of file tc_sm_04_private_ids.h
**=====================================================================================*/
    

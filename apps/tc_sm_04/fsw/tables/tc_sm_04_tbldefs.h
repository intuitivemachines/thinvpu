/*=======================================================================================
** File Name:  tc_sm_04_tbldefs.h
**
** Title:  Header File for TC_SM_04 Application's tables
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TC_SM_04's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_SM_04_TBLDEFS_H_
#define _TC_SM_04_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_sm_04_mission_cfg.h"

/*
** Local Defines
*/
#define TC_SM_04_ILOAD_MAX_ENTRIES  1
#define TC_SM_04_ILOAD_FILENAME     "/cf/apps/tc_sm_04_tbl.tbl"
#define TC_SM_04_ILOAD_TABLENAME    "tc_sm_04_tbl"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    TC_SM_04_ILOAD_T iloads;
} TC_SM_04_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _TC_SM_04_TBLDEFS_H_ */

/*=======================================================================================
** End of file tc_sm_04_tbldefs.h
**=====================================================================================*/
    

// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_sm_04_iload_utils.c
**
** Title:  Iload Tables' Utilities for TC_SM_04 Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This source file contains definitions of ILoad table-related utility
**           function for TC_SM_04 application.
**
** Limitations, Assumptions, External Events, and Notes:
**    1. One source file per CFS table!
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/

/*
** Include Files
*/
#include "tc_sm_04_app.h"
#include "tc_sm_04_iload_utils.h"

/*
** External Global Variables
*/
extern TC_SM_04_AppData_t  g_TC_SM_04_AppData;

/*
** Function Definitions
*/
    
/*=====================================================================================
** Name: TC_SM_04_InitILoadTbl
**
** Purpose: To initialize the TC_SM_04's ILoad tables
**
** Routines Called:
**    CFE_TBL_Register
**    CFE_TBL_Load
**    CFE_TBL_Manage
**    CFE_TBL_GetAddress
**    CFE_ES_WriteToSysLog
**    TC_SM_04_ValidateILoadTbl
**    TC_SM_04_ProcessNewILoadTbl
**=====================================================================================*/
int32 TC_SM_04_InitILoadTbl()
{
    int32  iStatus=0;

    /* Register ILoad table */
    iStatus = CFE_TBL_Register(&g_TC_SM_04_AppData.ILoadTblHdl,
                               TC_SM_04_ILOAD_TABLENAME,
                               (sizeof(TC_SM_04_ILoadTblEntry_t) * TC_SM_04_ILOAD_MAX_ENTRIES),
                               CFE_TBL_OPT_DEFAULT,
                               TC_SM_04_ValidateILoadTbl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_04 - Failed to register ILoad table (0x%08X)\n", iStatus);
        goto TC_SM_04_InitILoadTbl_Exit_Tag;
    }

    /* Load ILoad table file */
    iStatus = CFE_TBL_Load(g_TC_SM_04_AppData.ILoadTblHdl,
                           CFE_TBL_SRC_FILE,
                           TC_SM_04_ILOAD_FILENAME);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_04 - Failed to load ILoad Table (0x%08X)\n", iStatus);
        goto TC_SM_04_InitILoadTbl_Exit_Tag;
    }

    /* Manage ILoad table */
    iStatus = CFE_TBL_Manage(g_TC_SM_04_AppData.ILoadTblHdl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_04 - Failed to manage ILoad table (0x%08X)\n", iStatus);
        goto TC_SM_04_InitILoadTbl_Exit_Tag;
    }

    /* Make sure ILoad table is accessible by getting referencing it */
    iStatus = CFE_TBL_GetAddress((void*)&g_TC_SM_04_AppData.ILoadTblPtr,
                                 g_TC_SM_04_AppData.ILoadTblHdl);
    if (iStatus != CFE_TBL_INFO_UPDATED)
    {
        CFE_ES_WriteToSysLog("TC_SM_04 - Failed to get ILoad table's address (0x%08X)\n", iStatus);
        goto TC_SM_04_InitILoadTbl_Exit_Tag;
    }

    /* Validate ILoad table */
    iStatus = TC_SM_04_ValidateILoadTbl(g_TC_SM_04_AppData.ILoadTblPtr);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_04 - Failed to validate ILoad table (0x%08X)\n", iStatus);
        goto TC_SM_04_InitILoadTbl_Exit_Tag;
    }

    /* Set new parameter values */
    TC_SM_04_ProcessNewILoadTbl();

TC_SM_04_InitILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_04_ValidateILoadTbl
**
** Purpose: To validate the TC_SM_04's ILoad tables
**
** Arguments:
**    TC_SM_04_ILoadTblEntry_t*  iLoadTblPtr - pointer to the ILoad table
**=====================================================================================*/
int32 TC_SM_04_ValidateILoadTbl(TC_SM_04_ILoadTblEntry_t* iLoadTblPtr)
{
    int32  iStatus=0;

    if (iLoadTblPtr == NULL)
    {
        iStatus = -1;
        goto TC_SM_04_ValidateILoadTbl_Exit_Tag;
    }

    /* TODO:  Add code to validate new data values here.
    **
    ** Examples:
    **    if (iLoadTblPtr->sParam <= 16)
    **    {
    **        CFE_ES_WriteToSysLog("TC_SM_04 - Invalid value for ILoad parameter sParam (%d)\n",
    **                             iLoadTblPtr->sParam);
    */

TC_SM_04_ValidateILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_04_ProcessNewILoadTbl
**
** Purpose: To process TC_SM_04's new ILoad tables and set ILoad parameters with new values
**=====================================================================================*/
void TC_SM_04_ProcessNewILoadTbl()
{
    /* TODO:  Add code to set new ILoad parameters with new values here.
    **
    ** Examples:
    **
    **    g_TC_SM_04_AppData.latest_sParam = g_TC_SM_04_AppData.ILoadTblPtr->sParam;
    **    g_TC_SM_04_AppData.latest_fParam = g_TC_SM_04.AppData.ILoadTblPtr->fParam;
    */

    /*
    ** Process new iload table...
    */
    for (int32 TableIndex = 0; TableIndex < TC_SM_04_ILOAD_MAX_ENTRIES; TableIndex++)
    {
        memcpy(&g_TC_SM_04_AppData.InData.tc_sm_04_iload, &g_TC_SM_04_AppData.ILoadTblPtr[TableIndex].iloads, sizeof(TC_SM_04_ILOAD_T));
    }
}
    
/*=======================================================================================
** End of file tc_sm_04_iload_utils.c
**=====================================================================================*/
    

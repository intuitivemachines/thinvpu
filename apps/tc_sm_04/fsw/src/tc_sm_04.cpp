#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_sm_04.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_sm_04::load_img(const char *filepath)
{
    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

int tc_sm_04::process_img(TC_SM_04_OUT_T *tc_sm_04_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    Mat TC_SM_04_output;
    equalizeHist(image, image);
    Canny( image, TC_SM_04_output, 200, 220, 3 );
    OS_TranslatePath("/cf/vpu/1_TC_SM_04_output.png", cTranslatedPath);
    cv::imwrite(cTranslatedPath, TC_SM_04_output);
   
    int num_canny_rows = TC_SM_04_output.rows;
    int num_canny_cols = TC_SM_04_output.cols;
    
    if(tc_sm_04_out_ptr != NULL)
    {
        if(num_canny_rows > SM_04_MAX_CANNY_ROW)
        {
            num_canny_rows = SM_04_MAX_CANNY_ROW;
        }

        if(num_canny_cols > SM_04_MAX_CANNY_COL)
        {
            num_canny_cols = SM_04_MAX_CANNY_COL;
        }

        // Converting Mat TC_SM_04_output to uchar[][] for C type out-packet
        for(int i=0; i<num_canny_rows; i++)
        {
            for(int j=0; j<num_canny_cols; j++)
            {
                tc_sm_04_out_ptr->sm_04_canny_arr[i][j] = TC_SM_04_output.at<unsigned char>(i,j);
            }
        }
    }
    return 0;
}

// Class Deconstructor
tc_sm_04::~tc_sm_04() = default;

#ifdef __cplusplus
}
#endif
#ifndef __TC_SM_04CVBUILD_H__
#define __TC_SM_04CVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_sm_04_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_sm_04_driver(TC_SM_04_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_SM_04CVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

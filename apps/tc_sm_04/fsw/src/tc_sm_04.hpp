#ifndef __TC_SM_04CLASS_H__
#define __TC_SM_04CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_sm_04_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_sm_04
{
public:
  cv::Mat image;
  int load_img(const char*);
  int process_img(TC_SM_04_OUT_T *);
  virtual ~tc_sm_04();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_SM_04CLASS_H__ */

    
/* ---------- end of tc_sm_04_class.hpp ---------- */
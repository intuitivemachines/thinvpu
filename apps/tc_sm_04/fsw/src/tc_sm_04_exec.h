/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_SM_04 executive.
**
**==============================================================================*/

#ifndef TC_SM_04_EXEC_H_
#define TC_SM_04_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_sm_04_types.h"
//#include "tcm_public.h"
#include "tc_sm_04_private_types.h"

/* function declarations */
int32 tc_sm_04_initialize(
     TC_SM_04_ILOAD_T*     pIload,
     TC_SM_04_INTERNAL_T*  pInternal,
     TC_SM_04_OUT_T*       pTC_SM_04 
);

int32 tc_sm_04_exec(
      /* INPUTS */
      TC_SM_04_InData_t   * tc_sm_04_indata_ptr,   /* (--)  InData structure               */
      TC_SM_04_ILOAD_T    * tc_sm_04_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_SM_04_INTERNAL_T * tc_sm_04_internal_ptr, /* (--)  State memory for this function */
      TC_SM_04_OUT_T      * tc_sm_04_out_ptr       /* (--)  Output from this function      */
);

int32 tc_sm_04_exec_zero_outputs( TC_SM_04_OUT_T *out_ptr );

int32 tc_sm_04_cmd_handler( TC_SM_04_InData_t   *tc_sm_04_indata_ptr,
                              TC_SM_04_INTERNAL_T *tc_sm_04_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_SM_04_EXEC_H_ */

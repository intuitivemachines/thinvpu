// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_xxxx_01_private_types.h
**
** Title:  Type Header File for the TC_XXXX_01 Application
**
** $Author:    Roscoe Ferguson
** $Date:      2015-01-07
**
** Purpose:  This header file contains declarations and definitions of all the TC_XXXX_01's private
**           data structures and data types.
**
** Modification History:
**   Date       | Author        | Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2015-01-20 | Scott Tamblyn  | 0026    | Populate CFS Message/App layer I/O members across AGN&C
**   2019-10-30 | Brian Butcher  |   --    | Configured for lander TC_XXXX_01 device
**
**=====================================================================================*/
    
#ifndef _TC_XXXX_01_PRIVATE_TYPES_H_
#define _TC_XXXX_01_PRIVATE_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "tc_xxxx_01_types.h"
//#include "tcm_public.h"


typedef enum {
//    TC_XXXX_01_DATAPIPE_TCM = 0,
    TC_XXXX_01_TOTAL_DATAPIPES
} TC_XXXX_01_pipes_enum;


typedef struct
{
    uint32  counter;
    TC_XXXX_01_ILOAD_T       tc_xxxx_01_iload;
    TC_XXXX_01_CMD_T         tc_cmd;
//    TCM_OutPacket_t    sTcm_In;
} TC_XXXX_01_InData_t;


#ifdef __cplusplus
}
#endif

#endif /* _TC_XXXX_01_PRIVATE_TYPES_H_ */

/*=======================================================================================
** End of file tc_xxxx_01_private_types.h
**=====================================================================================*/
    

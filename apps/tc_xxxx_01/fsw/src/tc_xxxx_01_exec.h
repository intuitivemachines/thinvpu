/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_XXXX_01 executive.
**
**==============================================================================*/

#ifndef TC_XXXX_01_EXEC_H_
#define TC_XXXX_01_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_xxxx_01_types.h"
//#include "tcm_public.h"
#include "tc_xxxx_01_private_types.h"

/* function declarations */
int32 tc_xxxx_01_initialize(
     TC_XXXX_01_ILOAD_T*     pIload,
     TC_XXXX_01_INTERNAL_T*  pInternal,
     TC_XXXX_01_OUT_T*       pTC_XXXX_01 
);

int32 tc_xxxx_01_exec(
      /* INPUTS */
      TC_XXXX_01_InData_t   * tc_xxxx_01_indata_ptr,   /* (--)  InData structure               */
      TC_XXXX_01_ILOAD_T    * tc_xxxx_01_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_XXXX_01_INTERNAL_T * tc_xxxx_01_internal_ptr, /* (--)  State memory for this function */
      TC_XXXX_01_OUT_T      * tc_xxxx_01_out_ptr       /* (--)  Output from this function      */
);

int32 tc_xxxx_01_exec_zero_outputs( TC_XXXX_01_OUT_T *out_ptr );

int32 tc_xxxx_01_cmd_handler( TC_XXXX_01_InData_t   *tc_xxxx_01_indata_ptr,
                              TC_XXXX_01_INTERNAL_T *tc_xxxx_01_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_XXXX_01_EXEC_H_ */

#ifndef __TC_XXXX_01CLASS_H__
#define __TC_XXXX_01CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_xxxx_01_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_xxxx_01
{
public:
  cv::Mat image;        // Orignal Image
  double resolution;              // nadir resolution
  int load_img(const char*);      // Load image
  virtual ~tc_xxxx_01();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_XXXX_01CLASS_H__ */

    
/* ---------- end of tc_xxxx_01_class.hpp ---------- */
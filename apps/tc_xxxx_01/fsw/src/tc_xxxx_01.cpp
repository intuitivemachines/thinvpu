#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_xxxx_01.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_xxxx_01::load_img(const char *filepath)
{
    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

// Class Deconstructor
tc_xxxx_01::~tc_xxxx_01() = default;

#ifdef __cplusplus
}
#endif
#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_xxxx_01_cv.hpp"
#include "tc_xxxx_01.hpp"
#include "tc_xxxx_01_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_xxxx_01_driver(TC_XXXX_01_OUT_T *tc_xxxx_01_out_ptr,const char *fileString)
{   
    LOG_INFO("TC_XXXX_01_DRIVER File Name %s", fileString);
    tc_xxxx_01 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        // TODO: Add function calls as needed
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
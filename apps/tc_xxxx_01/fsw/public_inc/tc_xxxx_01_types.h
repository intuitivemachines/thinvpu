// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
**
** Purpose:
**    Template application definitions and data structures
**
**============================================================================= */

#ifndef _TC_XXXX_01_TYPES_H_
#define _TC_XXXX_01_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

#define TC_XXXX_01_DBG DBG_ERR

/*
 ** Includes
 */
#include "common_types.h"

/**
 * Enums, Error codes and returns
 */
typedef enum
{
    ERR_TC_XXXX_01_OK                    =  0,
    ERR_TC_XXXX_01_NULL                  = -1,  
    ERR_TC_XXXX_01_ERROR                 = -2,
} TC_XXXX_01_ERR_T;

/*
 ** Defines
 */

/*
 Structure Declarations
*/

/* App command structure */
typedef struct TC_XXXX_01_CMD_T {

   uint8   do_iload_init;     // (--)   0 = don't do an iload init, 1 = do an iload init
   uint8   iload_table_id;    // (--)   I-load table ID
   uint8   ucTcStartTestCmd;  // (--)   1 = True, 0 = False 
   uint8   spare0[5];         // (--)   Padding for 64 bit / 8 byte alignment

} TC_XXXX_01_CMD_T;

/* TC_XXXX_01 I-loads */
typedef struct TC_XXXX_01_ILOAD_T {
  uint8  mode;               // (--)   Mode config
  uint8  FirstPassComplete;  // (--)   0 = not complete, 1 = complete
  uint8  sparebits[6];       // (--)   Padding
} TC_XXXX_01_ILOAD_T;

/* Internal state data used in TC_XXXX_01 I/O app */
typedef struct TC_XXXX_01_INTERNAL_T {
  uint8 ucStartTest;
} TC_XXXX_01_INTERNAL_T;

typedef struct {
  float  time;              // (s)    Time of current data set
} TC_XXXX_01_HK_T;

/** \brief Multi sample TC_XXXX_01 IO App buffer*/
typedef struct TC_XXXX_01_OUT_T
{
    double   time_tag;          /* (s)   Time tag */
    uint16   test_active;
    uint16   test_complete;
    uint16   test_count;
    uint16   spare;
} TC_XXXX_01_OUT_T;

#ifdef __cplusplus
}
#endif

#endif /* _TC_XXXX_01_TYPES_H_ */

/*=======================================================================================
** End of file tc_xxxx_01_types.h
**=====================================================================================*/


// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_xxxx_01_public.h
**
** Title:  Public Header File for the TC_XXXX_01 Application
**
** $Author:    Roscoe Ferguson
** $Date:      2015-01-07
**
** Purpose:  This header file contains declartions and definitions of all the TC_XXXX_01's 
**           public structs.
**
** Modification History:
**   Date       | Author        | Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2019-10-30 | Brian Butcher  |   --    | Configured for lander TC_XXXX_01 device
**
**=====================================================================================*/
    
#ifndef _TC_XXXX_01_PUBLIC_H_
#define _TC_XXXX_01_PUBLIC_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"
#include "tc_xxxx_01_types.h"

/*
** Local Structure Declarations
*/
typedef struct
{
    uint8  ucCmdHeader[CFE_SB_CMD_HDR_SIZE];
} TC_XXXX_01_NoArgCmd_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint16  usCmdCnt;
    uint16  usCmdErrCnt;

    TC_XXXX_01_HK_T  HkData;
    uint8 ucSpare[4]; // Padding for 8 byte alignment
} TC_XXXX_01_HkTlm_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint32  uiCounter;
    float   fElapsedTime;
    float   fRateTime;

    TC_XXXX_01_OUT_T OutData;
} TC_XXXX_01_OutPacket_t;


#ifdef __cplusplus
}
#endif

#endif /* _TC_XXXX_01_PUBLIC_H_ */

/*=======================================================================================
** End of file tc_xxxx_01_public.h
**=====================================================================================*/
    

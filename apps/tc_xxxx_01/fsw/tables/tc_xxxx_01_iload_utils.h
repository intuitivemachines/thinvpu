/*=======================================================================================
** File Name:  tc_xxxx_01_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for TC_XXXX_01 Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  To define TC_XXXX_01's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_XXXX_01_ILOAD_UTILS_H_
#define _TC_XXXX_01_ILOAD_UTILS_H_


/*
** Include Files
*/
#include "tc_xxxx_01_tbldefs.h"


/*
** Local Function Prototypes
*/
int32  TC_XXXX_01_InitILoadTbl(void);
int32  TC_XXXX_01_ValidateILoadTbl(TC_XXXX_01_ILoadTblEntry_t*);
void   TC_XXXX_01_ProcessNewILoadTbl(void);

#endif /* _TC_XXXX_01_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tc_xxxx_01_iload_utils.h
**=====================================================================================*/
    

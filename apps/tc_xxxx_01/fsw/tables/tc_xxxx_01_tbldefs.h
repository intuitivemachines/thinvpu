/*=======================================================================================
** File Name:  tc_xxxx_01_tbldefs.h
**
** Title:  Header File for TC_XXXX_01 Application's tables
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TC_XXXX_01's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_XXXX_01_TBLDEFS_H_
#define _TC_XXXX_01_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_xxxx_01_mission_cfg.h"

/*
** Local Defines
*/
#define TC_XXXX_01_ILOAD_MAX_ENTRIES  1
#define TC_XXXX_01_ILOAD_FILENAME     "/cf/apps/tc_xxxx_01_tbl.tbl"
#define TC_XXXX_01_ILOAD_TABLENAME    "tc_xxxx_01_tbl"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    TC_XXXX_01_ILOAD_T iloads;
} TC_XXXX_01_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _TC_XXXX_01_TBLDEFS_H_ */

/*=======================================================================================
** End of file tc_xxxx_01_tbldefs.h
**=====================================================================================*/
    

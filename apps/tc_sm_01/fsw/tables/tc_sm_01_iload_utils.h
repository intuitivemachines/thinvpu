/*=======================================================================================
** File Name:  tc_sm_01_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for TC_SM_01 Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  To define TC_SM_01's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_SM_01_ILOAD_UTILS_H_
#define _TC_SM_01_ILOAD_UTILS_H_


/*
** Include Files
*/
#include "tc_sm_01_tbldefs.h"


/*
** Local Function Prototypes
*/
int32  TC_SM_01_InitILoadTbl(void);
int32  TC_SM_01_ValidateILoadTbl(TC_SM_01_ILoadTblEntry_t*);
void   TC_SM_01_ProcessNewILoadTbl(void);

#endif /* _TC_SM_01_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tc_sm_01_iload_utils.h
**=====================================================================================*/
    

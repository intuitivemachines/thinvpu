#ifndef __TC_SM_01CVBUILD_H__
#define __TC_SM_01CVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_sm_01_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_sm_01_driver(TC_SM_01_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_SM_01CVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

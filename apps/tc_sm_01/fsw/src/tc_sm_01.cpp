#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#ifndef __PPC__
#include <opencv2/ximgproc.hpp>
#endif
#include <opencv2/features2d.hpp>
#include "tc_sm_01.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace cv::ximgproc;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_sm_01::load_img(const char *filepath)
{
    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

int tc_sm_01::process_img(TC_SM_01_OUT_T *tc_sm_01_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    int SLIC_algorithm = 100;
    int sp_size = 100;
    float ruler = 30.0f;
    int label_connectivity = 25;
    int iterations = 5;
    int number_of_superPixels;
    Mat contour_mask;

    // Create super pixels
    Ptr<SuperpixelSLIC> slic = createSuperpixelSLIC(image, SLIC_algorithm, sp_size, ruler);
    slic->iterate(iterations);
    slic->enforceLabelConnectivity(label_connectivity);
    slic->getLabelContourMask(contour_mask, true);
    number_of_superPixels = slic->getNumberOfSuperpixels();
    LOG_INFO("TC_SM_01 input Area         = %d", sp_size);
    LOG_INFO("TC_SM_01 Actual SuperPixels = %d", number_of_superPixels);
    OS_TranslatePath("/cf/vpu/1_tc_sm_01_output.png", cTranslatedPath);
    cv::imwrite(cTranslatedPath, contour_mask);

    if(tc_sm_01_out_ptr != NULL)
    {
        tc_sm_01_out_ptr->superpixel_area_input = static_cast<int>(sp_size);
        tc_sm_01_out_ptr->number_of_superpixels = static_cast<int>(number_of_superPixels);
    }

    return 0;
}

// Class Deconstructor
tc_sm_01::~tc_sm_01() = default;

#ifdef __cplusplus
}
#endif
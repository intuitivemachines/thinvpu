#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_sm_01_cv.hpp"
#include "tc_sm_01.hpp"
#include "tc_sm_01_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_sm_01_driver(TC_SM_01_OUT_T *tc_sm_01_out_ptr,const char *fileString)
{   
    LOG_INFO("TC_SM_01_DRIVER File Name %s", fileString);
    tc_sm_01 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.process_img(tc_sm_01_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
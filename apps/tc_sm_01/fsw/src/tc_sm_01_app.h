// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_sm_01_app.h
**
** Title:  Header File for the TC_SM_01 Application
**
** Purpose:  To define the TC_SM_01's internal macros, data types, global variables and
**           function prototypes
**
** Modification History:
**   Date       | Author         |Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2015-01-13 | Scott Tamblyn  | 0026    | Populate CFS Message/App layer I/O members across AGN&C
**   2018-10-17 | Brian Butcher  |   --    | Merged into cFE 6.5.0a app template
**
**=====================================================================================*/
    
#ifndef __TC_SM_01_APP_H__
#define __TC_SM_01_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "common/utils/perf.h"

#include "sch_app_rates.h"

#include "tc_sm_01_public.h"
#include "tc_sm_01_mission_cfg.h"
#include "tc_sm_01_tbldefs.h"
#include "tc_sm_01_types.h"


/*
** Local Defines
*/
#define TC_SM_01_SCH_PIPE_DEPTH  10
#define TC_SM_01_CMD_PIPE_DEPTH  10
#define TC_SM_01_TLM_PIPE_DEPTH  10

/*
** Local Structure Declarations
*/
typedef struct
{
   
    /* CFE Event table */
    CFE_EVS_BinFilter_t  EventTbl[TC_SM_01_EVT_CNT];

    /* CFE scheduling pipe */
    CFE_SB_PipeId_t  SchPipeId; 
    uint16           usSchPipeDepth;
    char             cSchPipeName[OS_MAX_API_NAME];

    /* CFE command pipe */
    CFE_SB_PipeId_t  CmdPipeId;
    uint16           usCmdPipeDepth;
    char             cCmdPipeName[OS_MAX_API_NAME];
    
    /* CFE telemetry pipe */
    CFE_SB_PipeId_t  TlmPipeId;
    uint16           usTlmPipeDepth;
    char             cTlmPipeName[OS_MAX_API_NAME];

    /* Task-related */
    uint32  uiRunStatus;
    double  prevExecTime;

    /* ILoad table-related */
    CFE_TBL_Handle_t  ILoadTblHdl;
    TC_SM_01_ILoadTblEntry_t*  ILoadTblPtr;

    /* Input data - from I/O devices or subscribed from other apps' output data.
       Data structure should be defined in tc_sm_01/fsw/mission_inc/tc_sm_01_private_types.h */
    TC_SM_01_InData_t   InData;

    /* Output data - to be published at the end of a Wakeup cycle.
       Data structure should be defined in tc_sm_01/fsw/platform_inc/tc_sm_01_platform_cfg.h */
    TC_SM_01_OutPacket_t  OutPacket;

    /* Housekeeping telemetry - for downlink only.
       Data structure should be defined in tc_sm_01/fsw/platform_inc/tc_sm_01_platform_cfg.h */
    TC_SM_01_HkTlm_t  HkTlm;

    

    /* Add declarations for additional private data here */
    TC_SM_01_INTERNAL_T StateData;
	
} TC_SM_01_AppData_t;


/*
** Local Function Prototypes
*/
int32  TC_SM_01_InitApp(void);
int32  TC_SM_01_InitEvent(void);
int32  TC_SM_01_InitData(void);
int32  TC_SM_01_InitPipe(void);

void  TC_SM_01_AppMain(void);

void  TC_SM_01_CleanupCallback(void);

int32  TC_SM_01_RcvMsg(int32 iBlocking);

void  TC_SM_01_ProcessNewData(void);
void  TC_SM_01_ProcessNewCmds(void);
void  TC_SM_01_ProcessCommandPacket(CFE_SB_Msg_t*);

void  TC_SM_01_ReportHousekeeping(void);
void  TC_SM_01_SendOutData(void);
void  TC_SM_01_WriteOutData(void);

void  TC_SM_01_CheckStatusOfTables(void);

boolean  TC_SM_01_VerifyCmdLength(CFE_SB_Msg_t*, uint16);


#ifdef __cplusplus
}
#endif

#endif /* ____TC_SM_01_APP_H__ */

    
/* ---------- end of tc_sm_01_app.h ---------- */

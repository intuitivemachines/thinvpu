#ifndef __TC_OD_04CVBUILD_H__
#define __TC_OD_04CVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_od_04_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_od_04_driver(TC_OD_04_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_04CVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

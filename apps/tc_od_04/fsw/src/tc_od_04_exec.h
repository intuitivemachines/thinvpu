/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_OD_04 executive.
**
**==============================================================================*/

#ifndef TC_OD_04_EXEC_H_
#define TC_OD_04_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_od_04_types.h"
//#include "tcm_public.h"
#include "tc_od_04_private_types.h"

/* function declarations */
int32 tc_od_04_initialize(
     TC_OD_04_ILOAD_T*     pIload,
     TC_OD_04_INTERNAL_T*  pInternal,
     TC_OD_04_OUT_T*       pTC_OD_04 
);

int32 tc_od_04_exec(
      /* INPUTS */
      TC_OD_04_InData_t   * tc_od_04_indata_ptr,   /* (--)  InData structure               */
      TC_OD_04_ILOAD_T    * tc_od_04_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_OD_04_INTERNAL_T * tc_od_04_internal_ptr, /* (--)  State memory for this function */
      TC_OD_04_OUT_T      * tc_od_04_out_ptr       /* (--)  Output from this function      */
);

int32 tc_od_04_exec_zero_outputs( TC_OD_04_OUT_T *out_ptr );

int32 tc_od_04_cmd_handler( TC_OD_04_InData_t   *tc_od_04_indata_ptr,
                              TC_OD_04_INTERNAL_T *tc_od_04_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_OD_04_EXEC_H_ */

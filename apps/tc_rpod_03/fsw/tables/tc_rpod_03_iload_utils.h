/*=======================================================================================
** File Name:  tc_rpod_03_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for TC_RPOD_03 Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  To define TC_RPOD_03's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_RPOD_03_ILOAD_UTILS_H_
#define _TC_RPOD_03_ILOAD_UTILS_H_


/*
** Include Files
*/
#include "tc_rpod_03_tbldefs.h"


/*
** Local Function Prototypes
*/
int32  TC_RPOD_03_InitILoadTbl(void);
int32  TC_RPOD_03_ValidateILoadTbl(TC_RPOD_03_ILoadTblEntry_t*);
void   TC_RPOD_03_ProcessNewILoadTbl(void);

#endif /* _TC_RPOD_03_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tc_rpod_03_iload_utils.h
**=====================================================================================*/
    

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_rpod_03.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_rpod_03::load_img(const char *filepath)
{
    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

int tc_rpod_03::process_img(TC_RPOD_03_OUT_T *tc_rpod_03_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    vector<Vec3f> circles;
    int inverse_ratio = 1;
    int param1 = 200;
    int param2 = 100;
    int minRadius=1300;
    int maxRadius=1800;
    int diameter;
    int total_cirlces;
    int x;
    int y;
    HoughCircles(image, circles, HOUGH_GRADIENT, inverse_ratio, image.rows/8, param1, param2, minRadius, maxRadius);
    // Draw the circles detected
    cvtColor(image, image, COLOR_GRAY2RGB );
    total_cirlces = circles.size();
    for( size_t i = 0; i < circles.size(); i++ )
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        diameter = cvRound(circles[i][2]);
        // circle outline
        circle( image, center, diameter, Scalar(0,255,0), 30, 8, 0 );
        // circle center
        circle( image, center, 2, Scalar(0,0,255), 40, 8, 0 );
        x = center.x;
        y = center.y;
    }

    LOG_INFO("Amount of Circles found = %d", total_cirlces);
    LOG_INFO("Diameter of Circle = %d", diameter);
    LOG_INFO("Center of Circle   = (%d, %d)", x, y);
    OS_TranslatePath("/cf/vpu/1_tc_rpod_03_output.png", cTranslatedPath);
    cv::imwrite(cTranslatedPath, image);

    if(tc_rpod_03_out_ptr != NULL)
    {
        tc_rpod_03_out_ptr->circle_diameter = diameter;
        tc_rpod_03_out_ptr->num_circles = total_cirlces;
        tc_rpod_03_out_ptr->centroid_point_x = x;
        tc_rpod_03_out_ptr->centroid_point_y = y;
    }

    return 0;
}

// Class Deconstructor
tc_rpod_03::~tc_rpod_03() = default;

#ifdef __cplusplus
}
#endif
#ifndef __TC_RPOD_03CVBUILD_H__
#define __TC_RPOD_03CVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_rpod_03_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_rpod_03_driver(TC_RPOD_03_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_RPOD_03CVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

#ifndef __TC_RPOD_03CLASS_H__
#define __TC_RPOD_03CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_rpod_03_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_rpod_03
{
public:
  cv::Mat image;
  int load_img(const char*);
  int process_img(TC_RPOD_03_OUT_T *);
  virtual ~tc_rpod_03();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_RPOD_03CLASS_H__ */

    
/* ---------- end of tc_rpod_03_class.hpp ---------- */
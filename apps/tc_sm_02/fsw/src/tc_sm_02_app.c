// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
**
** Purpose:  This source file contains all necessary function definitions to run the TC_SM_02
**           application.
**
** Functions Defined:
**    TC_SM_02_InitEvent()
**    TC_SM_02_InitPipe()
**    TC_SM_02_InitData()
**    TC_SM_02_InitApp()
**    TC_SM_02_CleanupCallback()
**    TC_SM_02_RecvMsg()
**    TC_SM_02_ProcessNewData()
**    TC_SM_02_ProcessNewCmds()
**    TC_SM_02_ProcessNewAppCmds()
**    TC_SM_02_ReportHousekeeping()
**    TC_SM_02_CheckStatusOfTables()
**    TC_SM_02_SendOutData()
**    TC_SM_02_VerifyCmdLength()
**    TC_SM_02_AppMain()
**
**=====================================================================================*/

/*
** Pragmas
*/

/*
** Include Files
*/
#include <string.h>

#include "tc_sm_02_app.h"
#include "tc_sm_02_exec.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/

/*
** Global Variables
*/
TC_SM_02_AppData_t  g_TC_SM_02_AppData;

MISSION_app_pipes_T tc_sm_02_pipes[TC_SM_02_TOTAL_DATAPIPES] =
{
  //MsgId                        PipeRate                 PipeName                 Pipeid
  //{TCM_OUT_DATA_MID,             MP_RATE_TCM,             "tc_sm_02_tcm_pipe",           0   }
};

/*
** Local Variables
*/

/*
** Local Function Definitions
*/

/*=====================================================================================
** Name: TC_SM_02_InitEvent
**
** Purpose: To initialize and register event table for TC_SM_02 application
**
** Routines Called:
**    CFE_EVS_Register
**    CFE_ES_WriteToSysLog
**
** Called By:
**    TC_SM_02_InitApp
**
** Global Outputs/Writes:
**    g_TC_SM_02_AppData.EventTbl
**=====================================================================================*/
int32 TC_SM_02_InitEvent()
{
    int32  iStatus=CFE_SUCCESS;

    /* Create the event table */
    memset((void*)g_TC_SM_02_AppData.EventTbl, 0x00, sizeof(g_TC_SM_02_AppData.EventTbl));

    g_TC_SM_02_AppData.EventTbl[0].EventID = TC_SM_02_RESERVED_EID;
    g_TC_SM_02_AppData.EventTbl[1].EventID = TC_SM_02_INF_EID;
    g_TC_SM_02_AppData.EventTbl[2].EventID = TC_SM_02_INIT_INF_EID;
    g_TC_SM_02_AppData.EventTbl[3].EventID = TC_SM_02_ILOAD_INF_EID;
    g_TC_SM_02_AppData.EventTbl[4].EventID = TC_SM_02_CDS_INF_EID;
    g_TC_SM_02_AppData.EventTbl[5].EventID = TC_SM_02_CMD_INF_EID;

    g_TC_SM_02_AppData.EventTbl[ 6].EventID = TC_SM_02_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[ 7].EventID = TC_SM_02_INIT_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[ 8].EventID = TC_SM_02_ILOAD_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[ 9].EventID = TC_SM_02_CDS_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[10].EventID = TC_SM_02_CMD_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[11].EventID = TC_SM_02_PIPE_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[12].EventID = TC_SM_02_MSGID_ERR_EID;
    g_TC_SM_02_AppData.EventTbl[13].EventID = TC_SM_02_MSGLEN_ERR_EID;

    /* Register the table with CFE */
    iStatus = CFE_EVS_Register(g_TC_SM_02_AppData.EventTbl,
                               TC_SM_02_EVT_CNT, CFE_EVS_BINARY_FILTER);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_02 - Failed to register with EVS (0x%08X)\n", iStatus);
    }

        return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_02_InitPipe
**
** Purpose: To initialize all message pipes and subscribe to messages for TC_SM_02 application
**
** Routines Called:
**    CFE_SB_CreatePipe
**    CFE_SB_Subscribe
**    CFE_ES_WriteToSysLog
**
** Called By:
**    TC_SM_02_InitApp
**
** Global Outputs/Writes:
**    g_TC_SM_02_AppData.usSchPipeDepth
**    g_TC_SM_02_AppData.cSchPipeName
**    g_TC_SM_02_AppData.SchPipeId
**    g_TC_SM_02_AppData.usCmdPipeDepth
**    g_TC_SM_02_AppData.cCmdPipeName
**    g_TC_SM_02_AppData.CmdPipeId
**    g_TC_SM_02_AppData.usTlmPipeDepth
**    g_TC_SM_02_AppData.cTlmPipeName
**    g_TC_SM_02_AppData.TlmPipeId
**=====================================================================================*/
int32 TC_SM_02_InitPipe()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init schedule pipe */
    g_TC_SM_02_AppData.usSchPipeDepth = TC_SM_02_SCH_PIPE_DEPTH;
    memset((void*)g_TC_SM_02_AppData.cSchPipeName, '\0', sizeof(g_TC_SM_02_AppData.cSchPipeName));
    strncpy(g_TC_SM_02_AppData.cSchPipeName, "TC_SM_02_SCH_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to Wakeup messages */
    iStatus = CFE_SB_CreatePipe(&g_TC_SM_02_AppData.SchPipeId,
                                 g_TC_SM_02_AppData.usSchPipeDepth,
                                 g_TC_SM_02_AppData.cSchPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        iStatus = CFE_SB_Subscribe(SCH_1HZ_WAKEUP_MID, g_TC_SM_02_AppData.SchPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TC_SM_02 - Sch Pipe failed to subscribe to SCH_1HZ_WAKEUP_MID. (0x%08X)\n", iStatus);
			return (iStatus);
        }

        iStatus = CFE_SB_Subscribe(SCH_1HZ_SEND_HK_MID, g_TC_SM_02_AppData.SchPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TC_SM_02 - Sch Pipe failed to subscribe to SCH_1HZ_SEND_HK_MID. (0x%08X)\n", iStatus);
			return (iStatus);
        }        
    }
    else
    {
        CFE_ES_WriteToSysLog("TC_SM_02 - Failed to create SCH pipe (0x%08X)\n", iStatus);
		return (iStatus);
    }

    /* Init command pipe */
    g_TC_SM_02_AppData.usCmdPipeDepth = TC_SM_02_CMD_PIPE_DEPTH ;
    memset((void*)g_TC_SM_02_AppData.cCmdPipeName, '\0', sizeof(g_TC_SM_02_AppData.cCmdPipeName));
    strncpy(g_TC_SM_02_AppData.cCmdPipeName, "TC_SM_02_CMD_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to command messages */
    iStatus = CFE_SB_CreatePipe(&g_TC_SM_02_AppData.CmdPipeId,
                                 g_TC_SM_02_AppData.usCmdPipeDepth,
                                 g_TC_SM_02_AppData.cCmdPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        /* Subscribe to command messages */
        iStatus = CFE_SB_Subscribe(TC_SM_02_CMD_MID, g_TC_SM_02_AppData.CmdPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("TC_SM_02 - CMD Pipe failed to subscribe to TC_SM_02_CMD_MID. (0x%08X)\n", iStatus);
			return (iStatus);
        }
        
    }
    else
    {
        CFE_ES_WriteToSysLog("TC_SM_02 - Failed to create CMD pipe (0x%08X)\n", iStatus);
		return (iStatus);
    }

    /* Init telemetry pipes */
    for (int pipes=0; pipes<TC_SM_02_TOTAL_DATAPIPES; pipes++) 
    {

        /* Subscribe to telemetry messages on each telemetry pipe */
        iStatus = CFE_SB_CreatePipe(
                      &tc_sm_02_pipes[pipes].PipeId,
                      APP_GET_Q_RATE_DEPTH(tc_sm_02_pipes[pipes].PipeRate,MP_RATE_TC_SM_02),
                      tc_sm_02_pipes[pipes].PipeName);

        if (iStatus == CFE_SUCCESS)
        {
            /*
            ** Subscribe to Out data packets
            */
    	    iStatus = CFE_SB_SubscribeEx(
                tc_sm_02_pipes[pipes].MsgId,
                tc_sm_02_pipes[pipes].PipeId,
                CFE_SB_Default_Qos,
                APP_GET_Q_RATE_DEPTH(tc_sm_02_pipes[pipes].PipeRate,MP_RATE_TC_SM_02));
            if ( iStatus != CFE_SUCCESS )
            {
                CFE_ES_WriteToSysLog("TC_SM_02 App: Error Subscribing to \
                        %s Outut RC = 0x%08X\n", tc_sm_02_pipes[pipes].PipeName, iStatus);
                return (iStatus);
            }          
        }
        else
        {
            CFE_ES_WriteToSysLog("TC_SM_02 App: Error Creating %s, \
                                 RC = 0x%08X\n", tc_sm_02_pipes[pipes].PipeName, iStatus);

            return (iStatus);
        }
    }

    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_02_InitData
**
** Purpose: To initialize global variables used by TC_SM_02 application
**
** Routines Called:
**    CFE_SB_InitMsg
**
** Called By:
**    TC_SM_02_InitApp
**
** Global Outputs/Writes:
**    g_TC_SM_02_AppData.InData
**    g_TC_SM_02_AppData.OutPacket
**    g_TC_SM_02_AppData.HkTlm
**=====================================================================================*/
int32 TC_SM_02_InitData()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init input data */
    memset((void*)&g_TC_SM_02_AppData.InData, 0x00, sizeof(g_TC_SM_02_AppData.InData));

    /* Init execution timer */
    g_TC_SM_02_AppData.prevExecTime = 0.0;

    /* Init output data */
    memset((void*)&g_TC_SM_02_AppData.OutPacket, 0x00, sizeof(g_TC_SM_02_AppData.OutPacket));
    CFE_SB_InitMsg(&g_TC_SM_02_AppData.OutPacket,
                   TC_SM_02_OUT_DATA_MID, sizeof(g_TC_SM_02_AppData.OutPacket), TRUE);

    /* Init housekeeping packet */
    memset((void*)&g_TC_SM_02_AppData.HkTlm, 0x00, sizeof(g_TC_SM_02_AppData.HkTlm));
    CFE_SB_InitMsg(&g_TC_SM_02_AppData.HkTlm,
                   TC_SM_02_HK_TLM_MID, sizeof(g_TC_SM_02_AppData.HkTlm), TRUE);

    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_02_InitApp
**
** Purpose: To initialize all data local to and used by TC_SM_02 application
**
** Routines Called:
**    CFE_ES_RegisterApp
**    CFE_ES_WriteToSysLog
**    CFE_EVS_SendEvent
**    OS_TaskInstallDeleteHandler
**    TC_SM_02_InitEvent
**    TC_SM_02_InitPipe
**    TC_SM_02_InitData
**
** Called By:
**    TC_SM_02_AppMain
**=====================================================================================*/
int32 TC_SM_02_InitApp()
{
    int32  iStatus=CFE_SUCCESS;

    g_TC_SM_02_AppData.uiRunStatus = CFE_ES_APP_RUN;

    iStatus = CFE_ES_RegisterApp();
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_SM_02 - Failed to register the app (0x%08X)\n", iStatus);
		CFE_ES_WriteToSysLog("TC_SM_02 - Application failed to initialize\n");
		return iStatus;
    }

    if ((TC_SM_02_InitEvent() != CFE_SUCCESS) ||
        (TC_SM_02_InitPipe() != CFE_SUCCESS) ||
        (TC_SM_02_InitData() != CFE_SUCCESS) ||
        (TC_SM_02_InitILoadTbl() != CFE_SUCCESS))
    {
        iStatus = -1;
		CFE_ES_WriteToSysLog("TC_SM_02 - Application failed to initialize\n");
		return iStatus;
    }


	/* Initialize TC_SM_02 connection */
	//g_TC_SM_02_AppData.SensorConfigError = 0;

	if (iStatus != CFE_SUCCESS)
	{
		//g_TC_SM_02_AppData.DataPacket.FdirFlags.ucFailBits.SerialPortOpenFail = 1;

		CFE_ES_WriteToSysLog(
			"TC_SM_02 App: Cannot initialize connection  RC = 0x%08X\n", iStatus);

		return (iStatus);
	}
	else
	{
		//g_TC_SM_02_AppData.DataPacket.FdirFlags.ucFailBits.ConfigSensorFail = 0;
		//g_TC_SM_02_AppData.SensorConfigError = 0;
		CFE_ES_WriteToSysLog(
			"TC_SM_02 App: Configured sensor\n");
	}

    /* Install the cleanup callback */
    OS_TaskInstallDeleteHandler((void*)&TC_SM_02_CleanupCallback);


  if (iStatus == CFE_SUCCESS) {
	CFE_EVS_SendEvent(TC_SM_02_INIT_INF_EID, CFE_EVS_INFORMATION,
                      "TC_SM_02 - Application initialized");
  } else {
    CFE_ES_WriteToSysLog("TC_SM_02 - Application failed to initialize\n");
  }

  LOG_DEBUG( "Attempting to start UDP Driver" );

  tc_sm_02_initialize(&g_TC_SM_02_AppData.InData.tc_sm_02_iload, &g_TC_SM_02_AppData.StateData, &g_TC_SM_02_AppData.OutPacket.OutData);

  return iStatus;
}
    
/*=====================================================================================
** Name: TC_SM_02_CleanupCallback
**
** Purpose: To handle any neccesary cleanup prior to application exit
**
** Called By:
**    TC_SM_02_InitApp
**=====================================================================================*/
void TC_SM_02_CleanupCallback()
{
    /* No memory cleanup code currently needed. Function intentionally left blank.
     Callback should be maintained, since it is registered with CFS.
     This registered callback provides a placeholder in case future cleanup needs are identified.*/
}
    
/*=====================================================================================
** Name: TC_SM_02_RcvMsg
**
** Purpose: To receive and process messages for TC_SM_02 application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    CFE_ES_PerfLogEntry
**    CFE_ES_PerfLogExit
**    TC_SM_02_ProcessNewCmds
**    TC_SM_02_ProcessNewData
**    TC_SM_02_SendOutData
**
** Called By:
**    TC_SM_02_Main
**
** Global Inputs/Reads:
**    g_TC_SM_02_AppData.SchPipeId
**
** Global Outputs/Writes:
**    g_TC_SM_02_AppData.uiRunStatus
**=====================================================================================*/
int32 TC_SM_02_RcvMsg(int32 iBlocking)
{
    int32           iStatus=CFE_SUCCESS;
    CFE_SB_Msg_t*   MsgPtr=NULL;
    CFE_SB_MsgId_t  MsgId;
    double          dEnterTime = 0.0;
    static int      iStartDelay = 30;
    static int      prevTestCount = 0;

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(TC_SM_02_MAIN_TASK_PERF_ID);

    /* Wait for WakeUp messages from scheduler */
    iStatus = CFE_SB_RcvMsg(&MsgPtr, g_TC_SM_02_AppData.SchPipeId, iBlocking);

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(TC_SM_02_MAIN_TASK_PERF_ID);
    
    if (iStatus == CFE_SUCCESS)
    {
        MsgId = CFE_SB_GetMsgId(MsgPtr);
        switch (MsgId)
        {
            case SCH_1HZ_WAKEUP_MID:
                if( perf_get_time_seconds(&dEnterTime)  != PT_SUCCESS )
                {
                    LOG_ERROR("Error setting dEnterTime for exec call");
                }
                TC_SM_02_ProcessNewCmds();
                TC_SM_02_ProcessNewData();

                /* Call the TC_SM_02 Executive Layer */
                tc_sm_02_exec(
                     &g_TC_SM_02_AppData.InData,                           /* In: TC_SM_02 In Data         */
                     &g_TC_SM_02_AppData.InData.tc_sm_02_iload,          /* In: TC_SM_02 I-loads      */
                     &g_TC_SM_02_AppData.StateData,                        /* Out: TC_SM_02 state data   */
                     &g_TC_SM_02_AppData.OutPacket.OutData                 /* Out: TC_SM_02 IO App outputs      */
                   );

                perf_calc_times(dEnterTime,
                                &g_TC_SM_02_AppData.prevExecTime,
                                &g_TC_SM_02_AppData.OutPacket.fRateTime,
                                &g_TC_SM_02_AppData.OutPacket.fElapsedTime
                                );

                g_TC_SM_02_AppData.OutPacket.uiCounter++;

                /* Only send outdata packet after new test has completed */
                if(prevTestCount < g_TC_SM_02_AppData.OutPacket.OutData.test_count &&
                              g_TC_SM_02_AppData.OutPacket.OutData.test_count == TRUE)
                {
                    TC_SM_02_SendOutData();
                    TC_SM_02_WriteOutData();
                    prevTestCount = g_TC_SM_02_AppData.OutPacket.OutData.test_count;
                    LOG_INFO("Test Complete - Writing Results");
                }

                break;

            case SCH_1HZ_SEND_HK_MID:
                TC_SM_02_ReportHousekeeping();
                break;

            default:
                CFE_EVS_SendEvent(TC_SM_02_MSGID_ERR_EID, CFE_EVS_ERROR,
                                  "TC_SM_02 - Recvd invalid SCH msgId (0x%08X)", MsgId);
        }
    }
    else if (iStatus == CFE_SB_NO_MESSAGE)
    {
        /* If there's no incoming message, you can do something here, or do nothing */
    }
    else
    {
        /* This is an example of exiting on an error.
        ** Note that a SB read error is not always going to result in an app quitting.
        */
        CFE_EVS_SendEvent(TC_SM_02_PIPE_ERR_EID, CFE_EVS_ERROR,
                         "TC_SM_02:SB pipe read error (0x%08X), app will exit", iStatus);
        g_TC_SM_02_AppData.uiRunStatus= CFE_ES_APP_ERROR;
    }
    
    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_SM_02_ProcessNewData
**
** Purpose: To process incoming data subscribed by TC_SM_02 application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**
** Called By:
**    TC_SM_02_RcvMsg
**=====================================================================================*/
void TC_SM_02_ProcessNewData()
{
    int iStatus = CFE_SUCCESS;
    CFE_SB_Msg_t*   TlmMsgPtr=NULL;
    CFE_SB_MsgId_t  TlmMsgId;
    int                pipes;
    int                msgsleft;

    /* process all data pipes */
    for (pipes=0; pipes<TC_SM_02_TOTAL_DATAPIPES; pipes++)
    {
        msgsleft = CFE_SB_GetLatestMsg(&TlmMsgPtr,  tc_sm_02_pipes[pipes].PipeId);
        if (msgsleft > 0)
        {
            TlmMsgId = CFE_SB_GetMsgId(TlmMsgPtr);
            switch (TlmMsgId)
            {
                /* Subscriptions from TCM */
                // case TCM_OUT_DATA_MID:
                //     memcpy (&g_TC_SM_02_AppData.InData.sTcm_In,TlmMsgPtr,sizeof(TCM_OutPacket_t));
                //     break;
                 default:
                    CFE_EVS_SendEvent(TC_SM_02_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "TC_SM_02 Invalid Data Pipe Message, msgId: (0x%08X)", TlmMsgId);
                    break;
            }
        }
    }
}
    
/*=====================================================================================
** Name: TC_SM_02_ProcessNewCmds
**
** Purpose: To process incoming command messages for TC_SM_02 application
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    TC_SM_02_ProcessCommandPacket
**
** Called By:
**    TC_SM_02_RcvMsg
**=====================================================================================*/
void TC_SM_02_ProcessNewCmds()
{
    int iStatus = CFE_SUCCESS;
    CFE_SB_Msg_t*   CmdMsgPtr=NULL;
    CFE_SB_MsgId_t  CmdMsgId;

    /* Process command messages till the pipe is empty */
    while (1)
    {
        iStatus = CFE_SB_RcvMsg(&CmdMsgPtr, g_TC_SM_02_AppData.CmdPipeId, CFE_SB_POLL);
        if(iStatus == CFE_SUCCESS)
        {
            CmdMsgId = CFE_SB_GetMsgId(CmdMsgPtr);
            switch (CmdMsgId)
            {
                case TC_SM_02_CMD_MID:
                    TC_SM_02_ProcessCommandPacket(CmdMsgPtr);
                    break;

                default:
                    CFE_EVS_SendEvent(TC_SM_02_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "TC_SM_02 - Recvd invalid CMD msgId (0x%08X)", CmdMsgId);
                    break;
            }
        }
        else if (iStatus == CFE_SB_NO_MESSAGE)
        {
            break;
        }
        else
        {
            CFE_EVS_SendEvent(TC_SM_02_PIPE_ERR_EID, CFE_EVS_ERROR,
                  "TC_SM_02: CMD pipe read error (0x%08X)", iStatus);
            g_TC_SM_02_AppData.uiRunStatus = CFE_ES_APP_ERROR;
            break;
        }
    }
}
    
/*=====================================================================================
** Name: TC_SM_02_ProcessCommandPacket
**
** Purpose: To process command messages targeting TC_SM_02 application
**
** Routines Called:
**    CFE_SB_GetCmdCode
**    CFE_EVS_SendEvent
**
** Called By:
**    TC_SM_02_ProcessNewCmds
**
** Global Outputs/Writes:
**    g_TC_SM_02_AppData.HkTlm.usCmdCnt
**    g_TC_SM_02_AppData.HkTlm.usCmdErrCnt
**=====================================================================================*/
void TC_SM_02_ProcessCommandPacket(CFE_SB_Msg_t* MsgPtr)
{
    uint32  uiCmdCode=0;

    if (MsgPtr != NULL)
    {
        uiCmdCode = CFE_SB_GetCmdCode(MsgPtr);
        switch (uiCmdCode)
        {
            case TC_SM_02_NOOP_CC:
                g_TC_SM_02_AppData.HkTlm.usCmdCnt++;
                CFE_EVS_SendEvent(TC_SM_02_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TC_SM_02 - Recvd NOOP cmd (%d)", uiCmdCode);
                break;

            case TC_SM_02_RESET_CC:
                g_TC_SM_02_AppData.HkTlm.usCmdCnt = 0;
                g_TC_SM_02_AppData.HkTlm.usCmdErrCnt = 0;
                CFE_EVS_SendEvent(TC_SM_02_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TC_SM_02 - Recvd RESET cmd (%d)", uiCmdCode);
                break;

            case TC_SM_02_START_CC:
                g_TC_SM_02_AppData.HkTlm.usCmdCnt++;
                g_TC_SM_02_AppData.InData.tc_cmd.ucTcStartTestCmd = TRUE;
                CFE_EVS_SendEvent(TC_SM_02_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "TC_SM_02 - Recvd START cmd (%d)", uiCmdCode);
                break;

            default:
                g_TC_SM_02_AppData.HkTlm.usCmdErrCnt++;
                CFE_EVS_SendEvent(TC_SM_02_MSGID_ERR_EID, CFE_EVS_ERROR,
                                  "TC_SM_02 - Recvd invalid cmdId (%d)", uiCmdCode);
                break;
        }
    }
}
    
/*====================================================================
** Name:     TC_SM_02_CheckStatusOfTables()
**
** Purpose:  Check the status of the I-load table
**
** Assumptions, External Events, and Notes:
**  1.   NOT called in the Trick-sim instantiation
**
** Routines Called:
**   CFE_TBL_GetStatus()
**   CFE_TBL_Validate()
**   CFE_TBL_ReleaseAddress()
**   CFE_TBL_Update()
**   CFE_TBL_GetAddress()
**   CFE_ES_WriteToSysLog()
**   CFE_EVS_SendEvent()
**
** Global Inputs:
**   TC_SM_02_iLoadTblHandle  - Handle to TC_SM_02 I-load tables
**
** Global Outputs:
**   TC_SM_02_iload_data               - TC_SM_02's I-loads
**
**====================================================================*/
void TC_SM_02_CheckStatusOfTables(void)
{

	int32   Status = CFE_SUCCESS;

	/* Determine if the iload table has a validation or update that needs to be performed */
	Status = CFE_TBL_GetStatus(g_TC_SM_02_AppData.ILoadTblHdl);

	if (Status == CFE_TBL_INFO_VALIDATION_PENDING)
	{
		LOG_INFO( "Validating the ILoad table" );

		/* Validate the specified table */
		Status = CFE_TBL_Validate(g_TC_SM_02_AppData.ILoadTblHdl);
		if (Status != CFE_SUCCESS)
		{
			CFE_ES_WriteToSysLog("TC_SM_02 - Failed to validate ILoad table (0x%08X)\n", Status);
			return;
		}
	}
	else if (Status == CFE_TBL_INFO_UPDATE_PENDING)
	{
		LOG_INFO( "Updating the ILoad table" );

		/* release address must be called for update to take */
		CFE_TBL_ReleaseAddress(g_TC_SM_02_AppData.ILoadTblHdl);

		/* Update the iload table */
		CFE_TBL_Update(g_TC_SM_02_AppData.ILoadTblHdl);

		/* Get address of the newly updated iload table */
		CFE_TBL_GetAddress((void*)&g_TC_SM_02_AppData.ILoadTblPtr, g_TC_SM_02_AppData.ILoadTblHdl);

		/* Set new parameter values */
		TC_SM_02_ProcessNewILoadTbl();
	}
	else if (Status != CFE_SUCCESS)
	{
		CFE_EVS_SendEvent(99, CFE_EVS_ERROR,
			"Unexpected CFE_TBL_GetStatus return (0x%08X) for TC_SM_02 ILoad Table",
			Status);
	}

	return;

}   /* end TC_SM_02_CheckStatusOfTables */

/*=====================================================================================
** Name: TC_SM_02_ReportHousekeeping
**
** Purpose: To send housekeeping message
**
** Called By:
**    TC_SM_02_ProcessNewCmds
**=====================================================================================*/
void TC_SM_02_ReportHousekeeping()
{
    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TC_SM_02_AppData.HkTlm);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TC_SM_02_AppData.HkTlm);
}
    
/*=====================================================================================
** Name: TC_SM_02_SendOutData
**
** Purpose: To publish 1-Wakeup cycle output data
**
** Called By:
**    TC_SM_02_RcvMsg
**=====================================================================================*/
void TC_SM_02_SendOutData()
{
    CFE_TIME_SysTime_t cfetime;
    cfetime = CFE_TIME_GetTime();
    double time_tag = cfetime.Seconds
	     + (double )  CFE_TIME_Sub2MicroSecs( cfetime.Subseconds) / 1000000.0;

    g_TC_SM_02_AppData.OutPacket.OutData.time_tag = time_tag;

    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TC_SM_02_AppData.OutPacket);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_TC_SM_02_AppData.OutPacket);
}

/*=====================================================================================
** Name: TC_SM_02_WriteOutData
**
** Purpose: To write output data to a binary file
**
** Called By:
**    TC_SM_02_RcvMsg
**=====================================================================================*/
void TC_SM_02_WriteOutData()
{
    int32 fileHandle = -1;
    char fileName[OS_MAX_PATH_LEN] = "";
    char testNum[5] = "";
    int32 bytesWritten = 0;
    CFE_TIME_SysTime_t cfetime;
    cfetime = CFE_TIME_GetTime();
    double time_tag = cfetime.Seconds
	     + (double )  CFE_TIME_Sub2MicroSecs( cfetime.Subseconds) / 1000000.0;

    g_TC_SM_02_AppData.OutPacket.OutData.time_tag = time_tag;

    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_TC_SM_02_AppData.OutPacket);

    snprintf(testNum,5,"%04d",g_TC_SM_02_AppData.OutPacket.OutData.test_count);
    strcat(fileName,"/cf/log/TC_SM_02_Test_");
    strcat(fileName,testNum);
    strcat(fileName,".bin");

    /* Create directory listing output file */
    fileHandle = OS_creat(fileName, OS_READ_WRITE);
    if (fileHandle >= OS_SUCCESS)
    {
        /* Write blank FM directory statistics structure as a place holder */
        bytesWritten = OS_write(fileHandle, &g_TC_SM_02_AppData.OutPacket, sizeof(TC_SM_02_OutPacket_t));
        if (bytesWritten != sizeof(TC_SM_02_OutPacket_t))
        {
            /* Print error message */
            LOG_ERROR("Error writing outdata result = %d, expected = %d",
                                (int)bytesWritten, sizeof(TC_SM_02_OutPacket_t));
        }
        
        OS_close(fileHandle);
    }
    else
    {
        /* Print error message */
        LOG_ERROR("OS_creat failed: result = %d, file = %s", (int)fileHandle, fileName);
    }
}

/*=====================================================================================
** Name: TC_SM_02_VerifyCmdLength
**
** Purpose: To verify command length for a particular command message
**
** Arguments:
**    CFE_SB_Msg_t*  MsgPtr      - command message pointer
**    uint16         usExpLength - expected command length
**
** Called By:
**    TC_SM_02_ProcessNewCmds
**=====================================================================================*/
boolean TC_SM_02_VerifyCmdLength(CFE_SB_Msg_t* MsgPtr,
                           uint16 usExpectedLen)
{
    boolean bResult=FALSE;
    uint16  usMsgLen=0;

    if (MsgPtr != NULL)
    {
        usMsgLen = CFE_SB_GetTotalMsgLength(MsgPtr);

        if (usExpectedLen != usMsgLen)
        {
            CFE_SB_MsgId_t MsgId = CFE_SB_GetMsgId(MsgPtr);
            uint16 usCmdCode = CFE_SB_GetCmdCode(MsgPtr);

            CFE_EVS_SendEvent(TC_SM_02_MSGLEN_ERR_EID, CFE_EVS_ERROR,
                              "TC_SM_02 - Rcvd invalid msgLen: msgId=0x%08X, cmdCode=%d, "
                              "msgLen=%d, expectedLen=%d",
                              MsgId, usCmdCode, usMsgLen, usExpectedLen);
            g_TC_SM_02_AppData.HkTlm.usCmdErrCnt++;
        }
    }

    return (bResult);
}
    
/*=====================================================================================
** Name: TC_SM_02_AppMain
**
** Purpose: To define TC_SM_02 application's entry point and main process loop
**
** Routines Called:
**    CFE_ES_RunLoop
**    CFE_ES_ExitApp
**    TC_SM_02_InitApp
**    TC_SM_02_RcvMsg
**=====================================================================================*/
void TC_SM_02_AppMain()
{
    int32  iStatus=CFE_SUCCESS;

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(TC_SM_02_MAIN_TASK_PERF_ID);
    
    /* Perform application initializations */
    if (TC_SM_02_InitApp() != CFE_SUCCESS)
    {
        g_TC_SM_02_AppData.uiRunStatus = CFE_ES_APP_ERROR;
    }
    else {
        /* Do not perform performance monitoring on startup sync */
        CFE_ES_PerfLogExit(TC_SM_02_MAIN_TASK_PERF_ID);
        CFE_ES_WaitForStartupSync(APP_TIMEOUT_DEFAULT_MSEC);
        CFE_ES_PerfLogEntry(TC_SM_02_MAIN_TASK_PERF_ID);
    }

    /* Application main loop */
    while (CFE_ES_RunLoop(&g_TC_SM_02_AppData.uiRunStatus) == TRUE)
    {          
        iStatus = TC_SM_02_RcvMsg(CFE_SB_PEND_FOREVER); 
    }

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(TC_SM_02_MAIN_TASK_PERF_ID);

    /* Exit the application */
    CFE_ES_ExitApp(g_TC_SM_02_AppData.uiRunStatus);

} 
    
/*=======================================================================================
** End of file tc_sm_02_app.c
**=====================================================================================*/
    

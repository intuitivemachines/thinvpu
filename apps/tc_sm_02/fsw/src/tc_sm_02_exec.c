/* Copyright 2019 Intuitive Machines, LLC. All rights reserved.
**=======================================================================================
**
** Purpose:  TC_SM_02 I/O App Executive Layer
**
**==============================================================================*/

#include "tc_sm_02_exec.h"

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"
#include "tc_sm_02_cv.hpp"
/*
Global Variables
 */

/* initialize */

/* From App InitApp */

/*==============================================================================
 ** Function Name:  tc_sm_02_initialize()
 **
 ** Purpose:
 **    Top level initialization function for TC_SM_02 
 **
 **==============================================================================*/
 int32 tc_sm_02_initialize(
     TC_SM_02_ILOAD_T*     pIload,
     TC_SM_02_INTERNAL_T*  pInternal,
     TC_SM_02_OUT_T*     pTC_SM_02 )
 {
    /* Error checking */
    ERROR_NULL_CHK_RTRN(pIload);
    ERROR_NULL_CHK_RTRN(pInternal);
    ERROR_NULL_CHK_RTRN(pTC_SM_02);

    /* Initialize output structures to zero */
    memset(pInternal, 0, sizeof(TC_SM_02_INTERNAL_T) );
    memset(pTC_SM_02, 0, sizeof(TC_SM_02_OUT_T) );

    //pInternal->dataCycleCounter = 0; //Structure is empty at the moment

    LOG_TRACE( "in init" );

    return ( 0 );
}

int32 tc_sm_02_exec( /* RETURN:  Always return 0 for successful execution */
      /* INPUTS */
      TC_SM_02_InData_t   *tc_sm_02_indata_ptr,    /* (--)  InData structure                */
      TC_SM_02_ILOAD_T    *tc_sm_02_iload_ptr,     /* (--)  Iload structure                 */
      /* OUTPUTS */
      TC_SM_02_INTERNAL_T *tc_sm_02_internal_ptr,  /* (--)  State memory for this function  */
      TC_SM_02_OUT_T      *tc_sm_02_out_ptr        /* (--) Output from TC_SM_02 App       */
)
{
    /* Variable Declarations */
    int8 iStatus = ERR_TC_SM_02_OK;  // OK until proven otherwise
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];

    /* Error checking */
    ERROR_NULL_CHK_RTRN( tc_sm_02_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_sm_02_iload_ptr );
    ERROR_NULL_CHK_RTRN( tc_sm_02_internal_ptr );
    ERROR_NULL_CHK_RTRN( tc_sm_02_out_ptr );

    iStatus = tc_sm_02_cmd_handler(tc_sm_02_indata_ptr,tc_sm_02_internal_ptr);
    if( iStatus != ERR_TC_SM_02_OK)
    {
        LOG_ERROR("Exec Command Handler Error");
    }

    /* Start test can be commmanded via SB Command or TCM Outdata */
    if(tc_sm_02_internal_ptr->ucStartTest)
    {
        tc_sm_02_internal_ptr->ucStartTest = 0;
        tc_sm_02_out_ptr->test_active = TRUE;
        tc_sm_02_out_ptr->test_complete = FALSE;
        LOG_INFO("TC_SM_02 Test Started");
        OS_TranslatePath("/cf/apps/FRB.JPG", cTranslatedPath);
        tc_sm_02_driver(tc_sm_02_out_ptr, cTranslatedPath);

        tc_sm_02_out_ptr->test_active = FALSE;
        tc_sm_02_out_ptr->test_complete = TRUE;
        tc_sm_02_out_ptr->test_count++;
        LOG_INFO("TC_SM_02 Test Complete");
    }

    return iStatus;
}

/*==============================================================================
 ** Function Name:  tc_sm_02_exec_zero_outputs()
 **
 ** Purpose:
 **    Zeros the output
 **
 **==============================================================================
 */
int32 tc_sm_02_exec_zero_outputs( TC_SM_02_OUT_T *tc_sm_02_out_ptr )
{
   ERROR_NULL_CHK_RTRN( tc_sm_02_out_ptr );
   memset( tc_sm_02_out_ptr, 0, sizeof(TC_SM_02_OUT_T) );
   return ( 0 );
}

/*==============================================================================
 ** Function Name:  tc_sm_02_cmd_handler()
 **
 ** Purpose:
 **    Process incoming commands from app layer or TCM
 **
 **==============================================================================
 */
int32 tc_sm_02_cmd_handler( TC_SM_02_InData_t   *tc_sm_02_indata_ptr,
                              TC_SM_02_INTERNAL_T *tc_sm_02_internal_ptr )
{
    ERROR_NULL_CHK_RTRN( tc_sm_02_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_sm_02_internal_ptr );

    if(tc_sm_02_indata_ptr->tc_cmd.ucTcStartTestCmd)
    {
        if(tc_sm_02_internal_ptr->ucStartTest)
        {
            LOG_INFO("TC_SM_02 Already Running - No Action");
        }
        else
        {
            tc_sm_02_internal_ptr->ucStartTest = TRUE;
        }

        tc_sm_02_indata_ptr->tc_cmd.ucTcStartTestCmd = FALSE;
    }

    /* TODO: Add TCM processing when available */

   return ( ERR_TC_SM_02_OK );
}
 /* ---------- end of tc_sm_02_exec_zero_outputs() ----------*/


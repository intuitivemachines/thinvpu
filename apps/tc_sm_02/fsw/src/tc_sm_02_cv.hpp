#ifndef __TC_SM_02CVBUILD_H__
#define __TC_SM_02CVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_sm_02_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_sm_02_driver(TC_SM_02_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_SM_02CVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_sm_02_mission_cfg.h
**
** Title:  Mission Configuration Header File for the TC_SM_02 Application
**
** $Author:    Brian Butcher
** $Date:      2015-01-07
**
** Purpose:  This header file contains declartions and definitions of all the TC_SM_02's 
**           mission-specific configurations.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_SM_02_MISSION_CFG_H_
#define _TC_SM_02_MISSION_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"

#include "MISSION_perf_ids.h"         // PERF IDs
#include "MISSION_cmd_ids.h"          // CMD MIDs
#include "MISSION_cmd_codes.h"        // APP CCs
#include "MISSION_tlm_ids.h"          // TLM MIDs

#include "tc_sm_02_private_ids.h"    // EIDs
#include "tc_sm_02_private_types.h"  // APP_InData_t


#ifdef __cplusplus
}
#endif

#endif /* _TC_SM_02_MISSION_CFG_H_ */

/*=======================================================================================
** End of file tc_sm_02_mission_cfg.h
**=====================================================================================*/
    

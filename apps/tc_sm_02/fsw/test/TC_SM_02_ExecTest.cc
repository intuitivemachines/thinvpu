/*==============================================================================
** File Name: tc_SM_02ExecTest.cc
**
** Title: Unit Tests for tc_SM_02 Executive Layer
**
** Author: James Blakeslee
** Date: 2019-01-08
**============================================================================*/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestFixture.h>

#include "MathAssert.hh"  // convenience macros for matrix and vector comparisons

extern "C"
{
//#include "tc_sm_02_exec.h" TODO: include when executive layer becomes available
}

class TC_SM_02ExecTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE( TC_SM_02ExecTest );
    CPPUNIT_TEST( testMyTest1 );  // TODO: add unit tests here
    CPPUNIT_TEST( testMyTest2 );  // TODO: add unit tests here
    CPPUNIT_TEST_SUITE_END();

    public:
        void setUp();
        void tearDown();
        void testMyTest1();  // TODO: public visibility for all unit test functions
        void testMyTest2();  // TODO: public visibility for all unit test functions
};

/* called on instantiation */
void TC_SM_02ExecTest::setUp()
{
}

/* called after tests are complete (falls out-of-scope) */
void TC_SM_02ExecTest::tearDown()
{
}

/* TODO: short test description */
void TC_SM_02ExecTest::testMyTest1()
{
}

/* TODO: short test description */
void TC_SM_02ExecTest::testMyTest2()
{
}

CPPUNIT_TEST_SUITE_REGISTRATION( TC_SM_02ExecTest );

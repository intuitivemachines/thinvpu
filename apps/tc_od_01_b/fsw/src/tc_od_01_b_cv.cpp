#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_od_01_b_cv.hpp"
#include "tc_od_01_b.hpp"
#include "tc_od_01_b_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_od_01_b_driver(TC_OD_01_B_OUT_T *tc_od_01_b_out_ptr,const char *fileString)
{   
    LOG_INFO("TC_OD_01_B_DRIVER File Name %s", fileString);
    tc_od_01_b tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.orb_detector(tc_od_01_b_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
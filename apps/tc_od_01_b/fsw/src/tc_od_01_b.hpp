#ifndef __TC_OD_01_BCLASS_H__
#define __TC_OD_01_BCLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_od_01_b_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_od_01_b
{
public:
  cv::Mat image;                  // Orignal Image
  int load_img(const char*);      // Load image
  int orb_detector(TC_OD_01_B_OUT_T *);  // Orb detection on loaded Image
  virtual ~tc_od_01_b();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_01_BCLASS_H__ */

    
/* ---------- end of tc_od_01_b_class.hpp ---------- */
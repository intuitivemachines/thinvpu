/* Copyright 2019 Intuitive Machines, LLC. All rights reserved.
**=======================================================================================
**
** Purpose:  TC_OD_01_B I/O App Executive Layer
**
**==============================================================================*/

#include "tc_od_01_b_exec.h"

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"
#include "tc_od_01_b_cv.hpp"
/*
Global Variables
 */

/* initialize */

/* From App InitApp */

/*==============================================================================
 ** Function Name:  tc_od_01_b_initialize()
 **
 ** Purpose:
 **    Top level initialization function for TC_OD_01_B 
 **
 **==============================================================================*/
 int32 tc_od_01_b_initialize(
     TC_OD_01_B_ILOAD_T*     pIload,
     TC_OD_01_B_INTERNAL_T*  pInternal,
     TC_OD_01_B_OUT_T*     pTC_OD_01_B )
 {
    /* Error checking */
    ERROR_NULL_CHK_RTRN(pIload);
    ERROR_NULL_CHK_RTRN(pInternal);
    ERROR_NULL_CHK_RTRN(pTC_OD_01_B);

    /* Initialize output structures to zero */
    memset(pInternal, 0, sizeof(TC_OD_01_B_INTERNAL_T) );
    memset(pTC_OD_01_B, 0, sizeof(TC_OD_01_B_OUT_T) );

    //pInternal->dataCycleCounter = 0; //Structure is empty at the moment

    LOG_TRACE( "in init" );

    return ( 0 );
}

int32 tc_od_01_b_exec( /* RETURN:  Always return 0 for successful execution */
      /* INPUTS */
      TC_OD_01_B_InData_t   *tc_od_01_b_indata_ptr,    /* (--)  InData structure                */
      TC_OD_01_B_ILOAD_T    *tc_od_01_b_iload_ptr,     /* (--)  Iload structure                 */
      /* OUTPUTS */
      TC_OD_01_B_INTERNAL_T *tc_od_01_b_internal_ptr,  /* (--)  State memory for this function  */
      TC_OD_01_B_OUT_T      *tc_od_01_b_out_ptr        /* (--) Output from TC_OD_01_B App       */
)
{
    /* Variable Declarations */
    int8 iStatus = ERR_TC_OD_01_B_OK;  // OK until proven otherwise
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];

    /* Error checking */
    ERROR_NULL_CHK_RTRN( tc_od_01_b_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_01_b_iload_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_01_b_internal_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_01_b_out_ptr );

    iStatus = tc_od_01_b_cmd_handler(tc_od_01_b_indata_ptr,tc_od_01_b_internal_ptr);
    if( iStatus != ERR_TC_OD_01_B_OK)
    {
        LOG_ERROR("Exec Command Handler Error");
    }

    /* Start test can be commmanded via SB Command or TCM Outdata */
    if(tc_od_01_b_internal_ptr->ucStartTest)
    {
        tc_od_01_b_internal_ptr->ucStartTest = 0;
        tc_od_01_b_out_ptr->test_active = TRUE;
        tc_od_01_b_out_ptr->test_complete = FALSE;
        LOG_INFO("TC_OD_01_B Test Started");

        OS_TranslatePath("/cf/apps/OD_ORB.png", cTranslatedPath);
        tc_od_01_b_driver(tc_od_01_b_out_ptr,cTranslatedPath);

        tc_od_01_b_out_ptr->test_active = FALSE;
        tc_od_01_b_out_ptr->test_complete = TRUE;
        tc_od_01_b_out_ptr->test_count++;
        LOG_INFO("TC_OD_01_B Test Complete");
    }

    return iStatus;
}

/*==============================================================================
 ** Function Name:  tc_od_01_b_exec_zero_outputs()
 **
 ** Purpose:
 **    Zeros the output
 **
 **==============================================================================
 */
int32 tc_od_01_b_exec_zero_outputs( TC_OD_01_B_OUT_T *tc_od_01_b_out_ptr )
{
   ERROR_NULL_CHK_RTRN( tc_od_01_b_out_ptr );
   memset( tc_od_01_b_out_ptr, 0, sizeof(TC_OD_01_B_OUT_T) );
   return ( 0 );
}

/*==============================================================================
 ** Function Name:  tc_od_01_b_cmd_handler()
 **
 ** Purpose:
 **    Process incoming commands from app layer or TCM
 **
 **==============================================================================
 */
int32 tc_od_01_b_cmd_handler( TC_OD_01_B_InData_t   *tc_od_01_b_indata_ptr,
                              TC_OD_01_B_INTERNAL_T *tc_od_01_b_internal_ptr )
{
    ERROR_NULL_CHK_RTRN( tc_od_01_b_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_01_b_internal_ptr );

    if(tc_od_01_b_indata_ptr->tc_cmd.ucTcStartTestCmd)
    {
        if(tc_od_01_b_internal_ptr->ucStartTest)
        {
            LOG_INFO("TC_OD_01_B Already Running - No Action");
        }
        else
        {
            tc_od_01_b_internal_ptr->ucStartTest = TRUE;
        }

        tc_od_01_b_indata_ptr->tc_cmd.ucTcStartTestCmd = FALSE;
    }

    /* TODO: Add TCM processing when available */

   return ( ERR_TC_OD_01_B_OK );
}
 /* ---------- end of tc_od_01_b_exec_zero_outputs() ----------*/


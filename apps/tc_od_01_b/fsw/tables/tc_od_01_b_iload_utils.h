/*=======================================================================================
** File Name:  tc_od_01_b_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for TC_OD_01_B Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  To define TC_OD_01_B's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_OD_01_B_ILOAD_UTILS_H_
#define _TC_OD_01_B_ILOAD_UTILS_H_


/*
** Include Files
*/
#include "tc_od_01_b_tbldefs.h"


/*
** Local Function Prototypes
*/
int32  TC_OD_01_B_InitILoadTbl(void);
int32  TC_OD_01_B_ValidateILoadTbl(TC_OD_01_B_ILoadTblEntry_t*);
void   TC_OD_01_B_ProcessNewILoadTbl(void);

#endif /* _TC_OD_01_B_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tc_od_01_b_iload_utils.h
**=====================================================================================*/
    

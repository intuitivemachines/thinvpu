/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_RPOD_02 executive.
**
**==============================================================================*/

#ifndef TC_RPOD_02_EXEC_H_
#define TC_RPOD_02_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_rpod_02_types.h"
//#include "tcm_public.h"
#include "tc_rpod_02_private_types.h"

/* function declarations */
int32 tc_rpod_02_initialize(
     TC_RPOD_02_ILOAD_T*     pIload,
     TC_RPOD_02_INTERNAL_T*  pInternal,
     TC_RPOD_02_OUT_T*       pTC_RPOD_02 
);

int32 tc_rpod_02_exec(
      /* INPUTS */
      TC_RPOD_02_InData_t   * tc_rpod_02_indata_ptr,   /* (--)  InData structure               */
      TC_RPOD_02_ILOAD_T    * tc_rpod_02_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_RPOD_02_INTERNAL_T * tc_rpod_02_internal_ptr, /* (--)  State memory for this function */
      TC_RPOD_02_OUT_T      * tc_rpod_02_out_ptr       /* (--)  Output from this function      */
);

int32 tc_rpod_02_exec_zero_outputs( TC_RPOD_02_OUT_T *out_ptr );

int32 tc_rpod_02_cmd_handler( TC_RPOD_02_InData_t   *tc_rpod_02_indata_ptr,
                              TC_RPOD_02_INTERNAL_T *tc_rpod_02_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_RPOD_02_EXEC_H_ */

#ifndef __TC_RPOD_02CLASS_H__
#define __TC_RPOD_02CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_rpod_02_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_rpod_02
{
public:
  cv::Mat tc_rpod_02_img_1;     // image 1 to be loaded
  cv::Mat tc_rpod_02_img_2;     // image 2 to be loaded
  int load_img(const char*);    // Load image
  int process_img(TC_RPOD_02_OUT_T*); // process
  virtual ~tc_rpod_02();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_RPOD_02CLASS_H__ */

    
/* ---------- end of tc_rpod_02_class.hpp ---------- */
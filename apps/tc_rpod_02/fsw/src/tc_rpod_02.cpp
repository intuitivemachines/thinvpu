#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_rpod_02.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_rpod_02::load_img(const char *filepath)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    OS_TranslatePath("/cf/apps/dragon5.jpg", cTranslatedPath);
    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    tc_rpod_02_img_1 = imread(cTranslatedPath, IMREAD_GRAYSCALE);
    tc_rpod_02_img_2 = imread(filepath, IMREAD_GRAYSCALE);

    // Check image loaded correctly
    if(tc_rpod_02_img_1.empty()||tc_rpod_02_img_2.empty())
    {
        LOG_INFO("Warning Empty image found in TC_RPOD_02");
        return -1;
    }
    return 0;
}

int tc_rpod_02::process_img(TC_RPOD_02_OUT_T *tc_rpod_02_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    const float ratio_thresh = 0.7f;
    vector<KeyPoint> keypoints_1;
    vector<KeyPoint> keypoints_2;
    Mat descriptors_1;
    Mat descriptors_2;
    vector<vector<DMatch> > knn_matches;
    vector<DMatch> good_matches;
    Mat img_matches;
    vector<Point2f> first_2d_points;
    vector<Point2f> second_2d_points;

    // =====================
    // ORB Feature Matching
    // =====================
    Ptr<ORB> detector = ORB::create(
        2500,    // nfeatures: Maximum features to compute
        1.2f,   // scaleFactor: Pyramid ratio (Greater than 1.0)
        8,      // nLevels: number of pyramid levels to use
        31,     // edgeThreshold: Size of no-search border
        0,      // firstlevel; always 0
        2,      // WTA_K: Pts in each comparison: 2, 3, or 4
        ORB::HARRIS_SCORE,  // ScoreType: Harris_Score = 0, Fast_Score = 1
        31,     // PatchSize: Size of patch for each descriptors
        20);    // fastThreshold: threshold for fast detection
    detector->detectAndCompute(tc_rpod_02_img_1, cv::Mat(), keypoints_1, descriptors_1); //ORB outputs a binary descriptor
    detector->detectAndCompute(tc_rpod_02_img_2, cv::Mat(), keypoints_2, descriptors_2);
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    
    // Filter matches using the Lowe's ratio test
    matcher->knnMatch( descriptors_1, descriptors_2, knn_matches, 2 );
    for (size_t i = 0; i < knn_matches.size(); i++)
    {
        if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }

    LOG_INFO("Key points img 1 found %d", keypoints_1.size());
    LOG_INFO("Key points img 2 found %d", keypoints_2.size());
    LOG_INFO("Number of matches found %d", knn_matches.size());
    LOG_INFO("Lowes number of good matches %d", good_matches.size());
    
    for (int i=0;i<good_matches.size();i++)
    {
        int idx1=good_matches[i].trainIdx;
        int idx2=good_matches[i].queryIdx;
        first_2d_points.push_back(keypoints_1[idx1].pt);
        second_2d_points.push_back(keypoints_2[idx2].pt);
    }

    // Draw matches
    drawMatches( tc_rpod_02_img_1, keypoints_1, tc_rpod_02_img_2, keypoints_2, good_matches, img_matches, Scalar(0,255,0),
                 Scalar(0,255,0), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    // Write to file to view the Matched results given the name currently being processed
    OS_TranslatePath("/cf/vpu/1_tc_rpod_02_output.jpg", cTranslatedPath);
    cv::imwrite(cTranslatedPath, img_matches);

    if(tc_rpod_02_out_ptr != NULL)
    {
        tc_rpod_02_out_ptr->img_1_num_features = static_cast<int>( keypoints_1.size());
        tc_rpod_02_out_ptr->img_2_num_features = static_cast<int>( keypoints_2.size());
        tc_rpod_02_out_ptr->lowes_features = static_cast<int>( good_matches.size());
    }
    return 0;
}

// Class Deconstructor
tc_rpod_02::~tc_rpod_02() = default;

#ifdef __cplusplus
}
#endif
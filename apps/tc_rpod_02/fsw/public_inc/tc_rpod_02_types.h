// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
**
** Purpose:
**    Template application definitions and data structures
**
**============================================================================= */

#ifndef _TC_RPOD_02_TYPES_H_
#define _TC_RPOD_02_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

#define TC_RPOD_02_DBG DBG_ERR

/*
 ** Includes
 */
#include "common_types.h"

/**
 * Enums, Error codes and returns
 */
typedef enum
{
    ERR_TC_RPOD_02_OK                    =  0,
    ERR_TC_RPOD_02_NULL                  = -1,  
    ERR_TC_RPOD_02_ERROR                 = -2,
} TC_RPOD_02_ERR_T;

/*
 ** Defines
 */

/*
 Structure Declarations
*/

/* App command structure */
typedef struct TC_RPOD_02_CMD_T {

   uint8   do_iload_init;     // (--)   0 = don't do an iload init, 1 = do an iload init
   uint8   iload_table_id;    // (--)   I-load table ID
   uint8   ucTcStartTestCmd;  // (--)   1 = True, 0 = False 
   uint8   spare0[5];         // (--)   Padding for 64 bit / 8 byte alignment

} TC_RPOD_02_CMD_T;

/* TC_RPOD_02 I-loads */
typedef struct TC_RPOD_02_ILOAD_T {
  uint8  mode;               // (--)   Mode config
  uint8  FirstPassComplete;  // (--)   0 = not complete, 1 = complete
  uint8  sparebits[6];       // (--)   Padding
} TC_RPOD_02_ILOAD_T;

/* Internal state data used in TC_RPOD_02 I/O app */
typedef struct TC_RPOD_02_INTERNAL_T {
  uint8 ucStartTest;
} TC_RPOD_02_INTERNAL_T;

typedef struct {
  float  time;              // (s)    Time of current data set
} TC_RPOD_02_HK_T;

/** \brief Multi sample TC_RPOD_02 IO App buffer*/
typedef struct TC_RPOD_02_OUT_T
{
    double   time_tag;          /* (s)   Time tag */
    uint16   test_active;
    uint16   test_complete;
    uint16   test_count;
    uint16   spare;
    uint16   img_1_num_features;
    uint16   img_2_num_features;
    uint16   lowes_features; 
} TC_RPOD_02_OUT_T;

#ifdef __cplusplus
}
#endif

#endif /* _TC_RPOD_02_TYPES_H_ */

/*=======================================================================================
** End of file tc_rpod_02_types.h
**=====================================================================================*/


// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_plha_03_app.h
**
** Title:  Header File for the TC_PLHA_03 Application
**
** Purpose:  To define the TC_PLHA_03's internal macros, data types, global variables and
**           function prototypes
**
** Modification History:
**   Date       | Author         |Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2015-01-13 | Scott Tamblyn  | 0026    | Populate CFS Message/App layer I/O members across AGN&C
**   2018-10-17 | Brian Butcher  |   --    | Merged into cFE 6.5.0a app template
**
**=====================================================================================*/
    
#ifndef __TC_PLHA_03_APP_H__
#define __TC_PLHA_03_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "common/utils/perf.h"

#include "sch_app_rates.h"

#include "tc_plha_03_public.h"
#include "tc_plha_03_mission_cfg.h"
#include "tc_plha_03_tbldefs.h"
#include "tc_plha_03_types.h"


/*
** Local Defines
*/
#define TC_PLHA_03_SCH_PIPE_DEPTH  10
#define TC_PLHA_03_CMD_PIPE_DEPTH  10
#define TC_PLHA_03_TLM_PIPE_DEPTH  10

/*
** Local Structure Declarations
*/
typedef struct
{
   
    /* CFE Event table */
    CFE_EVS_BinFilter_t  EventTbl[TC_PLHA_03_EVT_CNT];

    /* CFE scheduling pipe */
    CFE_SB_PipeId_t  SchPipeId; 
    uint16           usSchPipeDepth;
    char             cSchPipeName[OS_MAX_API_NAME];

    /* CFE command pipe */
    CFE_SB_PipeId_t  CmdPipeId;
    uint16           usCmdPipeDepth;
    char             cCmdPipeName[OS_MAX_API_NAME];
    
    /* CFE telemetry pipe */
    CFE_SB_PipeId_t  TlmPipeId;
    uint16           usTlmPipeDepth;
    char             cTlmPipeName[OS_MAX_API_NAME];

    /* Task-related */
    uint32  uiRunStatus;
    double  prevExecTime;

    /* ILoad table-related */
    CFE_TBL_Handle_t  ILoadTblHdl;
    TC_PLHA_03_ILoadTblEntry_t*  ILoadTblPtr;

    /* Input data - from I/O devices or subscribed from other apps' output data.
       Data structure should be defined in tc_plha_03/fsw/mission_inc/tc_plha_03_private_types.h */
    TC_PLHA_03_InData_t   InData;

    /* Output data - to be published at the end of a Wakeup cycle.
       Data structure should be defined in tc_plha_03/fsw/platform_inc/tc_plha_03_platform_cfg.h */
    TC_PLHA_03_OutPacket_t  OutPacket;

    /* Housekeeping telemetry - for downlink only.
       Data structure should be defined in tc_plha_03/fsw/platform_inc/tc_plha_03_platform_cfg.h */
    TC_PLHA_03_HkTlm_t  HkTlm;

    

    /* Add declarations for additional private data here */
    TC_PLHA_03_INTERNAL_T StateData;
	
} TC_PLHA_03_AppData_t;


/*
** Local Function Prototypes
*/
int32  TC_PLHA_03_InitApp(void);
int32  TC_PLHA_03_InitEvent(void);
int32  TC_PLHA_03_InitData(void);
int32  TC_PLHA_03_InitPipe(void);

void  TC_PLHA_03_AppMain(void);

void  TC_PLHA_03_CleanupCallback(void);

int32  TC_PLHA_03_RcvMsg(int32 iBlocking);

void  TC_PLHA_03_ProcessNewData(void);
void  TC_PLHA_03_ProcessNewCmds(void);
void  TC_PLHA_03_ProcessCommandPacket(CFE_SB_Msg_t*);

void  TC_PLHA_03_ReportHousekeeping(void);
void  TC_PLHA_03_SendOutData(void);
void  TC_PLHA_03_WriteOutData(void);

void  TC_PLHA_03_CheckStatusOfTables(void);

boolean  TC_PLHA_03_VerifyCmdLength(CFE_SB_Msg_t*, uint16);


#ifdef __cplusplus
}
#endif

#endif /* ____TC_PLHA_03_APP_H__ */

    
/* ---------- end of tc_plha_03_app.h ---------- */

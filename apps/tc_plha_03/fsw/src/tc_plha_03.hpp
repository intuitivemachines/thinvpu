#ifndef __TC_PLHA_03CLASS_H__
#define __TC_PLHA_03CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_plha_03_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_plha_03
{
public:
  cv::Mat image;                // Orignal Image
  int load_img(const char*);    // Load image
  int process_img(TC_PLHA_03_OUT_T*);
  virtual ~tc_plha_03();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_PLHA_03CLASS_H__ */

    
/* ---------- end of tc_plha_03_class.hpp ---------- */
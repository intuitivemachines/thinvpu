/*=======================================================================================
** File Name:  tc_plha_03_tbldefs.h
**
** Title:  Header File for TC_PLHA_03 Application's tables
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TC_PLHA_03's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_PLHA_03_TBLDEFS_H_
#define _TC_PLHA_03_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_plha_03_mission_cfg.h"

/*
** Local Defines
*/
#define TC_PLHA_03_ILOAD_MAX_ENTRIES  1
#define TC_PLHA_03_ILOAD_FILENAME     "/cf/apps/tc_plha_03_tbl.tbl"
#define TC_PLHA_03_ILOAD_TABLENAME    "tc_plha_03_tbl"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    TC_PLHA_03_ILOAD_T iloads;
} TC_PLHA_03_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _TC_PLHA_03_TBLDEFS_H_ */

/*=======================================================================================
** End of file TC_PLHA_03_tbldefs.h
**=====================================================================================*/
    

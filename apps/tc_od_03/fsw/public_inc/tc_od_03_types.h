// Copyright 2019 Intuitive Machines, LLC. All rights reserved.
/*==============================================================================
**
** Purpose:
**    Template application definitions and data structures
**
**============================================================================= */

#ifndef _TC_OD_03_TYPES_H_
#define _TC_OD_03_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

#define TC_OD_03_DBG DBG_ERR

/*
 ** Includes
 */
#include "common_types.h"

/**
 * Enums, Error codes and returns
 */
typedef enum
{
    ERR_TC_OD_03_OK                    =  0,
    ERR_TC_OD_03_NULL                  = -1,  
    ERR_TC_OD_03_ERROR                 = -2,
} TC_OD_03_ERR_T;

/*
 ** Defines
 */
#define OD_03_MAX_CANNY_ROW         2160
#define OD_03_MAX_CANNY_COL         32

/*
 Structure Declarations
*/

/* App command structure */
typedef struct TC_OD_03_CMD_T {

   uint8   do_iload_init;     // (--)   0 = don't do an iload init, 1 = do an iload init
   uint8   iload_table_id;    // (--)   I-load table ID
   uint8   ucTcStartTestCmd;  // (--)   1 = True, 0 = False 
   uint8   spare0[5];         // (--)   Padding for 64 bit / 8 byte alignment

} TC_OD_03_CMD_T;

/* TC_OD_03 I-loads */
typedef struct TC_OD_03_ILOAD_T {
  uint8  mode;               // (--)   Mode config
  uint8  FirstPassComplete;  // (--)   0 = not complete, 1 = complete
  uint8  sparebits[6];       // (--)   Padding
} TC_OD_03_ILOAD_T;

/* Internal state data used in TC_OD_03 I/O app */
typedef struct TC_OD_03_INTERNAL_T {
  uint8 ucStartTest;
} TC_OD_03_INTERNAL_T;

typedef struct {
  float  time;              // (s)    Time of current data set
} TC_OD_03_HK_T;

/** \brief Multi sample TC_OD_03 IO App buffer*/
typedef struct TC_OD_03_OUT_T
{
    double   time_tag;          /* (s)   Time tag */
    uint16   test_active;
    uint16   test_complete;
    uint16   test_count;
    uint16   spare;
    unsigned char od_03_canny_arr[OD_03_MAX_CANNY_ROW][OD_03_MAX_CANNY_COL];
} TC_OD_03_OUT_T;

#ifdef __cplusplus
}
#endif

#endif /* _TC_OD_03_TYPES_H_ */

/*=======================================================================================
** End of file tc_od_03_types.h
**=====================================================================================*/


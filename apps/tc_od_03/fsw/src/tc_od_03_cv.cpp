#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_od_03_cv.hpp"
#include "tc_od_03.hpp"
#include "tc_od_03_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_od_03_driver(TC_OD_03_OUT_T *tc_od_03_out_ptr,const char *fileString)
{
    tc_od_03 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        // TODO: Add function calls as needed
        tc.canny_edge_detection(tc_od_03_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
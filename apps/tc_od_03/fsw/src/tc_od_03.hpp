#ifndef __TC_OD_03CLASS_H__
#define __TC_OD_03CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_od_03_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_od_03
{
public:
  cv::Mat image;                              // Orignal Image
  int load_img(const char*);                  // Load Image
  int canny_edge_detection(TC_OD_03_OUT_T *); // Canny edge detection on loaded Image
  virtual ~tc_od_03();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_03CLASS_H__ */

    
/* ---------- end of tc_od_03_class.hpp ---------- */
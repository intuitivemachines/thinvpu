/* Copyright 2019 Intuitive Machines, LLC. All rights reserved.
**=======================================================================================
**
** Purpose:  TC_OD_03 I/O App Executive Layer
**
**==============================================================================*/

#include "tc_od_03_exec.h"

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"
#include "tc_od_03_cv.hpp"
/*
Global Variables
 */

/* initialize */

/* From App InitApp */

/*==============================================================================
 ** Function Name:  tc_od_03_initialize()
 **
 ** Purpose:
 **    Top level initialization function for TC_OD_03 
 **
 **==============================================================================*/
 int32 tc_od_03_initialize(
     TC_OD_03_ILOAD_T*     pIload,
     TC_OD_03_INTERNAL_T*  pInternal,
     TC_OD_03_OUT_T*     pTC_OD_03 )
 {
    /* Error checking */
    ERROR_NULL_CHK_RTRN(pIload);
    ERROR_NULL_CHK_RTRN(pInternal);
    ERROR_NULL_CHK_RTRN(pTC_OD_03);

    /* Initialize output structures to zero */
    memset(pInternal, 0, sizeof(TC_OD_03_INTERNAL_T) );
    memset(pTC_OD_03, 0, sizeof(TC_OD_03_OUT_T) );

    //pInternal->dataCycleCounter = 0; //Structure is empty at the moment

    LOG_TRACE( "in init" );

    return ( 0 );
}

int32 tc_od_03_exec( /* RETURN:  Always return 0 for successful execution */
      /* INPUTS */
      TC_OD_03_InData_t   *tc_od_03_indata_ptr,    /* (--)  InData structure                */
      TC_OD_03_ILOAD_T    *tc_od_03_iload_ptr,     /* (--)  Iload structure                 */
      /* OUTPUTS */
      TC_OD_03_INTERNAL_T *tc_od_03_internal_ptr,  /* (--)  State memory for this function  */
      TC_OD_03_OUT_T      *tc_od_03_out_ptr        /* (--) Output from TC_OD_03 App       */
)
{
    /* Variable Declarations */
    int8 iStatus = ERR_TC_OD_03_OK;  // OK until proven otherwise
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];

    /* Error checking */
    ERROR_NULL_CHK_RTRN( tc_od_03_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_03_iload_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_03_internal_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_03_out_ptr );

    iStatus = tc_od_03_cmd_handler(tc_od_03_indata_ptr,tc_od_03_internal_ptr);
    if( iStatus != ERR_TC_OD_03_OK)
    {
        LOG_ERROR("Exec Command Handler Error");
    }

    /* Start test can be commmanded via SB Command or TCM Outdata */
    if(tc_od_03_internal_ptr->ucStartTest)
    {
        tc_od_03_internal_ptr->ucStartTest = 0;
        tc_od_03_out_ptr->test_active = TRUE;
        tc_od_03_out_ptr->test_complete = FALSE;
        LOG_INFO("TC_OD_03 Test Started");

        OS_TranslatePath("/cf/apps/OD_ORB.png", cTranslatedPath);
        tc_od_03_driver(tc_od_03_out_ptr, cTranslatedPath);

        tc_od_03_out_ptr->test_active = FALSE;
        tc_od_03_out_ptr->test_complete = TRUE;
        tc_od_03_out_ptr->test_count++;
        LOG_INFO("TC_OD_03 Test Complete");
    }

    return iStatus;
}

/*==============================================================================
 ** Function Name:  tc_od_03_exec_zero_outputs()
 **
 ** Purpose:
 **    Zeros the output
 **
 **==============================================================================
 */
int32 tc_od_03_exec_zero_outputs( TC_OD_03_OUT_T *tc_od_03_out_ptr )
{
   ERROR_NULL_CHK_RTRN( tc_od_03_out_ptr );
   memset( tc_od_03_out_ptr, 0, sizeof(TC_OD_03_OUT_T) );
   return ( 0 );
}

/*==============================================================================
 ** Function Name:  tc_od_03_cmd_handler()
 **
 ** Purpose:
 **    Process incoming commands from app layer or TCM
 **
 **==============================================================================
 */
int32 tc_od_03_cmd_handler( TC_OD_03_InData_t   *tc_od_03_indata_ptr,
                              TC_OD_03_INTERNAL_T *tc_od_03_internal_ptr )
{
    ERROR_NULL_CHK_RTRN( tc_od_03_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_od_03_internal_ptr );

    if(tc_od_03_indata_ptr->tc_cmd.ucTcStartTestCmd)
    {
        if(tc_od_03_internal_ptr->ucStartTest)
        {
            LOG_INFO("TC_OD_03 Already Running - No Action");
        }
        else
        {
            tc_od_03_internal_ptr->ucStartTest = TRUE;
        }

        tc_od_03_indata_ptr->tc_cmd.ucTcStartTestCmd = FALSE;
    }

    /* TODO: Add TCM processing when available */

   return ( ERR_TC_OD_03_OK );
}
 /* ---------- end of tc_od_03_exec_zero_outputs() ----------*/


#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_od_03.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_od_03::load_img(const char *filepath)
{
    // Read image
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

int tc_od_03::canny_edge_detection( TC_OD_03_OUT_T *tc_od_03_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    OS_TranslatePath("/cf/vpu/1_TC_OD_03_output.png", cTranslatedPath);

    Mat TC_OD_03_output;
    equalizeHist(image, image);
    Canny( image, TC_OD_03_output, 0, 50, 3 );
    cv::imwrite(cTranslatedPath, TC_OD_03_output);
   
    int num_canny_rows = TC_OD_03_output.rows;
    int num_canny_cols = TC_OD_03_output.cols;
    
    if(tc_od_03_out_ptr != NULL)
    {
        if(num_canny_rows > OD_03_MAX_CANNY_ROW)
        {
            num_canny_rows = OD_03_MAX_CANNY_ROW;
        }

        if(num_canny_cols > OD_03_MAX_CANNY_COL)
        {
            num_canny_cols = OD_03_MAX_CANNY_COL;
        }

        // Converting Mat TC_OD_03_output to uchar[][] for C type out-packet
        for(int i=0; i<num_canny_rows; i++)
        {
            for(int j=0; j<num_canny_cols; j++)
            {
                tc_od_03_out_ptr->od_03_canny_arr[i][j] = TC_OD_03_output.at<unsigned char>(i,j);
            }
        }
    }
    return 0;
}


// Class Deconstructor
tc_od_03::~tc_od_03() = default;

#ifdef __cplusplus
}
#endif
/*=======================================================================================
** File Name:  tc_od_03_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for TC_OD_03 Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  To define TC_OD_03's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_OD_03_ILOAD_UTILS_H_
#define _TC_OD_03_ILOAD_UTILS_H_


/*
** Include Files
*/
#include "tc_od_03_tbldefs.h"


/*
** Local Function Prototypes
*/
int32  TC_OD_03_InitILoadTbl(void);
int32  TC_OD_03_ValidateILoadTbl(TC_OD_03_ILoadTblEntry_t*);
void   TC_OD_03_ProcessNewILoadTbl(void);

#endif /* _TC_OD_03_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file tc_od_03_iload_utils.h
**=====================================================================================*/
    

// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_od_01_a_iload_utils.c
**
** Title:  Iload Tables' Utilities for TC_OD_01_A Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This source file contains definitions of ILoad table-related utility
**           function for TC_OD_01_A application.
**
** Limitations, Assumptions, External Events, and Notes:
**    1. One source file per CFS table!
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/

/*
** Include Files
*/
#include "tc_od_01_a_app.h"
#include "tc_od_01_a_iload_utils.h"

/*
** External Global Variables
*/
extern TC_OD_01_A_AppData_t  g_TC_OD_01_A_AppData;

/*
** Function Definitions
*/
    
/*=====================================================================================
** Name: TC_OD_01_A_InitILoadTbl
**
** Purpose: To initialize the TC_OD_01_A's ILoad tables
**
** Routines Called:
**    CFE_TBL_Register
**    CFE_TBL_Load
**    CFE_TBL_Manage
**    CFE_TBL_GetAddress
**    CFE_ES_WriteToSysLog
**    TC_OD_01_A_ValidateILoadTbl
**    TC_OD_01_A_ProcessNewILoadTbl
**=====================================================================================*/
int32 TC_OD_01_A_InitILoadTbl()
{
    int32  iStatus=0;

    /* Register ILoad table */
    iStatus = CFE_TBL_Register(&g_TC_OD_01_A_AppData.ILoadTblHdl,
                               TC_OD_01_A_ILOAD_TABLENAME,
                               (sizeof(TC_OD_01_A_ILoadTblEntry_t) * TC_OD_01_A_ILOAD_MAX_ENTRIES),
                               CFE_TBL_OPT_DEFAULT,
                               TC_OD_01_A_ValidateILoadTbl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_OD_01_A - Failed to register ILoad table (0x%08X)\n", iStatus);
        goto TC_OD_01_A_InitILoadTbl_Exit_Tag;
    }

    /* Load ILoad table file */
    iStatus = CFE_TBL_Load(g_TC_OD_01_A_AppData.ILoadTblHdl,
                           CFE_TBL_SRC_FILE,
                           TC_OD_01_A_ILOAD_FILENAME);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_OD_01_A - Failed to load ILoad Table (0x%08X)\n", iStatus);
        goto TC_OD_01_A_InitILoadTbl_Exit_Tag;
    }

    /* Manage ILoad table */
    iStatus = CFE_TBL_Manage(g_TC_OD_01_A_AppData.ILoadTblHdl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_OD_01_A - Failed to manage ILoad table (0x%08X)\n", iStatus);
        goto TC_OD_01_A_InitILoadTbl_Exit_Tag;
    }

    /* Make sure ILoad table is accessible by getting referencing it */
    iStatus = CFE_TBL_GetAddress((void*)&g_TC_OD_01_A_AppData.ILoadTblPtr,
                                 g_TC_OD_01_A_AppData.ILoadTblHdl);
    if (iStatus != CFE_TBL_INFO_UPDATED)
    {
        CFE_ES_WriteToSysLog("TC_OD_01_A - Failed to get ILoad table's address (0x%08X)\n", iStatus);
        goto TC_OD_01_A_InitILoadTbl_Exit_Tag;
    }

    /* Validate ILoad table */
    iStatus = TC_OD_01_A_ValidateILoadTbl(g_TC_OD_01_A_AppData.ILoadTblPtr);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("TC_OD_01_A - Failed to validate ILoad table (0x%08X)\n", iStatus);
        goto TC_OD_01_A_InitILoadTbl_Exit_Tag;
    }

    /* Set new parameter values */
    TC_OD_01_A_ProcessNewILoadTbl();

TC_OD_01_A_InitILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_OD_01_A_ValidateILoadTbl
**
** Purpose: To validate the TC_OD_01_A's ILoad tables
**
** Arguments:
**    TC_OD_01_A_ILoadTblEntry_t*  iLoadTblPtr - pointer to the ILoad table
**=====================================================================================*/
int32 TC_OD_01_A_ValidateILoadTbl(TC_OD_01_A_ILoadTblEntry_t* iLoadTblPtr)
{
    int32  iStatus=0;

    if (iLoadTblPtr == NULL)
    {
        iStatus = -1;
        goto TC_OD_01_A_ValidateILoadTbl_Exit_Tag;
    }

    /* TODO:  Add code to validate new data values here.
    **
    ** Examples:
    **    if (iLoadTblPtr->sParam <= 16)
    **    {
    **        CFE_ES_WriteToSysLog("TC_OD_01_A - Invalid value for ILoad parameter sParam (%d)\n",
    **                             iLoadTblPtr->sParam);
    */

TC_OD_01_A_ValidateILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: TC_OD_01_A_ProcessNewILoadTbl
**
** Purpose: To process TC_OD_01_A's new ILoad tables and set ILoad parameters with new values
**=====================================================================================*/
void TC_OD_01_A_ProcessNewILoadTbl()
{
    /* TODO:  Add code to set new ILoad parameters with new values here.
    **
    ** Examples:
    **
    **    g_TC_OD_01_A_AppData.latest_sParam = g_TC_OD_01_A_AppData.ILoadTblPtr->sParam;
    **    g_TC_OD_01_A_AppData.latest_fParam = g_TC_OD_01_A.AppData.ILoadTblPtr->fParam;
    */

    /*
    ** Process new iload table...
    */
    for (int32 TableIndex = 0; TableIndex < TC_OD_01_A_ILOAD_MAX_ENTRIES; TableIndex++)
    {
        memcpy(&g_TC_OD_01_A_AppData.InData.tc_od_01_a_iload, &g_TC_OD_01_A_AppData.ILoadTblPtr[TableIndex].iloads, sizeof(TC_OD_01_A_ILOAD_T));
    }
}
    
/*=======================================================================================
** End of file tc_od_01_a_iload_utils.c
**=====================================================================================*/
    

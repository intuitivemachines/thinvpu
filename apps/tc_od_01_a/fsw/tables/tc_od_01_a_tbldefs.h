/*=======================================================================================
** File Name:  tc_od_01_a_tbldefs.h
**
** Title:  Header File for TC_OD_01_A Application's tables
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TC_OD_01_A's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_OD_01_A_TBLDEFS_H_
#define _TC_OD_01_A_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_od_01_a_mission_cfg.h"

/*
** Local Defines
*/
#define TC_OD_01_A_ILOAD_MAX_ENTRIES  1
#define TC_OD_01_A_ILOAD_FILENAME     "/cf/apps/tc_od_01_a_tbl.tbl"
#define TC_OD_01_A_ILOAD_TABLENAME    "tc_od_01_a_tbl"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    TC_OD_01_A_ILOAD_T iloads;
} TC_OD_01_A_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _TC_OD_01_A_TBLDEFS_H_ */

/*=======================================================================================
** End of file tc_od_01_a_tbldefs.h
**=====================================================================================*/
    

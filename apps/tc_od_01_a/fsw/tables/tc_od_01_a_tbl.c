/*=======================================================================================
** File Name:  tc_od_01_a_iloads.c
**
** Title:  Default Iload Table for TC_OD_01_A Application
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This source file contains definition of table content for TC_OD_01_A application's 
**           default ILoad table.
**
** Limitations, Assumptions, External Events, and Notes:
**    1. One source file per CFS table!
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/

/*
** Pragmas
*/

/*
** Include Files
*/
#include "cfe_tbl_filedef.h"
#include "tc_od_01_a_tbldefs.h"

/*
** Table file header
*/
static CFE_TBL_FileDef_t CFE_TBL_FileDef =
{
    /* Content format: ObjName[64], TblName[38], Desc[32], TgtFileName[20], ObjSize 
    **    ObjName - variable name of ILoad table, e.g., TC_OD_01_A_ILoadDefTbl[]
    **    TblName - app's table name, e.g., TC_OD_01_A.ILOAD_TBL, where TC_OD_01_A is the same app name
    **              used in cfe_es_startup.scr, and TC_OD_01_A_defILoadTbl is the same table
    **              name passed in to CFE_TBL_Register()
    **    Desc - description of table in string format
    **    TgtFileName[20] - table file name, compiled as .tbl file extension
    **    ObjSize - size of the entire table
    */

    "tc_od_01_a_iLoadTable", "TC_OD_01_A.tc_od_01_a_tbl", "TC_OD_01_A iLoads table",
    "tc_od_01_a_tbl.tbl", (sizeof(TC_OD_01_A_ILoadTblEntry_t) * TC_OD_01_A_ILOAD_MAX_ENTRIES)

};

/*
** Default TC_OD_01_A iLoad table data
*/
TC_OD_01_A_ILoadTblEntry_t tc_od_01_a_iLoadTable[TC_OD_01_A_ILOAD_MAX_ENTRIES] =
{
	/* #0  */
    {
        .iloads.mode               = 0,
        .iloads.FirstPassComplete  = 0,

        .iloads.sparebits[0]       = 0,
        .iloads.sparebits[1]       = 0,
        .iloads.sparebits[2]       = 0,
        .iloads.sparebits[3]       = 0,
        .iloads.sparebits[4]       = 0,
        .iloads.sparebits[5]       = 0
    }
}; /* end TC_OD_01_A_iLoadTable */

/*=======================================================================================
** End of file tc_od_01_a_iloads.c
**=====================================================================================*/
    

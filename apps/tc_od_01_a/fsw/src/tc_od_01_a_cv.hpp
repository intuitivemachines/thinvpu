#ifndef __TC_OD_01_ACVBUILD_H__
#define __TC_OD_01_ACVBUILD_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "tc_od_01_a_types.h"

/*
** Local Defines
*/


/*
** Local Structure Declarations
*/


/*
** Local Function Prototypes
*/
int  tc_od_01_a_driver(TC_OD_01_A_OUT_T*,const char*);

#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_01_ACVBUILD_H__ */

    
/* ---------- end of cvbuild.hpp ---------- */

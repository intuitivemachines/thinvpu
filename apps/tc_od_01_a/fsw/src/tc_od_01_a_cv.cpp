#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_od_01_a_cv.hpp"
#include "tc_od_01_a.hpp"
#include "tc_od_01_a_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif


int tc_od_01_a_driver(TC_OD_01_A_OUT_T *tc_od_01_a_out_ptr,const char *fileString)
{
    tc_od_01_a tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.orb_detector(tc_od_01_a_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
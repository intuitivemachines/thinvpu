// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_od_01_a_private_types.h
**
** Title:  Type Header File for the TC_OD_01_A Application
**
** $Author:    Roscoe Ferguson
** $Date:      2015-01-07
**
** Purpose:  This header file contains declarations and definitions of all the TC_OD_01_A's private
**           data structures and data types.
**
** Modification History:
**   Date       | Author        | Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2015-01-20 | Scott Tamblyn  | 0026    | Populate CFS Message/App layer I/O members across AGN&C
**   2019-10-30 | Brian Butcher  |   --    | Configured for lander TC_OD_01_A device
**
**=====================================================================================*/
    
#ifndef _TC_OD_01_A_PRIVATE_TYPES_H_
#define _TC_OD_01_A_PRIVATE_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "tc_od_01_a_types.h"
//#include "tcm_public.h"


typedef enum {
//    TC_OD_01_A_DATAPIPE_TCM = 0,
    TC_OD_01_A_TOTAL_DATAPIPES
} TC_OD_01_A_pipes_enum;


typedef struct
{
    uint32  counter;
    TC_OD_01_A_ILOAD_T       tc_od_01_a_iload;
    TC_OD_01_A_CMD_T         tc_cmd;
//    TCM_OutPacket_t    sTcm_In;
} TC_OD_01_A_InData_t;


#ifdef __cplusplus
}
#endif

#endif /* _TC_OD_01_A_PRIVATE_TYPES_H_ */

/*=======================================================================================
** End of file tc_od_01_a_private_types.h
**=====================================================================================*/
    

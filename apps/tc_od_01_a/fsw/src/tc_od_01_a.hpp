#ifndef __TC_OD_01_ACLASS_H__
#define __TC_OD_01_ACLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_od_01_a_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_od_01_a
{
public:
  cv::Mat image;                      // Orignal Image
  int load_img(const char*);          // Load Image
  int orb_detector(TC_OD_01_A_OUT_T *);  // Orb detection on loaded Image
  virtual ~tc_od_01_a();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_OD_01_ACLASS_H__ */

    
/* ---------- end of tc_od_01_a_class.hpp ---------- */
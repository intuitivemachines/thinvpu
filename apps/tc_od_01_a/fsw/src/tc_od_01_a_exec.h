/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_OD_01_A executive.
**
**==============================================================================*/

#ifndef TC_OD_01_A_EXEC_H_
#define TC_OD_01_A_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_od_01_a_types.h"
//#include "tcm_public.h"
#include "tc_od_01_a_private_types.h"

/* function declarations */
int32 tc_od_01_a_initialize(
     TC_OD_01_A_ILOAD_T*     pIload,
     TC_OD_01_A_INTERNAL_T*  pInternal,
     TC_OD_01_A_OUT_T*       pTC_OD_01_A 
);

int32 tc_od_01_a_exec(
      /* INPUTS */
      TC_OD_01_A_InData_t   * tc_od_01_a_indata_ptr,   /* (--)  InData structure               */
      TC_OD_01_A_ILOAD_T    * tc_od_01_a_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_OD_01_A_INTERNAL_T * tc_od_01_a_internal_ptr, /* (--)  State memory for this function */
      TC_OD_01_A_OUT_T      * tc_od_01_a_out_ptr       /* (--)  Output from this function      */
);

int32 tc_od_01_a_exec_zero_outputs( TC_OD_01_A_OUT_T *out_ptr );

int32 tc_od_01_a_cmd_handler( TC_OD_01_A_InData_t   *tc_od_01_a_indata_ptr,
                              TC_OD_01_A_INTERNAL_T *tc_od_01_a_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_OD_01_A_EXEC_H_ */

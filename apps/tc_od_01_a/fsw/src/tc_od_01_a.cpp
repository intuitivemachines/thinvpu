#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_od_01_a.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_od_01_a::load_img(const char *filepath)
{
    // Read image
    image = imread(filepath, IMREAD_GRAYSCALE);
    
    // Check image loaded correctly
    if(image.empty())
    {
        LOG_ERROR("Error occured while opening Image");
        return -1;
    }
    return 0;
}

int tc_od_01_a::orb_detector(TC_OD_01_A_OUT_T *tc_od_01_a_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    OS_TranslatePath("/cf/vpu/1_TC_OD_01_A_output.png", cTranslatedPath);

    // initiate STAR detector
    Ptr<ORB> ORB_detector = ORB::create(
        2500,    // nfeatures: Maximum features to compute
        1.2f,   // scaleFactor: Pyramid ratio (Greater than 1.0)
        8,      // nLevels: number of pyramid levels to use
        31,     // edgeThreshold: Size of no-search border
        0,      // firstlevel; always 0
        2,      // WTA_K: Pts in each comparison: 2, 3, or 4
        ORB::HARRIS_SCORE,  // ScoreType: Harris_Score = 0, Fast_Score = 1
        31,     // PatchSize: Size of patch for each descriptors
        20);    // fastThreshold: threshold for fast detection
    
    // Find keypoint and compute with ORB
    vector<KeyPoint> keypoints;
    Mat descriptors;
    Mat TC_OD_01_A_output;

    // Uncomment to get more features by equalizing the image
    // equalizeHist(image, image);

    ORB_detector->detectAndCompute(image, TC_OD_01_A_output, keypoints, descriptors);
    // Output keypoints
    LOG_INFO("TC_OD_01_A Number of Key Point found %d", keypoints.size());
    LOG_INFO("TC_OD_01_A Centroid X/Y of the first 3 Key Points");
    LOG_INFO("TC_OD_01_A Keypoint 1: (%.1f, %.1f)", keypoints[0].pt.x, keypoints[0].pt.y);
    LOG_INFO("TC_OD_01_A Keypoint 2: (%.1f, %.1f)", keypoints[1].pt.x, keypoints[1].pt.y);
    LOG_INFO("TC_OD_01_A Keypoint 3: (%.1f, %.1f)", keypoints[2].pt.x, keypoints[2].pt.y);

    LOG_INFO("TC_OD_01_A Number of Descriptors Rows %d Columns %d", descriptors.rows,descriptors.cols);

    // Output first 5 Key points of the descriptors
    LOG_INFO("TC_OD_01_A Descriptor of the first 3 key points");
    LOG_INFO(
        "TC_OD_01_A Descriptor 1: [%d,%d,%d,%d,%d,%d, ...]",
        static_cast<int>(descriptors.at<uchar>(0,1)),
        static_cast<int>(descriptors.at<uchar>(0,2)),
        static_cast<int>(descriptors.at<uchar>(0,3)),
        static_cast<int>(descriptors.at<uchar>(0,4)),
        static_cast<int>(descriptors.at<uchar>(0,5)));
    LOG_INFO(
        "TC_OD_01_A Descriptor 2: [%d,%d,%d,%d,%d, ...]",
        static_cast<int>(descriptors.at<uchar>(1,1)),
        static_cast<int>(descriptors.at<uchar>(1,2)),
        static_cast<int>(descriptors.at<uchar>(1,3)),
        static_cast<int>(descriptors.at<uchar>(1,4)));
    LOG_INFO(
        "TC_OD_01_A Descriptor 3: [%d,%d,%d,%d,%d, ...]",
        static_cast<int>(descriptors.at<uchar>(2,1)),
        static_cast<int>(descriptors.at<uchar>(2,2)),
        static_cast<int>(descriptors.at<uchar>(2,3)),
        static_cast<int>(descriptors.at<uchar>(2,4)));

    // Uncomment below to print entire matrix on command line
    // cout << cv::format(descriptors, cv::Formatter::FMT_NUMPY) << endl;
    
    // Draw only keypoints with location, not size and orientation
    drawKeypoints(image, keypoints, TC_OD_01_A_output, Scalar(0,255,0), DrawMatchesFlags::DEFAULT);
    cv::imwrite(cTranslatedPath, TC_OD_01_A_output);

    int num_key_rows = keypoints.size();
    int num_desc_rows = descriptors.rows;
    int num_desc_cols = descriptors.cols;

    if(tc_od_01_a_out_ptr != NULL)
    {
        if(num_key_rows > OD_01_A_MAX_KEYPOINTS)
        {
            num_key_rows = OD_01_A_MAX_KEYPOINTS;
        }

        // Converting Mat with points to float[][] for C type out-packet
        for(int i=0; i<num_key_rows; i++)
        {
            tc_od_01_a_out_ptr->od_01_a_keypoint_arr[i][0] = static_cast<float> (keypoints[i].pt.x);
            tc_od_01_a_out_ptr->od_01_a_keypoint_arr[i][1] = static_cast<float> (keypoints[i].pt.y);
        }

        if(num_desc_rows > OD_01_A_MAX_DESC)
        {
            num_desc_rows = OD_01_A_MAX_DESC;
        }

        if(num_desc_cols > OD_01_A_MAX_DESC_COL)
        {
            num_desc_cols = OD_01_A_MAX_DESC_COL;
        }

        // Converting Mat descriptors to uchar[][] for C type out-packet
        for(int i=0; i<num_desc_rows; i++)
        {
            for(int j=0; j<num_desc_cols; j++)
            {
                tc_od_01_a_out_ptr->od_01_a_descriptors_arr[i][j] = descriptors.at<unsigned char>(i,j);
            }
        }
    }

    return 0;
}

// Class Deconstructor
tc_od_01_a::~tc_od_01_a() = default;

#ifdef __cplusplus
}
#endif
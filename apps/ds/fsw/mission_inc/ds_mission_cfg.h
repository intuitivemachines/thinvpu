// Copyright 2020 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  ds_mission_cfg.h
**
** Title:  Mission Configuration Header File for the DS Application
**
** $Author:    Miguel Rosales
** $Date:      2015-01-07
**
** Purpose:  This header file contains declartions and definitions of all the DS's 
**           mission-specific configurations.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**   2020-06-23 | Miguel Rosales| Build #: Modified for lander DS app
**
**=====================================================================================*/
    
#ifndef _DS_MISSION_CFG_H_
#define _DS_MISSION_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"

#include "MISSION_perf_ids.h"         // PERF IDs
#include "MISSION_cmd_ids.h"          // CMD MIDs
#include "MISSION_cmd_codes.h"        // APP CCs
#include "MISSION_tlm_ids.h"          // TLM MIDs

#ifdef __cplusplus
}
#endif

#endif /* _DS_MISSION_CFG_H_ */

/*=======================================================================================
** End of file ds_mission_cfg.h
**=====================================================================================*/
    

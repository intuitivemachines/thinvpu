/*=======================================================================================
** File Name:  tc_rpod_01_tbldefs.h
**
** Title:  Header File for TC_RPOD_01 Application's tables
**
** $Author:    Brian Butcher
** $Date:      2019-10-30
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in TC_RPOD_01's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2019-10-30 | Brian Butcher | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _TC_RPOD_01_TBLDEFS_H_
#define _TC_RPOD_01_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_rpod_01_mission_cfg.h"

/*
** Local Defines
*/
#define TC_RPOD_01_ILOAD_MAX_ENTRIES  1
#define TC_RPOD_01_ILOAD_FILENAME     "/cf/apps/tc_rpod_01_tbl.tbl"
#define TC_RPOD_01_ILOAD_TABLENAME    "tc_rpod_01_tbl"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    TC_RPOD_01_ILOAD_T iloads;
} TC_RPOD_01_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _TC_RPOD_01_TBLDEFS_H_ */

/*=======================================================================================
** End of file tc_rpod_01_tbldefs.h
**=====================================================================================*/
    

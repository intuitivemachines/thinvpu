#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_rpod_01_cv.hpp"
#include "tc_rpod_01.hpp"
#include "tc_rpod_01_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_rpod_01_driver(TC_RPOD_01_OUT_T *tc_rpod_01_out_ptr,const char *fileString)
{
    tc_rpod_01 tc;
    int loaded = tc.load_img(fileString);
    if(loaded == 0)
    {
        tc.orb_detector(tc_rpod_01_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
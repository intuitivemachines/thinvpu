#ifndef __TC_RPOD_01CLASS_H__
#define __TC_RPOD_01CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_rpod_01_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_rpod_01
{
public:
  cv::Mat image;                        // Orignal Image
  int load_img(const char*);            // Load Image
  int orb_detector(TC_RPOD_01_OUT_T*);  // Orb detection on loaded Image
  virtual ~tc_rpod_01();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_RPOD_01CLASS_H__ */

    
/* ---------- end of tc_rpod_01_class.hpp ---------- */
/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_RPOD_01 executive.
**
**==============================================================================*/

#ifndef TC_RPOD_01_EXEC_H_
#define TC_RPOD_01_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_rpod_01_types.h"
#include "tc_rpod_01_private_types.h"

/* function declarations */
int32 tc_rpod_01_initialize(
     TC_RPOD_01_ILOAD_T*     pIload,
     TC_RPOD_01_INTERNAL_T*  pInternal,
     TC_RPOD_01_OUT_T*       pTC_RPOD_01 
);

int32 tc_rpod_01_exec(
      /* INPUTS */
      TC_RPOD_01_InData_t   * tc_rpod_01_indata_ptr,   /* (--)  InData structure               */
      TC_RPOD_01_ILOAD_T    * tc_rpod_01_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_RPOD_01_INTERNAL_T * tc_rpod_01_internal_ptr, /* (--)  State memory for this function */
      TC_RPOD_01_OUT_T      * tc_rpod_01_out_ptr       /* (--)  Output from this function      */
);

int32 tc_rpod_01_exec_zero_outputs( TC_RPOD_01_OUT_T *out_ptr );

int32 tc_rpod_01_cmd_handler( TC_RPOD_01_InData_t   *tc_rpod_01_indata_ptr,
                              TC_RPOD_01_INTERNAL_T *tc_rpod_01_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_RPOD_01_EXEC_H_ */

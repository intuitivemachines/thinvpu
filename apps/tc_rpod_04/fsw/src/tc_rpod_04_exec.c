/* Copyright 2019 Intuitive Machines, LLC. All rights reserved.
**=======================================================================================
**
** Purpose:  TC_RPOD_04 I/O App Executive Layer
**
**==============================================================================*/

#include "tc_rpod_04_exec.h"

#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"
#include "tc_rpod_04_cv.hpp"
/*
Global Variables
 */

/* initialize */

/* From App InitApp */

/*==============================================================================
 ** Function Name:  tc_rpod_04_initialize()
 **
 ** Purpose:
 **    Top level initialization function for TC_RPOD_04 
 **
 **==============================================================================*/
 int32 tc_rpod_04_initialize(
     TC_RPOD_04_ILOAD_T*     pIload,
     TC_RPOD_04_INTERNAL_T*  pInternal,
     TC_RPOD_04_OUT_T*     pTC_RPOD_04 )
 {
    /* Error checking */
    ERROR_NULL_CHK_RTRN(pIload);
    ERROR_NULL_CHK_RTRN(pInternal);
    ERROR_NULL_CHK_RTRN(pTC_RPOD_04);

    /* Initialize output structures to zero */
    memset(pInternal, 0, sizeof(TC_RPOD_04_INTERNAL_T) );
    memset(pTC_RPOD_04, 0, sizeof(TC_RPOD_04_OUT_T) );

    //pInternal->dataCycleCounter = 0; //Structure is empty at the moment

    LOG_TRACE( "in init" );

    return ( 0 );
}

int32 tc_rpod_04_exec( /* RETURN:  Always return 0 for successful execution */
      /* INPUTS */
      TC_RPOD_04_InData_t   *tc_rpod_04_indata_ptr,    /* (--)  InData structure                */
      TC_RPOD_04_ILOAD_T    *tc_rpod_04_iload_ptr,     /* (--)  Iload structure                 */
      /* OUTPUTS */
      TC_RPOD_04_INTERNAL_T *tc_rpod_04_internal_ptr,  /* (--)  State memory for this function  */
      TC_RPOD_04_OUT_T      *tc_rpod_04_out_ptr        /* (--) Output from TC_RPOD_04 App       */
)
{
    /* Variable Declarations */
    int8 iStatus = ERR_TC_RPOD_04_OK;  // OK until proven otherwise
    char cTranslatedPath1[OS_MAX_LOCAL_PATH_LEN] = {0};
    char cTranslatedPath2[OS_MAX_LOCAL_PATH_LEN] = {0};

    /* Error checking */
    ERROR_NULL_CHK_RTRN( tc_rpod_04_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_rpod_04_iload_ptr );
    ERROR_NULL_CHK_RTRN( tc_rpod_04_internal_ptr );
    ERROR_NULL_CHK_RTRN( tc_rpod_04_out_ptr );

    iStatus = tc_rpod_04_cmd_handler(tc_rpod_04_indata_ptr,tc_rpod_04_internal_ptr);
    if( iStatus != ERR_TC_RPOD_04_OK)
    {
        LOG_ERROR("Exec Command Handler Error");
    }

    /* Start test can be commmanded via SB Command or TCM Outdata */
    if(tc_rpod_04_internal_ptr->ucStartTest)
    {
        tc_rpod_04_internal_ptr->ucStartTest = 0;
        tc_rpod_04_out_ptr->test_active = TRUE;
        tc_rpod_04_out_ptr->test_complete = FALSE;
        LOG_INFO("TC_RPOD_04 Test Started");

        OS_TranslatePath("/cf/apps/SpaceShipOrigin.png", cTranslatedPath1);
        OS_TranslatePath("/cf/apps/SpaceShipCenter.png", cTranslatedPath2);
        tc_rpod_04_driver(tc_rpod_04_out_ptr, cTranslatedPath1, cTranslatedPath2);

        tc_rpod_04_out_ptr->test_active = FALSE;
        tc_rpod_04_out_ptr->test_complete = TRUE;
        tc_rpod_04_out_ptr->test_count++;
        LOG_INFO("TC_RPOD_04 Test Complete");
    }

    return iStatus;
}

/*==============================================================================
 ** Function Name:  tc_rpod_04_exec_zero_outputs()
 **
 ** Purpose:
 **    Zeros the output
 **
 **==============================================================================
 */
int32 tc_rpod_04_exec_zero_outputs( TC_RPOD_04_OUT_T *tc_rpod_04_out_ptr )
{
   ERROR_NULL_CHK_RTRN( tc_rpod_04_out_ptr );
   memset( tc_rpod_04_out_ptr, 0, sizeof(TC_RPOD_04_OUT_T) );
   return ( 0 );
}

/*==============================================================================
 ** Function Name:  tc_rpod_04_cmd_handler()
 **
 ** Purpose:
 **    Process incoming commands from app layer or TCM
 **
 **==============================================================================
 */
int32 tc_rpod_04_cmd_handler( TC_RPOD_04_InData_t   *tc_rpod_04_indata_ptr,
                              TC_RPOD_04_INTERNAL_T *tc_rpod_04_internal_ptr )
{
    ERROR_NULL_CHK_RTRN( tc_rpod_04_indata_ptr );
    ERROR_NULL_CHK_RTRN( tc_rpod_04_internal_ptr );

    if(tc_rpod_04_indata_ptr->tc_cmd.ucTcStartTestCmd)
    {
        if(tc_rpod_04_internal_ptr->ucStartTest)
        {
            LOG_INFO("TC_RPOD_04 Already Running - No Action");
        }
        else
        {
            tc_rpod_04_internal_ptr->ucStartTest = TRUE;
        }

        tc_rpod_04_indata_ptr->tc_cmd.ucTcStartTestCmd = FALSE;
    }

    /* TODO: Add TCM processing when available */

   return ( ERR_TC_RPOD_04_OK );
}
 /* ---------- end of tc_rpod_04_exec_zero_outputs() ----------*/


#include <opencv2/core/utility.hpp>
#include <iostream>
#include <string>
#include "tc_rpod_04_cv.hpp"
#include "tc_rpod_04.hpp"
#include "tc_rpod_04_exec.h"

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_rpod_04_driver(TC_RPOD_04_OUT_T *tc_rpod_04_out_ptr,const char *fileString, const char* templateString)
{
    tc_rpod_04 tc;
    int loaded = tc.load_img(fileString, templateString);
    
    if(loaded == 0)
    {
        tc.template_matching(tc_rpod_04_out_ptr);
    }
    return 0;   
}

#ifdef __cplusplus
}
#endif
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include "tc_rpod_04.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_rpod_04::load_img(const char *filepath, const char *template_path)
{
    // Read image
    origin_image = imread(filepath, IMREAD_COLOR);
    template_image = imread(template_path, IMREAD_COLOR);

    // Check image loaded correctly
    if(origin_image.empty()||template_image.empty())
    {
        LOG_ERROR("TC_RPOD_04 Error occured while opening Images");
        return -1;
    }
    return 0;
}

int tc_rpod_04::template_matching(TC_RPOD_04_OUT_T* tc_rpod_04_out_ptr)
{
    string name_str;
    string filename;
    string cTranslatedPath(OS_MAX_LOCAL_PATH_LEN,'\0');
    double min_val, max_val, threshold = 0.7;
    Point min_loc, max_loc;
    Point point_template_loc;

    // Template Match variables
    Mat result_calc_sqdiff      (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 1. Mat SQDIFF
    Mat result_calc_sqdiff_norm (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 2. Mat SQDIFF NORMED 
    Mat result_calc_ccorr       (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 3. Mat CCORR
    Mat result_calc_ccorr_norm  (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 4. Mat CCORR NORMED
    Mat result_calc_ccoeff      (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 5. Mat COEFF
    Mat result_calc_ccoeff_norm (origin_image.rows-template_image.rows+1, origin_image.cols-template_image.cols+1, CV_32FC1); // 6. Mat COEFF NORMED

    // Template Match Types from TC-RPOD-04 Metrics
    matchTemplate(template_image, origin_image, result_calc_sqdiff,      TM_SQDIFF);        // 1. SQDIFF
    matchTemplate(template_image, origin_image, result_calc_sqdiff_norm, TM_SQDIFF_NORMED); // 2. SQDIFF NORMED
    matchTemplate(template_image, origin_image, result_calc_ccorr,       TM_CCORR);         // 3. TM CCORR
    matchTemplate(template_image, origin_image, result_calc_ccorr_norm,  TM_CCORR_NORMED);  // 4. TM CCORR NORMED
    matchTemplate(template_image, origin_image, result_calc_ccoeff,      TM_CCOEFF);        // 5. TM COEFF
    matchTemplate(template_image, origin_image, result_calc_ccoeff_norm, TM_CCOEFF_NORMED); // 6. TM COEFF NORMED

    // Store Template Matches in a list
    vector<Mat> list_template_match;
    list_template_match.push_back(result_calc_sqdiff);
    list_template_match.push_back(result_calc_sqdiff_norm);
    list_template_match.push_back(result_calc_ccorr);
    list_template_match.push_back(result_calc_ccorr_norm);
    list_template_match.push_back(result_calc_ccoeff);
    list_template_match.push_back(result_calc_ccoeff_norm);

    // TC_RPOD_04 outputs
    Mat TC_RPOD_04_output_1 = origin_image.clone();
    Mat TC_RPOD_04_output_2 = origin_image.clone();
    Mat TC_RPOD_04_output_3 = origin_image.clone();
    Mat TC_RPOD_04_output_4 = origin_image.clone();
    Mat TC_RPOD_04_output_5 = origin_image.clone();
    Mat TC_RPOD_04_output_6 = origin_image.clone();
    
    // Store outputs in a list
    vector<Mat> list_TC_RPOD_04_output;
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_1);
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_2);
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_3);
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_4);
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_5);
    list_TC_RPOD_04_output.push_back(TC_RPOD_04_output_6);    

    for(int i=0; i < list_template_match.size(); i++)
    {
        normalize( list_template_match[i], list_template_match[i], 0, 1, NORM_MINMAX, -1, Mat());
        minMaxLoc(list_template_match[i], &min_val, &max_val, &min_loc, &max_loc);
        if (max_val >= threshold)
        {
            point_template_loc= max_loc;
            name_str = to_string(i+1);
            LOG_INFO("TC_RPOD_04 Min Value: %d", (int)min_val);
            LOG_INFO("TC_RPOD_04 Max Value: %d", (int)max_val);
            LOG_INFO("TC_RPOD_04 Min location: (%d, %d)", min_loc.x , min_loc.y);
            LOG_INFO("TC_RPOD_04 Max location: (%d, %d)", max_loc.x , max_loc.y);

            rectangle( list_TC_RPOD_04_output[i], point_template_loc, Point( point_template_loc.x + template_image.cols , point_template_loc.y + template_image.rows ), Scalar(0,255,0), 2, 8, 0 );
            filename = "/cf/vpu/"+name_str+"_TC_RPOD_04_output.png";
            OS_TranslatePath(filename.c_str(),const_cast<char*> (cTranslatedPath.c_str()));
            imwrite(cTranslatedPath.c_str(), list_TC_RPOD_04_output[i]);
            tc_rpod_04_out_ptr->metric[i].min = min_val;
            tc_rpod_04_out_ptr->metric[i].max = min_val;
            tc_rpod_04_out_ptr->metric[i].min_loc_x = min_loc.x;
            tc_rpod_04_out_ptr->metric[i].min_loc_y = min_loc.y;
            tc_rpod_04_out_ptr->metric[i].max_loc_x = max_loc.x;
            tc_rpod_04_out_ptr->metric[i].max_loc_y = max_loc.y;
            
        }
        else
        {
            LOG_ERROR("TC_RPOD_04 No template match found");
        }
    }
    return 0;
}

// Class Deconstructor
tc_rpod_04::~tc_rpod_04() = default;

#ifdef __cplusplus
}
#endif
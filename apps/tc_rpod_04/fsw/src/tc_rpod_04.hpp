#ifndef __TC_RPOD_04CLASS_H__
#define __TC_RPOD_04CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_rpod_04_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_rpod_04
{
public:
  cv::Mat origin_image;        // Orignal Image
  cv::Mat template_image;  // Template image
  int load_img(const char*, const char*); // Load image
  int template_matching(TC_RPOD_04_OUT_T*);
  virtual ~tc_rpod_04();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_RPOD_04CLASS_H__ */

    
/* ---------- end of tc_rpod_04_class.hpp ---------- */
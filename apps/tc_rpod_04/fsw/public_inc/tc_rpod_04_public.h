// Copyright 2015 Intuitive Machines, LLC. All rights reserved.
/*=======================================================================================
** File Name:  tc_rpod_04_public.h
**
** Title:  Public Header File for the TC_RPOD_04 Application
**
** $Author:    Roscoe Ferguson
** $Date:      2015-01-07
**
** Purpose:  This header file contains declartions and definitions of all the TC_RPOD_04's 
**           public structs.
**
** Modification History:
**   Date       | Author        | Redmine# | Description
**   ------------------------------------------------------
**   2015-01-07 | Roscoe Ferguson|   --    | Code Started
**   2019-10-30 | Brian Butcher  |   --    | Configured for lander TC_RPOD_04 device
**
**=====================================================================================*/
    
#ifndef _TC_RPOD_04_PUBLIC_H_
#define _TC_RPOD_04_PUBLIC_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
** Include Files
*/
#include "cfe.h"
#include "tc_rpod_04_types.h"

/*
** Local Structure Declarations
*/
typedef struct
{
    uint8  ucCmdHeader[CFE_SB_CMD_HDR_SIZE];
} TC_RPOD_04_NoArgCmd_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint16  usCmdCnt;
    uint16  usCmdErrCnt;

    TC_RPOD_04_HK_T  HkData;
    uint8 ucSpare[4]; // Padding for 8 byte alignment
} TC_RPOD_04_HkTlm_t;

typedef struct
{
    uint8   ucTlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint32  uiCounter;
    float   fElapsedTime;
    float   fRateTime;

    TC_RPOD_04_OUT_T OutData;
} TC_RPOD_04_OutPacket_t;


#ifdef __cplusplus
}
#endif

#endif /* _TC_RPOD_04_PUBLIC_H_ */

/*=======================================================================================
** End of file tc_rpod_04_public.h
**=====================================================================================*/
    

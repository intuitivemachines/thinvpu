/* Copyright 2019 Intuitive Machines, LLC. All rights reserved. */
/*==============================================================================
**
** Purpose:   
**    Contains function prototypes for the top-level TC_PLHA_02 executive.
**
**==============================================================================*/

#ifndef TC_PLHA_02_EXEC_H_
#define TC_PLHA_02_EXEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "tc_plha_02_types.h"
//#include "tcm_public.h"
#include "tc_plha_02_private_types.h"

/* function declarations */
int32 tc_plha_02_initialize(
     TC_PLHA_02_ILOAD_T*     pIload,
     TC_PLHA_02_INTERNAL_T*  pInternal,
     TC_PLHA_02_OUT_T*       pTC_PLHA_02 
);

int32 tc_plha_02_exec(
      /* INPUTS */
      TC_PLHA_02_InData_t   * tc_plha_02_indata_ptr,   /* (--)  InData structure               */
      TC_PLHA_02_ILOAD_T    * tc_plha_02_iload_ptr,    /* (--)  Iload structure                */

      /* OUTPUTS */
      TC_PLHA_02_INTERNAL_T * tc_plha_02_internal_ptr, /* (--)  State memory for this function */
      TC_PLHA_02_OUT_T      * tc_plha_02_out_ptr       /* (--)  Output from this function      */
);

int32 tc_plha_02_exec_zero_outputs( TC_PLHA_02_OUT_T *out_ptr );

int32 tc_plha_02_cmd_handler( TC_PLHA_02_InData_t   *tc_plha_02_indata_ptr,
                              TC_PLHA_02_INTERNAL_T *tc_plha_02_internal_ptr );

/* ---------- end of function prototypes and declarations ----------*/

#ifdef __cplusplus
}
#endif
#endif /* TC_PLHA_02_EXEC_H_ */

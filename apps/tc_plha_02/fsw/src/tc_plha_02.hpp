#ifndef __TC_PLHA_02CLASS_H__
#define __TC_PLHA_02CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_plha_02_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_plha_02
{
public:
  cv::Mat tc_plha_02_img_1;   // image 1 to be loaded
  cv::Mat tc_plha_02_img_2;   // image 2 to be loaded
  int load_img(const char*);  // Load image
  int process_img(TC_PLHA_02_OUT_T*);
  virtual ~tc_plha_02();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_PLHA_02CLASS_H__ */

    
/* ---------- end of tc_plha_02_class.hpp ---------- */
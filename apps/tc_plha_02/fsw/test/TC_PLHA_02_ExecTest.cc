/*==============================================================================
** File Name: TC_PLHA_02ExecTest.cc
**
** Title: Unit Tests for TC_PLHA_02 Executive Layer
**
** Author: James Blakeslee
** Date: 2019-01-08
**============================================================================*/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestFixture.h>

#include "MathAssert.hh"  // convenience macros for matrix and vector comparisons

extern "C"
{
//#include "tc_plha_02_exec.h" TODO: include when executive layer becomes available
}

class TC_PLHA_02ExecTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE( TC_PLHA_02ExecTest );
    CPPUNIT_TEST( testMyTest1 );  // TODO: add unit tests here
    CPPUNIT_TEST( testMyTest2 );  // TODO: add unit tests here
    CPPUNIT_TEST_SUITE_END();

    public:
        void setUp();
        void tearDown();
        void testMyTest1();  // TODO: public visibility for all unit test functions
        void testMyTest2();  // TODO: public visibility for all unit test functions
};

/* called on instantiation */
void TC_PLHA_02ExecTest::setUp()
{
}

/* called after tests are complete (falls out-of-scope) */
void TC_PLHA_02ExecTest::tearDown()
{
}

/* TODO: short test description */
void TC_PLHA_02ExecTest::testMyTest1()
{
}

/* TODO: short test description */
void TC_PLHA_02ExecTest::testMyTest2()
{
}

CPPUNIT_TEST_SUITE_REGISTRATION( TC_PLHA_02ExecTest );

#ifndef __TC_PLHA_04CLASS_H__
#define __TC_PLHA_04CLASS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** Pragmas
*/

/*
** Include Files
*/
#include "tc_plha_04_types.h"
#include "common/utils/error.h"
#include "common/utils/logger.h"
#include "osapi.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** Local Function Prototypes
*/

class tc_plha_04
{
public:
  cv::Mat tc_plha_04_img_1;
  cv::Mat tc_plha_04_img_2;
  int load_img(const char*);  // Load image
  int process_img(TC_PLHA_04_OUT_T*);
  virtual ~tc_plha_04();
};


#ifdef __cplusplus
}
#endif

#endif /* __TC_PLHA_04CLASS_H__ */

    
/* ---------- end of tc_plha_04_class.hpp ---------- */
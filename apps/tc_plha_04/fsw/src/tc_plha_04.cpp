#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d.hpp>
#include "tc_plha_04.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <math.h>

using namespace cv;
using namespace std;

/* use C name mangling */
#ifdef __cplusplus
extern "C" {
#endif

int tc_plha_04::load_img(const char *filepath)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    OS_TranslatePath("/cf/apps/pert95.png", cTranslatedPath);

    // Read image
    LOG_INFO("Filepath recieved = %s", filepath);
    tc_plha_04_img_1 = imread(cTranslatedPath, IMREAD_GRAYSCALE);
    tc_plha_04_img_2 = imread(filepath, IMREAD_GRAYSCALE);

    // Check image loaded correctly
    if(tc_plha_04_img_1.empty()||tc_plha_04_img_2.empty())
    {
        LOG_ERROR("Warning Empty image found in TC_PLHA_04");
        return -1;
    }
    return 0;
}

int tc_plha_04::process_img(TC_PLHA_04_OUT_T *tc_plha_04_out_ptr)
{
    char cTranslatedPath[OS_MAX_LOCAL_PATH_LEN];
    Mat K = (Mat_<double>(3,3) << 4280, 0.0, 1280, 0.0, 4316.1, 1080, 0.0, 0.0, 1.0); // Intrinsic Matrix
    Mat R = (Mat_<double>(3,3) << 9.99997291e-01, 1.79813773e-03,  1.47779569e-03, -1.79866996e-03, 9.99998318e-01, 3.58903053e-04, -1.47714785e-03, -3.61560147e-04, 9.99998844e-01); // Rotation Matrix
    Mat T = (Mat_<double>(3,1) << 0.08465726, -0.17399634, 0.98110062); // Tranformation Matrix;
    const float ratio_thresh = 0.7f;
    vector<KeyPoint> keypoints_1;
    vector<KeyPoint> keypoints_2;
    Mat descriptors_1;
    Mat descriptors_2;
    vector<vector<DMatch> > knn_matches;
    vector<DMatch> good_matches;
    Mat img_matches;
    vector<Point2f> first_2d_points;
    vector<Point2f> second_2d_points;
    Point2d pricipal_point = Point2d(1280, 1080); // This is point in the image where it intersects the optical axis
    Mat M_r;
    Mat M_l;
    Mat p1_un;
    Mat p2_un;
    Mat point_4d_hom;
    Mat triangulated_3d_point;

    // =====================
    // ORB Feature Matching
    // =====================
    Ptr<ORB> detector = ORB::create(
        200,    // nfeatures: Maximum features to compute
        1.2f,   // scaleFactor: Pyramid ratio (Greater than 1.0)
        8,      // nLevels: number of pyramid levels to use
        31,     // edgeThreshold: Size of no-search border
        0,      // firstlevel; always 0
        2,      // WTA_K: Pts in each comparison: 2, 3, or 4
        ORB::HARRIS_SCORE,  // ScoreType: Harris_Score = 0, Fast_Score = 1
        31,     // PatchSize: Size of patch for each descriptors
        20);    // fastThreshold: threshold for fast detection
    detector->detectAndCompute(tc_plha_04_img_1, cv::Mat(), keypoints_1, descriptors_1); //ORB outputs a binary descriptor
    detector->detectAndCompute(tc_plha_04_img_2, cv::Mat(), keypoints_2, descriptors_2);
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    
    // Filter matches using the Lowe's ratio test
    matcher->knnMatch( descriptors_1, descriptors_2, knn_matches, 2 );
    for (size_t i = 0; i < knn_matches.size(); i++)
    {
        if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }

    LOG_INFO("TC_PLHA_04 Key points img 1 found %d", keypoints_1.size());
    LOG_INFO("TC_PLHA_04 Key points img 2 found %d", keypoints_2.size());
    LOG_INFO("TC_PLHA_04 Lowes number of good matches %d", good_matches.size());
    
    for (int i=0;i<good_matches.size();i++)
    {
        int idx1=good_matches[i].trainIdx;
        int idx2=good_matches[i].queryIdx;
        first_2d_points.push_back(keypoints_1[idx1].pt);
        second_2d_points.push_back(keypoints_2[idx2].pt);
    }

    // Draw matches
    drawMatches( tc_plha_04_img_1, keypoints_1, tc_plha_04_img_2, keypoints_2, good_matches, img_matches, Scalar(0,255,0),
                 Scalar(0,255,0), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    // Write to file to view the Matched results given the name currently being processed
    OS_TranslatePath("/cf/vpu/1_tc_plha_04_output.png", cTranslatedPath);
    cv::imwrite(cTranslatedPath, img_matches);

    // =====================
    // Essential Matrix
    // =====================
    long num_matches = good_matches.size();

    // Get good points
    for (int i=0;i<num_matches;i++)
    {
        first_2d_points.push_back(  keypoints_1[ good_matches[i].queryIdx ].pt );
        second_2d_points.push_back( keypoints_2[ good_matches[i].trainIdx ].pt );
    }

    // =====================
    // Triangulation
    // =====================
    // Calculate the projected matrix for both cameras
    hconcat(R, T, M_r);
    hconcat(Mat::eye(3,3,CV_64FC1), Mat::zeros(3,1, CV_64FC1), M_l);
    Mat P_r = K*M_r; // 3x3 * 3x4
    Mat P_l = K*M_l; // 3x3 * 3x4

    // Undistort points
    undistortPoints(first_2d_points,  p1_un, K, Mat(), K );
    undistortPoints(second_2d_points, p2_un, K, Mat(), K );

    // Triangulates the points output is a 3x4 matrix
    triangulatePoints(P_r, P_l, p1_un.t(), p2_un.t(), point_4d_hom);

    // Normilize coordinates from 3x4 to 3x3 matrix
    transpose(point_4d_hom, triangulated_3d_point);
    convertPointsFromHomogeneous(triangulated_3d_point, triangulated_3d_point);

    // Amount of points
    int num_points = triangulated_3d_point.rows * triangulated_3d_point.cols; 
    LOG_INFO("Number of 3D points = %d", num_points);
    // =====================
    // CSV Result Output
    // =====================
    ofstream outputCSV;
    memset(cTranslatedPath,0,sizeof(cTranslatedPath));
    OS_TranslatePath("/cf/apps/2_tc_plha_04_3D_points.csv", cTranslatedPath);
    outputCSV.open(cTranslatedPath);
    outputCSV << format(triangulated_3d_point, cv::Formatter::FMT_CSV) << endl;
    outputCSV.close();

    if(tc_plha_04_out_ptr != NULL)
    {
        tc_plha_04_out_ptr->img_1_num_features = static_cast<int>( keypoints_1.size());
        tc_plha_04_out_ptr->img_2_num_features = static_cast<int>( keypoints_2.size());
        tc_plha_04_out_ptr->lowes_features = static_cast<int>( good_matches.size());
        tc_plha_04_out_ptr->points_3D_amount = static_cast<int>(num_points);
    }
    return 0;
}

// Class Deconstructor
tc_plha_04::~tc_plha_04() = default;

#ifdef __cplusplus
}
#endif
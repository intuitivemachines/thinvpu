/*=======================================================================================
** File Name:  sample_app_private_ids.h
**
** Title:  ID Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declarations and definitions of SAMPLE_APP's private IDs.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_PRIVATE_IDS_H_
#define _SAMPLE_APP_PRIVATE_IDS_H_

/*
** Pragmas
*/

/*
** Include Files
*/

/*
** Local Defines
*/

/* Event IDs */
#define SAMPLE_APP_RESERVED_EID  0

#define SAMPLE_APP_INF_EID        1
#define SAMPLE_APP_INIT_INF_EID   2
#define SAMPLE_APP_ILOAD_INF_EID  3
#define SAMPLE_APP_CDS_INF_EID    4
#define SAMPLE_APP_CMD_INF_EID    5

#define SAMPLE_APP_ERR_EID         51
#define SAMPLE_APP_INIT_ERR_EID    52
#define SAMPLE_APP_ILOAD_ERR_EID   53
#define SAMPLE_APP_CDS_ERR_EID     54
#define SAMPLE_APP_CMD_ERR_EID     55
#define SAMPLE_APP_PIPE_ERR_EID    56
#define SAMPLE_APP_MSGID_ERR_EID   57
#define SAMPLE_APP_MSGLEN_ERR_EID  58

#define SAMPLE_APP_EVT_CNT  14

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _SAMPLE_APP_PRIVATE_IDS_H_ */

/*=======================================================================================
** End of file sample_app_private_ids.h
**=====================================================================================*/
    
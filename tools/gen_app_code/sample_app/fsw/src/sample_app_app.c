/*=======================================================================================
** File Name:  sample_app_app.c
**
** Title:  Function Definitions for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This source file contains all necessary function definitions to run SAMPLE_APP
**           application.
**
** Functions Defined:
**    Function X - Brief purpose of function X
**    Function Y - Brief purpose of function Y
**    Function Z - Brief purpose of function Z
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to all functions in the file.
**    2. List the external source(s) and event(s) that can cause the funcs in this
**       file to execute.
**    3. List known limitations that apply to the funcs in this file.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/

/*
** Pragmas
*/

/*
** Include Files
*/
#include <string.h>

#include "cfe.h"

#include "sample_app_platform_cfg.h"
#include "sample_app_mission_cfg.h"
#include "sample_app_app.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/

/*
** Global Variables
*/
SAMPLE_APP_AppData_t  g_SAMPLE_APP_AppData;

/*
** Local Variables
*/

/*
** Local Function Definitions
*/
    
/*=====================================================================================
** Name: SAMPLE_APP_InitEvent
**
** Purpose: To initialize and register event table for SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization
**
** Routines Called:
**    CFE_EVS_Register
**    CFE_ES_WriteToSysLog
**
** Called By:
**    SAMPLE_APP_InitApp
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    g_SAMPLE_APP_AppData.EventTbl
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_InitEvent()
{
    int32  iStatus=CFE_SUCCESS;

    /* Create the event table */
    memset((void*)g_SAMPLE_APP_AppData.EventTbl, 0x00, sizeof(g_SAMPLE_APP_AppData.EventTbl));

    g_SAMPLE_APP_AppData.EventTbl[0].EventID = SAMPLE_APP_RESERVED_EID;
    g_SAMPLE_APP_AppData.EventTbl[1].EventID = SAMPLE_APP_INF_EID;
    g_SAMPLE_APP_AppData.EventTbl[2].EventID = SAMPLE_APP_INIT_INF_EID;
    g_SAMPLE_APP_AppData.EventTbl[3].EventID = SAMPLE_APP_ILOAD_INF_EID;
    g_SAMPLE_APP_AppData.EventTbl[4].EventID = SAMPLE_APP_CDS_INF_EID;
    g_SAMPLE_APP_AppData.EventTbl[5].EventID = SAMPLE_APP_CMD_INF_EID;

    g_SAMPLE_APP_AppData.EventTbl[ 6].EventID = SAMPLE_APP_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[ 7].EventID = SAMPLE_APP_INIT_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[ 8].EventID = SAMPLE_APP_ILOAD_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[ 9].EventID = SAMPLE_APP_CDS_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[10].EventID = SAMPLE_APP_CMD_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[11].EventID = SAMPLE_APP_PIPE_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[12].EventID = SAMPLE_APP_MSGID_ERR_EID;
    g_SAMPLE_APP_AppData.EventTbl[13].EventID = SAMPLE_APP_MSGLEN_ERR_EID;

    /* Register the table with CFE */
    iStatus = CFE_EVS_Register(g_SAMPLE_APP_AppData.EventTbl,
                               SAMPLE_APP_EVT_CNT, CFE_EVS_BINARY_FILTER);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to register with EVS (0x%08X)\n", iStatus);
    }

    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_InitPipe
**
** Purpose: To initialize all message pipes and subscribe to messages for SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization
**
** Routines Called:
**    CFE_SB_CreatePipe
**    CFE_SB_Subscribe
**    CFE_ES_WriteToSysLog
**
** Called By:
**    SAMPLE_APP_InitApp
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    g_SAMPLE_APP_AppData.usSchPipeDepth
**    g_SAMPLE_APP_AppData.cSchPipeName
**    g_SAMPLE_APP_AppData.SchPipeId
**    g_SAMPLE_APP_AppData.usCmdPipeDepth
**    g_SAMPLE_APP_AppData.cCmdPipeName
**    g_SAMPLE_APP_AppData.CmdPipeId
**    g_SAMPLE_APP_AppData.usTlmPipeDepth
**    g_SAMPLE_APP_AppData.cTlmPipeName
**    g_SAMPLE_APP_AppData.TlmPipeId
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_InitPipe()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init schedule pipe */
    g_SAMPLE_APP_AppData.usSchPipeDepth = SAMPLE_APP_SCH_PIPE_DEPTH;
    memset((void*)g_SAMPLE_APP_AppData.cSchPipeName, '\0', sizeof(g_SAMPLE_APP_AppData.cSchPipeName));
    strncpy(g_SAMPLE_APP_AppData.cSchPipeName, "SAMPLE_APP_SCH_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to Wakeup messages */
    iStatus = CFE_SB_CreatePipe(&g_SAMPLE_APP_AppData.SchPipeId,
                                 g_SAMPLE_APP_AppData.usSchPipeDepth,
                                 g_SAMPLE_APP_AppData.cSchPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        iStatus = CFE_SB_SubscribeEx(SAMPLE_APP_WAKEUP_MID, g_SAMPLE_APP_AppData.SchPipeId, CFE_SB_Default_Qos, 1);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("SAMPLE_APP - Sch Pipe failed to subscribe to SAMPLE_APP_WAKEUP_MID. (0x%08X)\n", iStatus);
            goto SAMPLE_APP_InitPipe_Exit_Tag;
        }
        
    }
    else
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to create SCH pipe (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitPipe_Exit_Tag;
    }

    /* Init command pipe */
    g_SAMPLE_APP_AppData.usCmdPipeDepth = SAMPLE_APP_CMD_PIPE_DEPTH ;
    memset((void*)g_SAMPLE_APP_AppData.cCmdPipeName, '\0', sizeof(g_SAMPLE_APP_AppData.cCmdPipeName));
    strncpy(g_SAMPLE_APP_AppData.cCmdPipeName, "SAMPLE_APP_CMD_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to command messages */
    iStatus = CFE_SB_CreatePipe(&g_SAMPLE_APP_AppData.CmdPipeId,
                                 g_SAMPLE_APP_AppData.usCmdPipeDepth,
                                 g_SAMPLE_APP_AppData.cCmdPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        /* Subscribe to command messages */
        iStatus = CFE_SB_Subscribe(SAMPLE_APP_CMD_MID, g_SAMPLE_APP_AppData.CmdPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("SAMPLE_APP - CMD Pipe failed to subscribe to SAMPLE_APP_CMD_MID. (0x%08X)\n", iStatus);
            goto SAMPLE_APP_InitPipe_Exit_Tag;
        }

        iStatus = CFE_SB_Subscribe(SAMPLE_APP_SEND_HK_MID, g_SAMPLE_APP_AppData.CmdPipeId);

        if (iStatus != CFE_SUCCESS)
        {
            CFE_ES_WriteToSysLog("SAMPLE_APP - CMD Pipe failed to subscribe to SAMPLE_APP_SEND_HK_MID. (0x%08X)\n", iStatus);
            goto SAMPLE_APP_InitPipe_Exit_Tag;
        }
        
    }
    else
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to create CMD pipe (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitPipe_Exit_Tag;
    }

    /* Init telemetry pipe */
    g_SAMPLE_APP_AppData.usTlmPipeDepth = SAMPLE_APP_TLM_PIPE_DEPTH;
    memset((void*)g_SAMPLE_APP_AppData.cTlmPipeName, '\0', sizeof(g_SAMPLE_APP_AppData.cTlmPipeName));
    strncpy(g_SAMPLE_APP_AppData.cTlmPipeName, "SAMPLE_APP_TLM_PIPE", OS_MAX_API_NAME-1);

    /* Subscribe to telemetry messages on the telemetry pipe */
    iStatus = CFE_SB_CreatePipe(&g_SAMPLE_APP_AppData.TlmPipeId,
                                 g_SAMPLE_APP_AppData.usTlmPipeDepth,
                                 g_SAMPLE_APP_AppData.cTlmPipeName);
    if (iStatus == CFE_SUCCESS)
    {
        /* TODO:  Add CFE_SB_Subscribe() calls for other apps' output data here.
        **
        ** Examples:
        **     CFE_SB_Subscribe(GNCEXEC_OUT_DATA_MID, g_SAMPLE_APP_AppData.TlmPipeId);
        */
    }
    else
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to create TLM pipe (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitPipe_Exit_Tag;
    }

SAMPLE_APP_InitPipe_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_InitData
**
** Purpose: To initialize global variables used by SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization
**
** Routines Called:
**    CFE_SB_InitMsg
**
** Called By:
**    SAMPLE_APP_InitApp
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    g_SAMPLE_APP_AppData.InData
**    g_SAMPLE_APP_AppData.OutData
**    g_SAMPLE_APP_AppData.HkTlm
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_InitData()
{
    int32  iStatus=CFE_SUCCESS;

    /* Init input data */
    memset((void*)&g_SAMPLE_APP_AppData.InData, 0x00, sizeof(g_SAMPLE_APP_AppData.InData));

    /* Init output data */
    memset((void*)&g_SAMPLE_APP_AppData.OutData, 0x00, sizeof(g_SAMPLE_APP_AppData.OutData));
    CFE_SB_InitMsg(&g_SAMPLE_APP_AppData.OutData,
                   SAMPLE_APP_OUT_DATA_MID, sizeof(g_SAMPLE_APP_AppData.OutData), TRUE);

    /* Init housekeeping packet */
    memset((void*)&g_SAMPLE_APP_AppData.HkTlm, 0x00, sizeof(g_SAMPLE_APP_AppData.HkTlm));
    CFE_SB_InitMsg(&g_SAMPLE_APP_AppData.HkTlm,
                   SAMPLE_APP_HK_TLM_MID, sizeof(g_SAMPLE_APP_AppData.HkTlm), TRUE);

    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_InitApp
**
** Purpose: To initialize all data local to and used by SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization
**
** Routines Called:
**    CFE_ES_RegisterApp
**    CFE_ES_WriteToSysLog
**    CFE_EVS_SendEvent
**    OS_TaskInstallDeleteHandler
**    SAMPLE_APP_InitEvent
**    SAMPLE_APP_InitPipe
**    SAMPLE_APP_InitData
**
** Called By:
**    SAMPLE_APP_AppMain
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_InitApp()
{
    int32  iStatus=CFE_SUCCESS;

    g_SAMPLE_APP_AppData.uiRunStatus = CFE_ES_APP_RUN;

    iStatus = CFE_ES_RegisterApp();
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to register the app (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitApp_Exit_Tag;
    }

    if ((SAMPLE_APP_InitEvent() != CFE_SUCCESS) || 
        (SAMPLE_APP_InitPipe() != CFE_SUCCESS) || 
        (SAMPLE_APP_InitData() != CFE_SUCCESS) ||
        (SAMPLE_APP_InitILoadTbl() != CFE_SUCCESS))
    {
        iStatus = -1;
        goto SAMPLE_APP_InitApp_Exit_Tag;
    }

    /* Install the cleanup callback */
    OS_TaskInstallDeleteHandler((void*)&SAMPLE_APP_CleanupCallback);

SAMPLE_APP_InitApp_Exit_Tag:
    if (iStatus == CFE_SUCCESS)
    {
        CFE_EVS_SendEvent(SAMPLE_APP_INIT_INF_EID, CFE_EVS_INFORMATION,
                          "SAMPLE_APP - Application initialized");
    }
    else
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Application failed to initialize\n");
    }

    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_CleanupCallback
**
** Purpose: To handle any neccesary cleanup prior to application exit
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    TBD
**
** Called By:
**    TBD
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_CleanupCallback()
{
    /* TODO:  Add code to cleanup memory and other cleanup here */
}
    
/*=====================================================================================
** Name: SAMPLE_APP_RcvMsg
**
** Purpose: To receive and process messages for SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization 
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    CFE_ES_PerfLogEntry
**    CFE_ES_PerfLogExit
**    SAMPLE_APP_ProcessNewCmds
**    SAMPLE_APP_ProcessNewData
**    SAMPLE_APP_SendOutData
**
** Called By:
**    SAMPLE_APP_Main
**
** Global Inputs/Reads:
**    g_SAMPLE_APP_AppData.SchPipeId
**
** Global Outputs/Writes:
**    g_SAMPLE_APP_AppData.uiRunStatus
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_RcvMsg(int32 iBlocking)
{
    int32           iStatus=CFE_SUCCESS;
    CFE_SB_Msg_t*   MsgPtr=NULL;
    CFE_SB_MsgId_t  MsgId;

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(SAMPLE_APP_MAIN_TASK_PERF_ID);

    /* Wait for WakeUp messages from scheduler */
    iStatus = CFE_SB_RcvMsg(&MsgPtr, g_SAMPLE_APP_AppData.SchPipeId, iBlocking);

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(SAMPLE_APP_MAIN_TASK_PERF_ID);

    if (iStatus == CFE_SUCCESS)
    {
        MsgId = CFE_SB_GetMsgId(MsgPtr);
        switch (MsgId)
	{
            case SAMPLE_APP_WAKEUP_MID:
                SAMPLE_APP_ProcessNewCmds();
                SAMPLE_APP_ProcessNewData();

                /* TODO:  Add more code here to handle other things when app wakes up */

                /* The last thing to do at the end of this Wakeup cycle should be to
                   automatically publish new output. */
                SAMPLE_APP_SendOutData();
                break;

            default:
                CFE_EVS_SendEvent(SAMPLE_APP_MSGID_ERR_EID, CFE_EVS_ERROR,
                                  "SAMPLE_APP - Recvd invalid SCH msgId (0x%08X)", MsgId);
        }
    }
    else if (iStatus == CFE_SB_NO_MESSAGE)
    {
        /* If there's no incoming message, you can do something here, or nothing */
    }
    else
    {
        /* This is an example of exiting on an error.
        ** Note that a SB read error is not always going to result in an app quitting.
        */
        CFE_EVS_SendEvent(SAMPLE_APP_PIPE_ERR_EID, CFE_EVS_ERROR,
			  "SAMPLE_APP: SB pipe read error (0x%08X), app will exit", iStatus);
        g_SAMPLE_APP_AppData.uiRunStatus= CFE_ES_APP_ERROR;
    }

    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_ProcessNewData
**
** Purpose: To process incoming data subscribed by SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**
** Called By:
**    SAMPLE_APP_RcvMsg
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    None
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_ProcessNewData()
{
    int iStatus = CFE_SUCCESS;
    CFE_SB_Msg_t*   TlmMsgPtr=NULL;
    CFE_SB_MsgId_t  TlmMsgId;

    /* Process telemetry messages till the pipe is empty */
    while (1)
    {
        iStatus = CFE_SB_RcvMsg(&TlmMsgPtr, g_SAMPLE_APP_AppData.TlmPipeId, CFE_SB_POLL);
        if (iStatus == CFE_SUCCESS)
        {
            TlmMsgId = CFE_SB_GetMsgId(TlmMsgPtr);
            switch (TlmMsgId)
            {
                /* TODO:  Add code to process all subscribed data here 
                **
                ** Example:
                **     case NAV_OUT_DATA_MID:
                **         SAMPLE_APP_ProcessNavData(TlmMsgPtr);
                **         break;
                */

                default:
                    CFE_EVS_SendEvent(SAMPLE_APP_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "SAMPLE_APP - Recvd invalid TLM msgId (0x%08X)", TlmMsgId);
                    break;
            }
        }
        else if (iStatus == CFE_SB_NO_MESSAGE)
        {
            break;
        }
        else
        {
            CFE_EVS_SendEvent(SAMPLE_APP_PIPE_ERR_EID, CFE_EVS_ERROR,
                  "SAMPLE_APP: CMD pipe read error (0x%08X)", iStatus);
            g_SAMPLE_APP_AppData.uiRunStatus = CFE_ES_APP_ERROR;
            break;
        }
    }
}
    
/*=====================================================================================
** Name: SAMPLE_APP_ProcessNewCmds
**
** Purpose: To process incoming command messages for SAMPLE_APP application
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    CFE_SB_RcvMsg
**    CFE_SB_GetMsgId
**    CFE_EVS_SendEvent
**    SAMPLE_APP_ProcessNewAppCmds
**    SAMPLE_APP_ReportHousekeeping
**
** Called By:
**    SAMPLE_APP_RcvMsg
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    None
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_ProcessNewCmds()
{
    int iStatus = CFE_SUCCESS;
    CFE_SB_Msg_t*   CmdMsgPtr=NULL;
    CFE_SB_MsgId_t  CmdMsgId;

    /* Process command messages till the pipe is empty */
    while (1)
    {
        iStatus = CFE_SB_RcvMsg(&CmdMsgPtr, g_SAMPLE_APP_AppData.CmdPipeId, CFE_SB_POLL);
        if(iStatus == CFE_SUCCESS)
        {
            CmdMsgId = CFE_SB_GetMsgId(CmdMsgPtr);
            switch (CmdMsgId)
            {
                case SAMPLE_APP_CMD_MID:
                    SAMPLE_APP_ProcessNewAppCmds(CmdMsgPtr);
                    break;

                case SAMPLE_APP_SEND_HK_MID:
                    SAMPLE_APP_ReportHousekeeping();
                    break;

                /* TODO:  Add code to process other subscribed commands here
                **
                ** Example:
                **     case CFE_TIME_DATA_CMD_MID:
                **         SAMPLE_APP_ProcessTimeDataCmd(CmdMsgPtr);
                **         break;
                */

                default:
                    CFE_EVS_SendEvent(SAMPLE_APP_MSGID_ERR_EID, CFE_EVS_ERROR,
                                      "SAMPLE_APP - Recvd invalid CMD msgId (0x%08X)", CmdMsgId);
                    break;
            }
        }
        else if (iStatus == CFE_SB_NO_MESSAGE)
        {
            break;
        }
        else
        {
            CFE_EVS_SendEvent(SAMPLE_APP_PIPE_ERR_EID, CFE_EVS_ERROR,
                  "SAMPLE_APP: CMD pipe read error (0x%08X)", iStatus);
            g_SAMPLE_APP_AppData.uiRunStatus = CFE_ES_APP_ERROR;
            break;
        }
    }
}
    
/*=====================================================================================
** Name: SAMPLE_APP_ProcessNewAppCmds
**
** Purpose: To process command messages targeting SAMPLE_APP application
**
** Arguments:
**    CFE_SB_Msg_t*  MsgPtr - new command message pointer
**
** Returns:
**    None
**
** Routines Called:
**    CFE_SB_GetCmdCode
**    CFE_EVS_SendEvent
**
** Called By:
**    SAMPLE_APP_ProcessNewCmds
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    g_SAMPLE_APP_AppData.HkTlm.usCmdCnt
**    g_SAMPLE_APP_AppData.HkTlm.usCmdErrCnt
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_ProcessNewAppCmds(CFE_SB_Msg_t* MsgPtr)
{
    uint32  uiCmdCode=0;

    if (MsgPtr != NULL)
    {
        uiCmdCode = CFE_SB_GetCmdCode(MsgPtr);
        switch (uiCmdCode)
        {
            case SAMPLE_APP_NOOP_CC:
                g_SAMPLE_APP_AppData.HkTlm.usCmdCnt++;
                CFE_EVS_SendEvent(SAMPLE_APP_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "SAMPLE_APP - Recvd NOOP cmd (%d)", uiCmdCode);
                break;

            case SAMPLE_APP_RESET_CC:
                g_SAMPLE_APP_AppData.HkTlm.usCmdCnt = 0;
                g_SAMPLE_APP_AppData.HkTlm.usCmdErrCnt = 0;
                CFE_EVS_SendEvent(SAMPLE_APP_CMD_INF_EID, CFE_EVS_INFORMATION,
                                  "SAMPLE_APP - Recvd RESET cmd (%d)", uiCmdCode);
                break;

            /* TODO:  Add code to process the rest of the SAMPLE_APP commands here */

            default:
                g_SAMPLE_APP_AppData.HkTlm.usCmdErrCnt++;
                CFE_EVS_SendEvent(SAMPLE_APP_MSGID_ERR_EID, CFE_EVS_ERROR,
                                  "SAMPLE_APP - Recvd invalid cmdId (%d)", uiCmdCode);
                break;
        }
    }
}
    
/*=====================================================================================
** Name: SAMPLE_APP_ReportHousekeeping
**
** Purpose: To send housekeeping message
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    TBD
**
** Called By:
**    SAMPLE_APP_ProcessNewCmds
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  GSFC, First-Name Last-Name
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_ReportHousekeeping()
{
    /* TODO:  Add code to update housekeeping data, if needed, here.  */

    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_SAMPLE_APP_AppData.HkTlm);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_SAMPLE_APP_AppData.HkTlm);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_SendOutData
**
** Purpose: To publish 1-Wakeup cycle output data
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    TBD
**
** Called By:
**    SAMPLE_APP_RcvMsg
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_SendOutData()
{
    /* TODO:  Add code to update output data, if needed, here.  */

    CFE_SB_TimeStampMsg((CFE_SB_Msg_t*)&g_SAMPLE_APP_AppData.OutData);
    CFE_SB_SendMsg((CFE_SB_Msg_t*)&g_SAMPLE_APP_AppData.OutData);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_VerifyCmdLength
**
** Purpose: To verify command length for a particular command message
**
** Arguments:
**    CFE_SB_Msg_t*  MsgPtr      - command message pointer
**    uint16         usExpLength - expected command length
**
** Returns:
**    boolean bResult - result of verification
**
** Routines Called:
**    TBD
**
** Called By:
**    SAMPLE_APP_ProcessNewCmds
**
** Global Inputs/Reads:
**    None
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
boolean SAMPLE_APP_VerifyCmdLength(CFE_SB_Msg_t* MsgPtr,
                           uint16 usExpectedLen)
{
    boolean bResult=FALSE;
    uint16  usMsgLen=0;

    if (MsgPtr != NULL)
    {
        usMsgLen = CFE_SB_GetTotalMsgLength(MsgPtr);

        if (usExpectedLen != usMsgLen)
        {
            CFE_SB_MsgId_t MsgId = CFE_SB_GetMsgId(MsgPtr);
            uint16 usCmdCode = CFE_SB_GetCmdCode(MsgPtr);

            CFE_EVS_SendEvent(SAMPLE_APP_MSGLEN_ERR_EID, CFE_EVS_ERROR,
                              "SAMPLE_APP - Rcvd invalid msgLen: msgId=0x%08X, cmdCode=%d, "
                              "msgLen=%d, expectedLen=%d",
                              MsgId, usCmdCode, usMsgLen, usExpectedLen);
            g_SAMPLE_APP_AppData.HkTlm.usCmdErrCnt++;
        }
    }

    return (bResult);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_AppMain
**
** Purpose: To define SAMPLE_APP application's entry point and main process loop
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    CFE_ES_RegisterApp
**    CFE_ES_RunLoop
**    CFE_ES_PerfLogEntry
**    CFE_ES_PerfLogExit
**    CFE_ES_ExitApp
**    CFE_ES_WaitForStartupSync
**    SAMPLE_APP_InitApp
**    SAMPLE_APP_RcvMsg
**
** Called By:
**    TBD
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Author(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_AppMain()
{
    /* Register the application with Executive Services */
    CFE_ES_RegisterApp();

    /* Start Performance Log entry */
    CFE_ES_PerfLogEntry(SAMPLE_APP_MAIN_TASK_PERF_ID);

    /* Perform application initializations */
    if (SAMPLE_APP_InitApp() != CFE_SUCCESS)
    {
        g_SAMPLE_APP_AppData.uiRunStatus = CFE_ES_APP_ERROR;
    } else {
        /* Do not perform performance monitoring on startup sync */
        CFE_ES_PerfLogExit(SAMPLE_APP_MAIN_TASK_PERF_ID);
        CFE_ES_WaitForStartupSync(SAMPLE_APP_TIMEOUT_MSEC);
        CFE_ES_PerfLogEntry(SAMPLE_APP_MAIN_TASK_PERF_ID);
    }

    /* Application main loop */
    while (CFE_ES_RunLoop(&g_SAMPLE_APP_AppData.uiRunStatus) == TRUE)
    {
        SAMPLE_APP_RcvMsg(CFE_SB_PEND_FOREVER);
    }

    /* Stop Performance Log entry */
    CFE_ES_PerfLogExit(SAMPLE_APP_MAIN_TASK_PERF_ID);

    /* Exit the application */
    CFE_ES_ExitApp(g_SAMPLE_APP_AppData.uiRunStatus);
} 
    
/*=======================================================================================
** End of file sample_app_app.c
**=====================================================================================*/
    
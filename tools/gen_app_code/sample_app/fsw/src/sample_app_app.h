/*=======================================================================================
** File Name:  sample_app_app.h
**
** Title:  Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  To define SAMPLE_APP's internal macros, data types, global variables and
**           function prototypes
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_APP_H_
#define _SAMPLE_APP_APP_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "sample_app_platform_cfg.h"
#include "sample_app_mission_cfg.h"
#include "sample_app_private_ids.h"
#include "sample_app_private_types.h"
#include "sample_app_perfids.h"
#include "sample_app_msgids.h"
#include "sample_app_msg.h"

 
#include "sample_app_iload_utils.h"

/*
** Local Defines
*/
#define SAMPLE_APP_TIMEOUT_MSEC    1000

/*
** Local Structure Declarations
*/
typedef struct
{
    /* CFE Event table */
    CFE_EVS_BinFilter_t  EventTbl[SAMPLE_APP_EVT_CNT];

    /* CFE scheduling pipe */
    CFE_SB_PipeId_t  SchPipeId; 
    uint16           usSchPipeDepth;
    char             cSchPipeName[OS_MAX_API_NAME];

    /* CFE command pipe */
    CFE_SB_PipeId_t  CmdPipeId;
    uint16           usCmdPipeDepth;
    char             cCmdPipeName[OS_MAX_API_NAME];
    
    /* CFE telemetry pipe */
    CFE_SB_PipeId_t  TlmPipeId;
    uint16           usTlmPipeDepth;
    char             cTlmPipeName[OS_MAX_API_NAME];

    /* Task-related */
    uint32  uiRunStatus;
    
    /* ILoad table-related */
    CFE_TBL_Handle_t  ILoadTblHdl;
    SAMPLE_APP_ILoadTblEntry_t*  ILoadTblPtr;
    
    /* Input data - from I/O devices or subscribed from other apps' output data.
       Data structure should be defined in sample_app/fsw/src/sample_app_private_types.h */
    SAMPLE_APP_InData_t   InData;

    /* Output data - to be published at the end of a Wakeup cycle.
       Data structure should be defined in sample_app/fsw/src/sample_app_private_types.h */
    SAMPLE_APP_OutData_t  OutData;

    /* Housekeeping telemetry - for downlink only.
       Data structure should be defined in sample_app/fsw/src/sample_app_msg.h */
    SAMPLE_APP_HkTlm_t  HkTlm;

    /* TODO:  Add declarations for additional private data here */
} SAMPLE_APP_AppData_t;

/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/
int32  SAMPLE_APP_InitApp(void);
int32  SAMPLE_APP_InitEvent(void);
int32  SAMPLE_APP_InitData(void);
int32  SAMPLE_APP_InitPipe(void);

void  SAMPLE_APP_AppMain(void);

void  SAMPLE_APP_CleanupCallback(void);

int32  SAMPLE_APP_RcvMsg(int32 iBlocking);

void  SAMPLE_APP_ProcessNewData(void);
void  SAMPLE_APP_ProcessNewCmds(void);
void  SAMPLE_APP_ProcessNewAppCmds(CFE_SB_Msg_t*);

void  SAMPLE_APP_ReportHousekeeping(void);
void  SAMPLE_APP_SendOutData(void);

boolean  SAMPLE_APP_VerifyCmdLength(CFE_SB_Msg_t*, uint16);

#endif /* _SAMPLE_APP_APP_H_ */

/*=======================================================================================
** End of file sample_app_app.h
**=====================================================================================*/
    
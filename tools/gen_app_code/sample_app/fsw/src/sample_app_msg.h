/*=======================================================================================
** File Name:  sample_app_msg.h
**
** Title:  Message Definition Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  To define SAMPLE_APP's command and telemetry message defintions 
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_MSG_H_
#define _SAMPLE_APP_MSG_H_

/*
** Pragmas
*/

/*
** Include Files
*/

 
#include "sample_app_iload_utils.h"

/*
** Local Defines
*/

/*
** SAMPLE_APP command codes
*/
#define SAMPLE_APP_NOOP_CC                 0
#define SAMPLE_APP_RESET_CC                1

/*
** Local Structure Declarations
*/
typedef struct
{
    uint8              TlmHeader[CFE_SB_TLM_HDR_SIZE];
    uint8              usCmdCnt;
    uint8              usCmdErrCnt;

    /* TODO:  Add declarations for additional housekeeping data here */

} SAMPLE_APP_HkTlm_t;


#endif /* _SAMPLE_APP_MSG_H_ */

/*=======================================================================================
** End of file sample_app_msg.h
**=====================================================================================*/
    
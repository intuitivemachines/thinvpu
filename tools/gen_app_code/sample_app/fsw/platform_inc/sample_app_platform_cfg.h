/*=======================================================================================
** File Name:  sample_app_platform_cfg.h
**
** Title:  Platform Configuration Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declartions and definitions of all SAMPLE_APP's 
**           platform-specific configurations.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_PLATFORM_CFG_H_
#define _SAMPLE_APP_PLATFORM_CFG_H_

/*
** sample_app Platform Configuration Parameter Definitions
*/
#define SAMPLE_APP_SCH_PIPE_DEPTH  2
#define SAMPLE_APP_CMD_PIPE_DEPTH  10
#define SAMPLE_APP_TLM_PIPE_DEPTH  10


/* TODO:  Add more platform configuration parameter definitions here, if necessary. */

#endif /* _SAMPLE_APP_PLATFORM_CFG_H_ */

/*=======================================================================================
** End of file sample_app_platform_cfg.h
**=====================================================================================*/
    
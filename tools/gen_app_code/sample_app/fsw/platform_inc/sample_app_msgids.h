/*=======================================================================================
** File Name:  sample_app_msgids.h
**
** Title:  Message ID Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declartions and definitions of all SAMPLE_APP's 
**           Message IDs.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_MSGIDS_H_
#define _SAMPLE_APP_MSGIDS_H_

/***** TODO:  These Message ID values are default and may need to be changed by the developer  *****/
#define SAMPLE_APP_CMD_MID            	0x18C0
#define SAMPLE_APP_SEND_HK_MID        	0x18C1
#define SAMPLE_APP_WAKEUP_MID        	0x18D0
#define SAMPLE_APP_OUT_DATA_MID        	0x18D1

#define SAMPLE_APP_HK_TLM_MID		0x08BB

    #include "sample_app_tbldefs.h"



#endif /* _SAMPLE_APP_MSGIDS_H_ */

/*=======================================================================================
** End of file sample_app_msgids.h
**=====================================================================================*/
    
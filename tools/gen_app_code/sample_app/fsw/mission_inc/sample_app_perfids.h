/*=======================================================================================
** File Name:  sample_app_perfids.h
**
** Title:  Performance ID Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declartions and definitions of all SAMPLE_APP's 
**           Performance IDs.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_PERFIDS_H_
#define _SAMPLE_APP_PERFIDS_H_

#define SAMPLE_APP_MAIN_TASK_PERF_ID            50

    #include "sample_app_tbldefs.h"



#endif /* _SAMPLE_APP_PERFIDS_H_ */

/*=======================================================================================
** End of file sample_app_perfids.h
**=====================================================================================*/
    
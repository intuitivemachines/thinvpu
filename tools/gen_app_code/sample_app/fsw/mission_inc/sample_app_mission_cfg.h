/*=======================================================================================
** File Name:  sample_app_mission_cfg.h
**
** Title:  Mission Configuration Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declartions and definitions of all SAMPLE_APP's 
**           mission-specific configurations.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_MISSION_CFG_H_
#define _SAMPLE_APP_MISSION_CFG_H_

/*
** Include Files
*/
#include "cfe.h"

#include "sample_app_tbldefs.h"


/*
** SAMPLE_APP Mission Configuration Parameter Definitions
*/

/* TODO:  Add mission configuration parameter definitions here, if necessary. */

#endif /* _SAMPLE_APP_MISSION_CFG_H_ */

/*=======================================================================================
** End of file sample_app_mission_cfg.h
**=====================================================================================*/
    
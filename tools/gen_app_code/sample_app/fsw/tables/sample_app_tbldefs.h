/*=======================================================================================
** File Name:  sample_app_tbldefs.h
**
** Title:  Header File for SAMPLE_APP Application's tables
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This header file contains declarations and definitions of data structures
**           used in SAMPLE_APP's tables.
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_TBLDEFS_H_
#define _SAMPLE_APP_TBLDEFS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "common_types.h"
#include "sample_app_mission_cfg.h"

/*
** Local Defines
*/
#define SAMPLE_APP_ILOAD_MAX_ENTRIES  1
#define SAMPLE_APP_ILOAD_FILENAME     "/cf/apps/sample_app_iloads.tbl"
#define SAMPLE_APP_ILOAD_TABLENAME    "ILOAD_TBL"


/*
** Local Structure Declarations
*/ 
/* Definition for Iload table entry */
typedef struct
{
    int32  iParam;

    /* TODO:  Add type declaration for ILoad parameters here.
    **
    ** Examples:
    **    int8/char            cParam;
    **    int8/char            cParams[16];
    **    uint8/unsigned char  ucParam;
    **    uint8/unsigned char  ucParams[16];
    **
    **    int16   sParam;
    **    int16   sParams[8];
    **    uint16  usParam;
    **    uint16  usParams[8];
    **
    **    int32   iParam;
    **    int32   iParams[5];
    **    uint32  uiParam;
    **    uint32  uiParams[5];
    **
    **    float  fParam;
    **    float  fParams[3];
    **
    **    double  dParam;
    **    double  dParams[3];
    */
} SAMPLE_APP_ILoadTblEntry_t;
    
/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/

#endif /* _SAMPLE_APP_TBLDEFS_H_ */

/*=======================================================================================
** End of file sample_app_tbldefs.h
**=====================================================================================*/
    
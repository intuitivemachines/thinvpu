/*=======================================================================================
** File Name:  sample_app_iload_utils.c
**
** Title:  Iload Tables' Utilities for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  This source file contains definitions of ILoad table-related utility
**           function for SAMPLE_APP application.
**
** Functions Defined:
**    None
**
** Limitations, Assumptions, External Events, and Notes:
**    1. One source file per CFS table!
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/

/*
** Pragmas
*/

/*
** Include Files
*/
#include "sample_app_iload_utils.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/
extern SAMPLE_APP_AppData_t  g_SAMPLE_APP_AppData;

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Function Prototypes
*/

/*
** Function Definitions
*/
    
/*=====================================================================================
** Name: SAMPLE_APP_InitILoadTbl
**
** Purpose: To initialize the SAMPLE_APP's ILoad tables
**
** Arguments:
**    None
**
** Returns:
**    int32 iStatus - Status of initialization
**
** Routines Called:
**    CFE_TBL_Register
**    CFE_TBL_Load
**    CFE_TBL_Manage
**    CFE_TBL_GetAddress
**    CFE_ES_WriteToSysLog
**    SAMPLE_APP_ValidateILoadTbl
**    SAMPLE_APP_ProcessNewILoadTbl
**
** Called By:
**    SAMPLE_APP_InitApp
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Programmer(s):  GSFC, First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_InitILoadTbl()
{
    int32  iStatus=0;

    /* Register ILoad table */
    iStatus = CFE_TBL_Register(&g_SAMPLE_APP_AppData.ILoadTblHdl,
                               SAMPLE_APP_ILOAD_TABLENAME,
                               (sizeof(SAMPLE_APP_ILoadTblEntry_t) * SAMPLE_APP_ILOAD_MAX_ENTRIES),
                               CFE_TBL_OPT_DEFAULT,
                               SAMPLE_APP_ValidateILoadTbl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to register ILoad table (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitILoadTbl_Exit_Tag;
    }

    /* Load ILoad table file */
    iStatus = CFE_TBL_Load(g_SAMPLE_APP_AppData.ILoadTblHdl,
                           CFE_TBL_SRC_FILE,
                           SAMPLE_APP_ILOAD_FILENAME);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to load ILoad Table (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitILoadTbl_Exit_Tag;
    }

    /* Manage ILoad table */
    iStatus = CFE_TBL_Manage(g_SAMPLE_APP_AppData.ILoadTblHdl);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to manage ILoad table (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitILoadTbl_Exit_Tag;
    }

    /* Make sure ILoad table is accessible by getting referencing it */
    iStatus = CFE_TBL_GetAddress((void*)&g_SAMPLE_APP_AppData.ILoadTblPtr,
                                 g_SAMPLE_APP_AppData.ILoadTblHdl);
    if (iStatus != CFE_TBL_INFO_UPDATED)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to get ILoad table's address (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitILoadTbl_Exit_Tag;
    }

    /* Validate ILoad table */
    iStatus = SAMPLE_APP_ValidateILoadTbl(g_SAMPLE_APP_AppData.ILoadTblPtr);
    if (iStatus != CFE_SUCCESS)
    {
        CFE_ES_WriteToSysLog("SAMPLE_APP - Failed to validate ILoad table (0x%08X)\n", iStatus);
        goto SAMPLE_APP_InitILoadTbl_Exit_Tag;
    }

    /* Set new parameter values */
    SAMPLE_APP_ProcessNewILoadTbl();

SAMPLE_APP_InitILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: sample_app_ValidateILoadTbl
**
** Purpose: To validate the SAMPLE_APP's ILoad tables
**
** Arguments:
**    SAMPLE_APP_ILoadTblEntry_t*  iLoadTblPtr - pointer to the ILoad table
**
** Returns:
**    int32 iStatus - Status of table updates
**
** Routines Called:
**    TBD
**
** Called By:
**    TBD
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Programmer(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
int32 SAMPLE_APP_ValidateILoadTbl(SAMPLE_APP_ILoadTblEntry_t* iLoadTblPtr)
{
    int32  iStatus=0;

    if (iLoadTblPtr == NULL)
    {
        iStatus = -1;
        goto SAMPLE_APP_ValidateILoadTbl_Exit_Tag;
    }

    /* TODO:  Add code to validate new data values here.
    **
    ** Examples:
    **    if (iLoadTblPtr->sParam <= 16)
    **    {
    **        CFE_ES_WriteToSysLog("SAMPLE_APP - Invalid value for ILoad parameter sParam (%d)\n",
    **                             iLoadTblPtr->sParam);
    */

SAMPLE_APP_ValidateILoadTbl_Exit_Tag:
    return (iStatus);
}
    
/*=====================================================================================
** Name: SAMPLE_APP_ProcessNewILoadTbl
**
** Purpose: To process SAMPLE_APP's new ILoad tables and set ILoad parameters with new values
**
** Arguments:
**    None
**
** Returns:
**    None
**
** Routines Called:
**    TBD
**
** Called By:
**    TBD
**
** Global Inputs/Reads:
**    TBD
**
** Global Outputs/Writes:
**    TBD
**
** Limitations, Assumptions, External Events, and Notes:
**    1. List assumptions that are made that apply to this function.
**    2. List the external source(s) and event(s) that can cause this function to execute.
**    3. List known limitations that apply to this function.
**    4. If there are no assumptions, external events, or notes then enter NONE.
**       Do not omit the section.
**
** Algorithm:
**    Psuedo-code or description of basic algorithm
**
** Programmer(s):  First-Name Last-Name 
**
** History:  Date Written  2018-10-04
**           Unit Tested   yyyy-mm-dd
**=====================================================================================*/
void SAMPLE_APP_ProcessNewILoadTbl()
{
    /* TODO:  Add code to set new ILoad parameters with new values here.
    **
    ** Examples:
    **
    **    g_SAMPLE_APP_AppData.latest_sParam = g_SAMPLE_APP_AppData.ILoadTblPtr->sParam;
    **    g_SAMPLE_APP_AppData.latest_fParam = g_SAMPLE_APP.AppData.ILoadTblPtr->fParam;
    */
}
    
/*=======================================================================================
** End of file sample_app_iload_utils.c
**=====================================================================================*/
    
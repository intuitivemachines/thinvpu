/*=======================================================================================
** File Name:  sample_app_iload_utils.h
**
** Title:  ILoad Tables' Utility Header File for SAMPLE_APP Application
**
** $Author:    First-Name Last-Name
** $Revision: 1.1 $
** $Date:      2018-10-04
**
** Purpose:  To define SAMPLE_APP's ILoad table-related utility functions
**
** Modification History:
**   Date | Author | Description
**   ---------------------------
**   2018-10-04 | First-Name Last-Name | Build #: Code Started
**
**=====================================================================================*/
    
#ifndef _SAMPLE_APP_ILOAD_UTILS_H_
#define _SAMPLE_APP_ILOAD_UTILS_H_

/*
** Pragmas
*/

/*
** Include Files
*/
#include "sample_app_app.h"

/*
** Local Defines
*/

/*
** Local Structure Declarations
*/

/*
** External Global Variables
*/

/*
** Global Variables
*/

/*
** Local Variables
*/

/*
** Local Function Prototypes
*/
int32  SAMPLE_APP_InitILoadTbl(void);
int32  SAMPLE_APP_ValidateILoadTbl(SAMPLE_APP_ILoadTblEntry_t*);
void   SAMPLE_APP_ProcessNewILoadTbl(void);

#endif /* _SAMPLE_APP_ILOAD_UTILS_H_ */

/*=======================================================================================
** End of file sample_app_iload_utils.h
**=====================================================================================*/
    
# Intuitive Machines
# J. Moore

import sys, os, time
import re
import pickle
import subprocess, shlex
import c_parser
from c_parser import Parsable, Namespace

### DEFAULT CONFIGURATION VARIABLES

# NON_PARAMETERS is a list of variable names that should not be included as parameters/payload
# to commands
NON_PARAMETERS = ['CmdHeader','ucCmdHeader']
# SETTINGS is a dictionary to translate variables not detected by a parser
SETTINGS = {'CFE_SB_MsgId_t':'uint16', 'PROP_Action_E': 'uint32'}



### CLASS DEFINITIONS
class TargetMachine(object):

    def __init__(self,name = None):
        self.name = name
        self.ip = None
        self.port = None
        self.endian = None

class TelemetryItem(object):

    def __init__(self, name = None):
        self.name = name
        self.data_type_orig = ''
        self.Alength = ''
        self.Blength = ''
        self.Tlength = ''
        self.data_type_orig = ''
        self.data_type_new = ''
        self.desc = ''
        self.position = ''
        self.mapper = {'char':'c','uchar':'B',
                  'uint8':'B', 'boolean':'?', '_Bool':'?',
                  'uint16':'H', 'uint32': 'L',
                  'uint64':'Q', 'float': 'f',
                  'double':'d'}
        self.len_map = {'c':1,'B': 1,'?':1 ,'H':2,
                        'I': 4,'L':4,'f':4,
                        'Q':8,'d': 8,'':0}

    def __repr__(self):
        msg = 'Telemetry Item:\n'
        msg = msg + '\tName          : {}\n'.format(self.name)
        msg = msg + '\tArray length  : {}\n'.format(self.Alength)
        msg = msg + '\tDescription   : {}\n'.format(self.desc)
        msg = msg + '\tC type        : {}\n'.format(self.data_type_orig)
        msg = msg + '\tPy struct type: {}\n'.format(self.data_type_new)
        msg = msg + '\tBytes         : {}\n'.format(self.Blength)
        return msg


class TelemetrySet(object):

    def __init__(self, name = None, app = None, struct = None):
        self.name = name
        self.app = app
        self.struct = struct
        self.header_len = 12
        self.enum_just = 25
        self.position = self.header_len
        self.filename = os.path.join(app.tlm_folder,app.tlm_filename)
        self.template = '{:<35},{:>4},{:>3},{:>2},{:>4},{}\n'
        self.mapper = {'char':'c','uchar':'B',
                  'uint8':'B', 'int8':'B', 'boolean':'?', '_Bool':'?',
                  'uint16':'H', 'uint32': 'L', 'int32': 'L',
                  'uint64':'Q', 'float': 'f',
                  'double':'d'}
        self.len_map = {'c':1,'B': 1,'?':1 ,'H':2,
                        'I': 4,'L':4,'f':4,
                        'Q':8,'d': 8,'':0}
        self.disp_map = {}
        self.roster = []

    def __repr__(self):
        msg = 'Telemetry Set:\n'
        msg = msg + '\tName  : {}\n'.format(self.name)
        msg = msg + '\tFile  : {}\n'.format(self.filename)
        msg = msg + '\tContents :{}\n'
        for item in self.struct:
            msg = msg + '\t\t{:<35}{:<12}\n'.format(item.name, item.type_str, item.bytes)
 
        msg = msg + '\tBytes : {}\n'.format(self.position)
        return msg
    

    @property
    def message_len(self):
        return sum([v.bytes for v in self.struct])
 
    def breakdown(self):
        print 'Telemetry Set:'
        print '  Name        : {}'.format(self.name)
        print '  File        : {}'.format(self.filename)
        print '  Total Bytes : {}'.format(self.position)
        print '  Composition :'
        for item in self.roster:
            print '  {}'.format(item.position)
            length_calc = '({}x{})'.format(item.type_len,item.Alength)
            print '    +{:<3} {:<7} {:<30}{:<12}'.format(item.Tlength,length_calc,item.name, item.type_str)
        print '  {}'.format(self.position)
        

    def save(self,fname=None):
        if fname is None:
            fname = self.filename 
        position = self.header_len
        lines = []
        for item in self.struct:
            if not item.is_header:
                lines.extend(self.convert(item))
        with open(fname, 'w') as txtfile:
            txtfile.writelines(lines)

    def convert(self,var):
        """Convert Variable to line of text for config file"""
        pytype = self.mapper[var.fund_dtype]
        length = self.len_map[pytype]
        if isinstance(var.dtype,c_parser.Enum):
            disp = 'Enm'
            enums = [e.name for e in var.dtype.contents]
            etmp = '{:>{x}},'*len(enums)
            estr = etmp[:-1].format(*enums, x = self.enum_just)
        else:
            disp = 'Dec'
            enums = ['NULL','NULL','NULL','NULL']
            estr = '{:>{x}},{:>{x}},{:>{x}},{:>{x}}'.format(*enums, x = self.enum_just)

        TI = TelemetryItem(var.name)
        TI.data_type_orig = var.fund_dtype
        TI.type_len = length
        TI.position = self.position
        TI.type_str = var.type_str
        TI.Alength = var.array_length
        TI.Tlength = var.array_length * length
        self.roster.append(TI)
        if var.array_length > 1:
            line = []
            for aa in xrange(var.array_length):
                l1 = self.template.format('{}[{}]'.format(var.name,aa), self.position, length, pytype, disp, estr)
                line.append(l1)
                self.position = self.position + length
        else:
            line = self.template.format(var.name, self.position, length, pytype, disp, estr)
            self.position = self.position + length
        return line



class Parameter(object):
    """Collection of arguments to be supplied with a command"""
    def __init__(self, cmd, app, var = None):
        self.cmd = cmd
        self.var = var
        self.param_name = ''
        self.param_desc = ''
        self.data_type_orig = ''
        self.data_type_new = ''
        self.param_length  = ''
        self.string_length = ''
        self.default_value = ''
        self.macros = app.macros
        self.mapper = {'uint8':'--byte', 'boolean':'--byte',
                  'uint16':'--half', 'uint32': '--long',
                  'uint64':'--double', 'float': '--float',
                  'double':'--double'}

        self.len_map = {'--byte':1,'--half': 2, '--long': 4, '--float': 4,
                        '--double': 8,'':0, None:0}
        if self.var is not None:
            self.import_var(self.var)

    def __format__(self,spc):
        msg = self.__repr__()
        nmsg = '\n\t'.join(msg.split('\n'))
        return '\t{}'.format(nmsg)
            
    @property
    def nobytes(self):
        if self.data_type_new == '--string':
            return int(self.string_length)
        else:
            return self.len_map[self.data_type_new] 
    
    def map_type(self, name, dtype):
        if '[' in name:
            self.data_type_new = '--string'
            self.process_array_type(name)
        elif dtype in self.mapper.keys():
            self.data_type_new = self.mapper[dtype]
        elif dtype in self.macros.keys():
            mtype = self.macros[dtype]
            if mtype[:2] == '--':
                # user specified the new type
                self.data_type_new = mtype
            else:
                # user specified the C type
                self.data_type_new = self.mapper[mtype]
        else:
            print 'WARNING: unable to translate type for {} of type {}'.format(name, dtype)
            self.data_type_new = None

    def process_array_type(self, name):
        array_name_size = re.split('\[|\]', name)
        self.param_name = array_name_size[0]
        self.param_length = array_name_size[1]
        self.string_length = self.validate_string_len(array_name_size[1])

    def validate_string_len(self,length):
        """Ensure string length is an integer and within a valid range"""
        if not length.isdigit():
            mac_length = self.check_for_macro(length)
            if mac_length is None:
                print 'WARNING: Could not evaluate length definition {}'.format(length)
            elif type(mac_length) is str and mac_length.isdigit():
                length = int(mac_length)
            else:
                length = mac_length
        else:
            length = int(length)
        if 0 <= length <= 128:
            result = str(length)
        else:
            result = 'ERROR: INVALID LENGTH'

            print 'WARNING: Illegal array size {} (0 < x < 128).'.format(length)
        return result

    def check_for_macro(self,key):
        """See if the unknown key is in the settings dictionary"""
        if key in self.macros.keys():
            val = self.macros[key]
        else:
            val = None
        return val

    def import_var(self,var):
        self.param_name = var.name
        self.data_type_orig = var.fund_dtype
        self.param_desc = var.comment
        self.map_type(var.name,var.fund_dtype)
        if var.array_length > 1:
            self.data_type_new = '--string'
        self.param_length = var.array_length
        self.string_length = self.validate_string_len(str(var.array_length))

    def __repr__(self):
        msg = 'Parameter:\n'
        msg = msg + '\tCommand      : {}\n'.format(self.cmd)
        msg = msg + '\tName         : {}\n'.format(self.param_name)
        msg = msg + '\tArray length : {}\n'.format(self.param_length)
        msg = msg + '\tString length: {}\n'.format(self.string_length)
        msg = msg + '\tDescription  : {}\n'.format(self.param_desc)
        msg = msg + '\tC type       : {}\n'.format(self.data_type_orig)
        msg = msg + '\tNew type     : {}\n'.format(self.data_type_new)
        msg = msg + '\tBytes        : {}\n'.format(self.nobytes)
        msg = msg + '\tDefault value: {}\n'.format(self.default_value)
        return msg



class Command(object):
    """Collection of all info related to a command"""
    def __init__(self, app = None):
        self.cmd = None
        self.app = app
        self.enum = None
        self.cmd_def_file = None
        self.func = None
        self.func_map_file = None
        self.func_def_file = None
        self.input = None
        self.input_string = None
        self.input_def_file = None
        self.parameters = []
            
    def __repr__(self):
        msg = 'Command Object:\n'
        msg = msg + '\tCommand            : {}\n'.format(self.cmd)
        msg = msg + '\tEnum               : {}\n'.format(self.enum)
        msg = msg + '\tFunction           : {}\n'.format(self.func)
        msg = msg + '\tInput              : {}\n'.format(self.input)
        msg = msg + '\tCommand definition : {}\n'.format(self.cmd_def_file)
        msg = msg + '\tFunction mapper    : {}\n'.format(self.func_map_file)
        msg = msg + '\tFunction definition: {}\n'.format(self.func_def_file)
        msg = msg + '\tInput definition   : {}\n'.format(self.input_def_file)
        msg = msg + '\tMessage length     : {} Bytes \n'.format(self.message_len)
        if len(self.parameters) > 0:
            msg = msg + '\tParameters         :\n'
            for p in self.parameters:
                msg = msg + '\t{}\n'.format(p)
        else:
            msg = msg + '\tParameters         :\nNone\n'
        return msg

    @property
    def message_len(self):
        return sum([p.nobytes for p in self.parameters])

    def set_def(self, def_dict):
        for p in self.parameters:
            if p.param_name in def_dict.keys():
                p.default_value = def_dict[p.param_name]


    def save(self, fname = None):
        if fname is None:
            self.filename = os.path.join(self.app.prm_folder,self.cmd)
        else:
            self.filename = os.path.join(self.app.prm_folder,fname)
        dataTypesOrig = [p.data_type_orig for p in self.parameters]
        paramNames = [p.param_name for p in self.parameters]
        paramLens = [p.param_length for p in self.parameters]
        paramDesc = [p.param_desc for p in self.parameters]
        dataTypesNew = [p.data_type_new for p in self.parameters]
        stringLens = [p.string_length for p in self.parameters]
        defaultInp = [p.default_value for p in self.parameters]
        with open( self.filename, 'wb') as pickle_obj:
            pickle.dump([dataTypesOrig, paramNames, paramLens, paramDesc, dataTypesNew, stringLens, defaultInp], pickle_obj)


class App(object):
    """Collection of files and commands related to an app"""
    def __init__(self, name = None, mid_files = None, cmd_mid = None, tlm_mid = None, name_in_cmd = True):
        self.name = name
        print '\n','='*60
        print 'Processing {} app'.format(self.name)
        print '='*60
        self.cmd_mid = cmd_mid
        self.tlm_mid = tlm_mid
        if type(mid_files) not in (tuple,list):
            self.mid_files = [mid_files]
        else:
            self.mid_files = mid_files
        if name_in_cmd:
            # require app name in command
            self.name_in_cmd = name
        else:
            self.name_in_cmd = False
        self.files = []
        self.cmd_folder = '../cmdGUI/CommandFiles'
        self.prm_folder = '../cmdGUI/ParameterFiles'
        self.tlm_folder = '../tlmGUI/'
        self.tlm_filename = '{}-tlm.txt'.format(name)
        self.command_maps = []
        self.format_defs = []
        self.type_maps = []
        self.commands = {}
        self.telemetry = None
        self.default_inputs = {}
        self.non_parameters = NON_PARAMETERS
        self._macros = SETTINGS
        self._target = None
        self.target_ip = None
        self.target_port = None
        self.target_endian = 'LE'
        self.enumParams = {}
        if cmd_mid is None and mid_files is not None:
            self.cmd_mid = self.find_mids('{}_CMD_MID'.format(self.name))
        else:
            self.cmd_mid = cmd_mid

        if tlm_mid is None and mid_files is not None:
            self.tlm_mid = self.find_mids('OUT_DATA_MID')
        else:
            self.tlm_mid = tlm_mid

        if name is not None:
            self.telemetry_format = '{}_OutPacket_t'.format(name)
        else:
            self.telemetry_format = None
        self.config_file = None
        self.namespace = Namespace()

    @property
    def enumdict(self):
        en = {int(v.enum): k for k, v in self.commands.iteritems() if v.enum is not None}
        return en

    @property
    def ordered(self):
        return [self.commands[self.enumdict[c]] for c in sorted(self.enumdict.keys())]

    @property
    def funcs(self):
        return [self.commands[c].func for c in self.commands.keys()]

    @property
    def inputs(self):
        return [self.commands[c].input for c in self.commands.keys()]

    @property
    def cmds(self):
        return [self.commands[c].cmd for c in self.commands.keys()]

    @property
    def ordered_cmds(self):
        return [c.cmd for c in self.ordered]

    @property
    def ordered_enums(self):
        return [c.enum for c in self.ordered]

    @property
    def telems(self):
        if type(self.telemetry_format) is list:
            return self.telemetry_format
        else:
            return [self.telemetry_format]

    @property
    def macros(self):
        return self._macros
    @macros.setter
    def macros(self, val):
        self._macros.update(val)
        
    @property
    def target(self):
        return self._target
    @target.setter
    def target(self, val):
        self._target = val
        self.target_ip = self._target.ip
        self.target_port = self._target.port
        self.target_endian = self._target.endian

    def add_default(self,cmd,inp,val):
        newdef = {inp:val}
        if cmd in self.default_inputs.keys():
            self.default_inputs[cmd].update(newdef)
        else:
            self.default_inputs[cmd] = newdef

    def apply_defaults(self):
        for c in self.default_inputs.keys():
            self.commands[c].set_def(self.default_inputs[c])



    def add_enum(self, paramName, enumName):
        print 'Adding definition for ' + paramName
        self.enumParams[paramName] = enumName

    def find_mids(self, match = None):
        print 'SEARCHING FOR MIDS: {}'.format(match)
        for ff in self.mid_files:
            p = Parsable(ff)
            df,dp = p.match_defines()
            mid = self.isolate_mid(dp,match)
            if mid is not None:
                break
        return mid

    def isolate_mid(self,dct,match):
        """Find the MID with the App name and the matching string"""
        possible_ids = dict()
        mid = None
        for key in dct.keys():
            if self.name in key and match in key:
                possible_ids[key] = dct[key]
        if len(possible_ids.keys()) == 1:
            mid = possible_ids.values()[0]
        elif len(possible_ids.keys()) > 1:
            print 'WARNING: unable to isolate MID: {}, {}'.format(self.name,match)
            print possible_ids

        return mid


    def update_commands(self, dct, fname):
        for cmd in dct.keys():
            if cmd in self.commands.keys():
                self.commands[cmd].func = dct[cmd]
                self.commands[cmd].func_map_file = fname
            else:
                c = Command(self)
                c.cmd = cmd
                c.func = dct[cmd]
                c.func_map_file = fname
                self.commands[cmd] = c

    def update_enums(self, dct, fname):
        for cmd in dct.keys():
            if cmd in self._macros.keys():
                # remove command from macros
                self._macros.pop(cmd)
            if cmd in self.commands.keys(): 
                self.commands[cmd].enum = dct[cmd]
                self.commands[cmd].cmd_def_file = fname
            else:
                c = Command(self)
                c.cmd = cmd
                c.enum = dct[cmd]
                c.cmd_def_file = fname
                self.commands[cmd] = c

    def update_functions(self, dct, fname, key = 'func'):
        for k in dct.keys():
            # check if we need to translate to get the command
            if key == 'func':
                fmap = {v.func: k for k, v in self.commands.iteritems()}
                cmd = fmap[k]
                func = k
            else:
                # we're directly supplying the command
                cmd = k
                func = None

            if cmd in self.commands.keys():
                self.commands[ cmd ].input = dct[k]
                self.commands[ cmd ].func_def_file = fname
            else:
                c = Command()
                c.cmd = cmd
                c.func = func
                c.input = dct[k]
                c.func_def_file = fname
                self.commands[ cmd ] = c

    def make_params(self):
        for cmd in self.commands.keys():
            if self.commands[cmd].input is not None:
                Plist = []
                if self.commands[cmd].input in self.namespace.definitions.keys():
                    struct =  self.namespace[self.commands[cmd].input]
                    for v in struct:
                        if not v.is_header:
                            P = Parameter(cmd, self, v)
                            Plist.append(P)
                else:
                    print 'Warning: Unable to find definition for {} parameter {}'.format(cmd, self.commands[cmd].input)
                self.commands[ cmd ].parameters.extend(Plist)


    def update_telem(self):
        self.telemetry = TelemetrySet(app = self)
        self.telemetry.file = fname
        self.telemetry.process_telem(dct)
        #print self.telemetry 

    def set_telem(self):
        if self.telemetry_format in self.namespace.definitions.keys():
            t_struct = self.namespace.definitions[self.telemetry_format]
            self.telemetry = TelemetrySet(self.telemetry_format, self, t_struct)
 
    def parse(self):
        """Parse all the files for this app"""
        # make a first pass through all files to get any definitions
        # that we might need later
        print '\n','-'*40
        print 'Parsing files for {} app'.format(self.name)
        print '-'*40
        for f in self.parameter_defs + self.command_defs + self.command_maps + self.format_defs:
            p = Parsable(f)
            df,dp = p.match_defines()
            self._macros.update(dp)
            self.namespace.update(dp)
            # find enums and structs
            ds = p.scrape_structs()
            self.namespace.update(ds)
            de = p.scrape_enums()
            self.namespace.update(de)
            for key in self.enumParams:
                if (self.enumParams[key] in de):
                    self.enumParams[key] = de[self.enumParams[key]]
        self.namespace.synchronize(self.enumParams)

        
        # make a second pass through each file looking for specific patterns
        for f in self.command_defs:
            # Look for #define statements in these files and assume they are commands
            p = Parsable(f)
            df,dp = p.match_a_define(name_in_cmd = self.name_in_cmd)
            self.update_enums(dp,f)
        for f in self.command_maps:
            # Look for case statements and find the function or struct associated with each command
            p = Parsable(f)
            dc,df = p.match_cmds(name_in_cmd = self.name_in_cmd)
            self.update_commands(dc,f)
            self.update_functions(df,f, key = 'cmd')
        for f in self.format_defs:
            # Look for the functions tied to the commands and determine the expected input
            p = Parsable(f)
            d = p.match_funcs(self.funcs)
            self.update_functions(d,f)
        self.make_params()
        self.apply_defaults()
        self.set_telem()




    def save(self, pickle_file, tlm_filename = None):
        if tlm_filename is None:
            tlm_filename = os.path.join(self.tlm_folder,'{}-tlm.txt'.format(pickle_file))
        else:
            tlm_filename = os.path.join(self.tlm_folder,tlm_filename)
        self.apply_defaults()
        self.pickle_file = os.path.join(self.cmd_folder,pickle_file)
        print 'Saving command config to: {}'.format(self.pickle_file )
        cmdDesc = self.ordered_cmds
        cmdCodes = self.ordered_enums
        with open(self.pickle_file , 'wb') as pickle_obj:
            pickle.dump([cmdDesc, cmdCodes, cmdDesc], pickle_obj)

        for this_cmd in self.commands:
            self.commands[this_cmd].save()
        if self.telemetry is not None:
            print 'Saving telemetry config to: {}'.format(tlm_filename)
            self.telemetry.save(tlm_filename)


    def send(self, command, *args):
        proceed = True
        if command in self.commands.keys():
            print 'ISSUING COMMAND [{}]: {}'.format(self.name, command)
            appcom = self.commands[command]

            launch_string = '../cmdUtil/cmdUtil' + ' --host=\"' + self.target_ip + '\" --port=' + self.target_port + ' --pktid=' + self.cmd_mid + ' --endian=' + self.target_endian + ' --cmdcode=' + appcom.enum
            # if requires parameters
            if len(appcom.parameters) > 0:
                
                if not len(appcom.parameters) == len(args):
                    print 'ERROR: IMPROPER FORMAT FOR {} {}:'.format(self.name,command)
                    self.cmd_man(command)
                    proceed = False
                else:
                    param_list = []
                    for ix,item in enumerate(appcom.parameters):
                        if item.data_type_new == '--string':
                            param_list.append('{}=\"{}:{}\"'.format(item.data_type_new, item.string_length, args[ix]) )
                        else:
                            if isinstance(args[ix],str) and self.commands[command].parameters[ix].param_name in self.enumParams:
                                # user provided enum name - do smart replace string
                                arg = self.as_enum(command,args,ix)
                            else:
                                arg = args[ix]
                            param_list.append('{}={}'.format(item.data_type_new,arg) )# --byte=4
                    param_string = ' '.join(param_list)
                    launch_string = launch_string +' '+ param_string

            print launch_string
            if proceed:
                cmd_args = shlex.split(launch_string)
                p1 = subprocess.Popen(cmd_args)
                p1.wait()
        else:
            print 'UNKNOWN COMMAND [{}]: {}'.format(self.name, command)

    def as_enum(self,command,args,ix):
        # replace string with equivalent enum
        prm = self.commands[command].parameters[ix]
        arg = args[ix]
        newstr = arg.replace(' ','_').upper()
        if self.enumParams[prm.param_name].findname(newstr) is not None:
            arg = self.enumParams[prm.param_name].findname(newstr)
        return arg

    def cmd_man(self, command):
        print '{} COMMAND: {}'.format(self.name, command)
        msg = '{}(code for {})'.format(self.commands[command].enum,command)
        enums = []
        if len(self.commands[command].parameters) > 0:
            print ' Requires {} arguments:'.format(len(self.commands[command].parameters))
            for item in self.commands[command].parameters:
                msg = msg + ' {}({})'.format(item.param_name, item.data_type_orig)
                if (item.param_name in self.enumParams):
                    enums.append(item.param_name)
            print '   Expected Format: {}'.format(msg)
        else:
            print ' No required arguments.'
        for item in enums:
            if (type(self.enumParams[item]) is str):
                print item + ' was not found'
            else:
                print '     ' + item + ':'
                for enumItem in self.enumParams[item].contents:
                    print '       ' + str(enumItem.value) + '\t' + enumItem.name

    def wait(self, duration = 10):
        print 'Pausing {} seconds. . .'.format(duration)
        time.sleep(duration)


    def help(self, command = None):
        if command is not None:
            self.cmd_man(command)
        else:
            print 'AVAILABLE {} COMMANDS:'.format(self.name)
            for item in self.commands.keys():
                self.cmd_man(item)

    def generateTemplate(self):
        import pprint;
        pp = pprint.PrettyPrinter()
        
        import json, ast;

        template = json.loads('{ "WAIT" : { "seconds" : "float" } }')
        for item in self.commands.keys():
            if len(self.commands[item].parameters) == 0:
                template[item] = json.loads("null")
            else:
                template[item] = json.loads("{ }")
                for param in self.commands[item].parameters:
                    template[item][param.param_name] = json.loads("{ }")
                    template[item][param.var.name]["type"] = param.data_type_orig
                    if (param.param_name in self.enumParams):
                        enumList = []
                        for enumItem in self.enumParams[param.param_name].contents:
                            enumList.append(str('"'+enumItem.name+'"'))
                        enumString = ",".join(enumList)
                        template[item][param.var.name]["enumvals"] = json.loads("[" + enumString + "]")
        return template
        



if __name__ == '__main__':

    # command_defs - files where commands are defined (#define COMMAND VALUE)
    # command_maps - files with switch structures mapping commands to function calls
    # format_defs - files where functions to be performed upon command are defined
    # parameter_defs - files where command parameters are defined (typedefs)
    # macros - variables that aren't defined in one of the parsed files
    MID_FILES = ['../../../../apps/inc/MISSION_cmd_ids.h']

    TO = App(name = 'TO', mid = '0x1880')
    #TO.macros = {'TO_MAX_IP_STRING_SIZE':16}
    TO.command_defs = ['../../../../apps/to/fsw/src/to_msgdefs.h']
    TO.command_maps = ['../../../../apps/to/fsw/src/to_app.c']
    TO.format_defs = ['../../../../apps/to/fsw/src/to_cmds.c']
    TO.parameter_defs = ['../../../../apps/to/fsw/src/to_cmds.h',
                         '../../../../apps/to/fsw/platform_inc/MISSION_to_types.h' ]
    TO.parse()
    
    TO.save('to')

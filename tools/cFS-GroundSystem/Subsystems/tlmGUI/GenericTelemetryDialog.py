# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GenericTelemetryDialog.ui'
#
# Created: Thu Jun 25 09:52:44 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class ValueWidget(QtGui.QTextBrowser):

    def __init__(self,*args,**kwargs):
        self.tag = None
        self.layout = None
        self.tag = kwargs.pop('num')
        self.layout = kwargs.pop('layout')
        self.lab = kwargs.pop('lab')
        if self.lab:
            self.size = (240,31)
            name = 'Label'
        else:
            self.size = (151,31)
            name = 'Value'

        super(ValueWidget, self).__init__(*args, **kwargs)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QtCore.QSize(*self.size))
        self.setMaximumSize(QtCore.QSize(*self.size))
        self.setObjectName(_fromUtf8("item{}TextBrowser_{}".format(name,self.tag)))
        if self.layout is not None:
            self.layout.addWidget(self)

class Ui_GenericTelemetryDialog(object):

    def __init__(self):
        self.labels = dict()
        self.values = dict()
        self.breakpoint = 20

    def add_label(self,num,lay):
        self.labels[num] = ValueWidget(self.scrollAreaWidgetContents, num = num, layout = lay, lab = True)

    def add_value(self,num,lay):
        self.values[num] = ValueWidget(self.scrollAreaWidgetContents, num = num, layout = lay, lab = False)

    def splitup(self,count):
        #make even
        if not count % 2 == 0:
            count = count + 1
        self.count = count
        self.breakpoint = int(count/2.0)
        

    def setupUi(self, GenericTelemetryDialog, num_items):
        self.splitup(num_items)
        GenericTelemetryDialog.setObjectName(_fromUtf8("GenericTelemetryDialog"))
        GenericTelemetryDialog.setWindowModality(QtCore.Qt.NonModal)
        GenericTelemetryDialog.resize(880, 600)
        font = QtGui.QFont()
        font.setPointSize(10)
        GenericTelemetryDialog.setFont(font)

        self.verticalLayout_8 = QtGui.QVBoxLayout(GenericTelemetryDialog)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.subSystemTelemetryPageLabel = QtGui.QLabel(GenericTelemetryDialog)
        self.subSystemTelemetryPageLabel.setObjectName(_fromUtf8("subSystemTelemetryPageLabel"))
        self.verticalLayout.addWidget(self.subSystemTelemetryPageLabel)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.subSystemTextBrowser = QtGui.QTextBrowser(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.subSystemTextBrowser.sizePolicy().hasHeightForWidth())
        self.subSystemTextBrowser.setSizePolicy(sizePolicy)
        self.subSystemTextBrowser.setMinimumSize(QtCore.QSize(141, 31))
        self.subSystemTextBrowser.setMaximumSize(QtCore.QSize(141, 31))
        self.subSystemTextBrowser.setObjectName(_fromUtf8("subSystemTextBrowser"))
        self.horizontalLayout.addWidget(self.subSystemTextBrowser)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))

        self.packetIdLabel = QtGui.QLabel(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.packetIdLabel.sizePolicy().hasHeightForWidth())
        self.packetIdLabel.setSizePolicy(sizePolicy)
        self.packetIdLabel.setMinimumSize(QtCore.QSize(0, 13))
        self.packetIdLabel.setMaximumSize(QtCore.QSize(16777215, 13))
        self.packetIdLabel.setObjectName(_fromUtf8("packetIdLabel"))
        self.verticalLayout_2.addWidget(self.packetIdLabel)

        self.packetId = QtGui.QLCDNumber(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.packetId.sizePolicy().hasHeightForWidth())
        self.packetId.setSizePolicy(sizePolicy)
        self.packetId.setObjectName(_fromUtf8("packetId"))
        self.verticalLayout_2.addWidget(self.packetId)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_5 = QtGui.QLabel(GenericTelemetryDialog)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout_3.addWidget(self.label_5)

        self.sequenceCount = QtGui.QTextBrowser(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sequenceCount.sizePolicy().hasHeightForWidth())
        self.sequenceCount.setSizePolicy(sizePolicy)
        self.sequenceCount.setMinimumSize(QtCore.QSize(101, 31))
        self.sequenceCount.setMaximumSize(QtCore.QSize(101, 31))
        self.sequenceCount.setObjectName(_fromUtf8("sequenceCount"))
        self.verticalLayout_3.addWidget(self.sequenceCount)

        self.horizontalLayout_2.addLayout(self.verticalLayout_3)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        spacerItem2 = QtGui.QSpacerItem(146, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        
        self.buttonBox = QtGui.QDialogButtonBox(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.horizontalLayout_2.addWidget(self.buttonBox)
        self.verticalLayout_8.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))

        self.label = QtGui.QLabel(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(240, 17))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_4.addWidget(self.label)

        self.label_2 = QtGui.QLabel(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QtCore.QSize(151, 0))
        self.label_2.setMaximumSize(QtCore.QSize(151, 16777215))
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_4.addWidget(self.label_2)
        self.line_2 = QtGui.QFrame(GenericTelemetryDialog)
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.horizontalLayout_4.addWidget(self.line_2)

        self.label_4 = QtGui.QLabel(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setMinimumSize(QtCore.QSize(240, 17))
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_4.addWidget(self.label_4)

        self.label_3 = QtGui.QLabel(GenericTelemetryDialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(151, 0))
        self.label_3.setMaximumSize(QtCore.QSize(151, 16777215))
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_4.addWidget(self.label_3)

        spacerItem3 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem3)

        self.verticalLayout_8.addLayout(self.horizontalLayout_4)
        self.scrollArea = QtGui.QScrollArea(GenericTelemetryDialog)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName(_fromUtf8("scrollArea"))
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 709, 1000))
        self.scrollAreaWidgetContents.setMinimumSize(QtCore.QSize(0, 1000))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.scrollAreaWidgetContents)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))



        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))


        # Add lefthand widgets
        for item in xrange(1,self.breakpoint + 1):
            self.add_label(item, self.verticalLayout_4)
            self.add_value(item, self.verticalLayout_5)

        self.horizontalLayout_3.addLayout(self.verticalLayout_5)

        self.line = QtGui.QFrame(self.scrollAreaWidgetContents)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.horizontalLayout_3.addWidget(self.line)

        self.verticalLayout_6 = QtGui.QVBoxLayout()
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.horizontalLayout_3.addLayout(self.verticalLayout_6)

        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))

        # Add righthand widgets
        for item in xrange(self.breakpoint + 1, self.count + 1):
            self.add_label(item, self.verticalLayout_6)
            self.add_value(item, self.verticalLayout_7)


        self.horizontalLayout_3.addLayout(self.verticalLayout_7)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout_8.addWidget(self.scrollArea)
        self.label_6 = QtGui.QLabel(GenericTelemetryDialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.verticalLayout_8.addWidget(self.label_6)

        self.retranslateUi(GenericTelemetryDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), GenericTelemetryDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), GenericTelemetryDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(GenericTelemetryDialog)

    def retranslateUi(self, GenericTelemetryDialog):
        GenericTelemetryDialog.setWindowTitle(_translate("GenericTelemetryDialog", "Telemetry Page", None))
        self.subSystemTelemetryPageLabel.setText(_translate("GenericTelemetryDialog", "Subsystem Telemetry Page", None))
        self.packetIdLabel.setText(_translate("GenericTelemetryDialog", "Packet ID", None))
        self.label_5.setText(_translate("GenericTelemetryDialog", "Sequence Count", None))
        self.label.setText(_translate("GenericTelemetryDialog", "Telemetry Point Label", None))
        self.label_2.setText(_translate("GenericTelemetryDialog", "Telemetry Point Value", None))
        self.label_4.setText(_translate("GenericTelemetryDialog", "Telemetry Point Label", None))
        self.label_3.setText(_translate("GenericTelemetryDialog", "Telemetry Point Value", None))
        self.label_6.setText(_translate("GenericTelemetryDialog", "*No packets? Remember to select the IP address of your spacecraft in the Main Window.", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    font=app.font()
    font.setPointSize(10)
    app.setFont(font)

    GenericTelemetryDialog = QtGui.QDialog()
    ui = Ui_GenericTelemetryDialog()
    ui.setupUi(GenericTelemetryDialog, 40)
    GenericTelemetryDialog.show()
    sys.exit(app.exec_())


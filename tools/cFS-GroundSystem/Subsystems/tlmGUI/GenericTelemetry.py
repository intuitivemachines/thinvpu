#!/usr/bin/env python
# 

import sys
import csv
import getopt 
import pdb
import zmq

from PyQt4 import QtGui, QtCore
from GenericTelemetryDialog import Ui_GenericTelemetryDialog
from struct import *

class SubsystemTelemetry(QtGui.QDialog):

    pktCount = 0

    #
    # Init the class
    #
    def __init__(self, count):
        QtGui.QDialog.__init__(self)
        self.ui = Ui_GenericTelemetryDialog()
        self.ui.setupUi(self, count)
        self.count = count

    # 
    # This method Decodes a telemetry item from the packet and displays it 
    #
    def displayTelemetryItem(self, datagram, tlmIndex, labelField, valueField):
       if tlmItemIsValid[tlmIndex] == True:
          TlmField =   unpack(tlmItemFormat[tlmIndex], datagram[int(tlmItemStart[tlmIndex]):(int(tlmItemStart[tlmIndex]) + int(tlmItemSize[tlmIndex]))])
          if tlmItemDisplayType[tlmIndex] == 'Dec':
             valueField.setText(str(TlmField[0]))
          elif tlmItemDisplayType[tlmIndex] == 'Hex':
             valueField.setText(hex(TlmField[0]))
          elif tlmItemDisplayType[tlmIndex] == 'Enm':
             valueField.setText(tlmItemEnum[tlmIndex][int(TlmField[0])])
          elif tlmItemDisplayType[tlmIndex] == 'Str':
             valueField.setText(TlmField[0])
          labelField.setText(tlmItemDesc[tlmIndex])
       else:
          labelField.setText("(unused)")

    # Start the telemetry receiver (see TlmReceiver class)
    def initTlmReceiver(self, subscription):
        self.setWindowTitle(pageTitle + ' for: ' + subscription)
        self.thread = TlmReceiver(self, subscription)
        self.connect(self.thread, self.thread.signalTlmDatagram, self.processPendingDatagrams)
        self.thread.start()

    #
    # This method processes packets. Called when the TelemetryReceiver receives a message/packet
    #
    def processPendingDatagrams(self, datagram):
        #
        # Show number of packets received
        #
        self.pktCount += 1
        self.ui.sequenceCount.setText(str(self.pktCount))

        #
        # Decode and display all packet elements
        #
        for item in xrange(self.count):
            self.displayTelemetryItem(datagram, item, self.ui.labels[item+1], self.ui.values[item+1])



# Subscribes and receives zeroMQ messages
class TlmReceiver(QtCore.QThread):
    
    def __init__(self, mainWindow, subscription):
        QtCore.QThread.__init__(self)

        # Setup signal to communicate with front-end GUI
        self.signalTlmDatagram = QtCore.SIGNAL("TlmDatagram")
        
        # Init zeroMQ
        self.context   = zmq.Context()
        self.subscriber = self.context.socket(zmq.SUB)
        self.subscriber.connect("ipc:///tmp/GroundSystem")
        self.subscriber.setsockopt(zmq.SUBSCRIBE, subscription)
    
    def run(self):
        while True:
            # Read envelope with address
            [address, datagram] = self.subscriber.recv_multipart()
            #print("[%s] %s" % (address, datagram))
            # Send signal with received packet to front-end/GUI
            self.emit(self.signalTlmDatagram, datagram)


#
# Display usage
#
def usage():
    print "Must specify --title=<page name> --port=<udp_port> --appid=<packet_app_id(hex)> --endian=<endian(L|B) --file=<tlm_def_file> --sub=<subscriber_string>"
    print "     example: --title=Executive Services --port=10800 --appid=800 --file=cfe-es-hk-table.txt --endian=L --sub=GroundSystem.Spacecraft1.0x886"
    print "            (quotes are not on the title string in this example)" 

#
# Main 
#
if __name__ == '__main__':

    #
    # Set defaults for the arguments
    #
    pageTitle = "Telemetry Page"               
    udpPort  = 10000
    appId = 999 
    tlmDefFile = "telemetry_def.txt"
    endian = "L"
    subscription = ""
    
    #
    # process cmd line args 
    #
    try:                                
        opts, args = getopt.getopt(sys.argv[1:], "htpafl", ["help", "title=", "port=", "appid=","file=", "endian=", "sub="])
    except getopt.GetoptError:           
        usage()                          
        sys.exit(2)                     

    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt in ("-p", "--port"): 
            udpPort = arg               
        elif opt in ("-t", "--title"): 
            pageTitle = arg               
        elif opt in ("-f", "--file"): 
            tlmDefFile = arg               
        elif opt in ("-t", "--appid"): 
            appId = arg               
        elif opt in ("-e", "--endian"): 
            endian = arg 
        elif opt in ("-s", "--sub"):
            subscription = arg

    if len(subscription) == 0:
        subscription = "GroundSystem"

    print 'Generic Telemetry Page started. Subscribed to ' + subscription

    if endian == 'L':
       py_endian = '<'
    else:
       py_endian = '>'


    #
    # Read in the contents of the telemetry packet defintion
    #
    tlmItemIsValid = []
    tlmItemDesc = []
    tlmItemStart = []
    tlmItemSize = []
    tlmItemDisplayType = []
    tlmItemFormat = []
    tlmItemEnum = []
    i = 0

    with open(tlmDefFile, 'rb') as tlmfile:
       reader = csv.reader(tlmfile, skipinitialspace = True)
       for row in reader:
          if row[0][0] != '#':
             tlmItemIsValid.append(True)
             tlmItemDesc.append(row[0])
             tlmItemStart.append(row[1])
             tlmItemSize.append(row[2])
             tlmItemEnum.append([])
             if row[3] == 's':
                tlmItemFormat.append(row[2]+row[3])
             else:
                tlmItemFormat.append(py_endian + row[3])

             tlmItemDisplayType.append(row[4])
             if row[4] == 'Enm':
                # include all remaining "rows" as enum items
                tlmItemEnum[i].extend(row[5:])

             i = i + 1
    num_items = i 


    # 
    # Mark the remaining values as invalid
    #
    for j in range (i, num_items + num_items%2):
       tlmItemIsValid.append(False)
    #
    # Init the QT application and the telemetry class
    #
    app = QtGui.QApplication(sys.argv)
    Telem = SubsystemTelemetry(num_items)
    Telem.ui.subSystemTextBrowser.setText(pageTitle)
    Telem.ui.packetId.display(appId)

    # 
    # Display the page 
    #
    Telem.show()
    Telem.raise_()
    Telem.initTlmReceiver(subscription)

    sys.exit(app.exec_())


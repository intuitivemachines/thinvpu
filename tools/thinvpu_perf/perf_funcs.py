#!/usr/bin/env python3
import sys, os
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np

from plotset import Plotset



## FUNCTION AND CLASS DEFINITIONS

class Test(object):
    """Individual test info"""
    def __init__(self, index, df, name = 'None'):
        self.index = index
        self.name = name
        self.df = df
        self.elapsed_time = None
        self.labels = {'elapsed_time': 'Time in Test [s]'}

    def set_elapsed_time(self, channel):
        """set the elapsed time for this test"""
        self.endpoints = (self.df[channel].iat[0], self.df[channel].iat[-1])
        self.elapsed_time =  self.endpoints[1]- self.endpoints[0]
        self.midpoint = self.df[channel].iat[0] + self.elapsed_time/2
        

class Metric(object):
    """Collector for trends on an individual metric"""
    def __init__(self, name, label = None, unit = 'nd'):
        self.name = name
        self.label = name if label is None else label
        self.unit = unit
        self.data_objs = dict()
        self.runs = []
        self.tests = dict()
        self.mean = []
        self.std  = []
        self.max = []
        self.min = []
        self.plotlist = Plotset()


    def __repr__(self):
        msg = f'{self.name}:\n'
        msg = msg + f'  runs: {self.runs}\n'
        msg = msg + f'  max:  {self.max}\n'
        msg = msg + f'  min:  {self.min}\n'
        msg = msg + f'  mean: {self.mean}\n'
        msg = msg + f'  std:  {self.std}\n'
        return msg

    @property
    def latest_run(self):
        return max(self.runs)
    
    @property
    def run_count(self):
        return len(self.runs)

    def plot_composite_by_test(self, feature = 'elapsed_time',history = 10, show = False):
        """Plot a composite test feature by test number"""
        newest = max(self.data_objs.keys()) # the latest run
        runs = range(newest,max(newest-history,-1), -1)
        r = len(runs)
        
        fig, ax = plt.subplots(1,1, figsize = (10,8))    
        for run in runs:
            tests = self.tests[run] # data for this run
            x = np.array([ii for ii in tests.keys()])
            y = [getattr(tests[ii], feature) for ii in x]
            ax.bar(x + run/r -1/r ,y, width = 1/r, alpha = 0.5, label = f'Run {run}',linewidth=0)
        ylab = tests[1].labels[feature]
        ax.set_xticks(x)
        ax.set_xticklabels(x)

        ax.set_xlabel('Test No.')
        ax.set_ylabel(ylab)
        ax.set_title(ylab)
        plt.legend()
        self.plotlist.add(fig)
        if show:
            plt.show()

    def plot_by_test(self, run = None, show = False):
        """Plot by test number"""
        run = self.latest_run if run is None else run
        tests = self.tests[run] # data for this run
        x = tests.keys()
        y = x
        fig, ax = plt.subplots(1,1, figsize = (10,8))    
        ax.bar(x,y)
        self.plotlist.add(fig)
        if show:
            plt.show()


    def plot_run(self, run = None, show = False):
        """Plot time history of one run"""
        run = self.latest_run if run is None else run
        temp_title = self.data_objs[run].title
        self.data_objs[run].title = f'{self.label} (Run {run})'
        ax = self.data_objs[run].plot(self.name, color = 'C0',ylabel = f'{self.label} [{self.unit}]', xlabel = 'Elapsed Time [s]',
                                fmt= {'label':f'Run {run}:{self.name}'}, pretty_labels = False)
        self.data_objs[run].title = temp_title
        self.plotlist.add(self.data_objs[run].plotlist.plots[-1])
        for t in self.tests[run].values(): 
            an = ax.annotate(f'{t.index}', xy=(t.midpoint, 0.0), xycoords=("data", "axes fraction"),
                  va="bottom", ha="center", fontsize = 8, rotation = 0,
                  bbox=dict(boxstyle="round", fc="w", ec=(0, 0, 0, 0.2)))
            ax.axvline(x=t.endpoints[0], color = 'gray', linestyle = '-', alpha = 0.1)
            ax.axvline(x=t.endpoints[1], color = 'gray', linestyle = '-', alpha = 0.1)
        if show:
            plt.show()

    def plot_trend(self, show = False):
        # Errorbar plots of the trends
        fig, ax = plt.subplots(1,1, figsize = (10,8))
        ax.errorbar(self.runs,self.mean, yerr=self.std, fmt=':o', capsize = 5)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_ylabel(f'{self.label} [{self.unit}]')
        ax.set_xlabel('Run No.')
        ax.set_title(f'Trend in {self.label}')
        self.plotlist.add(fig)
        if show:
            plt.show()

    def coplot(self,x = 'elapsed_time[s]', history = 10, runs = None, show = False):
        """Coplot time histories of metrics"""
        data = self.data_objs
        newest = max(data.keys()) # the latest run
        if runs is not None:
            # user provided a list of runs to coplot
            co_runs = [ data[x] for x in runs ]
            runstr = sorted([ x for x in runs ] + [newest])
            title = f'{self.label} Over Runs ' + ', '.join([str(r) for r in runstr]) 
        else:
            # count down the n most recent runs
            co_runs = [ data[x] for x in range(newest-1,max(newest-history,-1), -1)]
            title = f'{self.label} Over Last {len(co_runs)+1} Runs'
        data[newest].coplot(x, self.name, codata = co_runs , ylabel = f'{self.label} [{self.unit}]' , 
                            xlabel = 'Elapsed Time [s]',title = title, pretty_labels = False)
        self.plotlist.add(data[newest].plotlist.plots[-1])
        if show:
            plt.show()

    def pdf(self, fname):
        self.plotlist.pdf(fname)

def coplot(data, var, x = 'elapsed_time', history = 10, runs = None):
    """Coplot time histories of metrics"""
    # data is a dict of SimOutput objects

    newest = max(data.keys()) # the latest run
    if runs is not None:
        # user provided a list of runs to coplot
        co_runs = [ data[x] for x in runs ]
    else:
        # count down the n most recent runs
        co_runs = [ data[x] for x in range(newest-1,max(newest-history,-1), -1)]
    data[newest].coplot(x, var.name, codata = co_runs , ylabel = f'{var.label} [{var.unit}]' )
    
def bundle_plots(metrics):
    """Collect plots from a dictionary of Metric objects"""
    bundle = Plotset()
    for m in metrics.keys():
        # extend the list of plots with this object's plots
        bundle.plots.extend(metrics[m].plotlist.plots)
    return bundle

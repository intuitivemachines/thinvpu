#!/usr/bin/env python3
import sys, os
from pathlib import Path

from sim_interface.sim_output import *
from perf_funcs import Test, Metric, bundle_plots


# Define analysis data
data_location = Path(os.path.expanduser('~/code/fsw/tools/thinvpu_perf/data'))
prefix = 'RUN_'
history = 3 # number most recent runs to co-plot (newest included)

# initialize the trend collectors
metrics = dict()
metrics['ActiveTest']          = Metric('ActiveTest', 'Active Test', 'nd') 
metrics['UtilCpuAvg']          = Metric('UtilCpuAvg', 'Util CPU Avg', '%') 
metrics['UtilMemUsagePercent'] = Metric('UtilMemUsagePercent', 'Util Memory Usage', '%') 

tests = dict()

## BEGIN ANALYSIS

# identify the available run folders
search = str(data_location/f'{prefix}*')
run_folders =  glob.glob(search)

# process each run
for folder in sorted(run_folders):
    run = int(Path(folder).stem.replace(prefix,'') )
    tests[run] = dict()
    SO = SimOutput(folder, show = False, verbosity = 1, name = f'Run {run}', load_trk = False)

    SO.load('HS_HK.csv')
    SO.load('TCM.csv')
    print('DATA LOADED')

    # MAKE A COMMON TIME CHANNEL
    SO.make_elapsed_time('TIMESTAMP',name = 'elapsed_time[s]')
    SO.time = 'elapsed_time[s]'

    # BREAK OUT EACH TEST
    test_groups = SO['HS_HK'].groupby('ActiveTest')
    for test,item in test_groups:
        if test is not 0:
            T = Test(test,test_groups.get_group(test))
            T.set_elapsed_time('elapsed_time[s]')
            tests[run][test] = T

    # COLLECT STATISTICS
    for met in metrics.values():
        met.runs.append(run)
        met.data_objs[run] = SO
        met.tests[run] = tests[run]
        met.mean.append(SO[met.name].mean())
        met.std.append(SO[met.name].std())
        met.min.append(SO[met.name].min())
        met.max.append(SO[met.name].max())



# display info on a metric
print(metrics['UtilCpuAvg'])

## MAKE PLOTS

# plot a single metric from a single run
#metrics['UtilCpuAvg'].plot_run(2)


# plot all the metrics
for m in metrics.keys():
    if m == 'ActiveTest':
        # These are the same for all metrics
        metrics[m].plot_composite_by_test(feature = 'elapsed_time')
    else:
        # plot a single run (latest run by default)
        metrics[m].plot_run()
        # co-plot a single metric
        metrics[m].coplot(history = history)
        # plot trends in a single metric
        metrics[m].plot_trend()

bundle_plots(metrics).pdf('test3')
#plt.show()





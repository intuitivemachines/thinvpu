#!/bin/bash
#########################################################################################
# Purpose: Modify platform_incs in apps directory to support multiple builds.
#
# Inputs: The script will only run if 1 is passed in as arg.
#         This prevents accidental folder/build modifications.
#         VERIFY ALL PATHS AND BUILD NAMES IN THE SETTINGS SECTION BEFORE RUNNING.
#
# Execution: ./platform_inc_multibuild.sh 1
#########################################################################################

########## Settings 
echo "Current settings:"
APPS_PATH="~/code/fsw/apps"
echo "APPS_PATH = $APPS_PATH"
# Path to platform_inc folder from within a single app
PLATFORM_INC_SUBPATH="fsw/platform_inc"
echo "PLATFORM_INC_SUBPATH = $PLATFORM_INC_SUBPATH"
# Path to public_inc folder from within a single app
PUBLIC_INC_SUBPATH="fsw/public_inc"
echo "PUBLIC_INC_SUBPATH = $PUBLIC_INC_SUBPATH"
# Builds to create folders for
BUILDS=("flight" "payload_vpu")
echo "Builds = ${BUILDS[@]}"
printf "\n\n"

########## Script start 

# Verify settings are approved
if [ "$1" == "1" ]
then
    echo "Settings verified!"
elif [[ "$1" == "--help" ]]  || [[ "$1" == "-h" ]]
then
    echo "Verify script settings then run again with 1 as an argument"
    echo "Ex: $0 1"
    exit -1
else
    echo "ERROR: Script Settings not verified"
    echo "Verify script settings then run again with 1 as an argument"
    echo "Ex: $0 1"
    exit -1  
fi
# Change to fsw/apps directory
echo "Changing script directory to $APPS_PATH"
# Needs eval so resolve ~ 
eval cd $APPS_PATH
# Iterate through subdirectories and create build folders in platform_inc of each app
for SUBDIRECTORY in */
do
    echo "Starting work in $SUBDIRECTORY"
    PLATFORM_INC_PATH="$SUBDIRECTORY$PLATFORM_INC_SUBPATH"
    PUBLIC_INC_PATH="$SUBDIRECTORY$PUBLIC_INC_SUBPATH"
    if [ -d $PLATFORM_INC_PATH ]
    then
        # if [ ! -d $PUBLIC_INC_PATH ]
        # then
        #     echo "Creating $PUBLIC_INC_PATH directory"
        #     mkdir $PUBLIC_INC_PATH
        # else
        #     echo "$PUBLIC_INC_PATH already exists"
        # fi
        for build in ${BUILDS[@]}
        do
            if [ ! -d $PLATFORM_INC_PATH/$build ]
            then
                echo "Creating $PLATFORM_INC_PATH/$build directory"
                mkdir $PLATFORM_INC_PATH/$build
                cp $PLATFORM_INC_PATH/* $PLATFORM_INC_PATH/$build
            fi
        done
        echo "Removing copied files from $PLATFORM_INC_PATH"
        find $PLATFORM_INC_PATH -maxdepth 1 -type f -delete
    else
        echo "Nothing to do for $SUBDIRECTORY"
    fi
    echo "Finished work in $SUBDIRECTORY"
done
echo "Done!"
########## Script end 
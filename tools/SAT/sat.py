"""
 Situational Awareness Tool (SAT)
 version 1.0
 Author: Giovanni Molina
"""

import numpy as np
from os.path import join
import os, sys, time, math, getopt
import pandas as pd
import sat_config as config


def parser(x):
    return pd.to_datetime(x, unit='s')

def add_point(data_table, i):
    lon = str(np.array(data_table.iloc[[i]]["sc.lon"])[0])
    lat = str(np.array(data_table.iloc[[i]]["sc.lat"])[0])
    points_str = f"{lon},{lat}|"
    return points_str

def get_corners(data_table, i):
    x1 = data_table.iloc[[i]]["b1.lon"][0]
    x2 = data_table.iloc[[i]]["b2.lon"][0]
    x3 = data_table.iloc[[i]]["b3.lon"][0]
    x4 = data_table.iloc[[i]]["b4.lon"][0]
    y1 = data_table.iloc[[i]]["b1.lat"][0]
    y2 = data_table.iloc[[i]]["b2.lat"][0]
    y3 = data_table.iloc[[i]]["b3.lat"][0]
    y4 = data_table.iloc[[i]]["b4.lat"][0] 
    
    return [[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x1,y1]]

def add_rectangle(data_table, i):
    rect_str = ""
    
    # get the corners
    corners = get_corners(data_table, i)
    
    for corner in corners:
        rect_str += f"{corner[0]},{corner[1]},"

    return rect_str[:-1]+"|"

def add_landing_site(lon,lat):
    landing_str = ""
    x1 = float(lon) + config.LANDING_ARC
    x2 = float(lon) + config.LANDING_ARC
    x3 = float(lon) - config.LANDING_ARC
    x4 = float(lon) - config.LANDING_ARC
    y1 = float(lat) + config.LANDING_ARC
    y2 = float(lat) - config.LANDING_ARC
    y3 = float(lat) - config.LANDING_ARC
    y4 = float(lat) + config.LANDING_ARC
    
    corners = [[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x1,y1]]
    for corner in corners:
        landing_str += f"{corner[0]},{corner[1]},"
    
    return landing_str[:-1] + f"|{lon},{lat}|"

# Thanks Shen Ge
def xyztolatlon(x,y,z):
    lonrad = np.arctan2(y,x)
    lon = np.rad2deg(lonrad)
    
    latrad = np.arctan2(z*np.sin(lonrad),y)
    lat = np.rad2deg(latrad)
    return lat,lon

def xyztolatlon_pandas(val):
    et_s = val[0]
    x,y,z = val[1:]
    lonrad = np.arctan2(y,x)
    lon = np.rad2deg(lonrad)
    
    latrad = np.arctan2(z*np.sin(lonrad),y)
    lat = np.rad2deg(latrad)
    return pd.Series([et_s, lat, lon], index=["et_s", "sc.lat", "sc.lon"])

def print_url(points_str, rects_str):
    if len(points_str) > 0 and len(rects_str) > 0:
        url_str = f"https://quickmap.lroc.asu.edu/?extent={config.EXTENT_STR}&proj=16&features={rects_str}{points_str[:-1]}&layers=NrBsFYBoAZIRnpEoAsjYIHYFcA2vIBvAXwF1SizSg" 
    elif len(points_str) > 0:
        url_str = f"https://quickmap.lroc.asu.edu/?extent={config.EXTENT_STR}&proj=16&features={points_str[:-1]}&layers=NrBsFYBoAZIRnpEoAsjYIHYFcA2vIBvAXwF1SizSg" 
    elif len(rects_str) > 0:
        url_str = f"https://quickmap.lroc.asu.edu/?extent={config.EXTENT_STR}&proj=16&features={rects_str[:-1]}&layers=NrBsFYBoAZIRnpEoAsjYIHYFcA2vIBvAXwF1SizSg" 
    # print(len(url_str))
    # print(url_str,"\n\n")
    return url_str

def add_row(row):
    row_str = f'''
    <tr>
    <td class="tg-0lax">{row["interval"]}</td>
    <td class="tg-0lax">{row["cnt_pts"]}</td>
    <td class="tg-0lax">{row["cnt_fovs"]}</td>
    <td class="tg-0lax">{row["landing"]}</td>
    <td class="tg-0lax">{row["start_time"]}</td>
    <td class="tg-0lax">{row["end_time"]}</td>
    <td class="tg-0lax">{row["start_lon"]}</td>
    <td class="tg-0lax">{row["start_lat"]}</td>
    <td class="tg-0lax">{row["end_lon"]}</td>
    <td class="tg-0lax">{row["end_lat"]}</td>
    <td class="tg-0lax">{row["start_alt"]}</td>
    <td class="tg-0lax">{row["end_alt"]}</td>
    <td class="tg-0lax"><a href="{row["URL"]}" target="_blank">QuickMap</a></td>
  </tr>'''
    return row_str

# Construct the HTML table output
def construct_table(data_table, url_max_len=7250):
    points_str = ""
    rects_str = ""
    table_content = ""
    r = 0 
    num_elems = 0
    
    row = {"interval":config.INTERVAL_SEC, "cnt_pts":0, "cnt_fovs":0, "landing":config.SHOW_LANDING, "start_time":str((data_table.iloc[[0]].index.astype(int)/ 10**9).astype(float)[0]), "end_time":str((data_table.iloc[[0]].index.astype(int)/ 10**9).astype(float)[0]),
           "start_lat":str(data_table.iloc[[0]]["sc.lat"][0]), "start_lon":str(data_table.iloc[[0]]["sc.lon"][0]), "end_lat":str(data_table.iloc[[0]]["sc.lat"][0]), 
           "end_lon":str(data_table.iloc[[0]]["sc.lon"][0]), "start_alt":str(data_table.iloc[[0]]["sc.alt"][0]), "end_alt":str(data_table.iloc[[0]]["sc.alt"][0]), "URL":""}
    for i in range(0,len(data_table)):
        if config.SHOW_POINTS:
            points_str += add_point(data_table, i)
            row["cnt_pts"] += 1
        if config.SHOW_FOV:
            rects_str += add_rectangle(data_table, i)
            row["cnt_fovs"] += 1
        r = len(points_str) + len(rects_str)
        num_elems += 1
        
        # Reached max url length, got to break it up
        if r >= url_max_len or i == len(data_table) - 1 or num_elems == config.INTERVAL_ELEMS:
            if config.SHOW_LANDING:
                 rects_str += add_landing_site(config.LANDING_LON, config.LANDING_LAT)
            # Add values to row
            row["URL"] = print_url(points_str, rects_str)
            row["end_time"] = str((data_table.iloc[[i]].index.astype(int)/ 10**9).astype(float)[0])
            row["end_lat"] = str(data_table.iloc[[i]]["sc.lat"][0])
            row["end_lon"] = str(data_table.iloc[[i]]["sc.lon"][0])
            row["end_alt"] = str(data_table.iloc[[i]]["sc.alt"][0])
            table_content += add_row(row)
            
            # Reset values
            if i != len(data_table) - 1:
                row["cnt_pts"] = 0
                row["cnt_fovs"] = 0
                row["start_time"] = str((data_table.iloc[[i+1]].index.astype(int)/ 10**9).astype(float)[0])
                row["start_lat"] = str(data_table.iloc[[i+1]]["sc.lat"][0])
                row["start_lon"] = str(data_table.iloc[[i+1]]["sc.lon"][0])
                row["start_alt"] = str(data_table.iloc[[i+1]]["sc.alt"][0])
                points_str = ""
                rects_str = ""
                r = 0
                num_elems = 0
            
    return table_content

def filter_segments(segment_table, trajectory_table):
    unavailable_rows = []
    for i in range(len(segment_table)) : 
        temp_table = trajectory_table[trajectory_table.index>=parser(segment_table.iloc[i, 1]) ]
        temp_table = temp_table[temp_table.index<=parser(segment_table.iloc[i, 2]) ]
        if len(temp_table) <= 0:
            unavailable_rows.append(i)
    return segment_table.drop(unavailable_rows)

# Writes the output file
def write_file(body_content):
    done = False
    i = 1
    filename = "quickmap_table.html"
    while not done:
        try:
            output_file = open(filename, "x", encoding="utf-8")
            output_file.write(config.BODY_START+body_content+config.BODY_END)
            output_file.close()
            done = True
        except:
            filename = f"quickmap_table_{i}.html"
            i += 1
    print(f"File written to: {os.path.abspath(filename)}")
        

# Read the console arguments
def read_console_arguments(argv):
    input_file = ""
    try:
        opts, args = getopt.getopt(argv[1:],"hi:n:s:e:c:",["input=", "interval=", "start=", "end=", "segment="])
    except getopt.GetoptError:
        print (argv[0] + ' --input=<dir> [--interval=<sec:int> --start=<sec:float> --end=<sec:float> --segment=<string>]')
        sys.exit(2)

    if len(opts) < 1:
        print (argv[0] + ' --input=<dir> [--interval=<sec:int> --start=<sec:float> --end=<sec:float> --segment=<string>]')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print (argv[0] + ' --input=<dir> [--interval=<sec:int> --start=<sec:float> --end=<sec:float> --segment=<string>]')
            sys.exit()
        elif opt in ("-i", "--input"):
            input_file = arg
        elif opt in ("-n", "--interval"):
            config.INTERVAL_SEC = int(arg)
        elif opt in ("-s", "--start"):
            config.START_TIME = float(arg)
        elif opt in ("-e", "--end"):
            config.END_TIME = float(arg)
        elif opt in ("-c", "--segment"):
            config.SEGMENT = arg
    return input_file


def plot_drm(input_file):
    print("Reading data...")
    segment_table = pd.read_excel(input_file, sheet_name=0, skiprows=[0,1,2,3], usecols="A:C", names=["segment","start","end"], nrows=12)  #,"i","f"
    print(segment_table)
    trajectory_table = pd.read_excel(input_file, sheet_name=1, header=0, parse_dates=[0], index_col=0, date_parser=parser)
    print("Dropping empty values...")
    trajectory_table = trajectory_table.dropna()
    segment_table = segment_table.dropna().reset_index().drop("index", 1)
    print("Segments where camera data is available: ")
    segment_table = filter_segments(segment_table, trajectory_table)
    print(segment_table)

    ## Apply segment filter
    if config.SEGMENT != "":
        if len(segment_table[segment_table.segment==config.SEGMENT]) == 0:
            print("Segment entered is not valid.")
            sys.exit(1)
        print(f"Chosen segment: {config.SEGMENT}")
        config.START_TIME = segment_table[segment_table.segment==config.SEGMENT]["start"].values[0]
        config.END_TIME = segment_table[segment_table.segment==config.SEGMENT]["end"].values[0]
        trajectory_table = trajectory_table[trajectory_table.index >= parser(config.START_TIME) ]
        trajectory_table = trajectory_table[trajectory_table.index <= parser(config.END_TIME) ]
    elif config.START_TIME > 0 or config.END_TIME > 0:
        print(f"Chosen times: {config.START_TIME} - {config.END_TIME}")
        if config.START_TIME > 0:
            trajectory_table = trajectory_table[trajectory_table.index >= parser(config.START_TIME) ]
        if config.END_TIME > 0:
            trajectory_table = trajectory_table[trajectory_table.index <= parser(config.END_TIME) ]

    print(f"Total data points: {len(trajectory_table)}")
    print(f"At current interval of {config.INTERVAL_SEC} second(s) the amount of data plotted will be: {math.ceil(len(trajectory_table)/config.INTERVAL_SEC)}")
    if len(trajectory_table) == 0:
        print("I can't work with that! Try again..")
        sys.exit(1)
    rate_trajectory_table = trajectory_table[::config.INTERVAL_SEC]
    body_content = construct_table(rate_trajectory_table)
    return body_content

def plot_csv(input_file, url_max_len=7250):
    print("Reading data... ", end="")
    data_table = pd.read_csv(input_file, header=0)
    print("Done.")

    print(f"Total data points: {len(data_table)}")
    print(f"At current interval of {config.INTERVAL_SEC} second(s) the amount of data plotted will be: {math.ceil(len(data_table)/config.INTERVAL_SEC)}")
    data_table = data_table[::config.INTERVAL_SEC]
    if len(data_table) == 0:
        print("I can't work with that! Try again..")
        sys.exit(1)
    

    if len(data_table.columns) == 3:
        data_table.columns = ["et_s","sc.lat", "sc.lon"]
        if config.START_TIME > 0 or config.END_TIME > 0:
            print(f"Chosen times: {config.START_TIME} - {config.END_TIME}")
            if config.START_TIME > 0:
                data_table = data_table[data_table["et_s"] >= config.START_TIME ]
            if config.END_TIME > 0:
                data_table = data_table[data_table["et_s"] <= config.END_TIME]

    elif len(data_table.columns) == 2:
        data_table.columns = ["sc.lat", "sc.lon"]

    else:
        print("Converting XYZ coordinates to LAT/LON, may take some time...", end=" ")
        data_table = data_table[["et_s","rx_sc_PCPF", "ry_sc_PCPF", "rz_sc_PCPF"]]
        if config.START_TIME > 0 or config.END_TIME > 0:
            print(f"Chosen times: {config.START_TIME} - {config.END_TIME}")
            if config.START_TIME > 0:
                data_table = data_table[data_table["et_s"] >= config.START_TIME ]
            if config.END_TIME > 0:
                data_table = data_table[data_table["et_s"] <= config.END_TIME]

        
        data_table = data_table.apply(xyztolatlon_pandas, axis=1, result_type='expand')
        print("Done!")

    rects_str = ""
    points_str = ""
    table_content = ""
    r = 0 
    num_elems = 0
    et_s = 0

    print(data_table.head())
    
    if "et_s" in data_table.columns:
        et_s = data_table.iloc[[0]]["et_s"].values[0]
    row = {"interval":config.INTERVAL_SEC, "cnt_pts":0, "cnt_fovs":0, "landing":config.SHOW_LANDING, "start_time":et_s, "end_time":et_s,
           "start_lat":str(data_table.iloc[[0]]["sc.lat"].values[0]), "start_lon":str(data_table.iloc[[0]]["sc.lon"].values[0]), "end_lat":str(data_table.iloc[[0]]["sc.lat"].values[0]), 
           "end_lon":str(data_table.iloc[[0]]["sc.lon"].values[0]), "start_alt":0, "end_alt":0, "URL":""}

    for i in range(0,len(data_table)):
        if config.SHOW_POINTS:
            points_str += add_point(data_table, i)
            row["cnt_pts"] += 1
            
        if "et_s" in data_table.columns:
            et_s = str(data_table.iloc[[i]]["et_s"].values[0])

        r = len(points_str)
        num_elems += 1
        
        # Reached max url length, got to break it up
        if r >= url_max_len or i == len(data_table) - 1 or num_elems == config.INTERVAL_ELEMS:
            if config.SHOW_LANDING:
                 rects_str += add_landing_site(config.LANDING_LON, config.LANDING_LAT)
            # Add values to row
            row["URL"] = print_url(points_str, rects_str)
            row["end_time"] = et_s
            row["end_lat"] = str(data_table.iloc[[i]]["sc.lat"].values[0])
            row["end_lon"] = str(data_table.iloc[[i]]["sc.lon"].values[0])
            row["end_alt"] = "0"
            table_content += add_row(row)
            
            # Reset values
            if i != len(data_table) - 1:
                if "et_s" in data_table.columns:
                    et_s = str(data_table.iloc[[i]]["et_s"].values[0])
                row["cnt_pts"] = 0
                row["cnt_fovs"] = 0
                row["start_time"] = et_s
                row["start_lat"] = str(data_table.iloc[[i]]["sc.lat"].values[0])
                row["start_lon"] = str(data_table.iloc[[i]]["sc.lon"].values[0])
                row["start_alt"] = "0"
                points_str = ""
                rects_str = ""
                r = 0
                num_elems = 0
            
    return table_content

# Main funtion
def main(argv):
    input_file = read_console_arguments(argv)
    body_content = ""

    # check if .csv (plots simple points)
    if input_file.endswith(".csv"):
        body_content = plot_csv(input_file)
    else:
        body_content = plot_drm(input_file)

    print("Writing...")
    write_file(body_content)
    

# Script entry
if __name__ == "__main__":
    print("Working...")
    main(sys.argv)
    print("Done!")
    sys.exit(0)
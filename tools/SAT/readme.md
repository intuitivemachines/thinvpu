# Tools 

## Situational Awareness Tool (SAT)
This tool is meant to help provide situational awareness. Visit confluence page for details: http://confluence.intuitivemachines.com:8090/pages/viewpage.action?pageId=83069045

### Requirements:
Python 3.6+
numpy (pip install numpy)
pandas (pip install pandas)
xlrd (pip install xlrd)

### How to run:
Basic command:

`python sat.py -i input_file.xlsx <options>`

### Options:
| Flag            | Description |
| --------------- | ------------------------------------------------------------------ |
-i, --input       |	Path to input file. Must be an Excel document in the same format as the sample documents attached here.
-n, --interval    |	Interval in seconds between each data point plotted. (interval=10 will plot one point every 10 seconds). Default=100
-s, --start  	  | Start time in seconds from where the data will start plotting. Default=all
-e, --end	      | End time in seconds where the data will stop plotting. Default=all
-c, --segment     | Mission segment for which to plot data. (Supersedes start/end options). Default=all


#### Configuration File:

SHOW_POINTS = True # Whether to show a point where the spacecraft is located

SHOW_FOV = True # Whether to show the camera FOV

SHOW_LANDING = True # Whether to show the landing site

INTERVAL_SEC = 100 # Interval in seconds between each data point


#### Optional parameters:
START_TIME = 0

END_TIME = 0

SEGMENT = ""

### Example Runs:
Plot a point at 2000 datapoint intervals (seconds in DRM case) 
`python sat.py -i data/vpu_llo_orbit2.csv -n 2000`

Plot a point at 2000 datapoint intervals (seconds in DRM case) starting at a specific epoch
`python sat.py -i data/vpu_llo_orbit2.csv -n 2000 --start 687839595.614541`